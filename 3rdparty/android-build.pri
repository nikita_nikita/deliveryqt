android {
versionAtLeast(QT_VERSION, "5.14.0") {
    ANDROID_EXTRA_LIBS += \
        $$PWD/5.14/arm/libcrypto_1_1.so \
        $$PWD/5.14/arm/libssl_1_1.so \
        $$PWD/5.14/arm64/libcrypto_1_1.so \
        $$PWD/5.14/arm64/libssl_1_1.so \
        $$PWD/5.14/x86/libcrypto_1_1.so \
        $$PWD/5.14/x86/libssl_1_1.so \
        $$PWD/5.14/x86_64/libcrypto_1_1.so \
        $$PWD/5.14/x86_64/libssl_1_1.so
} else {
          lessThan(QT_MINOR_VERSION, 13):{
              message("include opens ssl 1.0.0c")
              INCLUDEPATH += $$absolute_path($$PWD/include)
           }
           greaterThan(QT_MINOR_VERSION, 12):{
                message("include opens ssl 1.1.1c")
                INCLUDEPATH += $$absolute_path($$PWD/1.1c/include)
           }

        equals(ANDROID_TARGET_ARCH, arm64-v8a) {
              lessThan(QT_MINOR_VERSION, 13):{
              message("version <13")
               LIBPATH = $$absolute_path($$PWD/libs/llvm/arm64-v8a)
          }
                greaterThan(QT_MINOR_VERSION, 12):{
                    message("version >12: opens ssl 1.1.1c")
                     LIBPATH = $$absolute_path($$PWD/1.1c/libs/llvm/arm64-v8a)

DEFINES += QT_DEPRECATED_WARNINGS
QTFIREBASE_CONFIG +=  messaging
include(vendor/QtFirebase/qtfirebase.pri)
                }

        }

        equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
                 lessThan(QT_MINOR_VERSION, 13):{
                    message("include opensssl  armeabi-v7a")
                   LIBPATH = $$absolute_path($$PWD/libs/llvm/armeabi-v7a)
                }
                greaterThan(QT_MINOR_VERSION, 12):{
                     LIBPATH = $$absolute_path($$PWD/1.1c/libs/llvm/armeabi-v7a)

                }

        }

        LIBS += \
            -L$$LIBPATH \
            -lssl -lcrypto

        ANDROID_EXTRA_LIBS += \
            $$LIBPATH/libssl.so \
            $$LIBPATH/libcrypto.so
        }

}
