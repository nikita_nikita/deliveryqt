QT += quick network quickcontrols2 sql svg sensors webview
VERSION = 0.1

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    src/config/settingapp.cpp \
    src/controllers/cartcontroller.cpp \
    src/controllers/checkoutdelivetycontroller.cpp \
    src/controllers/contactscontroller.cpp \
    src/controllers/feedbackcontroller.cpp \
    src/controllers/firebasemessagecontroller.cpp \
    src/controllers/loadercontroller.cpp \
    src/controllers/menucontroller.cpp \
    src/controllers/objectcontroller.cpp \
    src/controllers/paymentcontroller.cpp \
    src/controllers/profilecontroller.cpp \
    src/core.cpp \
    src/entity/address/addreseslist.cpp \
    src/entity/address/addressitem.cpp \
    src/entity/checkstreet.cpp \
    src/entity/citydata/productitem_kit.cpp \
    src/entity/citydata/producttype.cpp \
    src/entity/citydata/sityinfo.cpp \
    src/entity/citydata/topping.cpp \
    src/entity/citydata/toppinglist.cpp \
    src/entity/citydata/toppings.cpp \
    src/entity/citydata/toppings_kit.cpp \
    src/entity/citydata/workday.cpp \
    src/entity/citydata/workdayentity.cpp \
    src/entity/citydata/workdays.cpp \
    src/entity/citydata/worktimeentity.cpp \
    src/entity/client/clientdata.cpp \
    src/entity/client/clientinfo.cpp \
    src/entity/feedbackdata.cpp \
    src/entity/iikoorder.cpp \
    src/entity/loyality.cpp \
    src/entity/loyalitydata.cpp \
    src/entity/message.cpp \
    src/entity/models/categoryproducts.cpp \
    src/entity/models/pageitem.cpp \
    src/entity/orderhystory.cpp \
    src/entity/orderloadlist.cpp \
    src/models/addresslistmodel.cpp \
    src/models/imagelistmodel.cpp \
    src/models/orderhystorymodel.cpp \
    src/models/pagemodel.cpp \
    src/models/pagesmodel.cpp \
    src/models/productfiltemodel.cpp \
    src/models/productitemlistmodel.cpp \
    src/models/servivekitmodel.cpp \
    src/models/streetfiltermodel.cpp \
    src/models/timeorderlmodel.cpp \
    src/models/toppingkitmodel.cpp \
    src/models/worktimemodel.cpp \
    src/service/jwt/qjsonwebtoken.cpp \
    src/service/kapibaras.cpp \
    src/entity/city.cpp \
    src/entity/domain.cpp \
    src/entity/citylist.cpp \
    src/service/statusstorage.cpp \
    src/service/worktime.cpp \
    src/servicelocator.cpp \
    src/entity/citydata/organizationinfo.cpp \
    src/entity/citydata/category.cpp \
    src/entity/citydata/product.cpp \
    src/entity/pointdelivery.cpp \
    src/entity/citydata/offers.cpp \
    src/entity/citydata/servicekit.cpp \
    src/entity/citydata.cpp \
    src/entity/citydata/config.cpp \
    src/entity/versionapp.cpp \
    src/service/storage.cpp \
    src/service/database.cpp \
    src/service/fileloader.cpp \
    src/service/router.cpp \
    src/service/menu.cpp \
    src/service/cart.cpp \
    src/models/productsmodel.cpp \
    src/entity/citydata/productcategory.cpp \
    src/models/cartproductmodel.cpp \
    src/entity/cartproductentity.cpp \
    src/entity/street.cpp \
    src/models/streetmodel.cpp \
    src/models/offersmodel.cpp \
    src/models/citymodel.cpp \
    src/entity/orderrequest.cpp \
    src/entity/order.cpp \
    src/entity/orderinfo.cpp \
    src/entity/ordeaddress.cpp \
    src/entity/citydata/productaddeditem.cpp \
    src/entity/citydata/productitem.cpp \
    src/entity/cart/cartoptionsitem.cpp \
    src/entity/cart/cartoptions.cpp \
    src/entity/cart/cartadded.cpp \
    src/service/pay.cpp \
    src/entity/pay/carddata.cpp \
    src/service/pay/paybyplatform.cpp \
    src/entity/citydata/payments.cpp \
    src/entity/pay/cpcheckout.cpp \
    src/entity/pay/cpresponse.cpp \
    src/entity/pay/cpresponsemodel.cpp \
    src/service/pay/payconnecter.cpp


HEADERS += \
    src/config/settingapp.h \
    src/controllers/cartcontroller.h \
    src/controllers/checkoutdelivetycontroller.h \
    src/controllers/contactscontroller.h \
    src/controllers/feedbackcontroller.h \
    src/controllers/firebasemessagecontroller.h \
    src/controllers/loadercontroller.h \
    src/controllers/menucontroller.h \
    src/controllers/objectcontroller.h \
    src/controllers/paymentcontroller.h \
    src/controllers/profilecontroller.h \
    src/core.h \
    src/entity/address/addreseslist.h \
    src/entity/address/addressitem.h \
    src/entity/checkstreet.h \
    src/entity/citydata/productitem_kit.h \
    src/entity/citydata/producttype.h \
    src/entity/citydata/sityinfo.h \
    src/entity/citydata/topping.h \
    src/entity/citydata/toppinglist.h \
    src/entity/citydata/toppings.h \
    src/entity/citydata/toppings_kit.h \
    src/entity/citydata/workday.h \
    src/entity/citydata/workdayentity.h \
    src/entity/citydata/workdays.h \
    src/entity/citydata/worktimeentity.h \
    src/entity/client/clientdata.h \
    src/entity/client/clientinfo.h \
    src/entity/feedbackdata.h \
    src/entity/iikoorder.h \
    src/entity/loyality.h \
    src/entity/loyalitydata.h \
    src/entity/message.h \
    src/entity/models/categoryproducts.h \
    src/entity/models/pageitem.h \
    src/entity/orderhystory.h \
    src/entity/orderloadlist.h \
    src/models/addresslistmodel.h \
    src/models/imagelistmodel.h \
    src/models/orderhystorymodel.h \
    src/models/pagemodel.h \
    src/models/pagesmodel.h \
    src/models/productfiltemodel.h \
    src/models/productitemlistmodel.h \
    src/models/servivekitmodel.h \
    src/models/streetfiltermodel.h \
    src/models/timeorderlmodel.h \
    src/models/toppingkitmodel.h \
    src/models/worktimemodel.h \
    src/service/jwt/qjsonwebtoken.h \
    src/service/kapibaras.h \
    src/entity/city.h \
    src/entity/domain.h \
    src/entity/citylist.h \
    src/service/statusstorage.h \
    src/service/worktime.h \
    src/servicelocator.h \
    src/entity/citydata/organizationinfo.h \
    src/entity/citydata/category.h \
    src/entity/citydata/product.h \
    src/entity/pointdelivery.h \
    src/entity/citydata/offers.h \
    src/entity/citydata/servicekit.h \
    src/entity/citydata.h \
    src/entity/citydata/config.h \
    src/entity/versionapp.h \
    src/service/storage.h \
    src/service/database.h \
    src/service/fileloader.h \
    src/service/router.h \
    src/service/menu.h \
    src/service/cart.h \
    src/models/productsmodel.h \
    src/entity/citydata/productcategory.h \
    src/models/cartproductmodel.h \
    src/entity/cartproductentity.h \
    src/entity/street.h \
    src/models/streetmodel.h \
    src/models/offersmodel.h \
    src/models/citymodel.h \
    src/entity/orderrequest.h \
    src/entity/order.h \
    src/entity/orderinfo.h \
    src/entity/ordeaddress.h \
    src/entity/citydata/productaddeditem.h \
    src/entity/citydata/productitem.h \
    src/entity/cart/cartoptionsitem.h \
    src/entity/cart/cartoptions.h \
    src/entity/cart/cartadded.h \
    src/service/pay.h \
    src/entity/pay/carddata.h \
    src/service/pay/paybyplatform.h \
    src/entity/citydata/payments.h \
    src/entity/pay/cpcheckout.h \
    src/entity/pay/cpresponse.h \
    src/entity/pay/cpresponsemodel.h \
    src/service/pay/payconnecter.h

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
android {
greaterThan(QT_MINOR_VERSION, 12):{
#VERSION = 1
DEFINES += QT_DEPRECATED_WARNINGS
#QTFIREBASE_CONFIG +=  messaging

#include(vendor/QtFirebase/qtfirebase.pri)
}
    include($$PWD/3rdparty/android-build.pri)
    include($$PWD/android.pri)

}
ios {
#    VERSION = 1
    include($$PWD/ios.pri)
    DEFINES += QT_DEPRECATED_WARNINGS
#    QTFIREBASE_CONFIG +=  messaging
#    include(vendor/QtFirebase/qtfirebase.pri)


}
linux {
    include($$PWD/linux.pri)
}
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DISTFILES += \
    android/AndroidManifest.xml \
    src/controllers/ImagePickerAndroid.java \
    src/font/kapibaraFont.ttf \
    view/components/base/Conteiner.qml \
    view/components/orders/ProgressBarPickUp.qml \




