#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSslConfiguration>
#include "src/servicelocator.h"
#include "src/core.h"
#include <QTextCodec>
int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qDebug()<< "ssl "<< QSslSocket::supportsSsl();
    qDebug()<< "QSslSocket::sslLibraryBuildVersionString() "<< QSslSocket::sslLibraryBuildVersionString();
    qDebug()<< "QSslSocket::sslLibraryVersionString() "<< QSslSocket::sslLibraryVersionString();

    QGuiApplication app(argc, argv);
    Core core;
    if(!core.run())
        return -1;
    return app.exec();
}
