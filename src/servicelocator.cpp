#include "servicelocator.h"
#include <QStandardPaths>
#include "./config/settingapp.h"
ServiceLocator::ServiceLocator()
{
    kapibaras  =new Kapibaras(this);
    storage = new Storage(this);
    router = new Router(this);
    menu = new Menu(this);
    cart = new Cart(this);
    pay = new Pay(this);
    worktime = new WorkTime(this);
    statusStorage = new StatusStorage(this);
    kapibaras->setTmpPathUploads(SettingApp::saveFolder());
    storage->setTmpPathUploads(SettingApp::saveFolder());
    this->connectService();
    this->updateUser(storage->user());

}

ServiceLocator::~ServiceLocator()
{
    delete kapibaras;
    delete storage;
}

void ServiceLocator::run()
{
    //kapibaras->loadCurrentVersion();
}
//проверить скорей всего устарелло
void ServiceLocator::updateUser(QJsonObject user)
{
    if(user.contains("client_info")){
        QJsonObject client_info = user["client_info"].toObject();
        cart->setUserName(client_info["name"].toString());
        cart->setUserPhone(client_info["phone"].toString());
        cart->setUserEmail(client_info["email"].toString());
        //cart->setDataProcessing(client_info["agreement"].toBool());
    }
}

void ServiceLocator::PayCard(OrderRequest request, CardData card)
{
    this->pay->PayCard(request, card);
    this->storage->setUserEmail(request.getOrderInfo().getEmail());
}

void ServiceLocator::setToken(bool status, QString token)
{
    if(status) {
        storage->setTokenLk(token);
    }
}


WorkTime *ServiceLocator::getWorktime() const
{
    return worktime;
}

void ServiceLocator::setCityData(CityData cityData)
{
     this->worktime->setCityData(cityData);
     this->storage->setCityDataJson(cityData);
      this->menu->setCityDataJson(cityData);
      this->pay->setCityDataJson(cityData);

}

StatusStorage *ServiceLocator::getStatusStorage() const
{
    return statusStorage;
}

Pay *ServiceLocator::getPay() const
{
    return pay;
}

Cart *ServiceLocator::getCart() const
{
    return cart;
}

Menu *ServiceLocator::getMenu() const
{
    return menu;
}

Router *ServiceLocator::getRouter() const
{
    return router;
}

Storage *ServiceLocator::getStorage() const
{
    return storage;
}

Kapibaras *ServiceLocator::getKapibaras() const
{
    return kapibaras;
}



void ServiceLocator::registrateService(QQmlContext *context)
{
    context->setContextProperty("kapibaras", kapibaras);
    context->setContextProperty("storage", storage);
    context->setContextProperty("router", router);
    context->setContextProperty("menu", menu);
    context->setContextProperty("cart", cart);
    context->setContextProperty("payService", pay);
    context->setContextProperty("statusStorage", statusStorage);
    context->setContextProperty("worktime", worktime);


}

void ServiceLocator::connectService()
{    


    connect(kapibaras, &Kapibaras::streetLoaded, cart, &Cart::setStrets);

    connect(kapibaras, &Kapibaras::orderSended, router, &Router::popToOrderSuccess);
    connect(kapibaras, &Kapibaras::orderSended, cart, &Cart::clear);
    connect(kapibaras, &Kapibaras::orderSended, [this]{
        this->kapibaras->loadClientData(this->storage->tokenLk());
    });
    connect(kapibaras, &Kapibaras::domainChanged, pay, &Pay::setDomain);
    connect(kapibaras, &Kapibaras::cityIdChanged, pay, &Pay::setCity_id);
    connect(kapibaras, &Kapibaras::cuponLoaded, cart, &Cart::setCupon);

    connect(kapibaras, &Kapibaras::checkStreetLoaded, statusStorage, &StatusStorage::setCheckStreetResponse);


    connect(kapibaras, &Kapibaras::authLoaded, this, &ServiceLocator::setToken);
    connect(kapibaras, &Kapibaras::clientDataLoaded, storage, &Storage::setUser);
    connect(cart, &Cart::sendOrder, kapibaras, &Kapibaras::sendOrder);
    connect(cart, &Cart::payingByCard, this, &ServiceLocator::PayCard);
    connect(cart, &Cart::payingByMarket, pay, &Pay::PayMarket);
    connect(cart, &Cart::createdCheckStreetEntity, kapibaras, &Kapibaras::checkStreet);
    connect(kapibaras, &Kapibaras::endLoaded,cart,&Cart::clearPromocode);




    connect(router, &Router::popLast, menu, &Menu::backCategory);

    connect(storage, &Storage::userChanged, this, &ServiceLocator::updateUser);
    connect(storage, &Storage::userCityChanged, cart, &Cart::clear);
    connect(storage, &Storage::resetPosition, menu, &Menu::resetPositions);

    connect(storage, &Storage::tokenLkChanged, [this](QString token){
            this->kapibaras->setTokenLk(token);
            this->pay->setTokenLk(token);
            kapibaras->loadClientData(token);
    });

    connect(pay, &Pay::success, router, &Router::popToOrderSuccess);   
    connect(pay, &Pay::success, cart, &Cart::clear);
    connect(pay, &Pay::errorPay, router, &Router::popToOrderError);


    connect(pay, &Pay::success, [this]{
        this->kapibaras->loadClientData(this->storage->tokenLk());
    });


}


