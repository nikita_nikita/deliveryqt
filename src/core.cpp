#include "core.h"
//#include <QApplication>
Core::Core(QObject *parent) : QObject(parent)
{
    QQmlContext *context =engine.rootContext();

    serviceLocator= new ServiceLocator();
    serviceLocator->registrateService(context);
    feedbackController = new FeedbackController(this);
    profileConroller = new ProfileController(this);
    paymentController = new PaymentController(this);
    loaderController = new LoaderController(this);
    contactController = new ContactsController(this);
    deliveryController  = new CheckoutDelivetyController(this);
    menuController = new MenuController(this);
    cartController = new CartController(this);



    this->registerController(profileConroller);
    this->registerController(feedbackController);
    this->registerController(paymentController);
    this->registerController(loaderController);
    this->registerController(contactController);
    this->registerController(deliveryController);
    this->registerController(menuController);
    this->registerController(cartController);

    this->setContextPropertys(context);
}

Core::~Core()
{
    delete serviceLocator;
}

bool Core::run()
{
    qDebug()<<"Core run active  " << QString(APP_VERSION);

    serviceLocator->run();
//    if(QString(APP_VERSION).toInt() > 1){
//        qDebug()<<"Firebase active";

//        engine.load(QUrl(QStringLiteral("qrc:/mainFB.qml")));


//    }else{
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

//        qDebug()<<"Firebase  not active !! " << QString(APP_VERSION) ;

//    }


    loaderController->run();
    return !engine.rootObjects().isEmpty();
}

ServiceLocator *Core::getServiceLocator() const
{
    return serviceLocator;
}

void Core::registerController(ObjectController *controller)
{
    controller->setServiceLocator(this->serviceLocator);
    this->controllers.append(controller);
}

void Core::setContextPropertys(QQmlContext *context)
{
    context->setContextProperty("profileController", profileConroller);
    context->setContextProperty("paymentController", paymentController);
    context->setContextProperty("loaderController" , loaderController);
    context->setContextProperty("contactController" , contactController);
    context->setContextProperty("deliveryController" , deliveryController);
    context->setContextProperty("menuController", menuController);
    context->setContextProperty("cartController", cartController);
    context->setContextProperty("feedbackController",feedbackController);
//        if(QString(APP_VERSION).toInt() > 1){
//   context->setContextProperty("firebaseController", firebaseController);
//        }


}
