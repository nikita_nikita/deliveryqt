#ifndef ORDEADDRESS_H
#define ORDEADDRESS_H
#include <QString>
#include <QJsonObject>
#include "street.h"
class OrdeAddress
{
public:
    OrdeAddress();
    QString getStreet() const;
    void setStreet(const QString &value);

    int getEntrance() const;
    void setEntrance(int value);

    int getFloor() const;
    void setFloor(int value);

    int getRoom() const;
    void setRoom(int value);

    QString getBuilding() const;
    void setBuilding(const QString &value);

    QJsonObject toJson(bool entity=false) const;

    Street getStreetEntity() const;
    void setStreetEntity(const Street &value);

private:
    QString street;
    QString building;
    int entrance;
    int floor;
    int room;
    Street streetEntity;

};

#endif // ORDEADDRESS_H
