#include "iikoorder.h"
IikoOrder::IikoOrder()
{

}

IikoOrder::IikoOrder(QJsonObject obj)
{
    this->setData(obj);
}

int IikoOrder::getAmount() const
{
    return amount;
}

void IikoOrder::setAmount(int value)
{
    amount = value;
}

QString IikoOrder::getName() const
{
    return name;
}

void IikoOrder::setName(const QString &value)
{
    name = value;
}

QString IikoOrder::getCode() const
{
    return code;
}

void IikoOrder::setCode(QString value)
{
    code = value;
}

int IikoOrder::getSum() const
{
    return sum;
}

void IikoOrder::setSum(int value)
{
    sum = value;
}

bool IikoOrder::getCanLeaveFeedback() const
{
    return canLeaveFeedback;
}

void IikoOrder::setCanLeaveFeedback(bool value)
{
    canLeaveFeedback = value;
}

QDateTime IikoOrder::getDateTime() const
{
    return dateTime;
}

void IikoOrder::setDateTime(const QDateTime &value)
{
    dateTime = value;
}

int IikoOrder::getId() const
{
    return Id;
}

void IikoOrder::setId(int value)
{
    Id = value;
}

QJsonArray IikoOrder::getProducts() const
{
    return products;
}

void IikoOrder::setProducts(const QJsonArray &value)
{
    products = value;
}

QString IikoOrder::getStatus() const
{
    return status;
}

void IikoOrder::setStatus(const QString &value)
{
    status = value;
}

QString IikoOrder::getPoint() const
{
    return point;
}

void IikoOrder::setPoint(const QString &value)
{
    point = value;
}

bool IikoOrder::getDelivery() const
{
    return delivery;
}

void IikoOrder::setDelivery(bool value)
{
    delivery = value;
}

void IikoOrder::setData(QJsonObject jObj)
{

    this->setId(jObj.value("orderId").toInt());
    this->setDelivery(jObj.value("orderType").toObject().value("orderServiceType").toString()!="DELIVERY_PICKUP");
    if(!delivery){
        QString pointSring = jObj.value("comment").toString();
        QRegExp rxp("(Точка самовывоза:)");
        int n1 = rxp.indexIn(pointSring);
        pointSring.remove(0,n1);
        this->setPoint(pointSring);


    }


    this->setSum(jObj.value("sum").toInt());
    this->setCode(jObj.value("code").toString());
    this->setName(jObj.value("name").toString());
    this->setAmount(jObj.value("amount").toInt());
    this->setStatus(jObj.value("status").toString());

    this->setProducts(jObj.value("items").toArray());

}
