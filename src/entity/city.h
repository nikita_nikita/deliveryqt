#ifndef CITY_H
#define CITY_H

#include "domain.h"


class City
{
public:
    City();
    City(QJsonObject &json);
    int getId() const;
    void setId(int value);

    Domain getDomain() const;
    void setDomain(const Domain &value);

    QString getName() const;
    void setName(const QString &value);

    QString getCode() const;
    void setCode(const QString &value);

    int getEnable() const;
    void setEnable(int value);

    bool getCheckStreet() const;
    void setCheckStreet(bool value);

    int getHrefVisible() const;
    void setHrefVisible(int value);

    int getSortId() const;
    void setSortId(int value);
    QJsonObject toJson() const;
    void setData(QJsonObject &json);
    static City fromJson(QJsonObject json);


private:
    int id=-1;
    Domain domain;
    QString name;
    QString code;
    int enable;
    bool checkStreet;
    int hrefVisible;
    int sortId;
};

#endif // CITY_H
