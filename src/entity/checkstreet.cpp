#include "checkstreet.h"

CheckStreet::CheckStreet()
{

}

CartProductList CheckStreet::getProducts() const
{
    return products;
}

void CheckStreet::setProducts(const CartProductList &value)
{
    products = value;
}

OrdeAddress CheckStreet::getAddress() const
{
    return address;
}

void CheckStreet::setAddress(const OrdeAddress &value)
{
    address = value;
}

int CheckStreet::getCity() const
{
    return city;
}

void CheckStreet::setCity(int value)
{
    city = value;
}

QJsonObject CheckStreet::toJson()
{
    QJsonObject result;
    result["products"]= this->products.toJson();
    result["address"] = this->address.toJson(true);
    result["city"] = this->city;
    result["totalPrice"] = this->totalPrice;
    return result;
}

double CheckStreet::getTotalPrice() const
{
    return totalPrice;
}

void CheckStreet::setTotalPrice(double value)
{
    totalPrice = value;
}
