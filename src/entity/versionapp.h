#ifndef VERSIONAPP_H
#define VERSIONAPP_H

#include <QString>
#include <QJsonObject>
class VersionApp
{
public:
    VersionApp();
    VersionApp(const QJsonObject &json);

    int getVersion() const;
    void setVersion(int value);

    QString getTag() const;
    void setTag(const QString &value);

    bool getRequest() const;
    void setRequest(bool value);
    void setData(const QJsonObject &json);

private:
    int version;
    QString tag;
    bool request;
#ifdef Q_OS_ANDROID
QString platform = "android";
#endif
    #ifdef Q_OS_IOS
    QString platform = "ios";
#endif


};

#endif // VERSIONAPP_H
