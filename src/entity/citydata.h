#ifndef CITYDATA_H
#define CITYDATA_H

#include <QJsonObject>
#include "citydata/product.h"
#include "citydata/offers.h"
#include "citydata/servicekit.h"
#include "citydata/config.h"
#include "citydata/productcategory.h"
#include "citydata/category.h"
#include "citydata/organizationinfo.h"
#include "citydata/payments.h"
#include "citydata/toppings.h"
#include "citydata/worktimeentity.h"
#include "citydata/toppings_kit.h"
using namespace config;

class CityData
{
public:
    CityData();
    CityData(const QJsonObject &value);
    QJsonObject getData() const;
    void setData(const QJsonObject &value);


    void setProduct(QJsonObject &json);
    ProductList getProduct_recomended() const;
    void setProduct_recomended(const ProductList &value);

    ServiceKitList getService_kit() const;
    void setService_kit(const ServiceKitList &value);
    QList<QString> getFileNames() const;

    ProductList getProducts() const;
    void setProducts(const ProductList &value);

    Config getConfig() const;

    ProductCategoryList getProductCategory() const;

    CategoryList getCategories() const;

    OffersList getOffers() const;
    void setOffers(const OffersList &value);

    OrganizationInfo getOrganizationInfo() const;
    void setOrganizationInfo(const OrganizationInfo &value);

    Payments getPayments() const;
    void setPayments(const Payments &value);

    Toppings getToppings() const;
    void setToppings(const Toppings &value);

    WorkTimeEntity getWorkTime() const;
    void setWorkTime(const WorkTimeEntity &value);

    Toppings_kit_list getTopping_kit_list() const;
    void setTopping_kit_list(const Toppings_kit_list &value);    

    QJsonArray getToppingKit() const;
    void setToppingKit(const QJsonArray &value);

    QJsonArray getWarnings() const;
    void setWarnings(const QJsonArray &value);


private:
    QJsonObject data;
    ProductList products;
    ProductList product_recomended;
    ServiceKitList service_kit;    
    Config config;
    OffersList offers;
    ProductCategoryList productCategory;
    CategoryList categories;
    OrganizationInfo organizationInfo;
    Payments payments;
    Toppings toppings;
    WorkTimeEntity workTime;
    QJsonArray warnings;
    QJsonArray toppingKit;


};

#endif // CITYDATA_H
