#ifndef LOYALITY_H
#define LOYALITY_H
#include "loyalitydata.h"
#include <QList>
#include <QJsonArray>
#include <QMap>
class Loyality
{
public:
    enum Types {
        Discount = 2,
        Message = 6,
        BonusProduct = 1

    };
    enum MessageTypes {
        CartMessage = 1,
        PromocodeMessage = 0

    };
    Loyality();
    Loyality(QJsonObject json);
    int getType() const;
    void setType(int value);

    LoyalityData getData() const;
    void setData(const LoyalityData &value);

    QJsonObject getBonus() const;
    void setBonus(const QJsonObject &value);
    Loyality makeEmptyData();

    QJsonValue getMesage() const;
    void setMesage(const QJsonValue &value);

private:
    int type;
    LoyalityData data;
    QJsonValue mesage;
    QJsonObject bonus;

};
class LoyalityList :public QList<Loyality>
{
public:

    LoyalityList findByType(int type);
    QJsonArray getPromocodeMessage();
    QJsonArray getCartMessage();
    int indexByTarget(int target) const;
    void setJson(QJsonArray jArr);
    QMap<QString, double> getProductsDiscount();
    QJsonArray getBonusProductList();
};

#endif // LOYALITY_H
