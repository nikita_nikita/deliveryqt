#include "message.h"

Message::Message()
{

}

Message::Message(QJsonObject jObj)
{
 this->setData(jObj);
}

void Message::setData(QJsonObject jObj)
{
   setType(jObj["type"].toInt());
   setName(jObj["name"].toString());
   setDescription(jObj["description"].toString());
}

QString Message::getGoogColor() const
{
    return googColor;
}

QString Message::getErrorColor() const
{
    return errorColor;
}

QString Message::getDescription() const
{
    return description;
}

void Message::setDescription(const QString &value)
{
    description = value;
}

QString Message::getName() const
{
    return name;
}

void Message::setName(const QString &value)
{
    name = value;
}

int Message::getType() const
{
    return type;
}

void Message::setType(int value)
{
    type = value;
}
