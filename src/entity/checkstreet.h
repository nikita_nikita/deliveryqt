#ifndef CHECKSTREET_H
#define CHECKSTREET_H

#include "cartproductentity.h"
#include "ordeaddress.h"
class CheckStreet
{
public:
    CheckStreet();
    CartProductList getProducts() const;
    void setProducts(const CartProductList &value);

    OrdeAddress getAddress() const;
    void setAddress(const OrdeAddress &value);

    int getCity() const;
    void setCity(int value);
    QJsonObject toJson();

    double getTotalPrice() const;
    void setTotalPrice(double value);

private:
    CartProductList products;
    OrdeAddress address;
    int city;
    double totalPrice;

};

#endif // CHECKSTREET_H
