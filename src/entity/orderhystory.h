#ifndef ORDERHYSTORY_H
#define ORDERHYSTORY_H


#include <QJsonObject>
#include <QJsonArray>
#include <QList>
class OrderHystory
{

public:
    OrderHystory();

OrderHystory(QJsonObject jObj);

QString status() const;

int delivery() const;


QString takeAweypoint() const;

bool canFeedBack() const;

int id() const;

QJsonArray products() const;

public:
void setStatus(QString status);

void setDelivery(int delivery);


void setTakeAweypoint(QString takeAweypoint);

void setCanFeedBack(bool canFeedBack);

void setId(int id);

void setProducts(QJsonArray products);


QString m_status = "Закрыта";

int m_delivery;

QString m_takeAweypoint;

bool m_canFeedBack;

int m_id;
QString dateTime;

QString getProductString();
QJsonArray m_products;
bool operator !=  (const OrderHystory order);


QString getDateTime() const;
void setDateTime(const QString &value);
QJsonArray getProductArray();
};

class OrderHystoryList:public QList<OrderHystory>
{
public:
    int indexOfId(int id);
    OrderHystoryList();
    OrderHystoryList(QJsonArray jArr);

};

#endif // ORDERHYSTORY_H
