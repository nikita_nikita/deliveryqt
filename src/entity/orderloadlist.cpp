
#include "orderloadlist.h"

OrderLoadList::OrderLoadList()
{

}

int OrderLoadList::indexOfId(int index)
{
    for(int i =0; i < this->count();i++){
        if(this->at(i).getId() == index){
            return i;
        }
    }
    return -1;
}

OrderLoad::OrderLoad()
{

}

OrderLoad::OrderLoad(int id)
{
    this->setId(id);
    this->setLoad(false);
    this->setSend(true);
}

int OrderLoad::getId() const
{
    return id;
}

void OrderLoad::setId(int value)
{
    id = value;
}

bool OrderLoad::getSend() const
{
    return send;
}

void OrderLoad::setSend(bool value)
{
    send = value;
}

bool OrderLoad::getLoad() const
{
    return load;
}

void OrderLoad::setLoad(bool value)
{
    load = value;
}
