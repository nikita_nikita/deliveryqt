#include "loyality.h"
Loyality::Loyality()
{

}

Loyality::Loyality(QJsonObject json)
{
    auto type = json["type"].toInt();

    this->setType(type);
    if(type == Loyality::BonusProduct){
        setBonus(json["data"].toObject());
    }else if(type == Loyality::Message){
        this->setMesage((json["data"].toObject()));
    }else{
        this->setData(json["data"].toObject());
    }
}

int Loyality::getType() const
{
    return type;
}

void Loyality::setType(int value)
{
    type = value;
}

LoyalityData Loyality::getData() const
{
    return data;
}

void Loyality::setData(const LoyalityData &value)
{
    data = value;
}

QJsonObject Loyality::getBonus() const
{
    return bonus;
}

void Loyality::setBonus(const QJsonObject &value)
{
    bonus = value;
}

Loyality Loyality::makeEmptyData()
{
    LoyalityData data;
    data.setValue(0);
    Loyality loyality;
    loyality.setData(data);
    return loyality;
}

QJsonValue Loyality::getMesage() const
{
    return mesage;
}

void Loyality::setMesage(const QJsonValue &value)
{
    mesage = value;
}


LoyalityList LoyalityList::findByType(int type)
{
    LoyalityList result;
    for(int i = 0; i < this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==type){
            result.append(loyality);
        }

    }
    return result;
}

QJsonArray LoyalityList::getPromocodeMessage()
{
    QJsonArray result;
    for(int i = 0; i< this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==Loyality::Message){
            auto message = loyality.getMesage().toObject();
            if(message.value("target").toInt() == Loyality::PromocodeMessage){
                result.append(message);
            }
        }
    }

    return result;
}

QJsonArray LoyalityList::getCartMessage()
{
    QJsonArray result;
    for(int i = 0; i< this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==Loyality::Message){
            auto message = loyality.getMesage().toObject();
            if(message.value("target").toInt() == Loyality::CartMessage){
                result.append(message);
            }
        }
    }

    return result;
}

int LoyalityList::indexByTarget(int target) const
{
    for(int i = 0; i< this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==2){
            if(loyality.getData().getTarget() == target){
                return i;
            }
        }
    }
    return -1;
}


void LoyalityList::setJson(QJsonArray jArr)
{
    this->clear();
    for(int i =0; i < jArr.count();i++ ){
        this->append(Loyality(jArr.at(i).toObject()));
    }
}

QMap<QString, double> LoyalityList::getProductsDiscount()
{
    QMap<QString,double> result;
    for(int i = 0; i< this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==Loyality::Discount){
            auto data  = loyality.getData();
            auto iikoItemId = data.getIiko_Item_id();
            double discount = data.getValue();
            if(result.contains(iikoItemId)){
                result[iikoItemId]+=discount;
            }else{
                result[iikoItemId]=discount;
            }

        }
    }

    return result;


}

QJsonArray LoyalityList::getBonusProductList()
{
    QJsonArray result;
    for(int i = 0; i< this->count();i++){
        auto loyality = this->at(i);
        if(loyality.getType() ==Loyality::BonusProduct){
            result.append(loyality.getBonus());

        }
    }
    return result;
}
