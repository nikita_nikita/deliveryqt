#ifndef POINTDELIVERY_H
#define POINTDELIVERY_H

#include <QString>
#include <QJsonArray>
#include <QList>
class PointDelivery
{
public:
    PointDelivery();
    PointDelivery(QJsonObject &json);
    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getAddress() const;
    void setAddress(const QString &value);

    QString getPhone() const;
    void setPhone(const QString &value);

    QJsonArray getCoordinates() const;
    void setCoordinates(const QJsonArray &value);

    int getEnable() const;
    void setEnable(int value);
    void setData(QJsonObject &json);

private:
    int id;
    QString name;
    QString address;
    QString phone;
    QJsonArray coordinates;
    int enable;

};
class PointDeliveryList:public QList<PointDelivery>{
public:
PointDeliveryList();
PointDeliveryList(QJsonArray &json);
void setData(QJsonArray &json);
};
#endif // POINTDELIVERY_H
