#ifndef FEEDBACKDATA_H
#define FEEDBACKDATA_H

#include <QStringList>
class FeedbackData
{
public:
    FeedbackData();
    int getOrderId() const;
    void setOrderId(int value);

    QString getComment() const;
    void setComment(const QString &value);

    int getValue() const;
    void setValue(int value);

    QStringList getImagelist() const;
    void setImagelist(const QStringList &value);

private:
    int orderId;
    QString comment;
    int value;
    QStringList imagelist;
};

#endif // FEEDBACKDATA_H
