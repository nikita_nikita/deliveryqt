#ifndef ORDER_H
#define ORDER_H
#include "cartproductentity.h"

class Order
{
public:
    Order();
    CartProductList getProducts() const;
    void setProducts(const CartProductList &value);
    void appendProduct(const CartProductList &value);


    double getTotalPrice() const;
    void setTotalPrice(double value);
    QJsonObject toJson() const;


private:
    double totalPrice=0;
    CartProductList products;
};

#endif // ORDER_H
