#ifndef CITYLIST_H
#define CITYLIST_H

#include <QList>
#include "city.h"

class CityList: public QList<City>
{
public:
    CityList();
    CityList(QJsonArray &json);
    QJsonArray toJson() const;
    void setData(QJsonArray &json);
    CityList getHrefVisible() const;
    bool existCityId(int id);
    City cityByid(int id);
};


#endif // CITYLIST_H
