#ifndef OFFERS_H
#define OFFERS_H

#include <QString>
#include <QList>
#include <QJsonArray>
#include <QJsonObject>
class Offers
{
public:
    Offers();
     Offers(QJsonObject &json);
    int getId() const;
    void setId(int value);

    int getSortId() const;
    void setSortId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getImageBanner() const;
    void setImageBanner(const QString &value);

    QString getImageCatalog() const;
    void setImageCatalog(const QString &value);

    bool getVisibleBanner() const;
    void setVisibleBanner(bool value);

    bool getVisibleCatalog() const;
    void setVisibleCatalog(bool value);

    bool getVisibleButton() const;
    void setVisibleButton(bool value);

    QString getStartDate() const;
    void setStartDate(const QString &value);

    QString getButtonTitle() const;
    void setButtonTitle(const QString &value);

    QString getEndDate() const;
    void setEndDate(const QString &value);
     void setData(QJsonObject &json);
     QList<QString> getFileNames() const;
     QString getDescription() const;
     void setDescription(const QString &value);

     QString getPromocode() const;
     void setPromocode(const QString &value);

     int getSity() const;
     void setSity(int value);

private:
     int id;
     int sortId;
     QString name;
     QString description;
    QString imageBanner;
    QString imageCatalog;
    bool visibleBanner;
    bool visibleCatalog;
    bool visibleButton;
    QString startDate;
    QString buttonTitle;
    QString endDate;
    QString promocode;
    int  sity;


};
class OffersList:public QList<Offers> {
public:
 OffersList();
 void setData(QJsonArray &json);
 QList<QString> getFileNames() const;
 OffersList getOffersCatalog() const;
 OffersList getOffersBaner() const;
};

#endif // OFFERS_H
