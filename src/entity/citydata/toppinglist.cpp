#include "toppinglist.h"

ToppingList::ToppingList()
{

}

ToppingList::ToppingList(QJsonArray jArr)
{
    for(int i = 0; i < jArr.count();i++){
        this->append(Topping(jArr.at(i).toObject()));
    }
}
QJsonArray ToppingList::toJson()
{
    QJsonArray result;
    for(int i =0; i < this->count();i++){
        QJsonObject jObj;
        QJsonObject product;

        product["id"] = this->at(i).getId();
        product["name"] = this->at(i).getName();
        product["vendor"] = this->at(i).getVendor();
        product["image"] = this->at(i).getImage();
        jObj["product"] = product;
        jObj["quantity"] = this->at(i).getQuantity();
        jObj["count"] = this->at(i).getCount();
        result.append(jObj);
    }
    return result;
}
