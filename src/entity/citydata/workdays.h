#ifndef WORKDAYS_H
#define WORKDAYS_H

#include "workday.h"
#include <QList>
#include <QJsonObject>
class WorkDays:public QList<WorkDay>
{
public:
    WorkDays();
   void setJson(QJsonObject json);

   WorkDay GetByName(QString name);

};

#endif // WORKDAYS_H
