#ifndef WORKTIMEENTITY_H
#define WORKTIMEENTITY_H

#include "workdays.h"
#include "workdayentity.h"
class WorkTimeEntity
{
public:
    WorkTimeEntity();
    WorkTimeEntity(QJsonObject jObj);
    int id() const;
    void setId(int id);

    int getBy_days() const;
    void setBy_days(int value);

    WorkDays getPickup() const;
    void setPickup(const WorkDays &value);

    WorkDays getDelivery() const;
    void setDelivery(const WorkDays &value);


    void setJson(QJsonObject json);


    QList<WorkDayEntity> toWeekList();
    WorkDayEntity GetMonday();
    WorkDayEntity GetTuesday();
    WorkDayEntity GetWednesday();
    WorkDayEntity GetThursday();
    WorkDayEntity GetFriday();
    WorkDayEntity GetSaturday();
    WorkDayEntity GetSunday();



    int getDeliveryTimeLimit() const;
    void setDeliveryTimeLimit(int value);

    int getPickupTimeLimit() const;
    void setPickupTimeLimit(int value);

    WorkDayEntity getDayData(int i);
private:
    int m_id;
    int by_days;
    int deliveryTimeLimit;
    int pickupTimeLimit;
    WorkDays pickup;
    WorkDays delivery;
};

#endif // WORKTIMEENTITY_H
