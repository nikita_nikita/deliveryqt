#include "topping.h"

Topping::Topping()
{

}

Topping::Topping(QJsonObject value)
{
    auto product = value.value("product").toObject();
    this->setImage(product.value("image").toString());
    this->setId(product.value("id").toInt());
    this->setName(product.value("name").toString());
    this->setVendor(product.value("vendor").toString());
    this->setQuantity(1);
    this->setCount(0);
}

QString Topping::getImage() const
{
    return image;
}

void Topping::setImage(const QString &value)
{
    image = value;
}

QString Topping::getName() const
{
    return name;
}

void Topping::setName(const QString &value)
{
    name = value;
}

QString Topping::getVendor() const
{
    return vendor;
}

void Topping::setVendor(const QString &value)
{
    vendor = value;
}

int Topping::getQuantity() const
{
    return quantity;
}

void Topping::setQuantity(int value)
{
    quantity = value;
}

int Topping::getId() const
{
    return id;
}

void Topping::setId(int value)
{
    id = value;
}

int Topping::getCount() const
{
    return count;
}

void Topping::setCount(int value)
{
    count = value;
}
