﻿#ifndef PRODUCTCATEGORY_H
#define PRODUCTCATEGORY_H

#include "product.h"



class ProductCategory
{
public:
    ProductCategory();
    ProductCategory(QString category, QJsonArray &products);

    QString getCategory() const;
    void setCategory(const QString &value);

    ProductList getProduct() const;
    void setProduct(const ProductList &value);
    void setData(QString category, QJsonArray &products);
    bool isEmpty();
     QList<QString> getFileNames() const;
private:
    QString category;
    ProductList product;
};

class ProductCategoryList: public QList<ProductCategory>
{
public:
    ProductCategoryList ();
    void setData(QJsonObject &json);
    ProductCategory findByCategory(QString category) const;
    ProductList  getProductByCategory(QString alias);
    ProductList  getProductByCategory(QStringList aliases);
    QList<QString> getFileNames() const;
private:

};

#endif // PRODUCTCATEGORY_H
