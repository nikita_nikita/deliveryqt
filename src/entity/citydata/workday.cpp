#include "workday.h"

WorkDay::WorkDay()
{

}


QString WorkDay::getStart() const
{
    return start;
}

void WorkDay::setStart(const QString &value)
{
    start = value;
}

QString WorkDay::getEnd() const
{
    return end;
}

void WorkDay::setEnd(const QString &value)
{
    end = value;
}

void WorkDay::setJson(QJsonObject json)
{
   if(!json.isEmpty()) {
       this->setStart(json["start"].toString());
       this->setEnd(json["end"].toString());
   }
}

QString WorkDay::getDayweek() const
{
    return dayweek;
}

void WorkDay::setDayweek(const QString &value)
{
    dayweek = value;
}
