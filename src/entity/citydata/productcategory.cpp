#include "productcategory.h"

ProductCategory::ProductCategory()
{

}

ProductCategory::ProductCategory(QString category, QJsonArray &products)
{
 this->setData(category, products);
}

QString ProductCategory::getCategory() const
{
    return category;
}

void ProductCategory::setCategory(const QString &value)
{
    category = value;
}

ProductList ProductCategory::getProduct() const
{
    return product;
}

void ProductCategory::setProduct(const ProductList &value)
{
    product = value;
}

void ProductCategory::setData(QString category, QJsonArray &products)
{
    this->setCategory(category);
    this->setProduct(ProductList(products));
}

bool ProductCategory::isEmpty()
{
    return this->category.count()==0 && this->product.count()==0;
}

QList<QString> ProductCategory::getFileNames() const
{
    return this->product.getFileNames();
}




ProductCategoryList::ProductCategoryList()
{

}

void ProductCategoryList::setData(QJsonObject &json)
{
    this->clear();
    QStringList keys = json.keys();
    for (int i=0;i<keys.count();i++) {
        QString key = keys.at(i);
        QJsonArray jProducts = json[key].toArray();

        this->append(ProductCategory(key, jProducts));
    }
}

ProductCategory ProductCategoryList::findByCategory(QString category) const
{
    for (int i=0;i<this->count(); i++) {
        ProductCategory categ = this->at(i);
        if (categ.getCategory() == category) {
            return categ;
        }
    }
    return ProductCategory();
}

ProductList ProductCategoryList::getProductByCategory(QStringList aliases)
{
     ProductList result;
     for( int i=0; i<aliases.count(); i++) {
        result.append(this->getProductByCategory(aliases.at(i)));
     }
     return result;
}


ProductList ProductCategoryList::getProductByCategory(QString alias)
{
    for (int i=0;i<this->count(); i++) {
        ProductCategory categ = this->at(i);
        if (categ.getCategory() == alias) {
            return categ.getProduct();
        }
    }
    return ProductList();
}

QList<QString> ProductCategoryList::getFileNames() const
{
    QList<QString> result;
    for(int i=0; i<this->count(); i++) {
     result.append(this->at(i).getFileNames());
    }
    return result;
}
