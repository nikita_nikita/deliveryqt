#include "productitem.h"
Images::Images()
{

}

Images::Images(QJsonArray &json)
{
    this->setData(json);
}

void Images::setData(QJsonArray &json)
{
    this->clear();
    for (int i=0;i<json.count(); i++) {
        this->append(json.at(i).toString());
    }
}

QList<QString> Images::getFileNames() const
{
    QList<QString> result;
    for (int i=0; i<this->count();i++) {
        QString name= this->at(i);
        if (name.count()>0) {
            result.append(name);
        }
    }
    return result;
}

ProductItem::ProductItem()
{

}

ProductItem::ProductItem(QJsonObject json)
{
    this->setData(json);
}

int ProductItem::getId() const
{
    return id;
}

void ProductItem::setId(int value)
{
    id = value;
}

QString ProductItem::getName() const
{
    return name;
}

void ProductItem::setName(const QString &value)
{
    name = value;
}

QString ProductItem::getWeight() const
{
    return weight;
}

void ProductItem::setWeight(const QString &value)
{
    weight = value;
}

bool ProductItem::getNew() const
{
    return lnew;
}

void ProductItem::setNew(bool value)
{
    lnew = value;
}

bool ProductItem::getHot() const
{
    return hot;
}

void ProductItem::setHot(bool value)
{
    hot = value;
}

bool ProductItem::getHit() const
{
    return hit;
}

void ProductItem::setHit(bool value)
{
    hit = value;
}

int ProductItem::getCaloric() const
{
    return caloric;
}

void ProductItem::setCaloric(const int &value)
{
    caloric = value;
}

double ProductItem::getVolume() const
{
    return volume;
}

void ProductItem::setVolume(const double &value)
{
    volume = value;
}

QString ProductItem::getComposition() const
{
    return composition;
}

void ProductItem::setComposition(const QString &value)
{
    composition = value;
}

QString ProductItem::getNumber() const
{
    return number;
}

void ProductItem::setNumber(const QString &value)
{
    number = value;
}

QString ProductItem::getVendor1() const
{
    return vendor1;
}

void ProductItem::setVendor1(const QString &value)
{
    vendor1 = value;
}

QString ProductItem::getVendor2() const
{
    return vendor2;
}

void ProductItem::setVendor2(const QString &value)
{
    vendor2 = value;
}

ProductType ProductItem::getType() const
{
    return m_type;
}

void ProductItem::setType(const ProductType &value)
{
    m_type = value;
}

Images ProductItem::getImages() const
{
    return images;
}

void ProductItem::setImages(const Images &value)
{
    images = value;
}

void ProductItem::setData(const QJsonObject &json)
{
    this->m_data = json;
    this->setNumber(json["number"].toString());
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
    this->setWeight(json["weight"].toString());
    this->setNew(json["new"].toBool());
    this->setHot(json["hot"].toBool());
    this->setHit(json["hit"].toBool());
    this->setCaloric(json["caloric"].toInt());
    this->setVolume(json["volume"].toDouble());
    this->setComposition(json["composition"].toString());

    this->setVendor1(QString::number(json["vendor1"].toInt()));
    this->setVendor2(QString::number(json["vendor2"].toInt()));
    this->setNotDeliverySeparately(json["notDeliverySeparately"].toBool());
    QJsonObject type = json["type"].toObject();
    this->m_type.setData(type);
    QJsonArray images = json["images"].toArray();
    this->images.setData(images);

    QJsonObject siti_info = json["sity_info"].toObject();
    this->sityInfo.setData(siti_info);

}

QJsonObject ProductItem::toJson()
{
    if(this->m_data.isEmpty()){
        // тут возвращаем если мы  создали  клас без парсинга
    }

    return m_data;
}

bool ProductItem::operator ==(ProductItem product)
{
    return getId()==product.getId();
}

QList<QString> ProductItem::getFileNames() const
{
    return images.getFileNames();
}

SityInfo ProductItem::getSityInfo() const
{
    return sityInfo;
}

void ProductItem::setSityInfo(const SityInfo &value)
{
    sityInfo = value;
}

bool ProductItem::IsPizza()
{
    return this->m_type.isPizza();
}

bool ProductItem::IsSet()
{
  return this->m_type.isSet();
}

bool ProductItem::getNotDeliverySeparately() const
{
    return notDeliverySeparately;
}

void ProductItem::setNotDeliverySeparately(bool value)
{
    notDeliverySeparately = value;
}

void ProductItemList::setData(QJsonArray json)
{
    this->clear();
    for (int i =0;i<json.count(); i++) {
        this->append(ProductItem(json.at(i).toObject()));

    }
}

QList<QString> ProductItemList::getFileNames() const
{
    QList<QString> result;
    for (int i=0;i<this->count();i++) {
        result.append(this->at(i).getFileNames());
    }

    return  result;
}
