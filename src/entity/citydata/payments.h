#ifndef PAYMENTS_H
#define PAYMENTS_H

#include <QString>
#include <QJsonObject>
class Payments
{
public:
    Payments();
    QString getPublicid() const;
    void setPublicid(const QString &value);

    QString getCurrency() const;
    void setCurrency(const QString &value);

    QString getPrivateid() const;
    void setPrivateid(const QString &value);

    int getEnable() const;
    void setEnable(int value);

    int getId() const;
    void setId(int value);

    void setData(QJsonObject json);

    int getApple_pay() const;
    void setApple_pay(int value);

    int getGoogle_pay() const;
    void setGoogle_pay(int value);

    int getDelivery_cash() const;
    void setDelivery_cash(int value);

    int getDelivery_card() const;
    void setDelivery_card(int value);

    int getDelivery_online() const;
    void setDelivery_online(int value);

    int getPickup_cash() const;
    void setPickup_cash(int value);

    int getPickup_card() const;
    void setPickup_card(int value);

    int getPickup_online() const;
    void setPickup_online(int value);

private:
    int id;
    QString publicid;
    QString currency;
    QString privateid;
    int enable;
    int apple_pay;
    int google_pay;
    int delivery_cash = -1;//Наличка на доставке
    int delivery_card = -1;//Карта на доставке
    int delivery_online = -1;//Онлайн оплата на доставке
    int pickup_cash = -1;//Наличка на самовывоз
    int pickup_card = -1;//Карта на самовывоз
    int pickup_online = -1;//Онлайн оплата на самовывоз


};

#endif // PAYMENTS_H
