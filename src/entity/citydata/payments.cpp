#include "payments.h"
Payments::Payments()
{

}

QString Payments::getPublicid() const
{
    return publicid;
}

void Payments::setPublicid(const QString &value)
{
    publicid = value;
}

QString Payments::getCurrency() const
{
    return currency;
}

void Payments::setCurrency(const QString &value)
{
    currency = value;
}

QString Payments::getPrivateid() const
{
    return privateid;
}

void Payments::setPrivateid(const QString &value)
{
    privateid = value;
}

int Payments::getEnable() const
{
    return enable;
}

void Payments::setEnable(int value)
{
    enable = value;
}

int Payments::getId() const
{
    return id;
}

void Payments::setId(int value)
{
    id = value;
}

void Payments::setData(QJsonObject json)
{
   this->setId(json["id"].toInt());
    this->setPublicid(json["publicid"].toString());
    this->setCurrency(json["currency"].toString());
    this->setPrivateid(json["privateid"].toString());
    this->setApple_pay(json["apple_pay"].toInt());
    this->setGoogle_pay(json["google_pay"].toInt());

    this->setDelivery_card(json["delivery_card"].toInt());
    this->setDelivery_cash(json["delivery_cash"].toInt());
    this->setDelivery_online(json["delivery_online"].toInt());
    this->setPickup_card(json["pickup_card"].toInt());
    this->setPickup_cash(json["pickup_cash"].toInt());
    this->setPickup_online(json["pickup_online"].toInt());

    this->setEnable(json["enable"].toInt());



}

int Payments::getApple_pay() const
{
    return apple_pay;
}

void Payments::setApple_pay(int value)
{
    apple_pay = value;
}

int Payments::getGoogle_pay() const
{
    return google_pay;
}

void Payments::setGoogle_pay(int value)
{
    google_pay = value;
}

int Payments::getDelivery_cash() const
{
    return delivery_cash;
}

void Payments::setDelivery_cash(int value)
{
    delivery_cash = value;
}

int Payments::getDelivery_card() const
{
    return delivery_card;
}

void Payments::setDelivery_card(int value)
{
    delivery_card = value;
}

int Payments::getDelivery_online() const
{
    return delivery_online;
}

void Payments::setDelivery_online(int value)
{
    delivery_online = value;
}

int Payments::getPickup_cash() const
{
    return pickup_cash;
}

void Payments::setPickup_cash(int value)
{
    pickup_cash = value;
}

int Payments::getPickup_card() const
{
    return pickup_card;
}

void Payments::setPickup_card(int value)
{
    pickup_card = value;
}

int Payments::getPickup_online() const
{
    return pickup_online;
}

void Payments::setPickup_online(int value)
{
    pickup_online = value;
}
