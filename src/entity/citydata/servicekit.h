#ifndef SERVICEKIT_H
#define SERVICEKIT_H

#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QList>
#include <QMap>
class ServiceKitValue {
public:
    ServiceKitValue();
     ServiceKitValue(QJsonObject &json);
    QString getImage() const;
    void setImage(const QString &value);

    QString getText() const;
    void setText(const QString &value);
    void setData(QJsonObject &json);

private:
    QString image;
    QString text;
    int vendor = 0;
    int quantity = 1;
};

class ServiceKit
{
public:
    ServiceKit();
    ServiceKit(QJsonObject &json);
    void setData(QJsonObject &json);
    QList<ServiceKitValue> getValue() const;
    void setValue(const QList<ServiceKitValue> &value);
    void setValue(QJsonArray &json);
    QList<QString> getFileNames() const;
    double getFrom() const;
    void setFrom(double value);

    double getTo() const;
    void setTo(double value);

    QMap<QString, bool> getParams() const;
    void setParams(const QMap<QString, bool> &value);
    void setParams(const QJsonObject &json);
    bool IsSuit(QMap<QString, double> prices);

private:
    QList<ServiceKitValue> value;
    double from;
    double to;
    QMap<QString,bool> m_params;

};
class ServiceKitList: public QList<ServiceKit> {
public:
    ServiceKitList();
    ServiceKitList(QJsonArray &json);
    void setData(QJsonArray &json);
    QList<QString> getFileNames() const;
    QList<ServiceKitValue> getValue(QMap<QString, double> prices);
};


#endif // SERVICEKIT_H
