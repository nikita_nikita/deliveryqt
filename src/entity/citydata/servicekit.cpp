#include "servicekit.h"
ServiceKit::ServiceKit()
{

}

ServiceKit::ServiceKit(QJsonObject &json)
{
    this->setData(json);
}

void ServiceKit::setData(QJsonObject &json)
{
    QJsonArray jvalue = json["value"].toArray();
    this->setValue(jvalue);
     auto jfrom = json["from"];
     auto jto  = json["to"];
     if (jfrom.isString()){
         this->from = jfrom.toString().toDouble();
     } else {
         from = jfrom.toDouble();
     }
     if (jto.isString()){
           this->to = jto.toString().toDouble();
     } else {
         this->to = jto.toDouble();
     }

        this->setParams(json["params"].toObject());

}

QList<ServiceKitValue> ServiceKit::getValue() const
{
    return value;
}

void ServiceKit::setValue(const QList<ServiceKitValue> &value)
{
    this->value = value;
}

void ServiceKit::setValue(QJsonArray &json)
{
    this->value.clear();
    for (int i =0;i<json.count(); i++) {
        QJsonObject obj  = json.at(i).toObject();
        this->value.append(ServiceKitValue(obj));
    }

}

QList<QString> ServiceKit::getFileNames() const
{
    QList<QString> result;
    for (int i=0;i<this->value.count(); i++) {
        ServiceKitValue val = this->value.at(i);
        if(val.getImage().count()>0) {
            result.append(val.getImage());
        }
    }
    return result;
}

double ServiceKit::getFrom() const
{
    return from;
}

void ServiceKit::setFrom(double value)
{
    from = value;
}

double ServiceKit::getTo() const
{
    return to;
}

void ServiceKit::setTo(double value)
{
    to = value;
}

QMap<QString, bool> ServiceKit::getParams() const
{
    return m_params;
}

void ServiceKit::setParams(const QMap<QString, bool> &value)
{
    m_params = value;
}

void ServiceKit::setParams(const QJsonObject &json)
{
    this->m_params.clear();
    QJsonObject::const_iterator i= json.begin();
    while (i!=json.end()) {
        m_params[i.key()]= i.value().toBool();
        ++i;

    }

}

bool ServiceKit::IsSuit(QMap<QString, double> prices)
{
    double price = 0 ;
    QMap<QString,bool>::iterator i  = m_params.begin();
    while (i!=m_params.end()) {
        if(i.value()&& prices.contains(i.key())){
            price+=prices[i.key()];
        }
        ++i;
    }
  return price>=from&&price<=to;
}

ServiceKitValue::ServiceKitValue(QJsonObject &json)
{
    this->setData(json);
}

QString ServiceKitValue::getImage() const
{
    return image;
}

void ServiceKitValue::setImage(const QString &value)
{
    image = value;
}

QString ServiceKitValue::getText() const
{
    return text;
}

void ServiceKitValue::setText(const QString &value)
{
    text = value;
}

void ServiceKitValue::setData(QJsonObject &json)
{
    this->setImage(json["image"].toString());
    this->setText(json["text"].toString());
}

ServiceKitList::ServiceKitList()
{

}

ServiceKitList::ServiceKitList(QJsonArray &json)
{
    this->setData(json);
}

void ServiceKitList::setData(QJsonArray &json)
{
    this->clear();
    for (int i=0;i<json.count(); i++) {
        QJsonObject obj = json.at(i).toObject();
        this->append(ServiceKit(obj));

    }
}

QList<QString> ServiceKitList::getFileNames() const
{
    QList<QString> result;
    for (int i = 0; i < this->count(); ++i) {
            result.append(this->at(i).getFileNames());
    }
    return result;
}

QList<ServiceKitValue> ServiceKitList::getValue(QMap<QString, double> prices)
{
    QList<ServiceKitValue> result;
    for (int i=0; i<this->count(); i++) {
        auto kit = this->at(i);
        if (kit.IsSuit(prices)){
            result.append(kit.getValue());
        }
    }
    return result;
}
