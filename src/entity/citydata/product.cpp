#include "product.h"
#include <QDebug>


Product::Product()
{

}

Product::Product(QJsonObject &json)
{
    this->setData(json);
}

void Product::setAdded(QJsonObject &json)
{
    this->added.setData(json);

}

QList<Product> Product::toList(QJsonArray &json) const
{
    QList<Product> result;
    for (int i=0;i<json.count(); i++) {
        QJsonObject obj = json.at(i).toObject();
        QJsonObject jproduct = obj["product"].toObject();
        result.append(Product(jproduct));
    }
    return result;
}

void Product::setData(QJsonObject &json)
{
    ProductItem::setData(json);
    QJsonObject added = json["added"].toObject();
    this->setAdded(added);

    this->m_type.setData(json["type"].toObject());
    this->m_data = json;
}

QJsonObject Product::toJson() const
{
    if(this->m_data.isEmpty()){
        // тут возвращаем если мы  создали  клас без парсинга
    }
    return m_data;
}

QList<QString> Product::getFileNames() const
{
    QList<QString> result;
    result.append(this->images.getFileNames());
    result.append(this->added.getFileNames());
    return  result;
}



ProductAdded Product::getAdded() const
{
    return added;
}

double Product::getProfit() const
{
    return this->sityInfo.getProfit();
}

double Product::getBenefit()
{
    double result = this->getAdded().getComposite().getTotalPrice();
    return qRound(result*100)/100;
}



bool Product::operator ==(Product product)
{
    if ( this->getId() != product.getId()) return false;
    //if (this->getAdded() != product.getAdded()) return  false;
    return true;
}



double Product::getPrice() const
{
    return this->sityInfo.getPrice();
}


int ProductList::indexOfId(int id)
{
    int index = -1;
    for(int i= 0; i < this->count();i++ ){
        if(this->at(i).getId() == id){
            index = i;
        }
    }
    return index;
}

ProductList::ProductList()
{

}

ProductList::ProductList(QJsonArray &json)
{
    this->setData(json);
}

void ProductList::setData(QJsonArray &json)
{
    this->clear();
    for (int i=0;i<json.count(); i++) {
        QJsonObject obj = json.at(i).toObject();
        this->append(Product(obj));

    }
}

QList<QString> ProductList::getFileNames() const
{
    QList<QString> result;
    for (int i=0; i<this->count();i++) {
        result.append(this->at(i).getFileNames());
    }
    return result;
}

Product ProductList::getById(int id)
{
    for(int i=0; i<this->count(); i++){
        Product prod = this->at(i);
        if (prod.getId()==id){
            return prod;
        }
    }
    return Product();
}


ProductAddedItem ProductAdded::getMeat() const
{
    return meat;
}

void ProductAdded::setMeat(const ProductAddedItem &value)
{
    meat = value;
}

ProductAddedItem ProductAdded::getVegetable() const
{
    return vegetable;
}

void ProductAdded::setVegetable(const ProductAddedItem &value)
{
    vegetable = value;
}

ProductAddedItemList ProductAdded::getToping() const
{
    return toping;
}

void ProductAdded::setToping(const ProductAddedItemList &value)
{
    toping = value;
}

ProductAddedItemList ProductAdded::getNoodle() const
{
    return noodle;
}

void ProductAdded::setNoodle(const ProductAddedItemList &value)
{
    noodle = value;
}

void ProductAdded::setData(QJsonObject &json)
{
    if(json.contains("composite")) {
        QJsonArray arr = json["composite"].toArray();
        this->composite.setData(arr);
    }
    if(json.contains("toping")) {
        QJsonArray arr = json["toping"].toArray();
        this->toping.setData(arr);
    }

    if(json.contains("meat")) {
        QJsonObject  meat = json["meat"].toObject();
        this->meat.setData(meat);
    }
    if(json.contains("vegetable")) {
        QJsonObject  vegetable = json["vegetable"].toObject();
        this->vegetable.setData(vegetable);
    }
    if(json.contains("noodle")) {
        QJsonArray arr = json["noodle"].toArray();
        this->noodle.setData(arr);
    }
}

bool ProductAdded::operator ==(ProductAdded product)
{
    return this->composite==product.getComposite()&&toping==product.getToping()&&meat==product.getMeat()&&vegetable==product.getVegetable()&&noodle==product.getNoodle();
}

bool ProductAdded::operator !=(ProductAdded product)
{
    return !(this->composite==product.getComposite()&&toping==product.getToping()&&meat==product.getMeat()&&vegetable==product.getVegetable()&&noodle==product.getNoodle());
}

QList<QString> ProductAdded::getFileNames() const
{
    QList<QString>  result;
    result.append(composite.getFileNames());
    result.append(toping.getFileNames());
    result.append(meat.getFileNames());
    result.append(vegetable.getFileNames());
    result.append(noodle.getFileNames());
    return result;
}

ProductAddedItemList ProductAdded::getComposite() const
{
    return composite;
}

void ProductAdded::setComposite(const ProductAddedItemList &value)
{
    composite = value;
}
