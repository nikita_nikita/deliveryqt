#include "worktimeentity.h"
#include <QDebug>
WorkTimeEntity::WorkTimeEntity()
{

}

WorkTimeEntity::WorkTimeEntity(QJsonObject jObj)
{this->setJson(jObj);

}

int WorkTimeEntity::id() const
{
    return m_id;
}

void WorkTimeEntity::setId(int id)
{
    m_id = id;
}

int WorkTimeEntity::getBy_days() const
{
    return by_days;
}

void WorkTimeEntity::setBy_days(int value)
{
    by_days = value;
}

WorkDays WorkTimeEntity::getPickup() const
{
    return pickup;
}

void WorkTimeEntity::setPickup(const WorkDays &value)
{
    pickup = value;
}

WorkDays WorkTimeEntity::getDelivery() const
{
    return delivery;
}

void WorkTimeEntity::setDelivery(const WorkDays &value)
{
    delivery = value;
}

void WorkTimeEntity::setJson(QJsonObject json)
{

    this->setId(json["id"].toInt());
    this->setBy_days(json["by_days"].toInt());
    this->setDeliveryTimeLimit(json["delivery_time_limit"].toInt());
    this->setPickupTimeLimit(json["pickup_time_limit"].toInt());
    this->pickup.setJson(json["pickup"].toObject());
    this->delivery.setJson((json["delivery"]).toObject());
    if(by_days == 0){


        auto pickupTime = pickup.GetByName("monday");;
        auto deliveryTime = delivery.GetByName("monday");;

        for(int i = 1; i <pickup.count();i++){
            auto day = pickup.at(i);
            day.setStart(pickupTime.getStart());
            day.setEnd(pickupTime.getEnd());
            pickup.replace(i,day);
        }
        for(int i = 1; i <delivery.count();i++){
            auto day = delivery.at(i);
            day.setStart(deliveryTime.getStart());
            day.setEnd(deliveryTime.getEnd());
            delivery.replace(i,day);

        }
    }

}

QList<WorkDayEntity> WorkTimeEntity::toWeekList()
{
    QList<WorkDayEntity> result;
    result.append(GetMonday());
    result.append(GetTuesday());
    result.append(GetWednesday());
    result.append(GetThursday());
    result.append(GetFriday());
    result.append(GetSaturday());
    result.append(GetSunday());
    return result;
}

WorkDayEntity WorkTimeEntity::GetMonday()
{
    WorkDayEntity result;
    result.setName("Понедельник");
    WorkDay pickup = this->pickup.GetByName("monday");
    WorkDay delivety = this->delivery.GetByName("monday");
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetTuesday()
{
    WorkDayEntity result;
    result.setName("Вторник");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("tuesday");
        delivety = this->delivery.GetByName("tuesday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetWednesday()
{
    WorkDayEntity result;
    result.setName("Среда");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("wednesday");
        delivety = this->delivery.GetByName("wednesday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetThursday()
{
    WorkDayEntity result;
    result.setName("Четверг");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("thursday");
        delivety = this->delivery.GetByName("thursday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetFriday()
{
    WorkDayEntity result;
    result.setName("Пятница");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("friday");
        delivety = this->delivery.GetByName("friday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetSaturday()
{
    WorkDayEntity result;
    result.setName("Суббота");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("saturday");
        delivety = this->delivery.GetByName("saturday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::GetSunday()
{
    WorkDayEntity result;
    result.setName("Воскресенье");
    WorkDay pickup;
    WorkDay delivety;
    if(getBy_days()){
        pickup = this->pickup.GetByName("sunday");
        delivety = this->delivery.GetByName("sunday");
    } else {
        pickup = this->pickup.GetByName("monday");
        delivety = this->delivery.GetByName("monday");
    }
    result.setDelivery(delivety);
    result.setPickup(pickup);
    return result;
}

WorkDayEntity WorkTimeEntity::getDayData(int i)
{
     qDebug()<<"i am hear >> "<< this->getBy_days();
    if(this->getBy_days() == 0){
        qDebug()<<"i am hear";
        return this->GetMonday();
    }
    switch (i) {
    case 1:
        return this->GetMonday();
    case 2:
        return this->GetTuesday();
    case 3:
        return this->GetWednesday();
    case 4:
        return this->GetThursday();
    case 5:
        return this->GetFriday();
    case 6:
        return this->GetSaturday();
    case 7:
        return this->GetSunday();
    default:
        return this->GetMonday();
    }
}

int WorkTimeEntity::getDeliveryTimeLimit() const
{
    return deliveryTimeLimit;
}

void WorkTimeEntity::setDeliveryTimeLimit(int value)
{
    deliveryTimeLimit = value;
}

int WorkTimeEntity::getPickupTimeLimit() const
{
    return pickupTimeLimit;
}

void WorkTimeEntity::setPickupTimeLimit(int value)
{
    pickupTimeLimit = value;
}
