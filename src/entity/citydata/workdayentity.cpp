#include "workdayentity.h"

WorkDayEntity::WorkDayEntity()
{

}

QString WorkDayEntity::getName() const
{
    return name;
}

void WorkDayEntity::setName(const QString &value)
{
    name = value;
}

WorkDay WorkDayEntity::getPickup() const
{
    return pickup;
}

void WorkDayEntity::setPickup(const WorkDay &value)
{
    pickup = value;
}

WorkDay WorkDayEntity::getDelivery() const
{
    return delivery;
}

void WorkDayEntity::setDelivery(const WorkDay &value)
{
    delivery = value;
}

QTime WorkDayEntity::getTimeOpen()
{
    QTime pickup = QTime::fromString(this->pickup.getStart());
     QTime delivery = QTime::fromString(this->delivery.getStart());
     if (pickup<delivery){
         return pickup;
     }
     return delivery;
}

QTime WorkDayEntity::getTimeClose()
{
    QTime pickup = QTime::fromString(this->pickup.getEnd());
     QTime delivery = QTime::fromString(this->delivery.getEnd());
     if (pickup>delivery){
         return pickup;
     }
     return delivery;
}

QTime WorkDayEntity::getTimeCloseDelivery()
{
    return QTime::fromString(this->delivery.getEnd());
}

QTime WorkDayEntity::getTimeClosePickup()
{
    return QTime::fromString(this->pickup.getEnd());
}
