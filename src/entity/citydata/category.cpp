#include "category.h"
Category::Category()
{

}

Category::Category(QJsonObject &json)
{
    this->setData(json);
}

Category::Category(QString name, QString alias)
{
    this->name = name;
    this->alias = alias;
}

int Category::getId() const
{
    return id;
}

void Category::setId(int value)
{
    id = value;
}

QString Category::getName() const
{
    return name;
}

void Category::setName(const QString &value)
{
    name = value;
}

QString Category::getParent() const
{
    return parent;
}

void Category::setParent(const QString &value)
{
    parent = value;
}



QList<Category> Category::getChild() const
{
    return child;
}

void Category::setChild(const QList<Category> &value)
{
    child = value;
}

void Category::setChild(const QJsonArray json)
{
    this->child.clear();
    for (int i =0;i<json.count(); i++) {
        QJsonObject obj = json.at(i).toObject();
        this->child.append(Category(obj));
    }
}

void Category::setData(QJsonObject &json)
{
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
    if(json["info"] != QJsonValue::Null){
        this->setInfo(json["info"].toString());
    }
    this->setAlias(json["alias"].toString());
    this->setParent(json["parent"].toString());
    this->setSortId(json["sortId"].toInt());
    this->setChild(json["child"].toArray());
}

int Category::getSortId() const
{
    return sortId;
}

void Category::setSortId(int value)
{
    sortId = value;
}

QStringList Category::getChildAliases()
{
    QStringList result;
    for (int i=0;i<this->child.count();i++) {
        Category categ = child.at(i);
        result.append(categ.getAlias());
        result.append(categ.getChildAliases());
    }
    return result;
}

QString Category::getAlias() const
{
    return alias;
}

void Category::setAlias(const QString &value)
{
    alias = value;
}

QString Category::getInfo() const
{
    return info;
}

void Category::setInfo(const QString &value)
{
    info = value;
}

CategoryList::CategoryList()
{

}

CategoryList::CategoryList(QList<Category> categories)
{
    for (int i=0;i<categories.count();i++) {
        this->append(categories.at(i));
    }
}

void CategoryList::setData(QJsonArray &array)
{
    this->clear();
    for (int i=0;i<array.count(); i++) {
        QJsonObject categ = array.at(i).toObject();
        this->append(Category(categ));

    }
}

QStringList CategoryList::getAliases() const
{
    QStringList result;
    for (int i=0 ; i<this->count();i++) {
        Category categ  = this->at(i);
        result.append(categ.getAlias());
        result.append(categ.getChildAliases());
    }
    return result;
}
