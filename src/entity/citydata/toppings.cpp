#include "toppings.h"

Toppings::Toppings()
{

}

ProductList Toppings::getJap() const
{
    return jap;
}

void Toppings::setJap(const ProductList &value)
{
    jap = value;
}

ProductList Toppings::getPizza() const
{
    return pizza;
}

void Toppings::setPizza(const ProductList &value)
{
    pizza = value;
}

void Toppings::setData(QJsonObject object)
{
    QJsonArray jap = object["jap"].toArray();
    QJsonArray pizza = object["pizza"].toArray();
    this->jap.setData(jap);
    this->pizza.setData(pizza);
}
