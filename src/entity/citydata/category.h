#ifndef CATEGORY_H
#define CATEGORY_H

#include <QList>
#include <QString>
#include <QJsonObject>
#include <QJsonArray>
class CategoryList;
class Category
{
public:
    Category();
    Category(QJsonObject &json);
     Category(QString name, QString alias);

    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getParent() const;
    void setParent(const QString &value);



    QList<Category> getChild() const;
    void setChild(const QList<Category> &value);
    void setChild(const QJsonArray json);

    void setData(QJsonObject &json);

    int getSortId() const;
    void setSortId(int value);
    QStringList getChildAliases();


    QString getAlias() const;
    void setAlias(const QString &value);

    QString getInfo() const;
    void setInfo(const QString &value);

private:
    int id;
    QString name;
    QString parent;
    QString alias;
    int sortId;
    QString info = "";
    QList<Category> child;


};
class CategoryList: public QList<Category>{
public:
    CategoryList();
    CategoryList(QList<Category> categories);
    void setData(QJsonArray &array);
    QStringList getAliases() const;

};


#endif // CATEGORY_H
