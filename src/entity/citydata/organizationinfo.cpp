#include "organizationinfo.h"
OrganizationInfo::OrganizationInfo()
{

}

OrganizationInfo::OrganizationInfo(QJsonObject jObj)
{
   this->setData(jObj);
}

int OrganizationInfo::getId() const
{
    return id;
}

void OrganizationInfo::setId(int value)
{
    id = value;
}

QString OrganizationInfo::getName() const
{
    return name;
}

void OrganizationInfo::setName(const QString &value)
{
    name = value;
}

QString OrganizationInfo::getPhone() const
{
    return phone;
}

void OrganizationInfo::setPhone(const QString &value)
{
    phone = value;
}

QString OrganizationInfo::getPhone1() const
{
    return phone1;
}

void OrganizationInfo::setPhone1(const QString &value)
{
    phone1 = value;
}

QString OrganizationInfo::getPhone2() const
{
    return phone2;
}

void OrganizationInfo::setPhone2(const QString &value)
{
    phone2 = value;
}

QString OrganizationInfo::getUsp() const
{
    return usp;
}

void OrganizationInfo::setUsp(const QString &value)
{
    usp = value;
}

int OrganizationInfo::getDelivery() const
{
    return delivery;
}

void OrganizationInfo::setDelivery(int value)
{
    delivery = value;
}

int OrganizationInfo::getPickup() const
{
    return pickup;
}

void OrganizationInfo::setPickup(int value)
{
    pickup = value;
}

QString OrganizationInfo::getEmail() const
{
    return email;
}

void OrganizationInfo::setEmail(const QString &value)
{
    email = value;
}

QString OrganizationInfo::getPhoneMask() const
{
    return phoneMask;
}

void OrganizationInfo::setPhoneMask(const QString &value)
{
    phoneMask = value;
}

QString OrganizationInfo::getVk() const
{
    return vk;
}

void OrganizationInfo::setVk(const QString &value)
{
    vk = value;
}

QString OrganizationInfo::getInstagram() const
{
    return instagram;
}

void OrganizationInfo::setInstagram(const QString &value)
{
    instagram = value;
}

void OrganizationInfo::setData(QJsonObject json)
{
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
    this->setPhone(json["phone"].toString());
    this->setPhone1(json["phone1"].toString());
    this->setPhone2(json["phone2"].toString());
    this->setUsp(json["usp"].toString());
    this->setDelivery(json["delivery"].toInt());
    this->setPickup(json["pickup"].toInt());
    this->setEmail(json["email"].toString());
    this->setPhoneMask(json["phoneMask"].toString());
    this->setVk(json["vk"].toString());
    this->setInstagram(json["instagram"].toString());
    this->setCheckStreet(json["checkStreet"].toBool());
    this->setCardPayDelivery(json["cardPayDelivery"].toInt());
    this->setToppingsEnable(json["toppings_enable"].toInt() == 1);
    this->setContactless_delivery(json["contactless_delivery"].toInt() ==1);
    this->setCallbackEnagle(json["feedback_enable"].toInt() ==1);
    this->setOrder_status(json["order_status"].toInt()==1);
}

bool OrganizationInfo::getCheckStreet() const
{
    return checkStreet;
}

void OrganizationInfo::setCheckStreet(bool value)
{
    checkStreet = value;
}

int OrganizationInfo::getCardPayDelivery() const
{
    return cardPayDelivery;
}

void OrganizationInfo::setCardPayDelivery(int value)
{
    cardPayDelivery = value;
}

bool OrganizationInfo::getToppingsEnable() const
{
    return toppingsEnable;
}

void OrganizationInfo::setToppingsEnable(bool value)
{
    toppingsEnable = value;
}

bool OrganizationInfo::getContactless_delivery() const
{
    return contactless_delivery;
}

void OrganizationInfo::setContactless_delivery(bool value)
{
    contactless_delivery = value;
}

bool OrganizationInfo::getCallbackEnagle() const
{
    return callbackEnagle;
}

void OrganizationInfo::setCallbackEnagle(bool value)
{
    callbackEnagle = value;
}

bool OrganizationInfo::getOrder_status() const
{
    return order_status;
}

void OrganizationInfo::setOrder_status(bool value)
{
    order_status = value;
}
