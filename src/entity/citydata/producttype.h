#ifndef PRODUCTTYPE_H
#define PRODUCTTYPE_H

#include <QString>
#include <QJsonObject>
class ProductType
{
public:
    ProductType();
    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);
    void setData(QJsonObject json);
    bool isPizza();
    bool isSet();


private:
    int id;
    QString name;

};

#endif // PRODUCTTYPE_H
