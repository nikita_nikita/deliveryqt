#include "sityinfo.h"

SityInfo::SityInfo()
{

}

SityInfo::SityInfo(QJsonObject &json)
{
   this->setData(json);
}

int SityInfo::getId() const
{
    return id;
}

void SityInfo::setId(int value)
{
    id = value;
}

double SityInfo::getPrice() const
{
    return price;
}

void SityInfo::setPrice(double value)
{
    price = value;
}

double SityInfo::getProfit() const
{
    return profit;
}

void SityInfo::setProfit(double value)
{
    profit = value;
}

void SityInfo::setData(QJsonObject &json)
{
    this->setId(json["id"].toInt());
    this->setPrice(json["price"].toDouble());
    this->setProfit(json["profit"].toDouble());
}
