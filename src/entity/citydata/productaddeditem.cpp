#include "productaddeditem.h"


ProductAddedItem::ProductAddedItem()
{

}

ProductAddedItem::ProductAddedItem(QJsonObject obj)
{
   this->setData(obj);
}

QString ProductAddedItem::getType() const
{
    return type;
}

void ProductAddedItem::setType(const QString &value)
{
    type = value;
}

int ProductAddedItem::getSelected() const
{
    return selected;
}

void ProductAddedItem::setData(QJsonObject &obj)
{
    this->setType(obj["type"].toString());
    this->setSelected(obj["selected"].toInt());
    this->setQuanatity(obj["quantity"].toInt(1));
    QJsonObject pr = obj["product"].toObject();
    this->product.setData(pr);
}

bool ProductAddedItem::operator ==(ProductAddedItem product)
{
    return this->getType()==product.getType()&&getSelected()==product.getSelected()&&getProduct()==product.getProduct();
}

QList<QString> ProductAddedItem::getFileNames() const
{
    return product.getFileNames();
}

int ProductAddedItem::getQuanatity() const
{
    return quanatity;
}

void ProductAddedItem::setQuanatity(int value)
{
    quanatity = value;
}



ProductItem ProductAddedItem::getProduct() const
{
    return product;
}

void ProductAddedItem::setProduct(const ProductItem &value)
{
    product = value;
}

void ProductAddedItem::setSelected(int value)
{
    selected = value;
}

void ProductAddedItemList::setData(QJsonArray &json)
{
    for(int i =0; i< json.count(); i++){
        this->append(ProductAddedItem(json.at(i).toObject()));
    }
}

QList<QString> ProductAddedItemList::getFileNames() const
{
    QList<QString> result;
    for (int i=0; i<count(); i++) {
        result.append(at(i).getFileNames());
    }
    return result;
}

double ProductAddedItemList::getTotalPrice()
{
    double result=0;
    for (int i=0;i<count();i++) {
        ProductAddedItem item = at(i);
        result +=  (item.getProduct().getSityInfo().getPrice()*item.getQuanatity());
    }
    return result;
}

ProductItemList ProductAddedItemList::getProductItems() const
{
    ProductItemList result;
    for (int i=0;i<count();i++) {
        ProductAddedItem item = at(i);
        result.append(item.getProduct());
    }
    return result;
}
