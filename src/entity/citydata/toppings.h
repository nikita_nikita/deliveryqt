#ifndef TOPPINGS_H
#define TOPPINGS_H

#include "product.h"
class Toppings
{
public:
    Toppings();
    ProductList getJap() const;
    void setJap(const ProductList &value);

    ProductList getPizza() const;
    void setPizza(const ProductList &value);
    void setData(QJsonObject object);


private:
    ProductList jap;
    ProductList pizza;

};

#endif // TOPPINGS_H
