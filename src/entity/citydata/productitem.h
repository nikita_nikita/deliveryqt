#ifndef PRODUCTITEM_H
#define PRODUCTITEM_H


#include <QStringList>
#include <QJsonObject>
#include <QJsonArray>
#include <QList>
#include "sityinfo.h"
#include "producttype.h"


class Images:public QStringList {
public:
    Images();
    Images(QJsonArray &json);
    void setData(QJsonArray &json);
    QList<QString> getFileNames() const;

};

class ProductItem
{
public:
    ProductItem();
    ProductItem(QJsonObject json);
    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getWeight() const;
    void setWeight(const QString &value);

    bool getNew() const;
    void setNew(bool value);

    bool getHot() const;
    void setHot(bool value);

    bool getHit() const;
    void setHit(bool value);

    int getCaloric() const;
    void setCaloric(const int &value);

    double getVolume() const;
    void setVolume(const double &value);

    QString getComposition() const;
    void setComposition(const QString &value);

    QString getNumber() const;
    void setNumber(const QString &value);

    QString getVendor1() const;
    void setVendor1(const QString &value);

    QString getVendor2() const;
    void setVendor2(const QString &value);

    ProductType getType() const;
    void setType(const ProductType &value);

    Images getImages() const;
    void setImages(const Images &value);

    bool getNotDeliverySeparately() const;
    void setNotDeliverySeparately(bool value);

    void setData(const QJsonObject &data);
    QJsonObject toJson();

    bool operator == (ProductItem product);
     QList<QString> getFileNames() const;


     SityInfo getSityInfo() const;
     void setSityInfo(const SityInfo &value);

    bool IsPizza();
   bool IsSet();


protected:
    SityInfo sityInfo;
    int id;
    QString name;
    QString weight;
    bool lnew;
    bool hot;
    bool hit;
    int caloric;
    double volume;
    QString composition;
    QString number;
    QString vendor1;
    QString vendor2;
    ProductType m_type;
    Images images;
    bool notDeliverySeparately;
    QJsonObject m_data;    
};

class ProductItemList: public QList<ProductItem>
{
public:
    void setData(QJsonArray json);
    QList<QString> getFileNames() const;
private:
};
#endif // PRODUCTITEM_H
