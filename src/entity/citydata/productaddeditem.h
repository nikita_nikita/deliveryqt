#ifndef PRODUCTADDEDITEM_H
#define PRODUCTADDEDITEM_H

#include <QString>
#include <QJsonObject>
#include "productitem.h"
class ProductAddedItem
{
public:
    ProductAddedItem();
    ProductAddedItem(QJsonObject obj);

    QString getType() const;
    void setType(const QString &value);

    int getSelected() const;
    void setSelected(int value);

    ProductItem getProduct() const;
    void setProduct(const ProductItem &value);
    void setData(QJsonObject &obj);

    bool operator == (ProductAddedItem product);
     QList<QString> getFileNames() const;

     int getQuanatity() const;
     void setQuanatity(int value);

private:
     ProductItem product;
     QString type;
     int selected;
    int quanatity = 1;
};
class ProductAddedItemList:public QList<ProductAddedItem>
{
public:
  void  setData(QJsonArray &json);
   QList<QString> getFileNames() const;
   double getTotalPrice();
   ProductItemList getProductItems() const;
};
#endif // PRODUCTADDEDITEM_H
