#ifndef WORKDAY_H
#define WORKDAY_H

#include <QString>
#include <QJsonObject>
class WorkDay
{
public:
    WorkDay();


    QString getStart() const;
    void setStart(const QString &value);

    QString getEnd() const;
    void setEnd(const QString &value);

    void setJson(QJsonObject json);


    QString getDayweek() const;
    void setDayweek(const QString &value);

private:
    QString dayweek;
    QString start;
    QString end;

};

#endif // WORKDAY_H
