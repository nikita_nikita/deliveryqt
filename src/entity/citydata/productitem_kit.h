#ifndef PRODUCTITEM_KIT_H
#define PRODUCTITEM_KIT_H
#include "productitem.h"
#include <QList>
class ProductItem_kit
{
public:
    ProductItem_kit();
    ProductItem_kit(QJsonObject jObj);

    QString getName() const;
    void setName(const QString &value);

    int getId() const;
    void setId(int value);

    QString getVendor() const;
    void setVendor(const QString &value);

    QString getImage() const;
    void setImage(const QString &value);

    int getMax_quantity() const;
    void setMax_quantity(int value);

private:
    QString name;
    int id;
    QString vendor;
    QString image;
    int max_quantity;



};

class ProductItem_kit_list: public QList<ProductItem_kit>
{
public:
    ProductItem_kit_list();
    ProductItem_kit_list(QJsonArray jArr);

};
#endif // PRODUCTITEM_KIT_H
