#include "toppings_kit.h"


Toppings_kit::Toppings_kit()
{

}

Toppings_kit::Toppings_kit(QJsonObject jObj)
{

    this->setTo(jObj.value("to").toString().toInt());
    this->setFrom(jObj.value("from").toString().toInt());
    QJsonArray productArray = jObj.value("data").toArray();
    for(int i =0; i < productArray.count(); i++){
        auto data = productArray.at(i).toObject();
        auto id = data.value("product").toObject().value("id").toInt();
        auto quantity = data.value("quantity").toInt();
        id_quantity[id] = quantity;
    }

}


int Toppings_kit::getFrom() const
{
    return from;
}

void Toppings_kit::setFrom(int value)
{
    from = value;
}

int Toppings_kit::getTo() const
{
    return to;
}

void Toppings_kit::setTo(int value)
{
    to = value;
}

QHash<int, int> Toppings_kit::getId_quantity() const
{
    return id_quantity;
}

void Toppings_kit::setId_quantity(const QHash<int, int> &value)
{
    id_quantity = value;
}

Toppings_kit_list::Toppings_kit_list()
{

}

Toppings_kit_list::Toppings_kit_list(QJsonArray jArr)
{

    for(int i = 0; i < jArr.count(); i++){
        this->append(Toppings_kit(jArr.at(i).toObject()));
    }


}

QHash<int, int> Toppings_kit_list::getQuantitesByPrice(int value)
{
    for(int i =0; i < this->count();i++){
        if(value<this->at(i).getTo()){
            return this->at(i).getId_quantity();
        }
    }
    return this->last().getId_quantity();
}
