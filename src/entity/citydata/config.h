#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
namespace config {
class Gallery {
public:
    Gallery();
    Gallery(QJsonObject &json);
    QString getUrl() const;
    void setUrl(const QString &value);
    void setData(QJsonObject &json);

private:
    QString url;
};

class Config
{
public:
    Config();
    Gallery getGallery() const;
    void setGallery(const Gallery &value);
    void setData(QJsonObject &json);

    QJsonArray getCityIdStatusVisibleList() const;
    void setCityIdStatusVisibleList(const QJsonArray &value);

    QJsonArray getCityIdFeedbackVisibleList() const;
    void setCityIdFeedbackVisibleList(const QJsonArray &value);

private:
    Gallery gallery;
    QJsonArray cityIdStatusVisibleList;
    QJsonArray cityIdFeedbackVisibleList;
};
}

#endif // CONFIG_H
