#ifndef TOPPING_H
#define TOPPING_H

#include <QString>
#include <QJsonObject>
class Topping
{
public:
    Topping();
    Topping(QJsonObject json);

    QString getImage() const;
    void setImage(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getVendor() const;
    void setVendor(const QString &value);

    int getQuantity() const;
    void setQuantity(int value);

    int getId() const;
    void setId(int value);

    int getCount() const;
    void setCount(int value);

private:
private:
    QString image;
    QString name;
    QString vendor;
    int quantity;
    int id;
    int count;
};

#endif // TOPPING_H
