#ifndef ORGANIZATIONINFO_H
#define ORGANIZATIONINFO_H

#include <QString>
#include <QJsonObject>
class OrganizationInfo
{
public:
    OrganizationInfo();
    OrganizationInfo(QJsonObject jObj);
    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getPhone() const;
    void setPhone(const QString &value);

    QString getPhone1() const;
    void setPhone1(const QString &value);

    QString getPhone2() const;
    void setPhone2(const QString &value);

    QString getUsp() const;
    void setUsp(const QString &value);

    int getDelivery() const;
    void setDelivery(int value);

    int getPickup() const;
    void setPickup(int value);

    QString getEmail() const;
    void setEmail(const QString &value);

    QString getPhoneMask() const;
    void setPhoneMask(const QString &value);

    QString getVk() const;
    void setVk(const QString &value);

    QString getInstagram() const;
    void setInstagram(const QString &value);
    void setData(QJsonObject json);

    bool getCheckStreet() const;
    void setCheckStreet(bool value);

    int getCardPayDelivery() const;
    void setCardPayDelivery(int value);


    bool getToppingsEnable() const;
    void setToppingsEnable(bool value);

    bool getContactless_delivery() const;
    void setContactless_delivery(bool value);

    bool getCallbackEnagle() const;
    void setCallbackEnagle(bool value);

    bool getOrder_status() const;
    void setOrder_status(bool value);

private:
    int id;
    QString name;
    QString phone;
    QString phone1;
    QString phone2;
    QString usp;
    int delivery;
    int pickup;
    QString email;
    QString phoneMask;
    QString vk;
    QString instagram;
    bool checkStreet;
    int cardPayDelivery;
    bool toppingsEnable;
    bool contactless_delivery;
    bool callbackEnagle = false;
    bool order_status = false;

};

#endif // ORGANIZATIONINFO_H
