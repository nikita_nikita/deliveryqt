#include "productitem_kit.h"
ProductItem_kit::ProductItem_kit()
{

}

ProductItem_kit::ProductItem_kit(QJsonObject jObj)
{
    QJsonObject product =jObj.value("product").toObject();
    setId(product.value("id").toInt());
    setName(product.value("name").toString());
    setImage(product.value("id").toString());
    setVendor(product.value("id").toString());
    setMax_quantity(jObj.value("quantity").toString().toInt());
}

QString ProductItem_kit::getName() const
{
    return name;
}

void ProductItem_kit::setName(const QString &value)
{
    name = value;
}

int ProductItem_kit::getId() const
{
    return id;
}

void ProductItem_kit::setId(int value)
{
    id = value;
}

QString ProductItem_kit::getVendor() const
{
    return vendor;
}

void ProductItem_kit::setVendor(const QString &value)
{
    vendor = value;
}

QString ProductItem_kit::getImage() const
{
    return image;
}

void ProductItem_kit::setImage(const QString &value)
{
    image = value;
}

int ProductItem_kit::getMax_quantity() const
{
    return max_quantity;
}

void ProductItem_kit::setMax_quantity(int value)
{
    max_quantity = value;
}

ProductItem_kit_list::ProductItem_kit_list()
{

}

ProductItem_kit_list::ProductItem_kit_list(QJsonArray jArr)
{
    for(int i =0; i < jArr.count();i++){
        this->append(jArr.at(i).toObject());
    }
}
