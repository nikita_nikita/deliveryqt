#include "workdays.h"

WorkDays::WorkDays()
{

}

void WorkDays::setJson(QJsonObject json)
{
    this->clear();
        QJsonObject::iterator it;
        for (it=json.begin(); it!=json.end(); it++){
                 WorkDay day;
                 day.setDayweek(it.key());
                 day.setJson(it.value().toObject());
                 this->append(day);
        }
}

WorkDay WorkDays::GetByName(QString name)
{
    for(int i=0; i<count(); i++) {
        WorkDay day = this->at(i);
        if(day.getDayweek()==name){
            return day;
        }
    }
    return WorkDay();
}
