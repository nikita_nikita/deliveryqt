#ifndef TOPPINGS_KIT_H
#define TOPPINGS_KIT_H


#include <QJsonObject>
#include <QJsonArray>
#include<QHash>
class Toppings_kit
{
public:
    Toppings_kit();
    Toppings_kit(QJsonObject jObj);


    int getFrom() const;
    void setFrom(int value);

    int getTo() const;
    void setTo(int value);



    QHash<int, int> getId_quantity() const;
    void setId_quantity(const QHash<int, int> &value);

private:

    int from;
    int to;
    QHash<int,int> id_quantity;
};

class Toppings_kit_list: public QList<Toppings_kit>
{
public:
Toppings_kit_list();
Toppings_kit_list(QJsonArray jArr);
QHash<int,int> getQuantitesByPrice(int value);
};

#endif // TOPPINGS_KIT_H
