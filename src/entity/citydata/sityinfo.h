#ifndef SITYINFO_H
#define SITYINFO_H

#include <QJsonObject>

class SityInfo
{
public:
    SityInfo();
     SityInfo(QJsonObject &json);
    int getId() const;
    void setId(int value);

    double getPrice() const;
    void setPrice(double value);

    double getProfit() const;
    void setProfit(double value);

    void setData(QJsonObject &json);

private:
    int id;
    double price;
    double profit;
};

#endif // SITYINFO_H
