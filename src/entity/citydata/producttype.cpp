#include "producttype.h"

ProductType::ProductType()
{

}

int ProductType::getId() const
{
    return id;
}

void ProductType::setId(int value)
{
    id = value;
}

QString ProductType::getName() const
{
    return name;
}

void ProductType::setName(const QString &value)
{
    name = value;
}

void ProductType::setData(QJsonObject json)
{
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
}

bool ProductType::isPizza()
{
    return this->name=="Пицца";
}

bool ProductType::isSet()
{
    return this->name=="Сеты";
}
