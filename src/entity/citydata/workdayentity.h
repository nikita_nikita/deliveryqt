#ifndef WORKDAYENTITY_H
#define WORKDAYENTITY_H

#include "workday.h"
#include <QTime>

class WorkDayEntity
{
public:
    WorkDayEntity();
    QString getName() const;
    void setName(const QString &value);

    WorkDay getPickup() const;
    void setPickup(const WorkDay &value);

    WorkDay getDelivery() const;
    void setDelivery(const WorkDay &value);
    QTime getTimeOpen();
    QTime getTimeClose();

    QTime getTimeCloseDelivery();
    QTime getTimeClosePickup();

private:
    QString name;
    WorkDay pickup;
    WorkDay delivery;
};

#endif // WORKDAYENTITY_H
