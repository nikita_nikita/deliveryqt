#include "config.h"
using namespace config;
Config::Config()
{

}

Gallery Config::getGallery() const
{
    return gallery;
}

void Config::setGallery(const Gallery &value)
{
    gallery = value;
}

void Config::setData(QJsonObject &json)
{
    QJsonObject gallery = json["gallery"].toObject();
    this->setCityIdStatusVisibleList(json.value("orderStatusVisible").toArray());
    this->setCityIdFeedbackVisibleList(json.value("orderCallbackVisible").toArray());
   this->setGallery(Gallery(gallery));
}

QJsonArray Config::getCityIdStatusVisibleList() const
{
    return cityIdStatusVisibleList;
}

void Config::setCityIdStatusVisibleList(const QJsonArray &value)
{
    cityIdStatusVisibleList = value;
}

QJsonArray Config::getCityIdFeedbackVisibleList() const
{
    return cityIdFeedbackVisibleList;
}

void Config::setCityIdFeedbackVisibleList(const QJsonArray &value)
{
    cityIdFeedbackVisibleList = value;
}

Gallery::Gallery()
{

}

Gallery::Gallery(QJsonObject &json)
{
    this->setData(json);
}

QString Gallery::getUrl() const
{
    return url;
}

void Gallery::setUrl(const QString &value)
{
    url = value;
}

void Gallery::setData(QJsonObject &json)
{
    this->setUrl(json["url"].toString());
}
