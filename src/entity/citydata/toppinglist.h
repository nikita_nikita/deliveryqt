#ifndef TOPPINGLIST_H
#define TOPPINGLIST_H

#include "topping.h"
#include <QList>
#include <QJsonArray>
class ToppingList : public QList<Topping>
{
public:
    ToppingList();
    ToppingList(QJsonArray jArr);
    QJsonArray toJson();
};

#endif // TOPPINGLIST_H
