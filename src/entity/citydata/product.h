#ifndef PRODUCT_H
#define PRODUCT_H

#include <QList>
#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>
#include "productitem.h"
#include "productaddeditem.h"


class ProductAdded {
public:

    ProductAddedItem getMeat() const;
    void setMeat(const ProductAddedItem &value);

    ProductAddedItem getVegetable() const;
    void setVegetable(const ProductAddedItem &value);

    ProductAddedItemList getToping() const;
    void setToping(const ProductAddedItemList &value);

    ProductAddedItemList getNoodle() const;
    void setNoodle(const ProductAddedItemList &value);

    ProductAddedItemList getComposite() const;
    void setComposite(const ProductAddedItemList &value);

    void setData(QJsonObject &json);
     bool operator == (ProductAdded product);
     bool operator != (ProductAdded product);
     QList<QString> getFileNames() const;
private:
    ProductAddedItem meat;
    ProductAddedItem vegetable;
    ProductAddedItemList toping;
    ProductAddedItemList noodle;
    ProductAddedItemList composite;
};

class Product:public ProductItem
{
public:
    Product();
    Product(QJsonObject &json);

    ProductAdded getAdded() const;
    void setAdded(QJsonObject &json);

    QList<Product> toList(QJsonArray &json) const;

    void setData(QJsonObject &json);
    QJsonObject toJson() const;

    QList<QString> getFileNames() const;

    double getPrice() const;

    double getProfit() const;

    double getBenefit();

    bool operator == (Product product);

private:    
    ProductAdded added;



};
class ProductList:public QList<Product>{
public:
    int indexOfId(int id);
    ProductList();
    ProductList(QJsonArray &json);
    void setData(QJsonArray &json);
    QList<QString> getFileNames() const;
    Product getById(int id);
};

#endif // PRODUCT_H
