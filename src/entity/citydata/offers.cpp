#include "offers.h"

Offers::Offers()
{

}

Offers::Offers(QJsonObject &json)
{
    this->setData(json);
}

int Offers::getId() const
{
    return id;
}

void Offers::setId(int value)
{
    id = value;
}

int Offers::getSortId() const
{
    return sortId;
}

void Offers::setSortId(int value)
{
    sortId = value;
}

QString Offers::getName() const
{
    return name;
}

void Offers::setName(const QString &value)
{
    name = value;
}

QString Offers::getImageBanner() const
{
    return imageBanner;
}

void Offers::setImageBanner(const QString &value)
{
    imageBanner = value;
}

QString Offers::getImageCatalog() const
{
    return imageCatalog;
}

void Offers::setImageCatalog(const QString &value)
{
    imageCatalog = value;
}

bool Offers::getVisibleBanner() const
{
    return visibleBanner;
}

void Offers::setVisibleBanner(bool value)
{
    visibleBanner = value;
}

bool Offers::getVisibleCatalog() const
{
    return visibleCatalog;
}

void Offers::setVisibleCatalog(bool value)
{
    visibleCatalog = value;
}

bool Offers::getVisibleButton() const
{
    return visibleButton;
}

void Offers::setVisibleButton(bool value)
{
    visibleButton = value;
}

QString Offers::getStartDate() const
{
    return startDate;
}

void Offers::setStartDate(const QString &value)
{
    startDate = value;
}

QString Offers::getButtonTitle() const
{
    return buttonTitle;
}

void Offers::setButtonTitle(const QString &value)
{
    buttonTitle = value;
}

QString Offers::getEndDate() const
{
    return endDate;
}

void Offers::setEndDate(const QString &value)
{
    endDate = value;
}

void Offers::setData(QJsonObject &json)
{
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
    this->setSortId(json["sortId"].toInt());
    this->setImageBanner(json["imageBanner"].toString());
    this->setImageCatalog(json["imageCatalog"].toString());
    this->setVisibleBanner(json["visibleBanner"].toBool());
    this->setVisibleCatalog(json["visibleCatalog"].toBool());
    this->setVisibleButton(json["visibleButton"].toBool());
    this->setDescription(json["description"].toString());
    this->setStartDate(json["startDate"].toString());
    this->setEndDate(json["endDate"].toString());
    this->setButtonTitle(json["buttonTitle"].toString());
    this->setSity(json["sity"].toInt());
    this->setPromocode(json["promocode"].toString());
}

QList<QString> Offers::getFileNames() const
{
    QList<QString> result;
    /*if(this->imageBanner.count()>0) {
        result.append(this->imageBanner);
    }*/
    if(this->imageCatalog.count()>0) {
        result.append(this->imageCatalog);
    }
    return result;
}

QString Offers::getDescription() const
{
    return description;
}

void Offers::setDescription(const QString &value)
{
    description = value;
}

QString Offers::getPromocode() const
{
    return promocode;
}

void Offers::setPromocode(const QString &value)
{
    promocode = value;
}

int Offers::getSity() const
{
    return sity;
}

void Offers::setSity(int value)
{
    sity = value;
}

OffersList::OffersList()
{

}

void OffersList::setData(QJsonArray &json)
{
    this->clear();
    for(int i=0; i<json.count(); i++){
        QJsonObject obj = json.at(i).toObject();
        this->append(Offers(obj));
    }
}

QList<QString> OffersList::getFileNames() const
{
    QList<QString> result;
    for (int i=0;i<this->count();i++) {
        result.append(this->at(i).getFileNames());
    }
    return result;
}

OffersList OffersList::getOffersCatalog() const
{
    OffersList result;
    for (int i=0; i< count();i++) {
        Offers offers = at(i);
        if(offers.getVisibleCatalog()){
            result.append(offers);
        }
    }
    return result;
}

OffersList OffersList::getOffersBaner() const
{
    OffersList result;
    for (int i=0; i< count();i++) {
        Offers offers = at(i);
        if(offers.getVisibleBanner()&&offers.getImageCatalog().length()>0){
            result.append(offers);
        }
    }
    return result;
}
