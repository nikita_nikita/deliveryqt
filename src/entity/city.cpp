#include "city.h"

City::City()
{

}

City::City(QJsonObject &json)
{
  this->setData(json);
}

int City::getId() const
{
    return id;
}

void City::setId(int value)
{
    id = value;
}

Domain City::getDomain() const
{
    return domain;
}

void City::setDomain(const Domain &value)
{
    domain = value;
}

QString City::getName() const
{
    return name;
}

void City::setName(const QString &value)
{
    name = value;
}

QString City::getCode() const
{
    return code;
}

void City::setCode(const QString &value)
{
    code = value;
}

int City::getEnable() const
{
    return enable;
}

void City::setEnable(int value)
{
    enable = value;
}

bool City::getCheckStreet() const
{
    return checkStreet;
}

void City::setCheckStreet(bool value)
{
    checkStreet = value;
}

int City::getHrefVisible() const
{
    return hrefVisible;
}

void City::setHrefVisible(int value)
{
    hrefVisible = value;
}

int City::getSortId() const
{
    return sortId;
}

void City::setSortId(int value)
{
    sortId = value;
}

QJsonObject City::toJson() const
{
    return QJsonObject {
     {"id", this->getId()},
        {"domain", this->getDomain().toJson()},
        {"name", this->getName()},
        {"code", this->getCode()},
        {"enable",this->getEnable()},
        {"checkStreet", this->getCheckStreet()},
        {"hrefVisible", this->getHrefVisible()},
        {"sortId", this->getSortId()}
    };
}

void City::setData(QJsonObject &json)
{

    this->setId(json["id"].toInt());
    this->domain.setData(json["domain"].toObject());
    this->setName(json["name"].toString());
    this->setCode(json["code"].toString());
    this->setEnable(json["enable"].toInt());
    this->setCheckStreet(json["checkStreet"].toBool());
    this->setHrefVisible(json["hrefVisible"].toInt());
    this->setSortId(json["sortId"].toInt());
}

City City::fromJson(QJsonObject json)
{
    return City(json);
}

