#ifndef STREET_H
#define STREET_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QList>
class Street
{
public:
    Street();
    Street(QJsonObject json);
    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getType() const;
    void setType(const QString &value);

    QString getTypeShort() const;
    void setTypeShort(const QString &value);

    QString getCode() const;
    void setCode(const QString &value);

    int getStatus() const;
    void setStatus(int value);

    int getSity() const;
    void setSity(int value);
    void setData(QJsonObject json);
    QJsonObject toJson() const;

private:
    int id=0;
    QString name;
    QString type;
    QString typeShort;
    QString code;
    int status=0;
    int sity=0;
};
class StreetList:public QList<Street> {
public:
    StreetList();
    StreetList(QJsonArray json);
    void setData(QJsonArray json);
    Street findById(int id) const;
    QJsonArray toJson() const;
private:
};
#endif // STREET_H
