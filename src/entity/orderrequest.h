#ifndef ORDERREQUEST_H
#define ORDERREQUEST_H
#include "order.h"
#include "orderinfo.h"
#include "ordeaddress.h"

class OrderRequest
{
public:
    OrderRequest();
    Order getOrder() const;
    void setOrder(const Order &value);

    int getCityId() const;
    void setCityId(int value);

    OrderInfo getOrderInfo() const;
    void setOrderInfo(const OrderInfo &value);

    OrdeAddress getAddress() const;
    void setAddress(const OrdeAddress &value);

    QJsonObject toJson() const;


    QString getPlatform() const;
    void setPlatform(const QString &value);

    QString getToken() const;
    void setToken(const QString &value);

    QString getMac() const;
    void setMac(const QString &value);

    QString getTransactionMail() const;
    void setTransactionMail(const QString &value);

    bool getPromocodeValid() const;
    void setPromocodeValid(bool value);


    QJsonArray getClient_toppings() const;
    void setClient_toppings(const QJsonArray &value);

private:
    Order order;
    int cityId;
    bool promocodeValid;
    OrderInfo orderInfo;
    OrdeAddress address;
    QString platform;
    QString token;
    QString mac;
    QString transaction_mail;
    QJsonArray client_toppings = QJsonArray();
};

#endif // ORDERREQUEST_H
