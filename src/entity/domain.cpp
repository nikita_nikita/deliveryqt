#include "domain.h"

Domain::Domain()
{

}

QString Domain::getProtocol() const
{
    return protocol;
}

void Domain::setProtocol(const QString &value)
{
    protocol = value;
}

int Domain::getId() const
{
    return id;
}

void Domain::setId(int value)
{
    id = value;
}

QString Domain::getPath() const
{
    return path;
}

void Domain::setPath(const QString &value)
{
    path = value;
}

QString Domain::getCode() const
{
    return code;
}

void Domain::setCode(const QString &value)
{
    code = value;
}

QString Domain::getCountryName()
{
    if(this->getCode()=="BY"){
        return "Беларусь";
    }
    return "Россия";
}

QJsonObject Domain::toJson() const
{
    return QJsonObject {
        {"id", this->getId()},
        {"protocol", this->getProtocol()},
        {"path", this->getPath()},
        {"code", this->getCode()}
    };
}

void Domain::setData(QJsonObject json)
{
    this->setId(json["id"].toInt());
    this->setProtocol(json["protocol"].toString());
    this->setPath(json["path"].toString());
    this->setCode(json["code"].toString());
}
