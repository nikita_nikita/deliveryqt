#ifndef ORDERINFO_H
#define ORDERINFO_H

#include <QString>
#include <QJsonObject>
#include <QDateTime>

class OrderInfo
{
public:
    // оплата 1)онлайн 2)карта 3)наличка
    enum PayMethod{
        PayMethodOnline =1,
        PayMethodCard=2,
        PayMethodCash=3,
        PayMethodAndroidPay=4,
        PayMethodApplePay=5,
        PayMethodCashier = 6

    };
    OrderInfo();
    QString getName() const;
    void setName(const QString &value);

    QString getPhone() const;
    void setPhone(const QString &value);

    QString getOrderType() const;
    void setOrderType(const QString &value);

    int getTakeAwayPoint() const;
    void setTakeAwayPoint(int value);

    QString getComment() const;
    void setComment(const QString &value);

    QString getOrderTime() const;
    void setOrderTime(const QString &value);

    QString getTime() const;
    void setTime(const QString &value);

    int getPerson() const;
    void setPerson(int value);

    int getPayMethod() const;
    void setPayMethod(int value);

    double getCash() const;
    void setCash(double value);

    bool getNoChange() const;
    void setNoChange(bool value);

    QString getPromocode() const;
    void setPromocode(const QString &value);


    QJsonObject toJson() const;

    QString getEmail() const;
    void setEmail(const QString &value);

    double getBonusPay() const;
    void setBonusPay(double value);

    QJsonObject getPaidDelivery() const;
    void setPaidDelivery(QJsonObject value);

    bool getContactless_delivery() const;
    void setContactless_delivery(bool value);

private:
    QString name;
    QString phone;
    QString orderType;
    QString email;
    int takeAwayPoint;
    QString comment;
    QString orderTime;
    QString time;
    int person;
    int payMethod;
    double cash;
    bool noChange;
    QString promocode;
    double bonusPay;
    QJsonObject paidDelivery;
    bool contactless_delivery;


};

#endif // ORDERINFO_H
