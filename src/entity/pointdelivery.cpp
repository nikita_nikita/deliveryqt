#include "pointdelivery.h"
#include <QJsonObject>
PointDelivery::PointDelivery()
{

}

PointDelivery::PointDelivery(QJsonObject &json)
{
    this->setData(json);
}

int PointDelivery::getId() const
{
    return id;
}

void PointDelivery::setId(int value)
{
    id = value;
}

QString PointDelivery::getName() const
{
    return name;
}

void PointDelivery::setName(const QString &value)
{
    name = value;
}

QString PointDelivery::getAddress() const
{
    return address;
}

void PointDelivery::setAddress(const QString &value)
{
    address = value;
}

QString PointDelivery::getPhone() const
{
    return phone;
}

void PointDelivery::setPhone(const QString &value)
{
    phone = value;
}

QJsonArray PointDelivery::getCoordinates() const
{
    return coordinates;
}

void PointDelivery::setCoordinates(const QJsonArray &value)
{
    coordinates = value;
}

int PointDelivery::getEnable() const
{
    return enable;
}

void PointDelivery::setEnable(int value)
{
    enable = value;
}

void PointDelivery::setData(QJsonObject &json)
{
    this->setId(json["id"].toInt());
    this->setName(json["name"].toString());
    this->setAddress(json["address"].toString());
    this->setPhone(json["phone"].toString());
    this->setCoordinates(json["coordinates"].toArray());
    this->setEnable(json["enable"].toInt());
}

PointDeliveryList::PointDeliveryList(QJsonArray &json)
{
    this->setData(json);
}

void PointDeliveryList::setData(QJsonArray &json)
{
    this->clear();
    for (int i =0; i<json.count(); i++) {
        QJsonObject obj = json.at(i).toObject();
        this->append(PointDelivery(obj));
    }
}
