#include "orderinfo.h"
#include <QTime>

OrderInfo::OrderInfo()
{

}

QString OrderInfo::getName() const
{
    return name;
}

void OrderInfo::setName(const QString &value)
{
    name = value;
}

QString OrderInfo::getPhone() const
{
    return phone;
}

void OrderInfo::setPhone(const QString &value)
{
    phone = value;
}

QString OrderInfo::getOrderType() const
{
    return orderType;
}

void OrderInfo::setOrderType(const QString &value)
{
    orderType = value;
}

int OrderInfo::getTakeAwayPoint() const
{
    return takeAwayPoint;
}

void OrderInfo::setTakeAwayPoint(int value)
{
    takeAwayPoint = value;
}

QString OrderInfo::getComment() const
{
    return comment;
}

void OrderInfo::setComment(const QString &value)
{
    comment = value;
}

QString OrderInfo::getOrderTime() const
{
    return orderTime;
}

void OrderInfo::setOrderTime(const QString &value)
{
    orderTime = value;
}

QString OrderInfo::getTime() const
{
    return time;
}

void OrderInfo::setTime(const QString &value)
{
    time = value;
}

int OrderInfo::getPerson() const
{
    return person;
}

void OrderInfo::setPerson(int value)
{
    person = value;
}

int OrderInfo::getPayMethod() const
{
    return payMethod;
}

void OrderInfo::setPayMethod(int value)
{
    payMethod = value;
}

double OrderInfo::getCash() const
{
    return cash;
}

void OrderInfo::setCash(double value)
{
    cash = value;
}

bool OrderInfo::getNoChange() const
{
    return noChange;
}

void OrderInfo::setNoChange(bool value)
{
    noChange = value;
}

QString OrderInfo::getPromocode() const
{
    return promocode;
}

void OrderInfo::setPromocode(const QString &value)
{
    promocode = value;
}

QJsonObject OrderInfo::toJson() const
{
    QJsonObject result;
     result["name"] = this->getName();
     result["phone"] = this->getPhone();
     result["orderType"] = this->getOrderType();
     result["takeAwayPoint"] = this->getTakeAwayPoint();
     result["comment"] = this->getComment();
     result["orderTime"] = this->getOrderTime();
     result["time"] = this->getTime();
     result["persons"] = this->getPerson();
     result["payMethod"] = this->getPayMethod();
     result["cash"] = this->getCash();
     result["noChange"] = this->getNoChange();
     result["promocode"] = this->getPromocode();
     result["email"] = this->getEmail();
     result["bonusPay"] = this->getBonusPay();
     result["paidDelivery"] = this->getPaidDelivery();
     if(this->getContactless_delivery()){
         result["contactless_delivery"] = 1;
     }
     else{
         result["contactless_delivery"] = 0;
     }
    return result;
}

QString OrderInfo::getEmail() const
{
    return email;
}

void OrderInfo::setEmail(const QString &value)
{
    email = value;
}

double OrderInfo::getBonusPay() const
{
    return bonusPay;
}

void OrderInfo::setBonusPay(double value)
{
    bonusPay = value;
}

QJsonObject OrderInfo::getPaidDelivery() const
{
    return paidDelivery;
}

void OrderInfo::setPaidDelivery(QJsonObject value)
{
    paidDelivery = value;
}

bool OrderInfo::getContactless_delivery() const
{
    return contactless_delivery;
}

void OrderInfo::setContactless_delivery(bool value)
{
    contactless_delivery = value;
}
