#include "street.h"

Street::Street()
{

}

Street::Street(QJsonObject json)
{
 this->setData(json);
}

int Street::getId() const
{
    return id;
}

void Street::setId(int value)
{
    id = value;
}

QString Street::getName() const
{
    return name;
}

void Street::setName(const QString &value)
{
    name = value;
}

QString Street::getType() const
{
    return type;
}

void Street::setType(const QString &value)
{
    type = value;
}

QString Street::getTypeShort() const
{
    return typeShort;
}

void Street::setTypeShort(const QString &value)
{
    typeShort = value;
}

QString Street::getCode() const
{
    return code;
}

void Street::setCode(const QString &value)
{
    code = value;
}

int Street::getStatus() const
{
    return status;
}

void Street::setStatus(int value)
{
    status = value;
}

int Street::getSity() const
{
    return sity;
}

void Street::setSity(int value)
{
    sity = value;
}

void Street::setData(QJsonObject json)
{
    this->id = json["id"].toInt();
    this->name = json["name"].toString();
    this->type = json["type"].toString();
    this->typeShort = json["typeShort"].toString();
    this->code = json["code"].toString();
    this->status = json["status"].toInt();
    this->sity = json["sity"].toInt();
}

QJsonObject Street::toJson() const
{
    QJsonObject result;
    result["id"] = this->id;
    result["name"] =  this->name;
    result["type"] = this->type;
    result["typeShort"] = this->typeShort;
    result["code"]= this->code;
    result["status"] = this->status;
    result["sity"] = this->sity;
    return result;
}

StreetList::StreetList()
{

}

StreetList::StreetList(QJsonArray json)
{
    this->setData(json);
}

void StreetList::setData(QJsonArray json)
{
    this->clear();
    for (int i =0; i<json.count();i++) {
        this->append(Street(json.at(i).toObject()));

    }
}

Street StreetList::findById(int id) const
{
    for(int i=0; i<this->count(); i++){
        Street  st = this->at(i);
        if(st.getId() == id) return st;
    }
    return Street();
}

QJsonArray StreetList::toJson() const
{
    QJsonArray result;
    for (int i=0; i<count();i++) {
        result.append(this->at(i).toJson());
    }
    return result;
}
