#include "versionapp.h"

VersionApp::VersionApp()
{

}

VersionApp::VersionApp(const QJsonObject &json)
{
        this->setData(json);
}

int VersionApp::getVersion() const
{
    return version;
}

void VersionApp::setVersion(int value)
{
    version = value;
}

QString VersionApp::getTag() const
{
    return tag;
}

void VersionApp::setTag(const QString &value)
{
    tag = value;
}

bool VersionApp::getRequest() const
{
    return request;
}

void VersionApp::setRequest(bool value)
{
    request = value;
}

void VersionApp::setData(const QJsonObject &json)
{
    if(platform == "android"){
        this->setVersion(json["android"].toInt());

    }
    if(platform == "ios"){
        this->setVersion(json["ios"].toInt());

    }
    this->setTag(json["tag"].toString());
    this->setRequest(json["request"].toBool());

}
