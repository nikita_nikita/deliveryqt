#include "ordeaddress.h"

OrdeAddress::OrdeAddress()
{

}

QString OrdeAddress::getStreet() const
{
    return street;
}

void OrdeAddress::setStreet(const QString &value)
{
    street = value;
}



int OrdeAddress::getEntrance() const
{
    return entrance;
}

void OrdeAddress::setEntrance(int value)
{
    entrance = value;
}

int OrdeAddress::getFloor() const
{
    return floor;
}

void OrdeAddress::setFloor(int value)
{
    floor = value;
}

int OrdeAddress::getRoom() const
{
    return room;
}

void OrdeAddress::setRoom(int value)
{
    room = value;
}

QString OrdeAddress::getBuilding() const
{
    return building;
}

void OrdeAddress::setBuilding(const QString &value)
{
    building = value;
}

QJsonObject OrdeAddress::toJson(bool entity) const
{
    QJsonObject result;
    if(entity){
       result["street"] = this->streetEntity.toJson();
    } else {
        result["street"] = this->getStreet();
    }
    result["building"] = this->getBuilding();
    result["entrance"] = this->getEntrance();
    result["floor"] = this->getFloor();
    result["room"] = this->getRoom();
    return result;
}

Street OrdeAddress::getStreetEntity() const
{
    return streetEntity;
}

void OrdeAddress::setStreetEntity(const Street &value)
{
    streetEntity = value;
}
