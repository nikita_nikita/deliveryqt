#ifndef MESSAGE_H
#define MESSAGE_H
#include <QJsonObject>

class Message
{
public:
    Message();
    Message(QJsonObject jObj);
    int getType() const;
    void setType(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);
    QString getErrorColor() const;
    QString getGoogColor() const;

private:
    void setData(QJsonObject jObj);
    int type;
    QString name;
    QString description;
    QString errorColor = "#FF6666";
    QString googColor = "#e1f1c3";

};

#endif // MESSAGE_H
