#include "citydata.h"

CityData::CityData()
{

}

CityData::CityData(const QJsonObject &value)
{
    this->setData(value);
}

QJsonObject CityData::getData() const
{
    return data;
}

void CityData::setData(const QJsonObject &value)
{
    data = value;
    QJsonArray cats = value["category"].toArray();
    this->categories.setData(cats);
    QJsonObject products = value["products"].toObject();
    this->setProduct(products);
    this->productCategory.setData(products);
    QJsonArray pro_r = value["product_recomended"].toArray();
    this->product_recomended.setData(pro_r);
    QJsonArray service_kit = value["service_kit"].toArray();
    this->service_kit.setData(service_kit);
    QJsonArray offers = value["offers"].toArray();
    this->offers.setData(offers);
    QJsonObject config = value["config"].toObject();
    this->config.setData(config);
    QJsonObject organInfo = value["organizaton_info"].toObject();
    this->organizationInfo.setData(organInfo);
    this->toppings.setData(value["toppings"].toObject());
    this->payments.setData(value["payments"].toObject());
    this->workTime.setJson(value["work_time"].toObject());
    this->setToppingKit(value["toppings_kit"].toArray());
    if(!value["warnings"].toBool()){
    this->setWarnings(value["warnings"].toArray());
    }
    }



void CityData::setProduct(QJsonObject &json)
{
    QStringList keys = json.keys();
    for (int i=0;i<keys.count();i++) {
        QString key = keys.at(i);
        QJsonArray jProducts = json[key].toArray();       
        this->products.append(ProductList(jProducts));
    }
}

ProductList CityData::getProduct_recomended() const
{
    return product_recomended;
}

void CityData::setProduct_recomended(const ProductList &value)
{
    product_recomended = value;
}

ServiceKitList CityData::getService_kit() const
{
    return service_kit;
}

void CityData::setService_kit(const ServiceKitList &value)
{
    service_kit = value;
}

QList<QString> CityData::getFileNames() const
{
    QList<QString> result;
    result.append(products.getFileNames());
    result.append(product_recomended.getFileNames());
    result.append(offers.getFileNames());
    result.append(service_kit.getFileNames());
    result.append(toppings.getJap().getFileNames());
    result.append(toppings.getPizza().getFileNames());
    return result;
}

ProductList CityData::getProducts() const
{
    return products;
}

void CityData::setProducts(const ProductList &value)
{
    products = value;
}

Config CityData::getConfig() const
{
    return config;
}

ProductCategoryList CityData::getProductCategory() const
{
    return productCategory;
}

CategoryList CityData::getCategories() const
{
    return categories;
}

OffersList CityData::getOffers() const
{
    return offers;
}

void CityData::setOffers(const OffersList &value)
{
    offers = value;
}

OrganizationInfo CityData::getOrganizationInfo() const
{
    return organizationInfo;
}

void CityData::setOrganizationInfo(const OrganizationInfo &value)
{
    organizationInfo = value;
}

Payments CityData::getPayments() const
{
    return payments;
}

void CityData::setPayments(const Payments &value)
{
    payments = value;
}

Toppings CityData::getToppings() const
{
    return toppings;
}

void CityData::setToppings(const Toppings &value)
{
    toppings = value;
}

WorkTimeEntity CityData::getWorkTime() const
{
    return workTime;
}

void CityData::setWorkTime(const WorkTimeEntity &value)
{
    workTime = value;
}

QJsonArray CityData::getToppingKit() const
{
    return toppingKit;
}

void CityData::setToppingKit(const QJsonArray &value)
{
    toppingKit = value;
}

QJsonArray CityData::getWarnings() const
{
    return warnings;
}

void CityData::setWarnings(const QJsonArray &value)
{
    warnings = value;
}



