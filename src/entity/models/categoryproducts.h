#ifndef CATEGORYPRODUCTS_H
#define CATEGORYPRODUCTS_H

#include "../citydata/product.h"
#include "../citydata/category.h"
class CategoryProducts
{
public:
    CategoryProducts();


    Category getCategory() const;
    void setCategory(const Category &value);

    ProductList getProducts() const;
    void setProducts(const ProductList &value);

private:
   ProductList products;
    Category category;

};

#endif // CATEGORYPRODUCTS_H
