#include "categoryproducts.h"

CategoryProducts::CategoryProducts()
{

}



Category CategoryProducts::getCategory() const
{
    return category;
}

void CategoryProducts::setCategory(const Category &value)
{
    category = value;
}

ProductList CategoryProducts::getProducts() const
{
    return products;
}

void CategoryProducts::setProducts(const ProductList &value)
{
    products = value;
}
