#include "pageitem.h"

PageItem::PageItem()
{

}

QList<CategoryProducts> PageItem::getProducts() const
{
    return products;
}

void PageItem::setProducts(const QList<CategoryProducts> &value)
{
    products = value;
}

QString PageItem::getParent() const
{
    return parent;
}

void PageItem::setParent(const QString &value)
{
    parent = value;
}
