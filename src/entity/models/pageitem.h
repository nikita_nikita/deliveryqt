#ifndef PAGEITEM_H
#define PAGEITEM_H

#include "categoryproducts.h"

class PageItem
{
public:
    PageItem();
    QList<CategoryProducts> getProducts() const;
    void setProducts(const QList<CategoryProducts> &value);

    QString getParent() const;
    void setParent(const QString &value);

private:
    QList<CategoryProducts> products;
    QString parent;
};

#endif // PAGEITEM_H
