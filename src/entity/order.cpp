#include "order.h"
Order::Order()
{

}

CartProductList Order::getProducts() const
{
    return products;
}

void Order::setProducts(const CartProductList &value)
{
    products = value;
}

void Order::appendProduct(const CartProductList &value)
{
    this->products.append(value);
}

double Order::getTotalPrice() const
{
    return totalPrice;
}

void Order::setTotalPrice(double value)
{
    totalPrice = value;
}

QJsonObject Order::toJson() const
{
    QJsonObject result;
    result["totalPrice"]= this->totalPrice;    
    result["products"] = this->products.toJson();
    return result;
}

