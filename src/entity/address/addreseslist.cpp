#include "addreseslist.h"

AddresesList::AddresesList()
{

}

AddresesList AddresesList::fromJson(QJsonArray json)
{
    AddresesList result;
    for(int i=0; i<json.count(); i++){
        result.append(AddressItem::fromJson(json.at(i).toObject()));
    }
    return result;
}

AddresesList AddresesList::filter(bool checkStreet)
{
    AddresesList result;
    for (int i=0; i<count(); i++) {
        AddressItem item= at(i);
        if(checkStreet){
            if(item.getStreetId()){
                result.append(item);
            }
        } else {
            result.append(item);
        }
    }
    return result;
}
