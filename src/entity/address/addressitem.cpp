#include "addressitem.h"

AddressItem::AddressItem()
{

}

QString AddressItem::getAddressName() const
{
    return addressName;
}

void AddressItem::setAddressName(const QString &value)
{
    addressName = value;
}

QString AddressItem::getBuilding() const
{
    return building;
}

void AddressItem::setBuilding(const QString &value)
{
    building = value;
}

int AddressItem::getEntrance() const
{
    return entrance;
}

void AddressItem::setEntrance(const int &value)
{
    entrance = value;
}

int AddressItem::getFloor() const
{
    return floor;
}

void AddressItem::setFloor(const int &value)
{
    floor = value;
}

int AddressItem::getId() const
{
    return id;
}

void AddressItem::setId(const int &value)
{
    id = value;
}

int AddressItem::getRoom() const
{
    return room;
}

void AddressItem::setRoom(const int &value)
{
    room = value;
}

QString AddressItem::getStreet() const
{
    return street;
}

void AddressItem::setStreet(const QString &value)
{
    street = value;
}

int AddressItem::getStreetId() const
{
    return streetId;
}

void AddressItem::setStreetId(const int &value)
{
    streetId = value;
}

QString AddressItem::getType() const
{
    return type;
}

void AddressItem::setType(const QString &value)
{
    type = value;
}

AddressItem AddressItem::fromJson(QJsonObject json)
{
    AddressItem result;
    result.setAddressName(json["addressName"].toString());
    result.setBuilding(json["building"].toString());
    result.setEntrance(json["entrance"].toInt());
    result.setFloor(json["floor"].toInt());
    result.setId(json["id"].toInt());
    result.setRoom(json["room"].toInt());
    result.setStreet(json["street"].toString());
    result.setStreetId(json["street_id"].toInt());
    result.setType(json["type"].toString());
    return result;
}
