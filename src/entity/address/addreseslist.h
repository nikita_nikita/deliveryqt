#ifndef ADDRESESLIST_H
#define ADDRESESLIST_H

#include "addressitem.h"
#include <QList>
#include <QJsonArray>
class AddresesList: public QList<AddressItem>
{
public:
    AddresesList();

    static AddresesList fromJson(QJsonArray json);
    AddresesList filter(bool checkStreet=false);
};

#endif // ADDRESESLIST_H
