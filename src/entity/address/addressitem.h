#ifndef ADDRESSITEM_H
#define ADDRESSITEM_H

#include <QString>
#include <QJsonObject>
class AddressItem
{
public:
    AddressItem();
    QString getAddressName() const;
    void setAddressName(const QString &value);

    QString getBuilding() const;
    void setBuilding(const QString &value);

    int getEntrance() const;
    void setEntrance(const int &value);

    int getFloor() const;
    void setFloor(const int &value);

    int getId() const;
    void setId(const int &value);

    int getRoom() const;
    void setRoom(const int &value);

    QString getStreet() const;
    void setStreet(const QString &value);

    int getStreetId() const;
    void setStreetId(const int &value);

    QString getType() const;
    void setType(const QString &value);


    static AddressItem fromJson(QJsonObject);

private:
    QString addressName;
    QString building;
    int entrance;
    int floor;
    int id;
    int room;
    QString street;
    int streetId;
    QString type;
};

#endif // ADDRESSITEM_H
