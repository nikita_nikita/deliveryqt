#ifndef CARTPRODUCTENTITY_H
#define CARTPRODUCTENTITY_H

//#include "product.h"
#include <QList>
#include "cart/cartoptions.h"
#include <QUuid>
class CartProductEntity
{
public:
    CartProductEntity();

    CartProductEntity(Product product);
    CartProductEntity(Product product, int quantity);
     CartProductEntity(Product product,CartOptions options,  int quantity);
    CartProductEntity(QJsonObject json);
    Product getProduct() const;
    int getQuantity() const;
    void setProduct(Product product);
    void setQuality(int quantity);
    CartOptions getOptions() const;
    void setOptions(const CartOptions &value);
    void setData(QJsonObject &json);
    QJsonObject toJson() const;

    double getOfferValue() const;
    void setOfferValue(double value);

    QString getId() const;
    void setId(const QString &value);

private:
    QString generateQuuidString();
    Product product;
    int quantity = 1;
    CartOptions options;
    double offerValue = 0;
    QString id = generateQuuidString();
};
class CartProductList: public QList<CartProductEntity> {
public:
    void setOfferValueByIikoOrderId(QString id,int offerValue);
    void replaceAtIndex(int index, CartProductEntity entity);
    CartProductList getNotSeparetlyDeliveryList();
    CartProductList(QJsonArray jArr);
    CartProductList();
//    CartProductList(QJsonArray jArr);
//    CartProductList();

    void reset();
    void add(Product product);
    void add(Product product, CartOptions options);
    void setQuantity(Product product , int quantity);
    int findByProduct(Product product, bool withoutOptions=false);
    QJsonArray toJson() const;
    void setData(QJsonArray array);
    bool isContainsPizza();
    int indexOfId(QString id);
    QMap<QString, double> getCostsByTypeId(); // возврощает суммы продуктов по типу

};
#endif // CARTPRODUCTENTITY_H
