#ifndef LOYALITYDATA_H
#define LOYALITYDATA_H
#include <QJsonObject>

class LoyalityData
{
public:
    LoyalityData();
    LoyalityData(QJsonObject data);
    int getType() const;
    void setType(int value);

    int getTarget() const;
    void setTarget(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    double getValue() const;
    void setValue(double value);

    QString getIiko_action_id() const;
    void setIiko_action_id(const QString &value);



    QString getIiko_Item_id() const;
    void setIiko_Item_id(const QString &value);

private:
    int type = 0;
    int target;
    QString name = "";
    QString description = "";
    double value = 0;
    QString iiko_action_id = "";
    QString iiko_Item_id = "";
    void setData(QJsonObject onject);
};

#endif // LOYALITYDATA_H
