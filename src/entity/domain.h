#ifndef DOMAIN_H
#define DOMAIN_H

#include <QString>
#include <QJsonObject>
class Domain
{
public:
    Domain();
    QString getProtocol() const;
    void setProtocol(const QString &value);

    int getId() const;
    void setId(int value);

    QString getPath() const;
    void setPath(const QString &value);

    QString getCode() const;
    void setCode(const QString &value);

    QString getCountryName();

    QJsonObject toJson() const;
    void setData(QJsonObject json);
private:
    int id;
    QString protocol;
    QString path;
    QString code;
};

#endif // DOMAIN_H
