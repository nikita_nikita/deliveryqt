#ifndef ORDERINHYSTORY_H
#define ORDERINHYSTORY_H

#include <QDateTime>
#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QRegExp>
class IikoOrder
{
public:
    IikoOrder();
    IikoOrder(QJsonObject obj);
    int getAmount() const;
    void setAmount(int value);

    QString getName() const;
    void setName(const QString &value);

    QString getCode() const;
    void setCode(QString value);

    int getSum() const;
    void setSum(int value);

    bool getCanLeaveFeedback() const;
    void setCanLeaveFeedback(bool value);

    QDateTime getDateTime() const;
    void setDateTime(const QDateTime &value);

    int getId() const;
    void setId(int value);

    QJsonArray getProducts() const;
    void setProducts(const QJsonArray &value);

    QString getStatus() const;
    void setStatus(const QString &value);

    QString getPoint() const;
    void setPoint(const QString &value);

    bool getDelivery() const;
    void setDelivery(bool value);

private:
    int Id;
    QString status ="Закрыта";
    QDateTime dateTime;
    bool canLeaveFeedback;
    int sum;
    QString code;
    QString name;
    int amount;
    QJsonArray products;
    bool delivery;
    QString point;
    void setData(QJsonObject jObj);


};

#endif // ORDERINHYSTORY_H
