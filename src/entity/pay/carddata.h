#ifndef CARDDATA_H
#define CARDDATA_H

#include <QString>

class CardData
{
public:
    CardData();
    QString getCardNumber() const;
    void setCardNumber(const QString &value);

    QString getCardDate() const;
    void setCardDate(const QString &value);

    QString getCardCVC() const;
    void setCardCVC(const QString &value);

private:
    QString cardNumber;
    QString cardDate;
    QString cardCVC;
};

#endif // CARDDATA_H
