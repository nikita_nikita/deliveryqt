#include "cpresponsemodel.h"

CPResponseModel::CPResponseModel()
{

}

QString CPResponseModel::getTransactionId() const
{
    return TransactionId;
}

void CPResponseModel::setTransactionId(QString value)
{
    TransactionId = value;
}

QString CPResponseModel::getPaReq() const
{
    return PaReq;
}

void CPResponseModel::setPaReq(const QString &value)
{
    PaReq = value;
}

QString CPResponseModel::getAcsUrl() const
{
    return AcsUrl;
}

void CPResponseModel::setAcsUrl(const QString &value)
{
    AcsUrl = value;
}

double CPResponseModel::getAmount() const
{
    return Amount;
}

void CPResponseModel::setAmount(double value)
{
    Amount = value;
}

QString CPResponseModel::getCurrency() const
{
    return Currency;
}

void CPResponseModel::setCurrency(const QString &value)
{
    Currency = value;
}

int CPResponseModel::getCurrencyCode() const
{
    return CurrencyCode;
}

void CPResponseModel::setCurrencyCode(int value)
{
    CurrencyCode = value;
}

double CPResponseModel::getPaymentAmount() const
{
    return PaymentAmount;
}

void CPResponseModel::setPaymentAmount(double value)
{
    PaymentAmount = value;
}

QString CPResponseModel::getPaymentCurrency() const
{
    return PaymentCurrency;
}

void CPResponseModel::setPaymentCurrency(const QString &value)
{
    PaymentCurrency = value;
}

QString CPResponseModel::getPaymentCurrencyCode() const
{
    return PaymentCurrencyCode;
}

void CPResponseModel::setPaymentCurrencyCode(const QString &value)
{
    PaymentCurrencyCode = value;
}

QString CPResponseModel::getInvoiceId() const
{
    return InvoiceId;
}

void CPResponseModel::setInvoiceId(const QString &value)
{
    InvoiceId = value;
}

QString CPResponseModel::getAccountId() const
{
    return AccountId;
}

void CPResponseModel::setAccountId(const QString &value)
{
    AccountId = value;
}

QString CPResponseModel::getEmail() const
{
    return Email;
}

void CPResponseModel::setEmail(const QString &value)
{
    Email = value;
}

QString CPResponseModel::getDescription() const
{
    return Description;
}

void CPResponseModel::setDescription(const QString &value)
{
    Description = value;
}

QString CPResponseModel::getCreatedDate() const
{
    return CreatedDate;
}

void CPResponseModel::setCreatedDate(const QString &value)
{
    CreatedDate = value;
}

QString CPResponseModel::getCreatedDateIso() const
{
    return CreatedDateIso;
}

void CPResponseModel::setCreatedDateIso(const QString &value)
{
    CreatedDateIso = value;
}

QString CPResponseModel::getCardType() const
{
    return CardType;
}

void CPResponseModel::setCardType(const QString &value)
{
    CardType = value;
}

QString CPResponseModel::getIssuer() const
{
    return Issuer;
}

void CPResponseModel::setIssuer(const QString &value)
{
    Issuer = value;
}

QString CPResponseModel::getReason() const
{
    return Reason;
}

void CPResponseModel::setReason(const QString &value)
{
    Reason = value;
}

int CPResponseModel::getReasonCode() const
{
    return ReasonCode;
}

void CPResponseModel::setReasonCode(int value)
{
    ReasonCode = value;
}

QString CPResponseModel::getCardHolderMessage() const
{
    return CardHolderMessage;
}

void CPResponseModel::setCardHolderMessage(const QString &value)
{
    CardHolderMessage = value;
}

void CPResponseModel::setData(QJsonObject &json)
{
     this->setTransactionId(QString::number(json["TransactionId"].toInt()));
    this->setPaReq(json["PaReq"].toString());
    this->setAcsUrl(json["AcsUrl"].toString());
    this->setAmount(json["Amount"].toDouble());
    this->setCurrency(json["Currency"].toString());
    this->setReason(json["Reason"].toString());
    this->setReasonCode(json["ReasonCode"].toInt(0));
    this->setCardHolderMessage(json["CardHolderMessage"].toString());
}
