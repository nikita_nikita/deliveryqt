#ifndef CPCHECKOUT_H
#define CPCHECKOUT_H

#include  "../orderrequest.h"
#include <QJsonObject>
class CPCheckOut
{
public:
    CPCheckOut();
    OrderRequest getOrderRequest() const;
    void setOrderRequest(const OrderRequest &value);

    double getAmount() const;
    void setAmount(const double &value);

    QString getCurrency() const;
    void setCurrency(const QString &value);

    QString getEmail() const;
    void setEmail(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getCardCryptogramPacket() const;
    void setCardCryptogramPacket(const QString &value);
    QJsonObject toJson() const;
    void setName(const QString &value);

private:
    OrderRequest orderRequest;
    double Amount;
    QString Currency;
    QString Email;
    QString Description="оплата товаров в мобильном приложении";
    QString CardCryptogramPacket;
    QString Name;



};

#endif // CPCHECKOUT_H
