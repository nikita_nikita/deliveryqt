#include "cpresponse.h"

CPResponse::CPResponse()
{

}

CPResponse::CPResponse(QJsonObject json)
{
    this->setData(json);
}

bool CPResponse::getSuccess() const
{
    return Success;
}

void CPResponse::setSuccess(bool value)
{
    Success = value;
}

QString CPResponse::getMessage() const
{
    return Message;
}

void CPResponse::setMessage(const QString &value)
{
    Message = value;
}

void CPResponse::setData(QJsonObject &json)
{
    this->setSuccess(json["Success"].toBool());
    this->setMessage(json["Message"].toString());
    QJsonObject mod = json["Model"].toObject();
    this->Model.setData(mod);
}

CPResponseModel CPResponse::getModel() const
{
    return Model;
}

bool CPResponse::is3DSecure()
{
    if(this->getSuccess()){
        return false;
    }
    if(this->getModel().getAcsUrl().length()>0) {
        return true;
    }
    return false;
}
