#include "carddata.h"

CardData::CardData()
{

}

QString CardData::getCardNumber() const
{
    return cardNumber;
}

void CardData::setCardNumber(const QString &value)
{
    cardNumber = value;
}

QString CardData::getCardDate() const
{
    return cardDate;
}

void CardData::setCardDate(const QString &value)
{
    cardDate = value;
}

QString CardData::getCardCVC() const
{
    return cardCVC;
}

void CardData::setCardCVC(const QString &value)
{
    cardCVC = value;
}
