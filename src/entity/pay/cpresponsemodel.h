#ifndef CPRESPONSEMODEL_H
#define CPRESPONSEMODEL_H

#include <QString>
#include <QJsonObject>

class CPResponseModel
{
public:
    CPResponseModel();

    QString getTransactionId() const;
    void setTransactionId(QString value);

    QString getPaReq() const;
    void setPaReq(const QString &value);

    QString getAcsUrl() const;
    void setAcsUrl(const QString &value);

    double getAmount() const;
    void setAmount(double value);

    QString getCurrency() const;
    void setCurrency(const QString &value);

    int getCurrencyCode() const;
    void setCurrencyCode(int value);

    double getPaymentAmount() const;
    void setPaymentAmount(double value);

    QString getPaymentCurrency() const;
    void setPaymentCurrency(const QString &value);

    QString getPaymentCurrencyCode() const;
    void setPaymentCurrencyCode(const QString &value);

    QString getInvoiceId() const;
    void setInvoiceId(const QString &value);

    QString getAccountId() const;
    void setAccountId(const QString &value);

    QString getEmail() const;
    void setEmail(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getCreatedDate() const;
    void setCreatedDate(const QString &value);

    QString getCreatedDateIso() const;
    void setCreatedDateIso(const QString &value);

    QString getCardType() const;
    void setCardType(const QString &value);

    QString getIssuer() const;
    void setIssuer(const QString &value);

    QString getReason() const;
    void setReason(const QString &value);

    int getReasonCode() const;
    void setReasonCode(int value);

    QString getCardHolderMessage() const;
    void setCardHolderMessage(const QString &value);

    void setData(QJsonObject &json);

private:
    QString TransactionId;
    QString PaReq;
    QString AcsUrl;
    double Amount=0;
    QString Currency;
    int CurrencyCode;
    double PaymentAmount=0;
    QString PaymentCurrency;
    QString PaymentCurrencyCode;
    QString InvoiceId;
    QString AccountId;
    QString Email;
    QString Description;
    QString CreatedDate;
    QString CreatedDateIso;
    QString CardType;
    QString Issuer;
    QString Reason;
    int ReasonCode=0;
    QString CardHolderMessage;

};

#endif // CPRESPONSEMODEL_H
