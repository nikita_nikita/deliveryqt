#ifndef CPRESPONSE_H
#define CPRESPONSE_H

#include <QString>
#include "cpresponsemodel.h"
class CPResponse
{
public:
    CPResponse();
    CPResponse(QJsonObject json);
    bool getSuccess() const;
    void setSuccess(bool value);

    QString getMessage() const;
    void setMessage(const QString &value);
    void setData(QJsonObject &json);
    CPResponseModel getModel() const;
    bool is3DSecure();

private:
    bool Success = false;
    QString Message;
    CPResponseModel Model;
};

#endif // CPRESPONSE_H
