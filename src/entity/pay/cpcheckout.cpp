#include "cpcheckout.h"

CPCheckOut::CPCheckOut()
{

}

OrderRequest CPCheckOut::getOrderRequest() const
{
    return orderRequest;
}

void CPCheckOut::setOrderRequest(const OrderRequest &value)
{
    orderRequest = value;
}

double CPCheckOut::getAmount() const
{
    return Amount;
}

void CPCheckOut::setAmount(const double &value)
{
    Amount = value;
}

QString CPCheckOut::getCurrency() const
{
    return Currency;
}

void CPCheckOut::setCurrency(const QString &value)
{
    Currency = value;
}

QString CPCheckOut::getEmail() const
{
    return Email;
}

void CPCheckOut::setEmail(const QString &value)
{
    Email = value;
}

QString CPCheckOut::getDescription() const
{
    return Description;
}

void CPCheckOut::setDescription(const QString &value)
{
    Description = value;
}

QString CPCheckOut::getCardCryptogramPacket() const
{
    return CardCryptogramPacket;
}

void CPCheckOut::setCardCryptogramPacket(const QString &value)
{
    CardCryptogramPacket = value;
}

QJsonObject CPCheckOut::toJson() const
{
    QJsonObject result;
    result["Amount"] = this->Amount;
    result["Currency"] = this->Currency;
    result["Email"]= this->Email;
    result["Description"] = this->Description;
    result["CardCryptogramPacket"] = this->CardCryptogramPacket;
    result["Name"] = this->Name;
    result["JsonData"] = this->orderRequest.toJson();
    return result;
}

void CPCheckOut::setName(const QString &value)
{
    Name = value;
}
