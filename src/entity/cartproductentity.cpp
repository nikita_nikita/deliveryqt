#include "cartproductentity.h"



void CartProductEntity::setProduct(Product product)
{
    this->product = product;
}

void CartProductEntity::setQuality(int quantity)
{
    this->quantity = quantity;
}

QJsonObject CartProductEntity::toJson() const
{
    QJsonObject result;
    result["product"] = this->product.toJson();
    result["quantity"] = this->quantity;
    result["offerValue"] = this->getOfferValue();
    result["orderItemId"]= this->getId();

    if(!getOptions().isEmpty()) result["options"] = this->getOptions().toJson();
    return result;
}



double CartProductEntity::getOfferValue() const
{
    return offerValue;
}

void CartProductEntity::setOfferValue(double value)
{
    offerValue = value;
}

QString CartProductEntity::getId() const
{

    return id;
}

void CartProductEntity::setId(const QString &value)
{
    id = value;
}

QString CartProductEntity::generateQuuidString()
{
    return   QUuid::createUuid().toString(QUuid::WithoutBraces);

}

CartOptions CartProductEntity::getOptions() const
{
    return options;
}

void CartProductEntity::setOptions(const CartOptions &value)
{
    options = value;
}

void CartProductEntity::setData(QJsonObject &json)
{
    QJsonObject product = json["product"].toObject();
    this->setProduct(Product(product));
    this->setQuality(json["quantity"].toInt());
    this->setOptions(json["options"].toObject());
}

int CartProductEntity::getQuantity() const
{
    return quantity;
}

CartProductEntity::CartProductEntity()
{

}

CartProductEntity::CartProductEntity(Product product)
{
    this->product = product;
}

CartProductEntity::CartProductEntity(Product product, int quantity)
{
    this->product = product;
    this->quantity = quantity;
}

CartProductEntity::CartProductEntity(Product product, CartOptions options, int quantity)
{
    this->product = product;
    this->quantity = quantity;
    this->options = options;
}

CartProductEntity::CartProductEntity(QJsonObject json)
{
    this->setData(json);
}

Product CartProductEntity::getProduct() const
{
    return product;
}

void CartProductList::setOfferValueByIikoOrderId(QString id, int offerValue)
{
    auto index = this->indexOfId(id);
    if(index == -1){
        return;
    }
    auto product = this->at(index);
    product.setOfferValue(offerValue);
    this->replace(index,product);
}

void CartProductList::reset()
{
    this->clear();
}

void CartProductList::add(Product product)
{
    for(int i =0; i < this->count();i++){
        if(this->at(i).getId() == product.getId()){
            auto quantity = this->at(i).getQuantity();
            this->replace(i,CartProductEntity(product,quantity));
        }
    }
    this->append(CartProductEntity(product, 1));
}

void CartProductList::add(Product product, CartOptions options)
{

   this->append(CartProductEntity(product, options, 1));
}



void CartProductList::setQuantity(Product product, int quantity)
{
    int pos  = this->findByProduct(product);
    if(pos ==-1) {
        return;
    }
    if(quantity ==0) {
        this->removeAt(pos);
        return;
    }
    CartProductEntity entity = this->at(pos);

    entity.setQuality(quantity);
    this->replace(pos, entity);

}
int CartProductList::findByProduct(Product product, bool withoutOptions)
{
    for (int i=0; i<this->count(); i++){
        CartProductEntity entity = this->at(i);
        if(withoutOptions){
           if(entity.getOptions().isEmpty()&&entity.getProduct() == product) return i;
        }
         else if(entity.getProduct() == product) return i;

    }
    return  -1;
}

QJsonArray CartProductList::toJson() const
{
    QJsonArray result;
    for (int i =0; i<count(); i++) {
        if(this->at(i).getQuantity() != 0){
            result.append(this->at(i).toJson());
        }
    }

    return result;
}

void CartProductList::replaceAtIndex(int index, CartProductEntity entity)
{
    this->replace(index,entity);
}

CartProductList CartProductList::getNotSeparetlyDeliveryList()
{
    CartProductList result;
    for(int i = 0; i < this->count();i++){
        if(this->at(i).getProduct().getNotDeliverySeparately()){
            result.append(this->at(i));
        }
    }
    return result;
}

CartProductList::CartProductList(QJsonArray jArr)
{
    for(int i =0; i <jArr.count();i++){
        this->append(CartProductEntity(jArr.at(i).toObject()));
    }
}

CartProductList::CartProductList()
{

}

void CartProductList::setData(QJsonArray array)
{
    this->clear();
    for (int i=0;i<array.count(); i++) {
        this->append(array.at(i).toObject());
    }
}

bool CartProductList::isContainsPizza()
{
    for (int i=0; i<this->count(); i++){
        CartProductEntity entity = this->at(i);
        if (entity.getProduct().IsPizza()) {
            return true;
        }

    }
    return  false;
}

int CartProductList::indexOfId(QString id)
{
    for(int i =0; i < this->count(); i++){
        if(this->at(i).getId() == id){
            return i;
        }
    }
    return -1;
}

QMap<QString, double> CartProductList::getCostsByTypeId()
{
    QMap<QString, double> result;
    for (int i=0; i<this->count(); i++){
        CartProductEntity entity = this->at(i);
        auto key = QString::number(entity.getProduct().getType().getId());
        if (!result.contains(key)){
                result[key] = 0;
        }
        result[key] +=entity.getProduct().getPrice()*entity.getQuantity();

    }
    return result;
}




