#include "citylist.h"
#include <QJsonArray>
#include <QDebug>
CityList::CityList()
{

}

CityList::CityList(QJsonArray &json)
{
    this->setData(json);
}

QJsonArray CityList::toJson() const
{
    QJsonArray result;
    for (int i=0;i<this->count();i++) {
        result.append(this->at(i).toJson());
    }
    return result;
}

void CityList::setData(QJsonArray &json)
{
    for (int i=0;i<json.count(); i++) {
        QJsonObject jcity = json.at(i).toObject();
        this->append(City(jcity));
    }

}

CityList CityList::getHrefVisible() const
{
    CityList result;
    for (int i=0;i<count(); i++) {
        City city  = at(i);
        if(city.getHrefVisible()) {
            result.append(city);
        }
    }
    return result;
}

bool CityList::existCityId(int id)
{
    for(int i=0; i<this->count(); i++){
       City city = this->at(i);
       if (city.getId()==id){
           auto check = city.getEnable() == 1;
           qDebug()<<check <<"check";
           return city.getEnable() == 1;
       }
    }
    return false;
}

City CityList::cityByid(int id)
{
    if (existCityId(id)){
        for(int i = 0; i< this->count();i++){
            if(this->at(i).getId() == id){
                return this->at(i);
            }
        }
    }
    return City();
}
