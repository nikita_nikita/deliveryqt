#include "orderhystory.h"
#include "orderhystory.h"

OrderHystory::OrderHystory(QJsonObject jObj)
{
    QJsonObject orderinfo = jObj.value("orderInfo").toObject();

    this->setDelivery(orderinfo.value("delivery").toInt()==1);

    this->setId(jObj.value("id").toInt());
    this->setTakeAweypoint(orderinfo.value("take_away_point").toString());
    this->setProducts( jObj.value("products").toArray());
    this->setCanFeedBack(jObj.value("commentary").toObject().value("status").toBool());
    this->setDateTime(jObj.value("date_insert").toString());
}

OrderHystory::OrderHystory()
{
    
}


QString OrderHystory::status() const
{
    return m_status;
}

int OrderHystory::delivery() const
{
    return m_delivery;
}


QString OrderHystory::takeAweypoint() const
{
    return m_takeAweypoint;
}

bool OrderHystory::canFeedBack() const
{
    return m_canFeedBack;
}

int OrderHystory::id() const
{
    return m_id;
}

QJsonArray OrderHystory::products() const
{
    return m_products;
}

void OrderHystory::setStatus(QString status)
{
    if (m_status == status)
        return;

    m_status = status;

}

void OrderHystory::setDelivery(int delivery)
{
    if (m_delivery == delivery)
        return;

    m_delivery = delivery;

}


void OrderHystory::setTakeAweypoint(QString takeAweypoint)
{
    if (m_takeAweypoint == takeAweypoint)
        return;

    m_takeAweypoint = takeAweypoint;

}

void OrderHystory::setCanFeedBack(bool canFeedBack)
{
    if (m_canFeedBack == canFeedBack)
        return;

    m_canFeedBack = canFeedBack;

}

void OrderHystory::setId(int id)
{
    if (m_id == id)
        return;

    m_id = id;
}

void OrderHystory::setProducts(QJsonArray products)
{
    if (m_products == products)
        return;

    this->m_products = products;
}

QString OrderHystory::getDateTime() const
{
    return dateTime;
}

void OrderHystory::setDateTime(const QString &value)
{
    dateTime = value;
}

QJsonArray OrderHystory::getProductArray()
{
    return m_products;
}
QString OrderHystory::getProductString()
{
    QString result;
    for(int i = 0; i < m_products.count();i++){
        if(i != 0){
            result+=", ";
        }
        result+= m_products.at(i).toObject().value("product").toObject().value("name").toString();
    }
    return result;
}

bool OrderHystory::operator !=(OrderHystory order)
{
    if(order.id() != id()||order.products() != products()||order.delivery() != delivery()||order.takeAweypoint() != takeAweypoint()){
        return true;
    }
    return false;

}


OrderHystoryList::OrderHystoryList()
{

}

OrderHystoryList::OrderHystoryList(QJsonArray jArr)
{
    for(int i = 0; i < jArr.count();i++){
        this->append(OrderHystory(jArr.at(i).toObject()));
    }
}

int OrderHystoryList::indexOfId(int id)
{
    for(int i =0; i < this->count(); i++){

        if(this->at(i).id() == id){
            return i;
        }
    }
    return -1;
}


