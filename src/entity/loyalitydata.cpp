#include "loyalitydata.h"

LoyalityData::LoyalityData()
{

}

LoyalityData::LoyalityData(QJsonObject data)
{
    this->setData(data);
}

int LoyalityData::getType() const
{
    return type;
}

void LoyalityData::setType(int value)
{
    type = value;
}

int LoyalityData::getTarget() const
{
    return target;
}

void LoyalityData::setTarget(int value)
{
    target = value;
}

QString LoyalityData::getName() const
{
    return name;
}

void LoyalityData::setName(const QString &value)
{
    name = value;
}

QString LoyalityData::getDescription() const
{
    return description;
}

void LoyalityData::setDescription(const QString &value)
{
    description = value;
}

double LoyalityData::getValue() const
{
    return value;
}

void LoyalityData::setValue(double valueData)
{
    value = valueData;
}

QString LoyalityData::getIiko_action_id() const
{
    return iiko_action_id;
}

void LoyalityData::setIiko_action_id(const QString &value)
{
    iiko_action_id = value;
}

QString LoyalityData::getIiko_Item_id() const
{
    return iiko_Item_id;
}

void LoyalityData::setIiko_Item_id(const QString &value)
{
    iiko_Item_id = value;
}





void LoyalityData::setData(QJsonObject data)
{
    setName(data["name"].toString());
    setDescription(data["description"].toString());
    
    setIiko_action_id(data["iiko_action_id"].toString());
    setValue(data["value"].toDouble());
    setType(data["type"].toInt());
    setIiko_Item_id(data["orderItemId"].toString());
    setTarget(data["target"].toInt());
}
