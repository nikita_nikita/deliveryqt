#include "orderrequest.h"

OrderRequest::OrderRequest()
{

}

Order OrderRequest::getOrder() const
{
    return order;
}

void OrderRequest::setOrder(const Order &value)
{
    order = value;
}

int OrderRequest::getCityId() const
{
    return cityId;
}

void OrderRequest::setCityId(int value)
{
    cityId = value;
}

OrderInfo OrderRequest::getOrderInfo() const
{
    return orderInfo;
}

void OrderRequest::setOrderInfo(const OrderInfo &value)
{
    orderInfo = value;
}

OrdeAddress OrderRequest::getAddress() const
{
    return address;
}

void OrderRequest::setAddress(const OrdeAddress &value)
{
    address = value;
}

QJsonObject OrderRequest::toJson() const
{
    QJsonObject result;
    result["mac"] = QDateTime::currentDateTime().toString(" dd-MM-yyyy hh:mm:ss");
    result["orderInfo"] = this->getOrderInfo().toJson();
    auto order = this->getOrder().toJson();
//    Новая система топпингов
    result["clientToppings"] = getClient_toppings();
//     поставить if на наличие системы на точке

    result["order"] = order;
    result["cityId"] = this->getCityId();
    result["address"] = this->getAddress().toJson();
    result["platform"] = this->getPlatform();
    result["token"] = this->getToken();
//    result["mac"] =this->getMac();
    result["transaction_mail"]= this->getTransactionMail();
    return result;

}



QString OrderRequest::getPlatform() const
{
    return platform;
}

void OrderRequest::setPlatform(const QString &value)
{
    platform = value;
}

QString OrderRequest::getToken() const
{
    return token;
}

void OrderRequest::setToken(const QString &value)
{
    token = value;
}

QString OrderRequest::getMac() const
{
    return mac;
}

void OrderRequest::setMac(const QString &value)
{
    mac = value;
}

QString OrderRequest::getTransactionMail() const
{
    return transaction_mail;
}

void OrderRequest::setTransactionMail(const QString &value)
{
    transaction_mail = value;
}

bool OrderRequest::getPromocodeValid() const
{
    return promocodeValid;
}

void OrderRequest::setPromocodeValid(bool value)
{
    promocodeValid = value;
}

QJsonArray OrderRequest::getClient_toppings() const
{    
    QJsonArray result;
    for(int i =0; i < client_toppings.count();i++){
        if(client_toppings.at(i).toObject().value("count").toInt()!=0){
result.append(client_toppings.at(i));
        }
    }
    return result;
}

void OrderRequest::setClient_toppings(const QJsonArray &value)
{
    client_toppings = value;
}

