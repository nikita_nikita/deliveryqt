#include "cartoptions.h"

CartOptions::CartOptions()
{

}

CartOptions::CartOptions(QJsonObject json)
{
  this->setData(json);
}

void CartOptions::setData(QJsonObject &json)
{
    this->setPrice(json["price"].toDouble());
    this->setAdded(json["added"].toObject());

}

QJsonObject CartOptions::toJson() const
{
    QJsonObject result;
        result["price"] = this->getPrice();
        result["added"] = this->getAdded().toJson();
        return result;
}

bool CartOptions::isEmpty()
{
    return added.isEmpty();
}



CartAdded CartOptions::getAdded() const
{
    return added;
}

void CartOptions::setAdded(const CartAdded &value)
{
    added = value;
}

double CartOptions::getPrice() const
{
    return price;
}

void CartOptions::setPrice(double value)
{
    price = value;
}
