#ifndef CARTOPTIONS_H
#define CARTOPTIONS_H

#include "cartadded.h"

class CartOptions
{
public:
    CartOptions();
    CartOptions(QJsonObject json);
    CartAdded getAdded() const;
    void setAdded(const CartAdded &value);

    double getPrice() const;
    void setPrice(double value);

    void setData(QJsonObject &json);
    QJsonObject toJson() const;
    bool isEmpty();


private:
    CartAdded added;
    double price=0;

};

#endif // CARTOPTIONS_H
