#include "cartadded.h"

CartAdded::CartAdded()
{

}

CartAdded::CartAdded(QJsonObject json)
{
     this->setData(json);
}

CartOptionsItemList CartAdded::getToping() const
{
    return toping;
}

void CartAdded::setToping(const CartOptionsItemList &value)
{
    toping = value;
}

int CartAdded::getDoubleMeat() const
{
    return doubleMeat;
}

void CartAdded::setDoubleMeat(int value)
{
    doubleMeat = value;
}

int CartAdded::getDoubleVeg() const
{
    return doubleVeg;
}

void CartAdded::setDoubleVeg(int value)
{
    doubleVeg = value;
}

int CartAdded::getNoodle() const
{
    return noodle;
}

void CartAdded::setNoodle(int value)
{
    noodle = value;
}

void CartAdded::setData(QJsonObject &json)
{
    this->setDoubleMeat(json["doubleMeat"].toInt());
    this->setDoubleVeg(json["doubleVeg"].toInt());
    this->setNoodle(json["noodle"].toInt());
    this->setToping(json["toping"].toArray());
    this->setSouse(json["souse"].toInt());

}

QJsonObject CartAdded::toJson() const
{
    QJsonObject result;
    result["doubleMeat"] = this->getDoubleMeat();
    result["doubleVeg"] = this->getDoubleVeg();
    result["noodle"] = this->getNoodle();
    result["toping"] = this->getToping().toJson();
    result["souse"] = this->getSouse();
    return result;
}

bool CartAdded::isEmpty()
{
    return doubleMeat==0&&doubleVeg==0&&noodle==0&&toping.count()==0&&souse==0;
}

int CartAdded::getSouse() const
{
    return souse;
}

void CartAdded::setSouse(int value)
{
    souse = value;
}
