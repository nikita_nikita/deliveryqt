#include "cartoptionsitem.h"

CartOptionsItem::CartOptionsItem()
{

}

CartOptionsItem::CartOptionsItem(QJsonObject json)
{
    this->setData(json);
}

ProductItem CartOptionsItem::getItem() const
{
    return item;
}

void CartOptionsItem::setItem(const ProductItem &value)
{
    item = value;
}

int CartOptionsItem::getQuantity() const
{
    return quantity;
}

void CartOptionsItem::setQuantity(int value)
{
    quantity = value;
}

void CartOptionsItem::setData(QJsonObject &json)
{
    this->setQuantity(json["quantity"].toInt());
    this->item.setData(json["item"].toObject());
}

QJsonObject CartOptionsItem::toJson() const
{
    QJsonObject result;
    result["quantity"] = this->getQuantity();
    result["item"] = this->getItem().toJson();
    return  result;
}

CartOptionsItemList::CartOptionsItemList()
{

}

CartOptionsItemList::CartOptionsItemList(QJsonArray arr)
{
    this->setData(arr);
}

void CartOptionsItemList::setData(QJsonArray &arr)
{
    clear();
    for(int i =0; i<arr.count(); i++) {
        append(CartOptionsItem(arr.at(i).toObject()));
    }
}

QJsonArray CartOptionsItemList::toJson() const
{
    QJsonArray result;
    for (int i=0;i<count();i++) {
        result.append(this->at(i).toJson());
    }
    return result;
}
