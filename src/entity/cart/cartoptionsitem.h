#ifndef CARTOPTIONSITEM_H
#define CARTOPTIONSITEM_H

#include "../citydata/product.h"

class CartOptionsItem
{
public:
    CartOptionsItem();
    CartOptionsItem(QJsonObject json);
    ProductItem getItem() const;
    void setItem(const ProductItem &value);

    int getQuantity() const;
    void setQuantity(int value);
    void setData(QJsonObject &json);
    QJsonObject toJson() const;

private:
    ProductItem item;
    int quantity;
};
class CartOptionsItemList:public QList<CartOptionsItem> {
public:
    CartOptionsItemList();
    CartOptionsItemList(QJsonArray arr);
    void setData(QJsonArray &arr);
    QJsonArray toJson() const;
};

#endif // CARTOPTIONSITEM_H
