#ifndef CARTADDED_H
#define CARTADDED_H

#include "cartoptionsitem.h"

class CartAdded
{
public:
    CartAdded();
     CartAdded(QJsonObject json);
    CartOptionsItemList getToping() const;
    void setToping(const CartOptionsItemList &value);

    int getDoubleMeat() const;
    void setDoubleMeat(int value);

    int getDoubleVeg() const;
    void setDoubleVeg(int value);

    int getNoodle() const;
    void setNoodle(int value);

    void setData(QJsonObject &json);
    QJsonObject toJson() const;
    bool isEmpty();

    int getSouse() const;
    void setSouse(int value);

private:
    int doubleMeat=0;
    int doubleVeg=0;
    int noodle=0;
    int souse=0;
    CartOptionsItemList toping;
};

#endif // CARTADDED_H
