#ifndef ORDERLOADLIST_H
#define ORDERLOADLIST_H
#include <QList>

class OrderLoad
{
public:
    OrderLoad();
    OrderLoad(int id);
    int getId() const;
    void setId(int value);

    bool getSend() const;
    void setSend(bool value);

    bool getLoad() const;
    void setLoad(bool value);

private:
    int id;
    bool send;
    bool load;

};

class OrderLoadList : public QList<OrderLoad>
{
public:
    OrderLoadList();
    int indexOfId(int index);

};


#endif // ORDERLOADLIST_H
