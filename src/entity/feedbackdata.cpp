#include "feedbackdata.h"

FeedbackData::FeedbackData()
{

}

int FeedbackData::getOrderId() const
{
    return orderId;
}

void FeedbackData::setOrderId(int value)
{
    orderId = value;
}

QString FeedbackData::getComment() const
{
    return comment;
}

void FeedbackData::setComment(const QString &value)
{
    comment = value;
}

int FeedbackData::getValue() const
{
    return value;
}

void FeedbackData::setValue(int value)
{
    value = value;
}

QStringList FeedbackData::getImagelist() const
{
    return imagelist;
}

void FeedbackData::setImagelist(const QStringList &value)
{
    imagelist = value;
}
