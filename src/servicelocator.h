#ifndef SERVICELOCATOR_H
#define SERVICELOCATOR_H

#include <QQmlContext>
#include "service/kapibaras.h"
#include "service/storage.h"
#include "service/router.h"
#include "service/menu.h"
#include "service/cart.h"
#include "service/pay.h"
#include "service/statusstorage.h"
#include "service/worktime.h"
class ServiceLocator:public QObject
{
public:
    ServiceLocator();
    ~ServiceLocator();
    void run();
     void registrateService(QQmlContext * context);


     Kapibaras *getKapibaras() const;
     Storage *getStorage() const;
     Router *getRouter() const;
     Menu *getMenu() const;
     Cart *getCart() const;
     Pay *getPay() const;

     StatusStorage *getStatusStorage() const;
     WorkTime *getWorktime() const;
     void setCityData(CityData cityData);



private slots:
     void updateUser(QJsonObject user);
    void PayCard(OrderRequest request, CardData card);
    void setToken(bool status, QString token);

signals:
private:

    Kapibaras *kapibaras;
    Storage *storage;
    Router *router;
    Menu *menu;
    Cart *cart;
    Pay *pay;
    StatusStorage *statusStorage;
    WorkTime *worktime;

    void connectService();

};

#endif // SERVICELOCATOR_H
