#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QJsonObject>
#include <QSqlDatabase>
class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = nullptr);

    QString getTmpPathUploads() const;
    void setTmpPathUploads(const QString &value);

    void saveCity(QJsonObject city);
    void saveUser(QJsonObject user);
    void saveUserEmail(QString email);
    void saveToken(QString token);
    QJsonArray getOrderHystory();
    void setOrder(int id, QString status);
    void saveOrder(int id, QString status);
    void replaceOrder(int id, QString status);
    QString statusById(int id);
    QJsonObject loadUser();
    QJsonObject loadCity();
    QString loadToken();
    QString loadUserEmail();
    void addAddres(int city, QString domainPath,  QJsonObject address);
    QJsonArray fethAddress(int city, QString domainPath);
    void saveAddress(int id, QJsonObject address);
    void removeAddress(int id);

signals:
void domainChanged();
public slots:
private:
    QString tmpPathUploads;
    QString tableName="userData.sqlite";
    QSqlDatabase sdb;

    QString getPathDB();
    void createDb();
    void connect();
    void checkData();


};

#endif // DATABASE_H
