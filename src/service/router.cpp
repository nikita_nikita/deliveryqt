#include "router.h"
#include <QDesktopServices>
#include <QUrl>

#include <QDebug>

Router::Router(QObject *parent) : QObject(parent)
{
    this->popToMain();
    this->m_wokData = this->getWokDataStruct();
    connect(this, &Router::pageChanged, &Router::updatePageTitle);
    this->m_constructorData=QJsonObject{
    {"operation", "edit"},
    };    
    connect(this,&Router::pageChanged,this,&Router::processButtonStatus);
}

QString Router::page() const
{
    return m_page;
}

QString Router::footer() const
{
    return m_footer;
}

int Router::stack() const
{
    return m_stack;
}

QString Router::pageName() const
{
    return m_pageName;
}

bool Router::pageLoadedVisible() const
{
    return m_pageLoadedVisible;
}

QJsonValue Router::wokData() const
{
    return m_wokData;
}

QJsonValue Router::constructorData() const
{
    return m_constructorData;
}

bool Router::footerBottonValid() const
{
    return m_footerBottonValid;
}

void Router::onOrderDataChange()
{
    if(m_page == "cart"){
        setFooterBottonValid(cartCartEnable);
    }
    if(m_page == "checkout"){
        setFooterBottonValid(checkoutDelivery_valid);
    }
    if(m_page == "cart"){
        setFooterBottonValid(paymentController_valid);
    }
}



void Router::setPage(QString page)
{
    if (m_page == page)
        return;

    m_page = page;
    emit pageChanged(m_page);
    if(page == "cart" || page == "checkout" || page=="payment"){
        this->setFooter(FOOTER_CART);
        return;
    }
    if(page == "order_success"|| page=="product_not_zone"){
        this->setFooter(FOOTER_TOMAINT);
        return;
    }
    this->setFooter(FOOTER_MENU);
}

void Router::setFooter(QString footer)
{
    if (m_footer == footer)
        return;

    m_footer = footer;
    emit footerChanged(m_footer);
}

void Router::push(QString page)
{
    stack_pages.push(page);
    this->setPage(page);
    this->setStack(stack_pages.count());
}

void Router::pop()
{
    if(stack_pages.count()<=1){
        emit popLast();
        return;
    }
    stack_pages.pop();
    this->setPage(stack_pages.at(stack_pages.count()-1));
    this->setStack(stack_pages.count());
}

void Router::popToMain()
{
    QStack<QString> st ;
    st.push(MAIN_PAGE);
    this->stack_pages = st;
    this->setPage(MAIN_PAGE);
    this->setStack(stack_pages.count());

}

void Router::popToOrderSuccess()
{
    QStack<QString> st ;
    st.push(MAIN_PAGE);
    st.push(ORDER_SUCCESS_PAGE);
    this->stack_pages = st;
    this->setPage(ORDER_SUCCESS_PAGE);
    this->setStack(stack_pages.count());
}

void Router::popToCart()
{
    QStack<QString> st ;
    st.push(MAIN_PAGE);
    st.push("cart");
    this->stack_pages = st;
    this->setPage("cart");
    this->setStack(stack_pages.count());

}

void Router::pushToWokConstructor(QJsonValue data)
{
    this->setWokData(data);
    this->push("wok_constructor");
}

void Router::pushToConstructor(QJsonValue data)
{
    this->setConstructorData(data);
    this->push("constructor");
}

void Router::popToOrderError(QString error)
{
    Q_UNUSED(error);
    this->push(ORDER_ERROR_PAGE);
}

void Router::pushToEditStreet(QJsonObject address)
{
    emit editStreet(address);
    this->push("editstreet");
}



void Router::setStack(int stack)
{
    if (m_stack == stack)
        return;
    m_stack = stack;
    emit stackChanged(m_stack);
}

void Router::openUrl(QString url)
{
    QDesktopServices::openUrl(QUrl(url));
}

void Router::updatePageTitle(QString page)
{
    QMap<QString, QString> titlePages;
    titlePages["menu"]="";
    titlePages["citys"]="Выбор города";
    titlePages["orders"]="Заказы";
    titlePages["cart"]="Корзина";
    titlePages["profile"]="Профиль";
    titlePages["addstreet"]="Добавить улицу";
    titlePages["editstreet"]="Редактор улицы";
    titlePages["offers"]="Акции";
    titlePages["contacts"]="Контакты";
    titlePages["checkout"]="Оформление";
    titlePages["payment"]="Оплата";
    titlePages["wok_constructor"]="Конструктор";
    titlePages["constructor"]="Конструктор";
    titlePages["search"]="Поиск";
    titlePages["feedback"]="Страница отзыва";
    titlePages["order_success"]="";

    this->setPageName(titlePages[page]);
}

void Router::setPageName(QString pageName)
{
    if (m_pageName == pageName)
        return;

    m_pageName = pageName;
    emit pageNameChanged(m_pageName);
}

void Router::setPageLoadedVisible(bool pageLoadedVisible)
{

    qDebug()<<":::::::"<<pageLoadedVisible;
    if (m_pageLoadedVisible == pageLoadedVisible)
        return;

    m_pageLoadedVisible = pageLoadedVisible;
    emit pageLoadedVisibleChanged(m_pageLoadedVisible);
}

void Router::setWokData(QJsonValue wokData)
{
    if (m_wokData == wokData)
        return;

    m_wokData = wokData;
    emit wokDataChanged(m_wokData);
}

void Router::setConstructorData(QJsonValue constructorData)
{
    if (m_constructorData == constructorData)
        return;

    m_constructorData = constructorData;
    emit constructorDataChanged(m_constructorData);
}

void Router::setFooterBottonValid(bool footerBottonValid)
{
    if (m_footerBottonValid == footerBottonValid)
        return;

    m_footerBottonValid = footerBottonValid;
    emit footerBottonValidChanged(m_footerBottonValid);
}



void Router::onPaymentControllerValid(bool data)
{
 if(paymentController_valid == data){
     return;
 }
 paymentController_valid = data;
 emit helpDataChanged();

}

QJsonObject Router::getWokDataStruct()
{
    QJsonObject result;
    QJsonObject product;
    product["name"] = "";
    product["images"] ={""};
    product["sity_info"] = QJsonObject{
    {"price", 0}
};
    QJsonObject added;
    added["vegetable"]=QJsonObject{{"product", product}};;
    added["meat"]=QJsonObject{{"product", product}};

    result = product;
    result["added"]=added;

    return result;
}

bool Router::getPaymentController_valid() const
{
    return paymentController_valid;
}

void Router::setPaymentController_valid(bool value)
{
    paymentController_valid = value;    
    processButtonStatus();
}

void Router::processButtonStatus()
{


    if(m_page == "cart" && cartCartEnable){
        setFooterBottonValid(true);
        return;
    }if(m_page == "checkout" && checkoutDelivery_valid && cartCartEnable){
        setFooterBottonValid(true);
        return;
    }if(m_page == "payment" && paymentController_valid){
        setFooterBottonValid(true);
        return;
    }
    else{
        setFooterBottonValid(false);
    }

}


void Router::setCheckoutDelivery_valid(bool value)
{
    checkoutDelivery_valid = value;
    processButtonStatus();
}

void Router::setCartCartEnable(bool value)
{
    cartCartEnable = value;
    processButtonStatus();
}

