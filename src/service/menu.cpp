#include "menu.h"
Menu::Menu(QObject *parent) : QObject(parent)
{
    this->m_toppingsJap  = new ProductsModel(this);
    this->m_toppingsPizza = new ProductsModel(this);
    this->m_composite = new ProductItemListModel(this);

}

int Menu::currentOffers() const
{
    return m_currentOffers;
}

QString Menu::item_icon() const
{
    return m_item_icon;
}



void Menu::setCityDataJson(CityData cityData)
{

    this->m_toppingsJap->setProductList(cityData.getToppings().getJap());
    this->m_toppingsPizza->setProductList(cityData.getToppings().getPizza());
    this->allProduts = cityData.getProducts();
}

void Menu::backCategory()
{
    emit toBackPage();
}

void Menu::setCurrentDept(int dept)
{
    emit deptChanged(dept);
}
void Menu::setCurrentOffers(int currentOffers)
{
    if (m_currentOffers == currentOffers)
        return;

    m_currentOffers = currentOffers;   
    emit currentOffersChanged(m_currentOffers);
}

void Menu::resetPositions()
{
    this->setCurrentOffers(0);
}

void Menu::setInfoComposite(Product prod)
{
   ProductAddedItemList compostite=  prod.getAdded().getComposite();
   if(compostite.count()==0){
       return;
   }
   this->m_composite->setProducts(compostite.getProductItems());
    emit showInfoComposite(prod.getName());
}

void Menu::setItem_icon(QString item_icon)
{
    if (m_item_icon == item_icon)
        return;

    m_item_icon = item_icon;
    emit item_iconChanged(m_item_icon);
}
QJsonArray Menu::appendArray(QJsonArray target, QJsonArray source)
{
    for (int i=0; i<source.count();i++) {
        target.append(source.at(i));
    }
    return target;
}




