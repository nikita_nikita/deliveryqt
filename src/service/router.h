#ifndef ROUTER_H
#define ROUTER_H

#include <QObject>
#include <QJsonArray>
#include <QStack>
#include <QJsonObject>
#include <QMap>

class Router : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString page READ page WRITE setPage NOTIFY pageChanged)
    Q_PROPERTY(QString footer READ footer WRITE setFooter NOTIFY footerChanged)
    Q_PROPERTY(int stack READ stack WRITE setStack NOTIFY stackChanged)
    Q_PROPERTY(QString pageName READ pageName WRITE setPageName NOTIFY pageNameChanged)
    Q_PROPERTY(bool pageLoadedVisible READ pageLoadedVisible WRITE setPageLoadedVisible NOTIFY pageLoadedVisibleChanged)
    Q_PROPERTY(QJsonValue wokData READ wokData WRITE setWokData NOTIFY wokDataChanged)
    Q_PROPERTY(QJsonValue constructorData READ constructorData WRITE setConstructorData NOTIFY constructorDataChanged)
    Q_PROPERTY(bool footerBottonValid READ footerBottonValid WRITE setFooterBottonValid NOTIFY footerBottonValidChanged)


public:
    explicit Router(QObject *parent = nullptr);
    QString page() const;
    QString footer() const;
    int stack() const;
    QString pageName() const;
    bool pageLoadedVisible() const;
    QJsonValue wokData() const;

    QJsonValue constructorData() const;

    bool footerBottonValid() const;

    void setCartCartEnable(bool value);

    void setCheckoutDelivery_valid(bool value);

    void setPaymentController_valid(bool value);



signals:
    void pageChanged(QString page);
    void footerChanged(QString footer);
    void stackChanged(int stack);
    void popLast();
    void editStreet(QJsonObject address);
    void pageNameChanged(QString pageName);
    void pageLoadedVisibleChanged(bool pageLoadedVisible);
    void wokDataChanged(QJsonValue wokData);
    void constructorDataChanged(QJsonValue constructorData);
    void footerBottonValidChanged(bool footerBottonValid);

    void helpDataChanged();

private slots:
    void onOrderDataChange();
    void processButtonStatus();
public slots:
    bool getPaymentController_valid() const;
    void setPage(QString page);
    void setFooter(QString footer);
    void push(QString page);
    void pop();
    void popToMain();
    void popToOrderSuccess();
    void popToCart();
    void pushToWokConstructor(QJsonValue data);
    void pushToConstructor(QJsonValue data);
    void popToOrderError(QString error);
    void pushToEditStreet(QJsonObject address);
    void setStack(int stack);
    void openUrl(QString url);
    void updatePageTitle(QString page);
    void setPageName(QString pageName);
    void setPageLoadedVisible(bool pageLoadedVisible);    
    void setWokData(QJsonValue wokData);    
    void setConstructorData(QJsonValue constructorData);

    void setFooterBottonValid(bool footerBottonValid);
    void onPaymentControllerValid(bool data);


private:
    const QString MAIN_PAGE = "menu";
    const QString ORDER_SUCCESS_PAGE= "order_success";
    const QString ORDER_ERROR_PAGE = "order_error";
    const QString FOOTER_CART = "cart";
    const QString SEARCH_PAGE = "search";
    const QString FEEDBACK_PAGE = "feedback";


    const QString FOOTER_MENU = "menu";
    const QString FOOTER_TOMAINT = "tomaint";
    QString m_page = "menu";
    QString m_footer = "menu";
    QStack<QString>  stack_pages;
    int m_stack;
    QString m_pageName;
    bool m_pageLoadedVisible= false;
    QJsonValue m_wokData;
    QJsonObject getWokDataStruct();
    QJsonValue m_constructorData;
    //Активность кнопки футера на данной странице
    bool m_footerBottonValid = false;


    //Разрешена ли доставка
    bool checkoutDelivery_valid = false;


    //разрешена ли оплата(данные карты)
    bool paymentController_valid = false;



    //Проверка пустая ли корзина(основная и бонусная корзина)
    bool cartCartEnable = false;



};

#endif // ROUTER_H
