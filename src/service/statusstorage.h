#ifndef STATUSSTORAGE_H
#define STATUSSTORAGE_H

#include <QObject>
#include <QJsonObject>
class StatusStorage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool checkStreetLoaded READ checkStreetLoaded WRITE setCheckStreetLoaded NOTIFY checkStreetLoadedChanged) // флаг  проверки улиц( загруженна ли проверка улиц)
    Q_PROPERTY(QJsonObject checkStreetResponse READ checkStreetResponse WRITE setCheckStreetResponse NOTIFY checkStreetResponseChanged) //  ответ при проверке улиц


public:
    explicit StatusStorage(QObject *parent = nullptr);

    bool checkStreetLoaded() const;

    QJsonObject checkStreetResponse() const;

signals:

    void checkStreetLoadedChanged(bool checkStreetLoaded);

    void checkStreetResponseChanged(QJsonObject checkStreetResponse);

public slots:
    void setCheckStreetLoaded(bool checkStreetLoaded);
    void setCheckStreetResponse(QJsonObject checkStreetResponse);

private:
    bool m_checkStreetLoaded = false;
    QJsonObject m_checkStreetResponse= QJsonObject{
    {"status", true}
};
};

#endif // STATUSSTORAGE_H
