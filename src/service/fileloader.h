#ifndef FILELOADER_H
#define FILELOADER_H

#include <QObject>
#include <QNetworkAccessManager>

class FileLoader : public QObject
{
    Q_OBJECT
public:
    explicit FileLoader(QObject *parent = nullptr);

    QString getPathToLoad() const;
    void setPathToLoad(const QString &value);

    QUrl getUrlFromLoad() const;
    void setUrlFromLoad(const QUrl &value);
    bool fileExists(QString fileName);

signals:
    void filesLoaded();
    void error(QString error);
public slots:
    void load(QStringList files);
private slots:
    void onResult(QNetworkReply *reply);
private:
    QString pathToLoad;
    QUrl urlFromLoad;
    QNetworkAccessManager *networkManager;
    QStringList files;

    int pos=0;
    void loadFile(int i);
    QStringList getAddedFiles(QStringList filesLoad, QStringList isset);
    QStringList getRemoveFiles(QStringList filesLoad, QStringList isset);
    void removeFiles(QStringList files);


};

#endif // FILELOADER_H
