#include "cart.h"
#include <QtGlobal>
#include <QJSEngine>
#include <QJSValue>
#include <QTime>
#include <QJsonDocument>
#include <QDebug>
Cart::Cart(QObject *parent) : QObject(parent)
{
    m_model = new CartProductModel(this);
    m_toppingkit = new ToppingKitModel(this);
    m_cuponStatus = QJsonObject({
                                    {"message", ""},
                                    {"status", false}

                                });
    m_bonus = QJsonArray();
    connect(m_model, &CartProductModel::cartChanged, this, &Cart::updateContainer);

}

int Cart::getToppingCount()
{
    return m_toppingkit->getToppingsCount();
}

void Cart::updateToppingCount(int index, int count)
{
    m_toppingkit->setCount(index,count);
}

QJsonArray Cart::conteiner() const
{
    return m_conteiner;
}

int Cart::personCount() const
{
    return m_personCount;
}

QString Cart::promocode() const
{
    return m_promocode;
}

QString Cart::userName() const
{
    return m_userName;
}

QString Cart::userPhone() const
{
    return m_userPhone;
}



int Cart::takeAwayPoint() const
{
    return m_takeAwayPoint;
}

QString Cart::comment() const
{
    return m_comment;
}

QString Cart::orderTime() const
{
    return m_orderTime;
}



int Cart::payMethod() const
{
    return m_payMethod;
}

bool Cart::noChange() const
{
    return m_noChange;
}

QString Cart::street() const
{
    return m_street;
}



int Cart::entrance() const
{
    return m_entrance;
}

int Cart::floor() const
{
    return m_floor;
}

int Cart::room() const
{
    return m_room;
}

double Cart::cash() const
{
    return m_cash;
}



bool Cart::dataProcessing() const
{
    return m_dataProcessing;
}

int Cart::streetId() const
{
    return m_streetId;
}

QString Cart::orderType() const
{
    return m_orderType;
}

bool Cart::validCheckOut() const
{
    return m_validCheckOut;
}

QString Cart::building() const
{
    return m_building;
}

QString Cart::cardNumber() const
{
    return m_cardNumber;
}

QString Cart::cardDate() const
{
    return m_cardDate;
}

QString Cart::cardCVC() const
{
    return m_cardCVC;
}

CartProductModel *Cart::getModel()
{
    return m_model;
}



void Cart::add(QJsonObject product)
{

    this->m_model->add(Product(product));
}

void Cart::clearPromocode()
{
    this->setPromocode("");
    this->setComment("");
}

void Cart::addList(QJsonArray cartList)
{
    CartProductList list;
    list.setData(cartList);
    this->m_model->add(list);

}

void Cart::addWithOptions(QJsonObject product, QJsonObject options)
{
    this->m_model->add(Product(product), CartOptions(options));
}

void Cart::setOptions(int index, QJsonObject options)
{
    this->m_model->setOptions(index, CartOptions(options));
}

void Cart::setQuantity(QJsonObject product, int quantity, bool withoutOptions)
{
    this->m_model->setQuantity(Product(product), quantity, withoutOptions);
}

void Cart::setQuantityByIndex(int index, int quantity)
{
    this->m_model->setQuantityByIndex(index, quantity);
}

void Cart::setConteiner(QJsonArray conteiner)
{
    if (m_conteiner == conteiner)
        return;

    m_conteiner = conteiner;
    emit conteinerChanged(m_conteiner);
}

void Cart::updateContainer(CartProductList cart)
{

    this->m_cart = cart;
    this->setConteiner(cart.toJson());
    emit cartChanged(cart);

}

void Cart::setPersonCount(int personCount)
{
    if (m_personCount == personCount)
        return;

    m_personCount = personCount;
    emit personCountChanged(m_personCount);
}

void Cart::setPromocode(QString promocode)
{
    if (m_promocode == promocode)
        return;

    m_promocode = promocode;
    emit promocodeChanged(m_promocode);
}

void Cart::setUserName(QString userName)
{
    if (m_userName == userName)
        return;

    m_userName = userName;
    emit userNameChanged(m_userName);
}

void Cart::setUserPhone(QString userPhone)
{
    if (m_userPhone == userPhone)
        return;

    m_userPhone = userPhone;
    emit userPhoneChanged(m_userPhone);
}


void Cart::setTakeAwayPoint(int takeAwayPoint)
{
    if (m_takeAwayPoint == takeAwayPoint)
        return;
    m_takeAwayPoint = takeAwayPoint;
    emit takeAwayPointChanged(m_takeAwayPoint);
}

void Cart::setComment(QString comment)
{

    if (m_comment == comment)
        return;

    m_comment = comment;
    emit commentChanged(m_comment);
}

void Cart::setOrderTime(QString orderTime)
{
    qDebug()<<"order time >>"<< orderTime;
    if (m_orderTime == orderTime)
        return;


    m_orderTime = orderTime;
    emit orderTimeChanged(m_orderTime);

}



void Cart::setPayMethod(int payMethod)
{
    if (m_payMethod == payMethod)
        return;

    m_payMethod = payMethod;
    emit payMethodChanged(m_payMethod);
}

void Cart::setNoChange(bool noChange)
{
    if (m_noChange == noChange)
        return;

    m_noChange = noChange;
    emit noChangeChanged(m_noChange);
}

void Cart::setStreet(QString street)
{
    if (m_street == street)
        return;

    m_street = street;

    emit streetChanged(m_street);
}


void Cart::setEntrance(int entrance)
{
    if (m_entrance == entrance)
        return;

    m_entrance = entrance;
    emit entranceChanged(m_entrance);
}

void Cart::setFloor(int floor)
{

    if (m_floor == floor)
        return;

    m_floor = floor;

    emit floorChanged(m_floor);
}

void Cart::setRoom(int room)
{
    if (m_room == room)
        return;

    m_room = room;
    emit roomChanged(m_room);
}

void Cart::setCash(double cash)
{
    qDebug()<<cash;
    if (m_cash== cash)
        return;

    m_cash = cash;
    emit cashChanged(m_cash);
}

void Cart::onLoyalityDataLoaded(LoyalityList loyality)
{

    this->setLoyalityBonus(loyality.getBonusProductList());


    this->m_model->setProductsDiscount(loyality.getProductsDiscount());
    setCuponInfo(loyality.getPromocodeMessage());
    setCartInfo(loyality.getCartMessage());

}


void Cart::onLoyalityLoadError()
{

    this->onLoyalityDataLoaded(LoyalityList());

    QJsonObject promocodeMessage;
    promocodeMessage["type"] = 0;
    promocodeMessage["name"] = "Сервер купонов временно недоступен";
    promocodeMessage["description"] = "Купон будет обработан вручную";
    QJsonArray arr;
    arr.append(promocodeMessage);
    if(promocode() == ""){
        setCuponInfo(arr);

    }else{
        setCuponInfo(QJsonArray());
    }
}



void Cart::setDataProcessing(bool dataProcessing)
{
    if (m_dataProcessing == dataProcessing)
        return;

    m_dataProcessing = dataProcessing;
    emit dataProcessingChanged(m_dataProcessing);
}

void Cart::setStreetId(int streetId)
{
    if (m_streetId == streetId)
        return;

    m_streetId = streetId;
    emit streetIdChanged(m_streetId);
}

void Cart::setOrderType(QString orderType)
{

    if (m_orderType == orderType)
        return;

    m_orderType = orderType;
    if(orderType == "delivery"){
        setOrderTime(deliveryTime);
        setTime(deliveryTime);

    }else{
        setOrderTime(pickUpTime);
        setTime(pickUpTime);
    }
    emit orderTypeChanged(m_orderType);
}

void Cart::clear()
{
    this->setTime("");
    this->setOrderTime("");
    this->setDeliveryTime("");
    this->setPickUpTime("");
    this->m_model->reset();
    this->setStreetId(0);
    this->setStreet("");
    this->setBuilding("");
    this->setTakeAwayPoint(0);
    this->setPayMethod(-1);
    this->setCash(0);
    this->setBuilding(0);
    this->setFloor(0);
    this->setRoom(0);
    this->setEntrance(0);


}

void Cart::setValidCheckOut(bool validCheckOut)
{
    if (m_validCheckOut == validCheckOut)
        return;

    m_validCheckOut = validCheckOut;
    emit validCheckOutChanged(m_validCheckOut);
}

void Cart::setTotalCost(double cost)
{
    if(total_cost == cost)
        return ;

    this->total_cost = cost;
    if(toppings_enable()){
        this->updateToppingMaxQuantity(cost);
    }
    emit totalCostChanged(cost);
}

void Cart::setBuilding(QString building)
{
    if (m_building == building)
        return;

    m_building = building;
    emit buildingChanged(m_building);
}

void Cart::sendOrderToServer(int c)
{
    int payMethod = OrderInfo::PayMethodCash;
    /*if (c ==1) {
    payMethod = OrderInfo::PayMethodCard;
 }*/

    if (c >0) {
        payMethod = c;
    }
    OrderRequest request = this->getCurrentOrderRequest(payMethod);
    emit sendOrder(request);
}

void Cart::payByCard()
{
    CardData card;
    card.setCardNumber(cardNumber());
    card.setCardDate(cardDate());
    card.setCardCVC(cardCVC());
    OrderRequest request = this->getCurrentOrderRequest(OrderInfo::PayMethodOnline);
    emit payingByCard(request, card);
}

void Cart::payByMarket()
{
    int pay_methodMarket=OrderInfo::PayMethodOnline;
#if defined(Q_OS_ANDROID)
    pay_methodMarket = OrderInfo::PayMethodAndroidPay;
#elif defined(Q_OS_IOS)
    pay_methodMarket = OrderInfo::PayMethodApplePay;
#endif
    OrderRequest request = this->getCurrentOrderRequest(pay_methodMarket);
    emit payingByMarket(request);
}

void Cart::setCuponStatus(QJsonValue cuponStatus)
{
    if (m_cuponStatus == cuponStatus)
        return;

    m_cuponStatus = cuponStatus;
    emit cuponStatusChanged(m_cuponStatus);
}

void Cart::setBonus(QJsonValue bonus)
{
    if(bonus == m_bonus)
        return;
    m_bonus = bonus;
    emit bonusChanged(m_bonus.toArray());
}

void Cart::setDiscount(QJsonValue discount)
{
    if (m_discount == discount)
        return;

    m_discount = discount;
    emit discountChanged(m_discount);
}

void Cart::setCupon(QJsonObject cupon)
{
    if (m_cupon == cupon)
        return;

    m_cupon = cupon;
    emit cuponChanged(m_cupon);
}

void Cart::setEndCost(double end_cost)
{   
    if (qFuzzyCompare(m_end_cost, end_cost))
        return;

    m_end_cost = end_cost;
    emit endCostChanged(m_end_cost);
}

void Cart::setBonusCost(double bonusCost)
{   
    if (qFuzzyCompare(m_bonusCost, bonusCost))
        return;

    m_bonusCost = bonusCost;
    emit bonusCostChanged(m_bonusCost);
}

void Cart::setUserEmail(QString userEmail)
{
    if (m_userEmail == userEmail)
        return;

    m_userEmail = userEmail;
    emit userEmailChanged(m_userEmail);
}

void Cart::createCheckStreetEntity()
{
    CheckStreet data;
    CartProductList products;
    products.append(this->m_cart);
    CartProductList bonuses;
    bonuses.setData(this->m_bonus.toArray());
    products.append(bonuses);
    data.setProducts(products);
    data.setAddress(this->getCurrentOrderAddress());
    data.setTotalPrice(this->endCost());
    emit createdCheckStreetEntity(data);
}

void Cart::setStrets(StreetList streets)
{
    this->streets = streets;
}

void Cart::setBonusPay(double bonusPay)
{   
    if (qFuzzyCompare(m_bonusPay, bonusPay))
        return;

    m_bonusPay = bonusPay;
    emit bonusPayChanged(m_bonusPay);
}

void Cart::setAgreement(bool agreement)
{
    if (m_agreement == agreement)
        return;

    m_agreement = agreement;
    emit agreementChanged(m_agreement);
}

void Cart::setDeliveryCost(double deliveryCost)
{
    if (qFuzzyCompare(m_deliveryCost, deliveryCost))
        return;

    m_deliveryCost = deliveryCost;
    emit deliveryCostChanged(m_deliveryCost);
}

void Cart::setPaidDeliveryEntity(QJsonObject entity)
{
    this->m_paidDeliveryEntity = entity;
}

void Cart::setTime(QString time)
{
    qDebug()<<"time"<<time;

    if (m_time == time)
        return;
    m_time = time;
    emit timeChanged(m_time);
    if(orderType() == "delivery"){
        setDeliveryTime(time);
    }else{
        setPickUpTime(time);
    }
}

void Cart::setPromoCodeInfo(QString promoCodeInfo)
{
    if (m_promoCodeInfo == promoCodeInfo)
        return;

    m_promoCodeInfo = promoCodeInfo;
    emit promoCodeInfoChanged(m_promoCodeInfo);
}

void Cart::setPromoCodeName(QString promocodeName)
{
    if (m_promocodeName == promocodeName)
        return;

    m_promocodeName = promocodeName;
    emit promocodeNameChanged(m_promocodeName);
}

void Cart::setCuponName(QString cuponName)
{
    if (m_cuponName == cuponName)
        return;

    m_cuponName = cuponName;
    emit cuponNameChanged(m_cuponName);
}

void Cart::setCuponDescroption(QString cuponDescroption)
{
    if (m_cuponDescroption == cuponDescroption)
        return;

    m_cuponDescroption = cuponDescroption;
    emit cuponDescroptionChanged(m_cuponDescroption);
}

void Cart::setOfferValue(int offerValue)
{
    if (m_offerValue == offerValue)
        return;

    m_offerValue = offerValue;
    emit offerValueChanged(m_offerValue);
}

void Cart::onLoyalityLoadEmpty()
{


    setBonus(QJsonValue());
    setCuponInfo(QJsonValue());
    setCartInfo(QJsonValue());

}

void Cart::setCuponColor(QString cuponColor)
{
    if (m_cuponColor == cuponColor)
        return;

    m_cuponColor = cuponColor;
    emit cuponColorChanged(m_cuponColor);
}

void Cart::setLoyalityBonus(QJsonArray jArr)
{
    CartProductList bonusCart;
    for(int i =0; i< jArr.count();i++){
        auto object = jArr.at(i).toObject();
        bonusCart.append(Product(object));
    }
    this->setBonus(bonusCart.toJson());
}


void Cart::setCardNumber(QString cardNumber)
{
    if (m_cardNumber == cardNumber)
        return;

    m_cardNumber = cardNumber;
    emit cardNumberChanged(m_cardNumber);
}

void Cart::setCardDate(QString cardDate)
{
    if (m_cardDate == cardDate)
        return;

    m_cardDate = cardDate;
    emit cardDateChanged(m_cardDate);
}

void Cart::setCardCVC(QString cardCVC)
{
    if (m_cardCVC == cardCVC)
        return;

    m_cardCVC = cardCVC;
    emit cardCVCChanged(m_cardCVC);
}

OrderRequest Cart::getCurrentOrderRequest(int paymethod)
{
    auto orderRequest = getOrderToLoyality();
    auto order =orderRequest.getOrder();
    auto products =order.getProducts();
    for(int i = 0; i < this->bonus().count(); i++){
        products.append(CartProductEntity(this->bonus().at(i).toObject()));
    }
    order.setProducts(products);
    orderRequest.setOrder(order);

    auto orderInfo = orderRequest.getOrderInfo();
    orderInfo.setContactless_delivery(getContacles_delivery());
    if(!orderRequest.getPromocodeValid() && promocode() != ""){
        auto comment = orderInfo.getComment();
        auto cupon = orderInfo.getPromocode();

        orderInfo.setComment("{{ПРОВЕРИТЬ КУПОН-"+cupon +"}}\n" + comment);
        orderInfo.setPromocode("");

        orderRequest.setOrderInfo(orderInfo);
    }
    orderInfo.setPayMethod(paymethod);

    orderRequest.setOrderInfo(orderInfo);
    if(toppings_enable()){

    }
    return orderRequest;
}

OrderRequest Cart::getOrderToLoyality()
{
    OrderRequest result;
    result.setPlatform(this->getOsName());
    result.setTransactionMail(this->userEmail());
    result.setClient_toppings(m_toppingkit->getData());
    Order order;
    order.setProducts(this->m_cart);
    //    order.setClientToppings(m_toppingkit->getData());
    CartProductList bonuses;
    bonuses.setData(this->m_bonus.toArray());
    order.setTotalPrice(this->m_end_cost);
    OrderInfo orderInfo;
    orderInfo.setName(this->m_userName);
    orderInfo.setPhone(m_userPhone);
    orderInfo.setOrderType(m_orderType);
    orderInfo.setTakeAwayPoint(m_takeAwayPoint);
    orderInfo.setOrderTime(m_orderTime);
    orderInfo.setTime(this->time());
    orderInfo.setPerson(m_personCount);
    orderInfo.setCash(m_cash);
    orderInfo.setNoChange(this->m_noChange);


    result.setPromocodeValid(promocodeValid);

    orderInfo.setComment(comment());
    orderInfo.setPromocode(promocode());

    orderInfo.setEmail(m_userEmail);
    orderInfo.setBonusPay(this->m_bonusPay);
    orderInfo.setPaidDelivery(this->m_paidDeliveryEntity);
    orderInfo.setPayMethod(m_payMethod);

    result.setOrder(order);
    result.setAddress(this->getCurrentOrderAddress());
    result.setOrderInfo(orderInfo);
    return result;
}

void Cart::setToppingData(QJsonArray jArr)
{

    m_toppingkit->setToppingData(jArr);
}

QString Cart::name() const
{
    return m_name;
}

bool Cart::toppings_enable() const
{
    return m_toppings_enable;
}

void Cart::sendOrderSendSignal()
{
    emit orderCanSend();
}

void Cart::setCuponInfo(QJsonValue cuponInfo)
{
    if(this->promocode() == ""){
        setPromocodeValid(true);
        cuponInfo = QJsonArray();


    }else{

        if(cuponInfo.toArray().count() == 0)
        {
            QJsonArray arr;
            QJsonObject message;
            message["name"] = "";
            message["description"] = "Купон будет обработан вручную с уведомлением по телефону или в СМС.";
            message["type"] = 2;
            arr.append(message);

            cuponInfo = arr;
            setPromocodeValid(false);
        }
        else{

            setPromocodeValid(true);
        }
    }
    if (m_cuponInfo == cuponInfo)
        return;

    m_cuponInfo = cuponInfo;



    emit cuponInfoChanged(m_cuponInfo.toArray());



}

void Cart::setCartInfo(QJsonValue cartInfo)
{
    if (m_cartInfo == cartInfo)
        return;

    m_cartInfo = cartInfo;
    emit cartInfoChanged(m_cartInfo.toArray());
}

void Cart::setname(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void Cart::settoppings_enable(bool toppings_enable)
{
    if (m_toppings_enable == toppings_enable)
        return;

    m_toppings_enable = toppings_enable;
    emit toppings_enableChanged(m_toppings_enable);
}

void Cart::sendORderToLoyality()
{

}

void Cart::updateToppingMaxQuantity(int total_cost)
{

    if(toppings_enable()){
        m_toppingkit->processToppingsByTotalCost(total_cost);
    }
}


OrdeAddress Cart::getCurrentOrderAddress()
{
    OrdeAddress orderAddres;
    if(m_streetId!=0){
        orderAddres.setStreet(QString::number(m_streetId));
        orderAddres.setStreetEntity(this->streets.findById(m_streetId));
    }
    else orderAddres.setStreet(m_street);
    orderAddres.setBuilding(m_building);
    orderAddres.setEntrance(m_entrance);
    orderAddres.setRoom(m_room);
    orderAddres.setFloor(m_floor);
    return orderAddres;
}

QString Cart::getPickUpTime() const
{
    return pickUpTime;
}

void Cart::setPickUpTime(const QString &value)
{
    qDebug()<<"pick up time << "<< value;
    pickUpTime = value;
}

QString Cart::getDeliveryTime() const
{
    return deliveryTime;
}

void Cart::setDeliveryTime(const QString &value)
{
    qDebug()<<"newDeliveryTime " << value;
    deliveryTime = value;
}

bool Cart::getContacles_delivery() const
{
    return contacles_delivery;
}

void Cart::setContacles_delivery(bool value)
{
    contacles_delivery = value;
}

bool Cart::getPromocodeValid() const
{
    return promocodeValid;
}

void Cart::setPromocodeValid(bool value)
{
    promocodeValid = value;
}

QString Cart::getOsName()
{
#if defined(Q_OS_ANDROID)
    return QLatin1String("android");
#elif defined(Q_OS_BLACKBERRY)
    return QLatin1String("blackberry");
#elif defined(Q_OS_IOS)
    return QLatin1String("ios");
#elif defined(Q_OS_MACOS)
    return QLatin1String("macos");
#elif defined(Q_OS_TVOS)
    return QLatin1String("tvos");
#elif defined(Q_OS_WATCHOS)
    return QLatin1String("watchos");
#elif defined(Q_OS_WINCE)
    return QLatin1String("wince");
#elif defined(Q_OS_WIN)
    return QLatin1String("windows");
#elif defined(Q_OS_LINUX)
    return QLatin1String("linux");
#elif defined(Q_OS_UNIX)
    return QLatin1String("unix");
#else
    return QLatin1String("unknown");
#endif
}

double Cart::totalCost() const
{
    return total_cost;
}

QJsonValue Cart::cuponStatus() const
{
    return m_cuponStatus;
}

QJsonArray Cart::bonus() const
{
    return m_bonus.toArray();
}

QJsonValue Cart::discount() const
{
    return m_discount;
}

QJsonObject Cart::cupon() const
{
    return m_cupon;
}

double Cart::endCost() const
{
    return m_end_cost;
}

double Cart::bonusCost() const
{
    return m_bonusCost;
}

QString Cart::userEmail() const
{
    return m_userEmail;
}

double Cart::bonusPay() const
{
    return m_bonusPay;
}

bool Cart::agreement() const
{
    return m_agreement;
}

double Cart::deliveryCost() const
{
    return m_deliveryCost;
}

QString Cart::time() const
{
    return m_time;
}

QString Cart::promoCodeInfo() const
{
    return m_promoCodeInfo;
}

QString Cart::cuponName() const
{
    return m_cuponName;
}

QString Cart::cuponDescroption() const
{
    return m_cuponDescroption;
}

int Cart::offerValue() const
{
    return m_offerValue;
}

QString Cart::cuponColor() const
{
    return m_cuponColor;
}

QJsonValue Cart::cuponInfo() const
{
    return m_cuponInfo;
}

QJsonValue Cart::cartInfo() const
{
    return m_cartInfo;
}






