#include "fileloader.h"
#include <QUrl>
#include <QNetworkReply>
#include <QDebug>
#include <QFile>
#include <QDir>
FileLoader::FileLoader(QObject *parent) : QObject(parent)
{
    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, &QNetworkAccessManager::finished, this, &FileLoader::onResult);
}

QString FileLoader::getPathToLoad() const
{
    return pathToLoad;
}

void FileLoader::setPathToLoad(const QString &value)
{
    pathToLoad = value;
}

QUrl FileLoader::getUrlFromLoad() const
{
    return urlFromLoad;
}

void FileLoader::setUrlFromLoad(const QUrl &value)
{
    urlFromLoad = value;
}

bool FileLoader::fileExists(QString fileName)
{
    QFile file(this->pathToLoad+"/"+fileName);
    return file.exists();
}

void FileLoader::load(QStringList files)
{
    QStringList loadFiles = files;
    loadFiles.removeDuplicates();
    QDir dir(this->pathToLoad);
    QStringList iseetFiles = dir.entryList(QStringList(), QDir::Files);
    this->files= this->getAddedFiles(loadFiles, iseetFiles);
    this->pos =0;
    this->loadFile(0);
    this->removeFiles(this->getRemoveFiles(loadFiles, iseetFiles));
}

void FileLoader::onResult(QNetworkReply *reply)
{
    if(!reply->error()){
        QByteArray data = reply->readAll();
        QFile file(this->pathToLoad+"/"+reply->url().fileName());
        if(!file.open(QIODevice::WriteOnly))
        {
            qDebug()<< "не могу открыть файл для записи";
            return;
        }
        if(!file.write(data))
        {
           qDebug()<< "не могу записайть в файл";
            return;
        }
    }
    else
    {
        qDebug("ошибка загрузки файла");
        //emit error("Ошибка загрузки файла");
    }
    this->loadFile(this->pos++);
}

void FileLoader::loadFile(int i)
{

    if(this->files.count()==i ||this->files.count()<i ){
        emit filesLoaded();
        return;
    }
    QUrl url=this->urlFromLoad;
    url.setPath(url.path()+ this->files.at(i));
    QNetworkRequest request(url);
    networkManager->get(request);
}

QStringList FileLoader::getAddedFiles(QStringList filesLoad, QStringList isset)
{
    QStringList result;
    for (int i=0;i<filesLoad.count();i++) {
        QString file =filesLoad.at(i);
        if(!isset.contains(file)){
            result.append(file);
        }

    }    
    return result;
}

QStringList FileLoader::getRemoveFiles(QStringList filesLoad, QStringList isset)
{
    QStringList result;
    for (int i=0;i<isset.count();i++) {
        QString file = isset.at(i);
        if(!filesLoad.contains(file)){
            result.append(file);
        }

    }
    return result;

}

void FileLoader::removeFiles(QStringList files)
{
    for (int i=0;i<files.count();i++) {        
         QFile file(this->pathToLoad+"/"+files.at(i));
         file.remove();
    }
}
