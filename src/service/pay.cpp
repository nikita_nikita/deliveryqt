#include "pay.h"
#include <QJsonDocument>
#include "./src/entity/pay/cpresponse.h"
#include <QUrlQuery>
#include <QNetworkInterface>
#include "../config/settingapp.h"
#include <QtQuick>
#include <QDebug>
Pay::Pay(QObject *parent) : QObject(parent)
{
    checkoutManager = new QNetworkAccessManager(this);
    connect(checkoutManager, &QNetworkAccessManager::finished, this, &Pay::onCheckoutResult);
    connect(&PayByPlatform::instance().connecter(), &PayConnecter::errorPay, this, &Pay::errorPayMarket);
    connect(&PayByPlatform::instance().connecter(), &PayConnecter::tokenCreated, this, &Pay::setPayMartketToken);
    connect(&PayByPlatform::instance().connecter(), &PayConnecter::abortingPay, this, &Pay::abortPay);
}

void Pay::PayMarket(OrderRequest request)
{

     request.setCityId(this->cityId);
     request.setToken(this->tmp_token);
     request.setMac(this->getMacAddress());



     QString token;
      request.setCityId(this->cityId);
     this->tmp_request = request;
    double  total_cost = request.getOrder().getTotalPrice()-request.getOrderInfo().getBonusPay(); // стоимость с вычетом бонуса
    qDebug()<<"total cost"<<request.getOrder().getTotalPrice()<<" "<<request.getOrderInfo().getBonusPay();
    #ifdef Q_OS_ANDROID
    emit startLoaded();
    token = PayByPlatform::instance().getGooglePayToken(QString::number(total_cost));
    #elif defined(Q_OS_IOS)
    emit startLoaded();
    token = PayByPlatform::instance().getApplePayToken(total_cost, this->domain, request);
    #endif

}

void Pay::PayCard(OrderRequest request, CardData card)
{
   emit startLoaded();
    QString cardNumber = card.getCardNumber().replace(" ", "");
    QString expDate = card.getCardDate().replace("/", "").replace(" ", "");
    QString cvc = card.getCardCVC().replace(" ", "");
    PayByPlatform::instance().setData(cardNumber, expDate, cvc);

    if(!PayByPlatform::instance().isValidNumber()){
        emit error("В номере карты допущены ошибки");
        return;
    }

    if(!PayByPlatform::instance().isValidExpDate()){
        emit error("Карта просрочена или неверно указан срок действия");
        return;
    }

    QString crypto = PayByPlatform::instance().cardCryptogram();
    if(crypto.count()==0){
        crypto = PayByPlatform::instance().cardCryptogram();
        if(crypto.count()==0){
                emit error("Ошибка платежной системы");
            return;
        }
    }
    request.setCityId(this->cityId);
    request.setToken(this->tmp_token);
    request.setMac(this->getMacAddress());
    CPCheckOut checkout;

     double  total_cost = request.getOrder().getTotalPrice()-request.getOrderInfo().getBonusPay(); // стоимость с вычетом бонуса
    checkout.setAmount(total_cost);
    checkout.setEmail(request.getOrderInfo().getEmail());
    checkout.setCurrency(cityData.getPayments().getCurrency());
    checkout.setCardCryptogramPacket(crypto);
    checkout.setOrderRequest(request);

    this->sendCheckout(checkout);


}
void Pay::setCityDataJson(CityData cityData)
{
    this->cityData = cityData;
    PayByPlatform::instance().setPublicId(cityData.getPayments().getPublicid());
    PayByPlatform::instance().setCurrency(cityData.getPayments().getCurrency());
}

void Pay::sendCheckout(CPCheckOut checkout)
{

    QUrl url;
    url.setHost("");

    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonObject testResult= checkout.toJson();
    QJsonDocument doc(checkout.toJson());
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    checkoutManager->post(request, doc.toJson(QJsonDocument::Compact));
}

void Pay::setDomain(QString value)
{
    this->domain = value;

}

void Pay::setCity_id(int value)
{
    this->cityId = value;
}

void Pay::sendThreeDs(QString AcsUrl, QString PaReq, QString md)
{

    QUrl createUrl;
    createUrl.setHost("mob."+this->domain);
    createUrl.setScheme(SettingApp::defaultSheme());
    createUrl.setPath("/order/payform");
    QUrlQuery query;
    query.addQueryItem("a", AcsUrl);
    query.addQueryItem("p", PaReq);
    query.addQueryItem("m", md);
    createUrl.setQuery(query);

    emit openUrl(createUrl.toString(QUrl::FullyEncoded).replace("+","%2b"));


}

void Pay::checkPayUrl(QString url_string)
{
    QUrl url(url_string);
    if(url.path().contains("order/paystatus")){
        QUrlQuery query(url.query());
        if(query.queryItemValue("s").toInt()==0){
            emit errorPay(query.queryItemValue("m"));
        } else {
            emit success();
        }

    }

}

void Pay::setTokenLk(QString token)
{
  this->tmp_token = token;
}



void Pay::onCheckoutResult(QNetworkReply *reply)
{
    if(!reply->error()){
        QByteArray s_result = reply->readAll();
        QJsonDocument document = QJsonDocument::fromJson(s_result);
        QJsonObject root = document.object();
        CPResponse res(root);
        if(res.getSuccess()) {
            emit success();
            qDebug() << "Успех";
            return;
        }
        if(res.getMessage().length()>0) {
            emit errorPay(res.getMessage());
            return;
        }
        if(res.getModel().getReasonCode()!=0){
            emit errorPay(res.getModel().getCardHolderMessage());
            return;
        }
        if(res.is3DSecure()) {
            this->sendThreeDs(res.getModel().getAcsUrl(), res.getModel().getPaReq(), res.getModel().getTransactionId());
            return ;
        }
        emit errorPay("Неизвесная ошибка");

    }
    else
    {
        QByteArray text = reply->readAll();
        emit error(text);
    }

}

void Pay::errorPayMarket()
{
   emit errorPay("Ошибка оплаты");
}

void Pay::setPayMartketToken(QString token)
{

    CPCheckOut checkout;
     double  total_cost = tmp_request.getOrder().getTotalPrice()-tmp_request.getOrderInfo().getBonusPay(); // стоимость с вычетом бонуса
    checkout.setAmount(total_cost);
    checkout.setEmail(this->tmp_request.getOrderInfo().getEmail());
    checkout.setCurrency(cityData.getPayments().getCurrency());
    checkout.setCardCryptogramPacket(token);
    checkout.setOrderRequest(this->tmp_request);    
    checkout.setName(payByMarketName);
    this->sendCheckout(checkout);
}

void Pay::abortPay()
{
    emit endLoaded();
}

QString Pay::getMacAddress()
{
    QString text="00:00:00:00:00:00";
       foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces())
       {
           if(interface.hardwareAddress()!=text) {
               return interface.hardwareAddress();
           }
       }
       return text;
}


