#include "kapibaras.h"
#include <QUrl>
#include <QJsonDocument>
#include <QJsonArray>
#include <QUrlQuery>
#include <QDir>
#include <QFile>
#include <QNetworkInterface>
#include "../config/settingapp.h"
#include <QSysInfo>
#include <QMessageAuthenticationCode>
#include <QCryptographicHash>
Kapibaras::Kapibaras(QObject *parent) : QObject(parent)
{
    streetManager = new QNetworkAccessManager(this);
    promocodeManager  = new QNetworkAccessManager(this);
    sendOrderManager  = new QNetworkAccessManager(this);
    loyalityManager = new QNetworkAccessManager(this);
    fileLoader = new FileLoader(this);
    cuponManager = new QNetworkAccessManager(this);
    checkStreetManager = new QNetworkAccessManager(this);
    lkManager = new QNetworkAccessManager(this);
    lkManagerSave = new QNetworkAccessManager(this);
    citysManager  = new QNetworkAccessManager(this);
    cityDataManager  = new QNetworkAccessManager(this);
    versionManager  = new QNetworkAccessManager(this);
    clientDataManager = new QNetworkAccessManager(this);
    imageDownloadmanager = new QNetworkAccessManager(this);
    organizationInfoManager = new QNetworkAccessManager(this);
    onliHistoyClientManager = new QNetworkAccessManager(this);
    mainListManager = new QNetworkAccessManager(this);
    connect(fileLoader, &FileLoader::filesLoaded, this, &Kapibaras::filesLoaded);
    connect(fileLoader, &FileLoader::error, this, &Kapibaras::error);
    connect(streetManager,     &QNetworkAccessManager::finished, this, &Kapibaras::onStreetListLoaded);
    connect(promocodeManager,  &QNetworkAccessManager::finished, this, &Kapibaras::onPromocodeLoaded);
    connect(sendOrderManager,  &QNetworkAccessManager::finished, this, &Kapibaras::onSendOrder);
    connect(checkStreetManager,&QNetworkAccessManager::finished, this, &Kapibaras::onCheckStreet);
    connect(lkManagerSave,     &QNetworkAccessManager::finished, this, &Kapibaras::onSaveClient);
    connect(citysManager,      &QNetworkAccessManager::finished, this, &Kapibaras::onCityLoaded);
    connect(cityDataManager,   &QNetworkAccessManager::finished, this, &Kapibaras::onCityDataLoaded);
    connect(versionManager,    &QNetworkAccessManager::finished, this, &Kapibaras::onCurrentVersionLoaded);
    connect(clientDataManager, &QNetworkAccessManager::finished, this, &Kapibaras::onLoadedClientData);
    connect(loyalityManager,   &QNetworkAccessManager::finished,this,&Kapibaras::onLoyalityInfoLoaded);
    connect(imageDownloadmanager, &QNetworkAccessManager::finished, this,&Kapibaras::onOrderImageLoad);
    connect(organizationInfoManager, &QNetworkAccessManager::finished,this, &Kapibaras::onOrganizationInfoManager);
    connect(onliHistoyClientManager,&QNetworkAccessManager::finished,this, &Kapibaras::onClientHystoryLoaded);
//    connect(mainListManager, &QNetworkAccessManager::finished,this,&Kapibaras::onByCityListLoaded);
    m_timer = new QTimer(this);

    connect(m_timer,&QTimer::timeout,this,&Kapibaras::updateClientData);
}

void Kapibaras::loadCitys(QString domein)
{
    QUrl url(SettingApp::defaultUrl(domein));
    setDomain("");
    url.setPath("");
    QNetworkRequest request(url);
    citysManager->get(request);
}

void Kapibaras::loadCityData()
{
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QUrlQuery query;
    query.addQueryItem("", QString::number(this->getCity_id()));

    url.setQuery(query);
    QNetworkRequest request(url);
    emit loadingCityData();
    cityDataManager->get(request);
}

void Kapibaras::loadStreetList()
{
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QUrlQuery query;
    query.addQueryItem("sity", QString::number(this->getCity_id()));
    url.setQuery(query);
    QNetworkRequest request(url);
    streetManager->get(request);
}

void Kapibaras::loadCurrentVersion()
{
    QUrl url(SettingApp::defaultUrl("ru"));
    url.setPath("/version");
    QNetworkRequest request(url);
    versionManager->get(request);
}

void Kapibaras::loadPromocode(QString promocode, QString phone)
{
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonObject postJson;
    postJson["promocode"] = promocode;
    postJson["city"] = this->getCity_id();
    postJson["mac"] = this->getMacAddress();
    postJson["phone"] = phone;
    QJsonDocument doc(postJson);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    promocodeManager->post(request, doc.toJson(QJsonDocument::Compact));
}

void Kapibaras::onCityLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonArray root = document.array();
        emit citysLoaded(CityList(root));
    }
    else
    {
        qDebug()<< " ошибка загрузки списка городов";
        emit error("Ошибка загрузки списка городов");
    }

}

void Kapibaras::onCityDataLoaded(QNetworkReply *reply)
{    
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root = document.object();
        this->tmpCityData = CityData(root);
        this->fileLoader->setPathToLoad(this->tmpPathUploads+"/"+folderName);
        QUrl url;
        url.setScheme(SettingApp::defaultSheme());
        url.setHost("");
        url.setPath(this->tmpCityData.getConfig().getGallery().getUrl());
        this->fileLoader->setUrlFromLoad(url);
        this->setCountry(document.object().value("config").toObject().value("country").toString());
        emit cityDataLoaded(this->tmpCityData);
        this->setUrlImage(url.toString());
        QJsonObject clearCityDataData;
        clearCityDataData["status"] = false;
        emit checkStreetLoaded(clearCityDataData);

    }
    else
    {
        qDebug()<< "Ошибка загрузки данных города";
    }

}

void Kapibaras::onStreetListLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonArray json = document.array();
        emit streetLoaded(StreetList(json));
    }
    else
    {
        emit error("Ошибка загрузки улиц");
    }
}

void Kapibaras::onPromocodeLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root = document.object();
        emit cuponLoaded(root);
    }
    else
    {
        emit cuponLoaded(QJsonObject{{"status", false}});
    }
}

void Kapibaras::onSendOrder(QNetworkReply *reply)
{
    if(!reply->error()){
        QString  res = reply->readAll();
        auto doc = QJsonDocument::fromJson(reply->readAll()).object();
        emit orderSended();
    }
    else
    {
        emit error("Ошибка отправки заказа на сервер");
        this->writeLog("onSendOrder.html", reply->readAll());
    }
    emit endLoaded();
}

void Kapibaras::onCheckStreet(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root  = document.object();
        qDebug()<<"need this >>>>>>>>>>>"<<document;
        emit checkStreetLoaded(root);
    }
    else
    {

        qDebug() << "ошибка проверки улицы на сервере ";
    }
}

void Kapibaras::onAuthResult(QNetworkReply *reply)
{
    disconnect(lkManager, &QNetworkAccessManager::finished, this, &Kapibaras::onAuthResult);
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root  = document.object();
        bool status = root["status"].toInt();
        QString token= root["token"].toString();
        emit authLoaded(status, token);

    }
    else
    {
        emit authLoaded(false, "");
    }

}

void Kapibaras::onLoadedClientData(QNetworkReply *reply)
{
    QByteArray bytedata= reply->readAll();
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(bytedata);
        QJsonObject root = document.object();
        emit clientDataLoaded(root);
        this->setStatusClientDataLoad(false);
    }
    else
    {
        emit errorLoadClientData();
    }

    this->writeLog("loadclietnData.html", bytedata);
}


void Kapibaras::onCurrentVersionLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root = document.object();

        emit currentVersionLoaded(VersionApp(root["current"].toObject()));
    }
    else
    {
        //marker
        emit error("Ошибка проверки версии");
        qDebug()<< "------Ошибка проверки версии" << reply->errorString();
    }
}

void Kapibaras::onSaveClient(QNetworkReply *reply)
{
    Q_UNUSED(reply)
    this->loadClientData(tmp_tokenlk);
}

void Kapibaras::filesLoaded()
{
    //пока не надо
    //  emit cityDataLoaded(this->tmpCityData);
}

void Kapibaras::CheckResult(QNetworkReply *reply)
{
    if(!reply->error()){
        qDebug() << "сервер ответил  удачно";
    }
    else
    {
        qDebug() << "сервер ответил  неудачно";
    }
}

void Kapibaras::onLoyalityInfoLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        LoyalityList result;
        auto json = QJsonDocument::fromJson(reply->readAll());
        result.setJson(json.array());

        emit loyalityLoaded(result);

    }else{
        emit loyalityLoadError();
    }


}

void Kapibaras::onOrganizationInfoManager(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root = document.object();
        OrganizationInfo info(root.value("organizaton_info").toObject());
        WorkTimeEntity workTime(root.value("work_time").toObject());

        emit organizationInfoloaded(info,workTime);
    }

}

void Kapibaras::onOrderImageLoad(QNetworkReply *reply)
{
    qDebug()<<"i am Monkey";
    qDebug()<<reply->readAll();

}

void Kapibaras::onClientHystoryLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        auto hisory = QJsonDocument::fromJson(reply->readAll()).object().value("client_history").toArray();
        emit clientHystoryLoaded(hisory);
    }

}

void Kapibaras::processCityList(CityList list)
{
    for(int i =0; i < m_cityList.count();i++){
        auto city = m_cityList.at(i);
        if(city.getEnable()==0&&city.getHrefVisible()!=0){
            auto citySecond = list.cityByid(city.getId());
            if(citySecond.getId()!= -1){
                qDebug()<<"              m_cityList.replace(i,citySecond);";
                m_cityList.replace(i,citySecond);
            }
        }
    }

}



QString Kapibaras::createJWT(QString phone)
{
    QJsonObject obj;

    obj["phone"] = phone;
    obj["exp"] = 120000;

    return "";

}

void Kapibaras::setTokenLk(QString token)
{
    this->tmp_tokenlk = token;
}

void Kapibaras::setStatusClientDataLoad(bool statusClientDataLoad)
{
    qDebug()<<"sfar "<< statusClientDataLoad;
    if (m_statusClientDataLoad == statusClientDataLoad)
        return;

    m_statusClientDataLoad = statusClientDataLoad;
    emit statusClientDataLoadChanged(m_statusClientDataLoad);
}

void Kapibaras::updateClientData()
{
    if(m_timer->isActive()){
        m_timer->stop();
    }
    this->loadClientData(this->tmp_tokenlk);
}

void Kapibaras::downloadImageToCallback(QImage image)
{
    QByteArray arr;
    QBuffer buffer(&arr);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "yourformat");
    QUrl url;
    url.setHost("mob."+this->domain);
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("/order/loadCommentImage");
    QNetworkRequest request(url);
    qDebug()<<"i am hear";
    request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data; boundary=<calculated when request is sent>");
    promocodeManager->post(request, arr);

}

void Kapibaras::loadClientDataAfterMinute()
{
    m_timer->start(60000);

}



void Kapibaras::checkFolder()
{
    QDir tmp(this->tmpPathUploads);

    QDir uploads(this->tmpPathUploads+"/"+folderName);
    if(tmp.exists()){
        if(!uploads.exists()){
            if(!tmp.mkdir(this->folderName)){
                qDebug() << "не смог создать папку" ;
            }
        }
    }
}

QString Kapibaras::getMacAddress()
{
    QString text="00:00:00:00:00:00";
    QString dis="02:00:00:00:00:00";
    foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces())
    {
        QString mac = interface.hardwareAddress();
        if(mac!=text&&mac!=""&&mac!=dis) {
            return mac;
        }
    }
    return QString(QSysInfo::bootUniqueId());
}

void Kapibaras::writeLog(QString filename, QByteArray data)
{
#ifdef QT_DEBUG
    QFile file(SettingApp::logPath()+filename);
    if (file.open(QIODevice::WriteOnly)){
        file.write(data);
        file.close();
    } else {
        //        qDebug() <<"writeLog: не могу открыть файл для записи. ";
    }
#else
    Q_UNUSED(filename)
    Q_UNUSED(data)
#endif
}

QString Kapibaras::getCountry() const
{
    return country;
}

void Kapibaras::loadClientHistory()
{


    //this->sendFireBaseToketen();
    QJsonObject json;
    json["token"] = this->tmp_tokenlk ;
    json["city_id"] = this->city_id;
    json["mac"] = this->getMacAddress();
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(json);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    onliHistoyClientManager->post(request, doc.toJson(QJsonDocument::Compact));
    //    this->setStatusClientDataLoad(true);

}

void Kapibaras::setCountry(const QString &value)
{
    country = value;
}



QString Kapibaras::getTmpPathUploads() const
{
    return tmpPathUploads;
}

void Kapibaras::setTmpPathUploads(const QString &value)
{
    tmpPathUploads = value;
    this->checkFolder();
}

QString Kapibaras::urlImage() const
{
    return m_urlImage;
}

bool Kapibaras::fileExists(QString fileName)
{
    return fileLoader->fileExists(fileName);
}

bool Kapibaras::statusClientDataLoad() const
{
    return m_statusClientDataLoad;
}



int Kapibaras::getCity_id() const
{
    return city_id;
}

void Kapibaras::loadWorkTimeAndOrganizationInfo()
{
    qDebug()<<"loadWorkTime";
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QUrlQuery query;
    query.addQueryItem("", QString::number(this->getCity_id()));
    url.setQuery(query);
    QNetworkRequest request(url);
    organizationInfoManager->get(request);
}

QString Kapibaras::getDefaultSheme()
{
    return SettingApp::defaultSheme();
}

void Kapibaras::setCity_id(int value)
{
    city_id = value;
    emit cityIdChanged(city_id);
}

void Kapibaras::sendOrder(OrderRequest order)
{
    emit startLoaded();
    order.setToken(this->tmp_tokenlk);
    order.setCityId(this->city_id);
    order.setMac(this->getMacAddress());
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);

    QJsonDocument doc(order.toJson());
    qDebug()<<order.getOrder().getProducts().toJson();
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

    sendOrderManager->post(request, doc.toJson(QJsonDocument::Compact));
    //    this->writeLog("sendOrder.json", doc.toJson(QJsonDocument::Compact));

}

void Kapibaras::setUrlImage(QString urlImage)
{
    if (m_urlImage == urlImage)
        return;

    m_urlImage = urlImage;
    emit urlImageChanged(m_urlImage);
}

void Kapibaras::checkStreet(CheckStreet data)
{
    data.setCity(this->city_id);
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(data.toJson());
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    checkStreetManager->post(request, doc.toJson(QJsonDocument::Compact));
    this->writeLog("checkStreet",doc.toJson(QJsonDocument::Compact));
}

void Kapibaras::loadFiles()
{
    this->fileLoader->load(this->tmpCityData.getFileNames());
}

void Kapibaras::sendOrderToLoyality(OrderRequest order)
{
    order.setToken(this->tmp_tokenlk);
    order.setCityId(this->city_id);
    order.setMac(this->getMacAddress());

    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath(pathToLoyality);
    QNetworkRequest request(url);
    QJsonDocument doc(order.toJson());
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

    writeLog("ss.doc",doc.toJson(QJsonDocument::Compact));
    loyalityManager->post(request, doc.toJson(QJsonDocument::Compact));



}

void Kapibaras::sendSmsCode(QString phone)
{
    QJsonObject json;
    json["phone"] = phone;
    json["city"] = this->city_id;
    json["mac"] = this->getMacAddress();
    json["token"] = this->getMacAddress();



    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(json);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    lkManager->post(request, doc.toJson(QJsonDocument::Compact));

}

void Kapibaras::auth(QString phone, QString code)
{
    QJsonObject json;
    json["phone"] = phone;
    json["code"] = code;
    json["mac"] = this->getMacAddress();
    QUrl url;
    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(json);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    connect(lkManager, &QNetworkAccessManager::finished, this, &Kapibaras::onAuthResult);
    lkManager->post(request, doc.toJson(QJsonDocument::Compact));
}

void Kapibaras::loadClientData(QString token)
{   
    if (token.length()==0){
        qDebug()<<"Return";

        return;
    }
    if( this->tmp_tokenlk != token){
        this->tmp_tokenlk = token;
    }

    //this->sendFireBaseToketen();
    QJsonObject json;
    json["token"] = token;
    json["city_id"] = this->city_id;
    json["mac"] = this->getMacAddress();
    QUrl url;
    url.setHost("mob."+this->domain);
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(json);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    clientDataManager->post(request, doc.toJson(QJsonDocument::Compact));
    this->setStatusClientDataLoad(true);

}

void Kapibaras::saveClient(QJsonValue client_info, QString token)
{    
    if(token.length()==0) return;
    if( this->tmp_tokenlk != token){
        this->tmp_tokenlk = token;
    }
    QJsonObject json;
    json["token"] = token;
    json["client"] = client_info;
    json["mac"] = this->getMacAddress();
    QUrl url;

    url.setHost("");
    url.setScheme(SettingApp::defaultSheme());
    url.setPath("");
    QNetworkRequest request(url);
    QJsonDocument doc(json);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    lkManagerSave->post(request, doc.toJson(QJsonDocument::Compact));
}



QString Kapibaras::getDomain() const
{
    return domain;
}

void Kapibaras::setDomain(QString value)
{
    domain = value;
    emit domainChanged(domain);
}
