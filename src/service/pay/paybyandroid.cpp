#include "paybyandroid.h"
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroid>
#include <QAndroidJniEnvironment>
#include <QDebug>
PayByAndroid::PayByAndroid()
{


}

void PayByAndroid::setData(QString number, QString expDate, QString cvv)
{
    this->m_number = number;
    this->m_expDate = expDate;
    this->m_cvv = cvv;

}

bool PayByAndroid::isValidNumber()
{

    QAndroidJniObject cardNumber = QAndroidJniObject::fromString(this->m_number);
    jboolean isValid = QAndroidJniObject::callStaticMethod<jboolean>("mob/ru/kapibaras/CPCard", "isValidNumber","(Ljava/lang/String;)Z", cardNumber.object<jstring>());

    return isValid;
}

bool PayByAndroid::isValidExpDate()
{
    QAndroidJniObject expDate = QAndroidJniObject::fromString(this->m_expDate);
    jboolean isValid = QAndroidJniObject::callStaticMethod<jboolean>("mob/ru/kapibaras/CPCard", "isValidExpDate","(Ljava/lang/String;)Z", expDate.object<jstring>());
    return isValid;
}

QString PayByAndroid::cardCryptogram()
{
    QAndroidJniObject cardNumber = QAndroidJniObject::fromString(this->m_number);
    QAndroidJniObject cardDate = QAndroidJniObject::fromString(this->m_expDate);
    QAndroidJniObject cardCVC = QAndroidJniObject::fromString(this->m_cvv);
    QAndroidJniObject* cardClass =  new QAndroidJniObject("mob/ru/kapibaras/CPCard", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                                                          cardNumber.object<jstring>(),
                                                          cardDate.object<jstring>(),
                                                          cardCVC.object<jstring>()
                                                          );
    QAndroidJniObject publickId = QAndroidJniObject::fromString(this->publicId);

    QAndroidJniObject type = cardClass->callObjectMethod("cardCryptogram", "(Ljava/lang/String;)Ljava/lang/String;", publickId.object<jstring>());

    QAndroidJniEnvironment env;
      if (env->ExceptionCheck()) {
          qDebug()<< "Ошибка получения токена для  платежной системы";
          env->ExceptionClear();
           return "";
      }
    return type.toString();

}

QString PayByAndroid::getGooglePayToken(QString price)
{
    this->init();

    QAndroidJniObject publickId = QAndroidJniObject::fromString(this->publicId);
    QAndroidJniObject priceO  = QAndroidJniObject::fromString(price);
    QAndroidJniObject currency = QAndroidJniObject::fromString(this->currency);

    QAndroidJniObject activity = QtAndroid::androidActivity();

    activity.callMethod<void>("setData", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                              publickId.object<jstring>(),
                              priceO.object<jstring>(),
                              currency.object<jstring>());
    activity.callMethod<void>("requestPayment", "()V");

    return "";
}

void PayByAndroid::activityHandler(JNIEnv *env, jobject thiz, jint resultCode)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)


    switch (resultCode) {
    case -1:{
        QAndroidJniObject activity = QtAndroid::androidActivity();
        QAndroidJniObject object = activity.callObjectMethod("getToken", "()Ljava/lang/String;");
        PayByPlatform::instance().connecter().sendToken(object.toString());
        break;
    }
    case 0:
        PayByPlatform::instance().connecter().abortPay();
        break;
    case 1:
        PayByPlatform::instance().connecter().sendErrorPay();
        break;

    }
  //  QAndroidJniEnvironment env;
      if (env->ExceptionCheck()) {
          qDebug()<< "Ошибка оплаты через гугл pay";
          env->ExceptionClear();

      }

}

void PayByAndroid::tokenCreate()
{
    qDebug() << "token create";
}

void PayByAndroid::init()
{
    if(this->m_init) {
        return;
    }

    JNINativeMethod methods[] {{"activityHandler", "(I)V", reinterpret_cast<void *>(activityHandler)}};

    QAndroidJniEnvironment env;
    jclass objectClass = env->GetObjectClass(QtAndroid::androidActivity().object<jobject>());
    env->RegisterNatives(objectClass,
                         methods,
                         sizeof(methods) / sizeof(methods[0]));
    env->DeleteLocalRef(objectClass);

    this->m_init = true;
}
