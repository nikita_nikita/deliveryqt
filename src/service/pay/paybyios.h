#ifndef PAYBYIOS_H
#define PAYBYIOS_H

#include "paybyplatform.h"

class PayByIos: public PayByPlatform
{
public:
    PayByIos();
    virtual void setData(QString number, QString expDate, QString cvv);
    virtual bool isValidNumber();
    virtual bool isValidExpDate();
    virtual QString cardCryptogram();
    virtual QString getApplePayToken(double price, QString domain, OrderRequest order);
    void tokenCreate();
    void init();

private:
    QString m_number;
    QString m_expDate;
    QString m_cvv;
};

#endif // PAYBYIOS_H
