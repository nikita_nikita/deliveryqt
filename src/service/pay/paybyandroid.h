#ifndef PAYBYANDROID_H
#define PAYBYANDROID_H
#include "paybyplatform.h"
#include <QtAndroid>

class PayByAndroid: public PayByPlatform
{
public:
    PayByAndroid();
    virtual void setData(QString number, QString expDate, QString cvv);
    virtual bool isValidNumber();
    virtual bool isValidExpDate();
    virtual QString cardCryptogram();
    virtual QString getGooglePayToken(QString price);
    static  void activityHandler(JNIEnv *env, jobject thiz, jint resultCode);
    void tokenCreate();
    void init();

private:
    bool m_init= false;
    QString m_number;
    QString m_expDate;
    QString m_cvv;
    QAndroidJniObject payGoogleClass;

};

#endif // PAYBYANDROID_H
