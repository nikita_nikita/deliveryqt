#ifndef PAYCONNECTER_H
#define PAYCONNECTER_H

#include <QObject>

class PayConnecter : public QObject
{
    Q_OBJECT
public:
    explicit PayConnecter(QObject *parent = nullptr);

signals:
    void tokenCreated(QString token);
    void errorPay();
    void abortingPay();

public slots:
    void sendToken(QString token);
    void sendErrorPay();
    void abortPay();
};

#endif // PAYCONNECTER_H
