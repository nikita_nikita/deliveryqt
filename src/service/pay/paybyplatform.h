#ifndef PAYBYPLATFORM_H
#define PAYBYPLATFORM_H

#include <QString>
#include "payconnecter.h"
#include "./src/entity/orderrequest.h"

class PayByPlatform
{
public:
    static PayByPlatform &instance();
    static PayConnecter &connecter();

    virtual ~PayByPlatform();
    virtual void setData(QString number, QString expDate, QString cvv)=0;
    virtual bool isValidNumber()=0;
    virtual bool isValidExpDate()=0;
    virtual QString cardCryptogram()=0;

    virtual QString getGooglePayToken(QString price);
     virtual QString getApplePayToken(double price, QString domain,  OrderRequest request);
    virtual void saveOutput(QByteArray array);
    void setPublicId(const QString &value);
    void setCurrency(const QString &value);
protected:
    PayByPlatform();
    QString publicId;
    QString currency;
};

#endif // PAYBYPLATFORM_H
