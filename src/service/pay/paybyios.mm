#include "paybyios.h"
#include <QDate>
#include "ios/SDK/Card.h"
#include "ios/SDK/PKPaymentConverter.h"
#include <PassKit/PKPaymentRequest.h>
#include <QtGui>
#include <QtGui/qpa/qplatformnativeinterface.h>

#define let __auto_type const
#define var __auto_type



@interface PayResultControllerDelegate : NSObject <PKPaymentAuthorizationViewControllerDelegate>{
                                             PayByIos *m_paybayios;

}
@end
@implementation PayResultControllerDelegate
- (id) initWithPayByIos: (PayByIos *) paybyios
{
    self =[super init];
    if (self) {
        m_paybayios = paybyios;
    }
    return self;
}
- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller{
    [controller dismissViewControllerAnimated:YES completion: nil];
    //PayByPlatform::instance().connecter().abortPay();

}
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
  didAuthorizePayment:(PKPayment *)payment
  completion:(void (^)(PKPaymentAuthorizationStatus status))completion API_DEPRECATED("Use paymentAuthorizationViewController:didAuthorizePayment:handler: instead to provide more granular errors", ios(8.0, 11.0)){
    completion(PKPaymentAuthorizationStatusSuccess);
    NSString * crypto =[PKPaymentConverter convertToString:payment];
    PayByPlatform::instance().connecter().sendToken(QString::fromNSString(crypto));
}
@end
PayByIos::PayByIos()
{

}

void PayByIos::setData(QString number, QString expDate, QString cvv)
{
    QDate date = QDate::fromString(expDate, "MMyy");
    this->m_number = number;
    this->m_expDate = date.toString("MM/yy");
    this->m_cvv = cvv;
}

bool PayByIos::isValidNumber()
{
    return [Card isCardNumberValid:m_number.toNSString()];
}

bool PayByIos::isValidExpDate()
{
    return true;
}

QString PayByIos::cardCryptogram()
{
    Card *card = [[Card alloc] init];
    NSString *n_number = m_number.toNSString();
    NSString *n_expDate = m_expDate.toNSString();
    NSString *n_cvv = m_cvv.toNSString();
    NSString *n_publicId = this->publicId.toNSString();
    NSString *token =[card makeCardCryptogramPacket: n_number andExpDate:n_expDate andCVV:n_cvv andMerchantPublicID:n_publicId ];
    return QString::fromNSString(token);
}

QString PayByIos::getApplePayToken(double price, QString domain, OrderRequest order)
{
    PKPaymentRequest *request = [[PKPaymentRequest alloc] init];
    //Данные для регистрациии
    request.merchantIdentifier =@"merchant.ru.kapibaras";
    NSArray  *supportedNet = @[PKPaymentNetworkVisa,PKPaymentNetworkMasterCard];
            // code
            request.supportedNetworks = supportedNet;
            request.merchantCapabilities = PKMerchantCapability3DS;
     if(domain == "kapibaras.by") {
        request.countryCode=@"BY";
        request.currencyCode = @"BYN";
    } else {
    request.countryCode=@"RU";
    request.currencyCode = @"RUB";
}
    NSDecimalNumber *m_price  = [[NSDecimalNumber alloc] initWithDouble:price];
    auto products  = order.getOrder().getProducts();

    request.paymentSummaryItems = @[[PKPaymentSummaryItem summaryItemWithLabel:@"Kapibara" amount:m_price]];

    //PKPaymentAuthorizationController *controller = [[PKPaymentAuthorizationController alloc]ini t];
    let controller = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
    UIViewController *rootViewController = [[UIApplication sharedApplication].keyWindow rootViewController];
    PayResultControllerDelegate *resultController =[[PayResultControllerDelegate alloc] init];
    controller.delegate = id(resultController);
    [rootViewController presentViewController:controller animated:YES completion: nil];
    return "";

}

void PayByIos::tokenCreate()
{

}

void PayByIos::init()
{

}
