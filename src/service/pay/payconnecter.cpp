#include "payconnecter.h"

PayConnecter::PayConnecter(QObject *parent) : QObject(parent)
{

}

void PayConnecter::sendToken(QString token)
{
    emit tokenCreated(token);
}

void PayConnecter::sendErrorPay()
{
    emit errorPay();
}

void PayConnecter::abortPay()
{
    emit abortingPay();
}
