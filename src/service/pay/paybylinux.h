#ifndef PAYBYLINUX_H
#define PAYBYLINUX_H

#include "paybyplatform.h"


class PayByLinux: public PayByPlatform
{
public:
    PayByLinux();
    virtual void setData(QString number, QString expDate, QString cvv);
    virtual bool isValidNumber();
    virtual bool isValidExpDate();
    virtual QString cardCryptogram();

    virtual void saveOutput(QByteArray array);
private:
    QString m_number;
    QString m_expDate;
    QString m_cvv;
};

#endif // PAYBYLINUX_H
