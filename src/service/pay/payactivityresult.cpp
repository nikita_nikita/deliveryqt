#include "payactivityresult.h"
#include <QDebug>
PayActivityResult::PayActivityResult(QObject *parent) : QObject(parent)
{

}

void PayActivityResult::handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data)
{
    qDebug() << "result activity:" << receiverRequestCode << " resultCode:" << resultCode;
}
