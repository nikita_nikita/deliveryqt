#include "paybyplatform.h"

#ifdef Q_OS_ANDROID
#include "paybyandroid.h"
#elif defined(Q_OS_LINUX)
#include "paybylinux.h"
#elif defined(Q_OS_IOS)
#include "paybyios.h"


#endif


PayByPlatform &PayByPlatform::instance()
{
#ifdef Q_OS_ANDROID
    static PayByAndroid singleton;
#elif defined(Q_OS_LINUX)
    static PayByLinux singleton;
#elif defined(Q_OS_IOS)
    static PayByIos singleton;
#endif

    return singleton;
}

PayConnecter &PayByPlatform::connecter()
{
    static PayConnecter con;
    return con;
}



PayByPlatform::~PayByPlatform()
{

}



QString PayByPlatform::getGooglePayToken(QString price)
{
    Q_UNUSED(price)
    return "";
}

QString PayByPlatform::getApplePayToken(double price, QString domain, OrderRequest request)
{
    Q_UNUSED(price)
    Q_UNUSED(domain)
    Q_UNUSED(request)
    return "";
}

void PayByPlatform::saveOutput(QByteArray array)
{
    Q_UNUSED(array);
}

PayByPlatform::PayByPlatform()
{

}

void PayByPlatform::setCurrency(const QString &value)
{
    currency = value;
}

void PayByPlatform::setPublicId(const QString &value)
{
    publicId = value;
}




