#include "database.h"
#include <QDebug>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QSqlError>
#include <QSqlRecord>
DataBase::DataBase(QObject *parent) : QObject(parent)
{

}

QString DataBase::getTmpPathUploads() const
{
    return tmpPathUploads;
}

void DataBase::setTmpPathUploads(const QString &value)
{
    tmpPathUploads = value;

    this->connect();
}

void DataBase::saveCity(QJsonObject city)
{
    auto data = loadCity();
    if(data.value("domain").toObject().value("code").toString()!=city.value("domain").toObject().value("code").toString()){
emit domainChanged();
    }
    QJsonDocument dcity(city);
    QSqlQuery query;
    query.prepare("update data set city=:city");
    query.bindValue(":city",dcity.toJson(QJsonDocument::Compact));
    query.exec();
}

void DataBase::saveUser(QJsonObject user)
{
    QJsonDocument duser(user);
    QSqlQuery query;
    query.prepare("update data set user=:user");
    query.bindValue(":user",duser.toJson(QJsonDocument::Compact));
    query.exec();
}

void DataBase::saveUserEmail(QString email)
{
     QSqlQuery query;
    query.prepare("update data set userEmail=:email");
    query.bindValue(":email",email);
    query.exec();
}

void DataBase::saveToken(QString token)
{
    QSqlQuery query;
   query.prepare("update data set token=:token");
   query.bindValue(":token",token);
   query.exec();
}

QJsonArray DataBase::getOrderHystory()
{ QJsonArray result;
    QSqlQuery query;
    query.prepare("select * from hystory)");
    if(!query.exec()){
        qDebug() << " не могу прочитать адреса клиента"<< query.lastError();
    }
    QSqlRecord rec = query.record();
    int index_id = rec.indexOf("id");
    int index_address  = rec.indexOf("address");


    while (query.next()) {
        QJsonValue id(query.value(index_id).toInt());
        //QJsonValue address(query.value(index_address).toString());
        QJsonDocument order = QJsonDocument::fromJson(query.value(index_address).toByteArray());
        QJsonObject ob{
            {"id", id},
            {"address", order.object()}
        };
        result.append(ob);
    }
    return result;

}

void DataBase::setOrder(int id, QString status)
{


    auto orderStatusInTable = statusById(id);

    if(orderStatusInTable != status && orderStatusInTable !="Не найден"){
        replaceOrder(id,status);
    }
    if(orderStatusInTable == "Не найден"){
        saveOrder(id,status);
    }


}

void DataBase::saveOrder(int id, QString status)
{
    QSqlQuery query;

    query.prepare("INSERT INTO address (order, status) "
                  "VALUES (:order, :status)");
    query.bindValue(":order", id);
    query.bindValue(":domainPath", status);
    if(!query.exec()){
        qDebug() << " не смог добавить аддресс"<< query.lastError();

    }
}

void DataBase::replaceOrder(int id, QString status)
{
    QSqlQuery query;
    query.prepare("UPDATE address SET status=:status where order=:order");
    query.bindValue(":order", id);
    query.bindValue(":status", status);
    if(!query.exec()){
        qDebug() << " не смог добавить аддресс"<< query.lastError();

    }
}

QString DataBase::statusById(int id)
{
    QJsonArray result;
    QSqlQuery query;
    query.prepare("select * from hystory where order=:order");
    query.bindValue(":order", id);
    if(!query.exec()){
        qDebug() << " не могу прочитать адреса клиента"<< query.lastError();
    }
    QSqlRecord rec = query.record();
    int index_id = rec.indexOf("id");
    int index_order  = rec.indexOf("order");


    while (query.next()) {
        QJsonValue id(query.value(index_id).toInt());
        //QJsonValue address(query.value(index_address).toString());
        int order = query.value(index_order).toInt();
        QJsonObject ob{
            {"id", id},
            {"order", order},
            {"status", order}
        };
        result.append(ob);
    }
    if(result.count() == 0){
        return "Не найден";
    }
    if(result.count() > 1){
       qDebug()<<"Дублирующий id";
       return result.at(0)["status"].toString();
    }
    if(result.count() == 1){
       qDebug()<<"vse ok";
       return result.at(0)["status"].toString();
    }
    return "конец";

}

QJsonObject DataBase::loadUser()
{
    QJsonDocument result;
    QSqlQuery query;
    query.exec("select user from data");
    while(query.next()){
        result = QJsonDocument::fromJson(query.value(0).toByteArray());
        break;
    }

    qDebug()<<"userData"<<result.object();

    return result.object();
}

QJsonObject DataBase::loadCity()
{

    QJsonDocument result;
    QSqlQuery query;
    query.exec("select city from data");
    while(query.next()){
        result = QJsonDocument::fromJson(query.value(0).toByteArray());
        break;
    }

    return result.object();
}

QString DataBase::loadToken()
{
    QString result;
     QSqlQuery query;
       query.exec("select token from data");
    while(query.next()){
        result = query.value(0).toString();
        break;
    }

    return result;
}

QString DataBase::loadUserEmail()
{
    QString result;
    QSqlQuery query;
    query.exec("select userEmail from data");
    while(query.next()){
        result = query.value(0).toString();
        break;
    }
    return result;
}

void DataBase::addAddres(int city, QString domainPath, QJsonObject address)
{
    QJsonDocument doc(address);
    QSqlQuery query;
    query.prepare("INSERT INTO address (city, domainPath, address) "
                  "VALUES (:city, :domainPath, :address)");
    query.bindValue(":city", city);
    query.bindValue(":domainPath", domainPath);
    query.bindValue(":address", doc.toJson(QJsonDocument::Compact));
    if(!query.exec()){
        qDebug() << " не смог добавить аддресс"<< query.lastError();

    }
}

QJsonArray DataBase::fethAddress(int city, QString domainPath)
{
    QJsonArray result;
    QSqlQuery query;
    query.prepare("select * from address where city=:city and domainPath=:domainPath");
    query.bindValue(":city", city);
    query.bindValue(":domainPath", domainPath);
    if(!query.exec()){
        qDebug() << " не могу прочитать адреса клиента"<< query.lastError();
    }
    QSqlRecord rec = query.record();
    int index_id = rec.indexOf("id");
    int index_address  = rec.indexOf("address");


    while (query.next()) {
        QJsonValue id(query.value(index_id).toInt());
        //QJsonValue address(query.value(index_address).toString());
        QJsonDocument address = QJsonDocument::fromJson(query.value(index_address).toByteArray());
        QJsonObject ob{
            {"id", id},
            {"address", address.object()}
        };
        result.append(ob);
    }
    return result;

}

void DataBase::saveAddress(int id, QJsonObject address)
{
    QJsonDocument doc(address);
    QSqlQuery query;
    query.prepare("UPDATE address SET address=:address where id=:id");
    query.bindValue(":id", id);
    query.bindValue(":address", doc.toJson(QJsonDocument::Compact));
    if(!query.exec()){
        qDebug() << " не смог сохранить адрес"<< query.lastError();
    }


}

void DataBase::removeAddress(int id)
{
    QSqlQuery query;
    query.prepare("delete from address where id=:id");
    query.bindValue(":id", id);
    if(!query.exec()){
        qDebug() << " не смог удалить адрес"<< query.lastError();
    }
}





QString DataBase::getPathDB()
{
    return tmpPathUploads+"/"+tableName;
}

void DataBase::createDb()
{
    QSqlQuery query;
    if(!query.exec("CREATE TABLE IF NOT EXISTS 'data' ('user' TEXT,'city' TEXT,'userEmail' TEXT, 'token' TEXT );")){
        qDebug()<< "не смог создать базу данных: Data";
    }
    if(!query.exec("CREATE TABLE IF NOT EXISTS 'address' (`id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, 'city' INTEGER,'domainPath' TEXT,'address' TEXT );")){
        qDebug()<< "не смог создать базу данных: address";
    }
    if(!query.exec("CREATE TABLE IF NOT EXISTS 'hystory' (`id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, 'order' INTEGER,'status' TEXT);")){
        qDebug()<< "не смог создать базу данных: hystory";
    }
    this->checkData();
}

void DataBase::connect()
{
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName(this->getPathDB());
    if (!sdb.open()) {
        qDebug()<< "Не смог открыть  базу данных";
    }
    this->createDb();
}

void DataBase::checkData()
{
    QSqlQuery query;
    query.exec("select count(user) from data");
    int count=0;
    while(query.next()){
        count= query.value(0).toInt();
    }
    if(count==0) {
        QJsonObject user;
        user["name"]="";
        user["phone"]="";
        QJsonDocument doc(user);
        query.prepare("insert into data (user, city) values(:user, '{}')");
        query.bindValue(":user", doc.toJson(QJsonDocument::Compact));
        query.exec();

    }

}

