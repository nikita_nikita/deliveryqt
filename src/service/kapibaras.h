#ifndef KAPIBARAS_H
#define KAPIBARAS_H

#include <QObject>
#include "../entity/citylist.h"
#include "../entity/citydata.h"
#include "../entity/versionapp.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "fileloader.h"
#include "../entity/feedbackdata.h"
#include "../entity/street.h"
#include "../entity/orderrequest.h"
#include "../entity/checkstreet.h"
#include "../entity/loyality.h"
#include <QImage>
#include <QBuffer>
#include <QTimer>
class Kapibaras : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString urlImage READ urlImage WRITE setUrlImage NOTIFY urlImageChanged)
    Q_PROPERTY(bool statusClientDataLoad READ statusClientDataLoad WRITE setStatusClientDataLoad NOTIFY statusClientDataLoadChanged)
public:
    explicit Kapibaras(QObject *parent = nullptr);
    QString getDomain() const;
    int getCity_id() const;
    void loadWorkTimeAndOrganizationInfo();
    QString getDefaultSheme();
    QString getTmpPathUploads() const;
    void setTmpPathUploads(const QString &value);
    QString urlImage() const;
    Q_INVOKABLE bool fileExists(QString fileName);
    bool statusClientDataLoad() const;
    void setCountry(const QString &value);
    QString getCountry() const;

    void loadClientHistory();



signals:
    void loadingCityData();
    void organizationInfoloaded(OrganizationInfo,WorkTimeEntity);
    void citysLoaded(CityList list);
    void cityDataLoaded(CityData data);
    void streetLoaded(StreetList list);
    void currentVersionLoaded(VersionApp version);
    void domainChanged(QString domain);
    void cityIdChanged(int cityId);
    void orderSended(); // при успешной отпавке заказа на сервер
    void error(QString error);
    void cuponLoaded(QJsonObject cupon);
    void urlImageChanged(QString urlImage);
    void startLoaded();
    void endLoaded();
    void checkStreetLoaded(QJsonObject json);
    void authLoaded(bool status, QString token);
    void clientDataLoaded(QJsonObject client);
    void statusClientDataLoadChanged(bool statusClientDataLoad);
    void errorLoadClientData();
    void loyalityLoaded(LoyalityList loyality);
    void loyalityLoadError();
    void clientHystoryLoaded(QJsonArray jArr);
public slots:
    void loadCitys(QString domein);
    void loadCityData();
    void loadStreetList();
    void loadCurrentVersion();
    void loadPromocode(QString promocode, QString phone);
    void setDomain(QString value);
    void setCity_id(int value);
    // отправка заказа на сервер
    void sendOrder(OrderRequest  order);
    void setUrlImage(QString urlImage);
    void checkStreet(CheckStreet data);    // проверка улицы
    void loadFiles();
    void sendOrderToLoyality(OrderRequest order);
    //lk

    void sendSmsCode(QString phone);
    void auth(QString phone, QString code);
    void loadClientData(QString token);
    void saveClient(QJsonValue client_info, QString token);
    void setTokenLk(QString token);
    void setStatusClientDataLoad(bool statusClientDataLoad);
    void updateClientData();
    void downloadImageToCallback(QImage image);
    void loadClientDataAfterMinute();
private slots:
    void onCityLoaded(QNetworkReply *reply);
    void onCityDataLoaded(QNetworkReply *reply);
    void onStreetListLoaded(QNetworkReply *reply);
    void onPromocodeLoaded(QNetworkReply *reply);
    void onSendOrder(QNetworkReply *reply);
    void onCheckStreet(QNetworkReply *reply);
    void onAuthResult(QNetworkReply *reply);
    void onLoadedClientData(QNetworkReply * reply);
    void onCurrentVersionLoaded(QNetworkReply *reply);
    void onSaveClient(QNetworkReply *reply);
    void filesLoaded();
    void CheckResult(QNetworkReply *reply);
    void onLoyalityInfoLoaded(QNetworkReply *reply);
    void onOrganizationInfoManager(QNetworkReply * reply);
    void onOrderImageLoad(QNetworkReply *reply);
    void onClientHystoryLoaded(QNetworkReply *reply);

private:
    void processCityList(CityList citys);


    QNetworkAccessManager *streetManager;
    QNetworkAccessManager *promocodeManager;    
    QNetworkAccessManager *sendOrderManager;
    QNetworkAccessManager *cuponManager;
    QNetworkAccessManager *loyalityManager;
    QNetworkAccessManager *checkStreetManager;
    QNetworkAccessManager *lkManager;
    QNetworkAccessManager *lkManagerSave;
    QNetworkAccessManager *citysManager;
    QNetworkAccessManager *cityDataManager;
    QNetworkAccessManager *versionManager;
    QNetworkAccessManager *clientDataManager;
    QNetworkAccessManager *imageDownloadmanager;
    QNetworkAccessManager *organizationInfoManager;
    QNetworkAccessManager *onliHistoyClientManager;
    QNetworkAccessManager *mainListManager;


    QString createJWT(QString phone);
    CityList m_cityList;
    QString domain;
    int city_id;
    QString tmpPathUploads;
    QString folderName = "uploads";
    void checkFolder();
    CityData tmpCityData;
    FileLoader *fileLoader;
    QString m_urlImage;
    QString tmp_tokenlk;
    bool m_statusClientDataLoad=false;
    QString getMacAddress();
    QString m_firebase_token;
    void  writeLog(QString filename, QByteArray data);
    QString country ;
    QString pathToLoyality = "";
    QTimer *m_timer;
};

#endif // KAPIBARAS_H
