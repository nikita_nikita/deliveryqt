#include "statusstorage.h"
StatusStorage::StatusStorage(QObject *parent) : QObject(parent)
{

}

bool StatusStorage::checkStreetLoaded() const
{
    return m_checkStreetLoaded;
}

QJsonObject StatusStorage::checkStreetResponse() const
{
    return m_checkStreetResponse;
}

void StatusStorage::setCheckStreetLoaded(bool checkStreetLoaded)
{
    if (m_checkStreetLoaded == checkStreetLoaded)
        return;

    m_checkStreetLoaded = checkStreetLoaded;

    emit checkStreetLoadedChanged(m_checkStreetLoaded);
}

void StatusStorage::setCheckStreetResponse(QJsonObject checkStreetResponse)
{
    this->setCheckStreetLoaded(true);
    if (m_checkStreetResponse == checkStreetResponse)
        return;
    m_checkStreetResponse = checkStreetResponse;
    emit checkStreetResponseChanged(m_checkStreetResponse);
}
