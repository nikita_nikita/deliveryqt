#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <QJsonObject>
#include <QSortFilterProxyModel>
#include "database.h"
#include "./src/entity/versionapp.h"
#include "./src/entity/citydata.h"
#include "./src/entity/citylist.h"

#include "../models/offersmodel.h"
#include "../models/citymodel.h"
class Storage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray cityList READ cityList  NOTIFY cityListChanged)

    Q_PROPERTY(QJsonObject user READ user WRITE setUser NOTIFY userChanged)


    Q_PROPERTY(QJsonObject userCity READ userCity WRITE setUserCity NOTIFY userCityChanged)
    Q_PROPERTY(QJsonObject cityData READ cityData WRITE setCityData NOTIFY cityDataChanged)
    Q_PROPERTY(QString imagesPath READ imagesPath  WRITE setImagesPath  NOTIFY imagesPathChanged)
    Q_PROPERTY(QJsonArray userAddress READ userAddress WRITE setUserAddress NOTIFY userAddressChanged)
    Q_PROPERTY(OffersModel * offersCatalog MEMBER m_offersCatalogModel NOTIFY offersCatalogChanged)
    Q_PROPERTY(OffersModel * offersBaner MEMBER m_offersBanerModel NOTIFY offersBanerChanged)
    Q_PROPERTY(QSortFilterProxyModel* cityModel MEMBER m_sortFilterCityModel NOTIFY cityModelChanged)
    Q_PROPERTY(QString tokenLk READ tokenLk WRITE setTokenLk NOTIFY tokenLkChanged)
    Q_PROPERTY(bool isReadyCityData READ isReadyCityData WRITE setIsReadyCityData NOTIFY isReadyCityDataChanged)
    Q_PROPERTY(bool sorryMessageVisible READ sorryMessageVisible WRITE setSorryMessageVisible NOTIFY sorryMessageVisibleChanged)
public:
    explicit Storage(QObject *parent = nullptr);

    QJsonArray cityList() const;
    QJsonObject user() const;
    QJsonObject userCity() const;
    void setTmpPathUploads(const QString &value);
    QJsonObject cityData() const;
    QString imagesPath() const;
    QJsonArray userAddress() const;
    QString userEmail() const;
    QString tokenLk() const;
    bool isReadyCityData() const;



    bool sorryMessageVisible() const
    {
        return m_sorryMessageVisible;
    }

signals:
    void clearPhoneTextField();
    void cityListChanged(QJsonArray cityList);
    void userChanged(QJsonObject user);
    void userCityChanged(QJsonObject userCity);
    void cityDataChanged(QJsonObject cityData);
    void imagesPathChanged(QString imagesPath);
    void userAddressChanged(QJsonArray userAddress);
    void offersCatalogChanged(OffersModel * offersCatalog);
    void offersBanerChanged(OffersModel * offersBaner);
    void cityModelChanged(QSortFilterProxyModel* cityModel);
    void resetPosition();    
    void userEmailChanged(QString userEmail);
    void tokenLkChanged(QString tokenLk);
    void finishLoaded();

    void isReadyCityDataChanged(bool isReadyCityData);

    void sorryMessageVisibleChanged(bool sorryMessageVisible);

public slots:
    //void setCurrentVersion(VersionApp version);
    void logout();


    void setCityList(CityList cityList);
    void setUser(QJsonObject user);
    void setUserCity(QJsonObject userCity);
    void setCityData(QJsonObject cityData);
    void setCityDataJson(CityData cityData);
    void setImagesPath(QString imagesPath);

    void addAdress(QJsonObject address);
    void setUserAddress(QJsonArray userAddress);
    void saveAddress(QJsonObject data);
    void removeAddress(int id);    
    void setUserEmail(QString userEmail);
    void setTokenLk(QString tokenLk);
    void setIsReadyCityData(bool isReadyCityData);

    QJsonValue processProductToAwailablePoints(QJsonArray productIdList);

    void setSorryMessageVisible(bool sorryMessageVisible)
    {
        if (m_sorryMessageVisible == sorryMessageVisible)
            return;

        m_sorryMessageVisible = sorryMessageVisible;
        emit sorryMessageVisibleChanged(m_sorryMessageVisible);
    }

private:
    QJsonArray m_cityList;
    QJsonObject m_user;
    QJsonObject m_userCity;
    QString tmpPathUploads;
    DataBase *db;
    void updateFromDb();

    QJsonObject m_cityData;
    QString m_imagesPath;
    QString folderName = "uploads";


    QJsonArray m_userAddress;
    OffersModel *m_offersCatalogModel;
    OffersModel *m_offersBanerModel;
    CityModel *m_city_model;
    QSortFilterProxyModel *m_sortFilterCityModel;


    QString m_userEmail;
    QString m_tokenLk;
    bool m_isReadyCityData;
    bool m_sorryMessageVisible = false;

};

#endif // STORAGE_H
