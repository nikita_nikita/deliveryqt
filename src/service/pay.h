#ifndef PAY_H
#define PAY_H

#include <QObject>
#include "pay/paybyplatform.h"
#include "./src/entity/citydata.h"
#include "./src/entity/orderrequest.h"
#include "./src/entity/pay/carddata.h"
#include "./src/entity/pay/cpcheckout.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>

class Pay : public QObject
{
    Q_OBJECT

public:    
    explicit Pay(QObject *parent = nullptr);
    void PayMarket(OrderRequest request);
    void PayCard(OrderRequest request, CardData card);

signals:
    void success();
    void error(QString error);
    void openHtml(QByteArray sourceHtml,QString sourceUrl);
    void openUrl(QString sourceUrl);
    void errorPay(QString error);
    void startLoaded();
    void endLoaded();



public slots:
    void setCityDataJson(CityData cityData);
    void sendCheckout(CPCheckOut checkout);
    void setDomain(QString value);
    void setCity_id(int value);
    void sendThreeDs(QString AcsUrl, QString PaReq, QString md);
    void checkPayUrl(QString url_string);
     void setTokenLk(QString token);
private slots:
    void onCheckoutResult(QNetworkReply *reply);
    void errorPayMarket();
    void setPayMartketToken(QString token);
    void abortPay();

private:
    CityData cityData;
    QString domain;
    int cityId;

    QNetworkAccessManager *checkoutManager;
    PayConnecter * connecter;
    OrderRequest tmp_request;
    QString m_platform;
    QString tmp_token;
     QString getMacAddress();


     #ifdef Q_OS_ANDROID
     QString payByMarketName = "Google pay";
     #elif defined(Q_OS_IOS)

     QString payByMarketName = "Apple pay";
#elif defined(Q_OS_MAC)

QString payByMarketName = "Apple pay";
     #elif defined(Q_OS_LINUX)
     QString payByMarketName = "Apple pay";
     #endif

};

#endif // PAY_H
