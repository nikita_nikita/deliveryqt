#ifndef WORKTIME_H
#define WORKTIME_H

#include <QObject>
#include "./src/entity/citydata.h"
#include "../models/worktimemodel.h"
#include "../models/timeorderlmodel.h"
#include <QTimer>
#include <QJsonArray>
class WorkTime : public QObject
{
    Q_OBJECT
    Q_PROPERTY( WorkTimeModel* model MEMBER model NOTIFY modelChanged) 
    Q_PROPERTY(QJsonArray pickupIntervas READ pickupIntervas WRITE setPickupIntervas NOTIFY pickupIntervasChanged)
    Q_PROPERTY(QJsonArray deliveryIntervas READ deliveryIntervas WRITE setDeliveryIntervas NOTIFY deliveryIntervasChanged)

    Q_PROPERTY(int byDays READ byDays WRITE setByDays NOTIFY byDayChanged)
    Q_PROPERTY(QString currentDay READ currentDay WRITE setCurrentDay NOTIFY currentDayChanged)
    Q_PROPERTY(QString pickup READ pickup WRITE setPickup NOTIFY pickupChanged)
    Q_PROPERTY(QString delivery READ delivery WRITE setDelivery NOTIFY deliveryChanged)
    Q_PROPERTY(bool visibleMessage READ visibleMessage WRITE setVisibleMessage NOTIFY visibleMessageChanged)
    Q_PROPERTY(QString timeOpened READ timeOpened WRITE setTimeOpened NOTIFY timeOpenedChanged)
    Q_PROPERTY(QString timeClosed READ timeClosed WRITE setTimeClosed NOTIFY timeClosedChanged)
public:
    explicit WorkTime(QObject *parent = nullptr);
    int byDays() const;
    QString currentDay() const;
    bool canDelivery(QTime time);
    bool canPickUp(QTime time);
    QString pickup() const;

    QString delivery() const;

    bool visibleMessage() const;

    QString timeOpened() const;

    QString timeClosed() const;

    QJsonArray pickupIntervas() const;

    QJsonArray deliveryIntervas() const;

signals:
    void byDayChanged(int byDays);
    void currentDayChanged(QString currentDay);
    void pickupChanged(QString pickup);
    void deliveryChanged(QString delivery);
    void modelChanged(WorkTimeModel* model);
    void visibleMessageChanged(bool visibleMessage);
    void timeOpenedChanged(QString timeOpened);
    void timeClosedChanged(QString timeClosed);    
    void pickupIntervasChanged(QJsonArray pickupIntervas);
    void deliveryIntervasChanged(QJsonArray deliveryIntervas);

public slots:
    void setCityData(CityData &cityData);
    void setByDays(int byDays);
    void setCurrentDay(QString currentDay);
    void setPickup(QString pickup);
    void setDelivery(QString delivery);
    void setVisibleMessage(bool visibleMessage);
    void setTimeOpened(QString timeOpened);
    void setTimeClosed(QString timeClosed);
    void timerTimeOut();
    void updateModeles();

    void setPickupIntervas(QJsonArray pickupIntervas);

    void setDeliveryIntervas(QJsonArray deliveryIntervas);
    void checkPointWork();
private:
    WorkTimeModel *model;
    WorkTimeEntity workTimeEntity;
    int m_byDays;
    QString m_currentDay;
    QString m_pickup;
    QString m_delivery;

    bool m_visibleMessage=false;
    QString m_timeOpened;
    QString m_timeClosed;
    QTimer *timer;
    QTime m_open;
    QTime m_close;
    QTime m_deliveryStart;
    QTime m_pickUpStart;
    WorkDayEntity currentDayEntity;
    WorkDayEntity nextDayEntity;
    QTime getValidTime(QTime time);
    QJsonArray getTimeInterval(QTime start, QTime finish);

   QJsonArray m_pickupIntervas;
   QJsonArray m_deliveryIntervas;
};

#endif // WORKTIME_H
