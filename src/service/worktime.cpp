#include "worktime.h"
#include <QDate>

#include <QDebug>
WorkTime::WorkTime(QObject *parent) : QObject(parent)
{
    model = new WorkTimeModel(this);
    timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, &WorkTime::timerTimeOut);
}

int WorkTime::byDays() const
{
    return m_byDays;
}

QString WorkTime::currentDay() const
{
    return m_currentDay;
}

QString WorkTime::pickup() const
{
    return m_pickup;
}

QString WorkTime::delivery() const
{
    return m_delivery;
}

bool WorkTime::visibleMessage() const
{
    return m_visibleMessage;
}

QString WorkTime::timeOpened() const
{
    return m_timeOpened;
}

QString WorkTime::timeClosed() const
{
    return m_timeClosed;
}

QJsonArray WorkTime::pickupIntervas() const
{
    return m_pickupIntervas;
}

QJsonArray WorkTime::deliveryIntervas() const
{
    return m_deliveryIntervas;
}

void WorkTime::setCityData(CityData &cityData)
{
    //тут уставливаем временные промежутки на время заказа
    // прибавить к текущему времени  минимальное время заказа  и отнять от конечного времени

    QTime current = QTime::currentTime();
    int weekDate = QDate::currentDate().weekNumber();

    qDebug()<<"pick smart time ";


    this->workTimeEntity = cityData.getWorkTime();
    this->currentDayEntity = workTimeEntity.getDayData(weekDate);



    QTime pickup = QTime::fromString(this->currentDayEntity.getPickup().getStart());
    QTime delivety =  QTime::fromString(this->currentDayEntity.getDelivery().getStart());
    QTime pickUpEnd =  QTime::fromString(this->currentDayEntity.getPickup().getEnd());;
    QTime deliveryend =  QTime::fromString(this->currentDayEntity.getDelivery().getEnd());;
    m_open = this->currentDayEntity.getTimeOpen();
    qDebug()<<"openTime"<<m_open;
    m_close = this->currentDayEntity.getTimeClose();

    if(current>pickup&&current < pickUpEnd){   pickup= current;  }
    if(current>delivety&&current <deliveryend){  delivety=current;  }





    pickup = pickup.addSecs(this->workTimeEntity.getPickupTimeLimit()*60);
    delivety= delivety.addSecs(this->workTimeEntity.getDeliveryTimeLimit()*60);

    QTime pickupEnd = QTime::fromString(this->currentDayEntity.getPickup().getEnd());
    QTime deliveryEnd = QTime::fromString(this->currentDayEntity.getDelivery().getEnd());
    if(current > pickupEnd&&workTimeEntity.getBy_days() == 0){
        QTime pickup = QTime::fromString(this->currentDayEntity.getPickup().getStart());
        qDebug()<<"setPickupIntervas";
        this->setPickupIntervas(this->getTimeInterval(pickup,pickupEnd));
    }else{
        this->setPickupIntervas(this->getTimeInterval(pickup,pickupEnd));
    }
    if(current > deliveryEnd&&workTimeEntity.getBy_days() == 0){
        qDebug()<<"setDeliveryIntervas";
        delivety= delivety.addSecs(this->workTimeEntity.getDeliveryTimeLimit()*60);
        this->setDeliveryIntervas(this->getTimeInterval(delivety, deliveryEnd));
    }else{
        this->setDeliveryIntervas(this->getTimeInterval(delivety, deliveryEnd));
    }

    qDebug()<<"pickUp"<<pickup;
    qDebug()<<"delivety"<<delivety;
}

void WorkTime::setByDays(int byDay)
{
    if (m_byDays == byDay)
        return;

    m_byDays = byDay;
    emit byDayChanged(m_byDays);
}

void WorkTime::setCurrentDay(QString currentDay)
{
    if (m_currentDay == currentDay)
        return;

    m_currentDay = currentDay;
    emit currentDayChanged(m_currentDay);
}

void WorkTime::setPickup(QString pickup)
{
    if (m_pickup == pickup)
        return;

    m_pickup = pickup;
    emit pickupChanged(m_pickup);
}

void WorkTime::setDelivery(QString delivery)
{
    if (m_delivery == delivery)
        return;

    m_delivery = delivery;
    emit deliveryChanged(m_delivery);
}

void WorkTime::setVisibleMessage(bool visibleMessage)
{
    if (m_visibleMessage == visibleMessage)
        return;

    m_visibleMessage = visibleMessage;
    emit visibleMessageChanged(m_visibleMessage);
}

void WorkTime::setTimeOpened(QString timeOpened)
{
    if (m_timeOpened == timeOpened)
        return;

    m_timeOpened = timeOpened;
    emit timeOpenedChanged(m_timeOpened);
}

void WorkTime::setTimeClosed(QString timeClosed)
{
    if (m_timeClosed == timeClosed)
        return;

    m_timeClosed = timeClosed;
    emit timeClosedChanged(m_timeClosed);
}

void WorkTime::timerTimeOut()
{
    this->setVisibleMessage(false);
    timer->stop();
}

void WorkTime::updateModeles()
{
    //тут уставливаем временные промежутки на время заказа
    // прибавить к текущему времени  минимальное время заказа  и отнять от конечного времени
    QTime current = QTime::currentTime();


    QTime pickup = QTime::fromString(this->currentDayEntity.getPickup().getStart());
    QTime delivety =  QTime::fromString(this->currentDayEntity.getDelivery().getStart());
    QTime pickUpEnd =  QTime::fromString(this->currentDayEntity.getPickup().getEnd());;
    QTime deliveryend =  QTime::fromString(this->currentDayEntity.getDelivery().getEnd());;
    m_open = this->currentDayEntity.getTimeOpen();

    if(current>pickup&&current < pickUpEnd){   pickup= current;  }
    if(current>delivety&&current <deliveryend){  delivety=current;  }





    pickup = pickup.addSecs(this->workTimeEntity.getPickupTimeLimit()*60);
    delivety= delivety.addSecs(this->workTimeEntity.getDeliveryTimeLimit()*60);

    QTime pickupEnd = QTime::fromString(this->currentDayEntity.getPickup().getEnd());
    QTime deliveryEnd = QTime::fromString(this->currentDayEntity.getDelivery().getEnd());
    if(current > pickupEnd&&workTimeEntity.getBy_days() == 0){
        QTime pickup = QTime::fromString(this->currentDayEntity.getPickup().getStart());
        qDebug()<<"setPickupIntervas";
        this->setPickupIntervas(this->getTimeInterval(pickup,pickupEnd));
    }else{
        this->setPickupIntervas(this->getTimeInterval(pickup,pickupEnd));
    }
    if(current > deliveryEnd&&workTimeEntity.getBy_days() == 0){
        qDebug()<<"setDeliveryIntervas";
        delivety= delivety.addSecs(this->workTimeEntity.getDeliveryTimeLimit()*60);
        this->setDeliveryIntervas(this->getTimeInterval(delivety, deliveryEnd));
    }else{
        this->setDeliveryIntervas(this->getTimeInterval(delivety, deliveryEnd));
    }

}

void WorkTime::setPickupIntervas(QJsonArray pickupIntervas)
{
    //    if (m_pickupIntervas == pickupIntervas)
    //        return;
    qDebug()<<"pickupIntervas >> "<<pickupIntervas;
    m_pickupIntervas = pickupIntervas;
    emit pickupIntervasChanged(m_pickupIntervas);
}

void WorkTime::setDeliveryIntervas(QJsonArray deliveryIntervas)
{
    //    if (m_deliveryIntervas == deliveryIntervas)
    //        return;
    qDebug()<<"deliveryIntervas >> "<<deliveryIntervas;
    m_deliveryIntervas = deliveryIntervas;
    emit deliveryIntervasChanged(m_deliveryIntervas);
}

void WorkTime::checkPointWork()
{
    QTime currentTime = QTime::currentTime();
    QTime opened = this->currentDayEntity.getTimeOpen();
    QTime closed = this->currentDayEntity.getTimeClose();

    this->setTimeOpened(opened.toString("hh:mm"));
    this->setTimeClosed(closed.toString("hh:mm"));
    if (currentTime<opened) {
        //определяем сколько времени показывать таймер
        int deffrence = currentTime.msecsTo(opened);
        timer->start(deffrence);
        this->setVisibleMessage(true);
    }

    if (currentTime>closed){
        this->setVisibleMessage(true);
    }
    this->updateModeles();
}

QTime WorkTime::getValidTime(QTime time)
{
    qDebug()<<"time"<<time;
    int minute = time.minute();
    //    if (minute==0){
    //        return time;
    //    }
    if(minute>=0&&minute<15){
        time.setHMS(time.hour(), 15,0);
        return time;
    }
    if (minute>=15&&minute<30){
        time.setHMS(time.hour(), 30,0);
        return time;
    }
    if(minute>=30&&minute<45) {
        time.setHMS(time.hour(),45, 0);
        return time;
    }
    if(minute>=45&&minute<59) {
        time.setHMS(time.hour()+1, 0 , 0);
        return time;
    }
    return time;

}

QJsonArray WorkTime::getTimeInterval(QTime start, QTime finish)
{
    QJsonArray result;
    QTime indexTime = getValidTime(start);

    QTime end = getValidTime(finish);
    indexTime=indexTime.addSecs(900);
    qDebug()<<"m_open"<<m_open<< " >> "<<m_close;
    if(m_open < QTime::currentTime()&&QTime::currentTime()<m_close){
        result.append("Как можно скорее");
    }
    while (indexTime<=end) {
        result.append(indexTime.toString("hh:mm"));
        QTime oldtime = indexTime;
        indexTime=indexTime.addSecs(900);
        if (oldtime>indexTime) {
            break;
        }
    }

    return result;}
