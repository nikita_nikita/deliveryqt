#include "storage.h"
#include <QDir>
#include "../config/settingapp.h"
#include <QDebug>
Storage::Storage(QObject *parent) : QObject(parent)
{
    db = new DataBase(this);

    m_offersCatalogModel = new OffersModel(this);
    m_offersBanerModel = new OffersModel(this);
    m_city_model = new CityModel(this);
    m_sortFilterCityModel = new QSortFilterProxyModel(this);
    m_sortFilterCityModel->setSourceModel(m_city_model);
    m_sortFilterCityModel->setFilterRole(CityModel::NameRole);
    m_sortFilterCityModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    connect(db,&DataBase::domainChanged,this,&Storage::logout);

}

QJsonArray Storage::cityList() const
{
    return m_cityList;
}

QJsonObject Storage::user() const
{
    return m_user;
}

QJsonObject Storage::userCity() const
{
    return m_userCity;
}

void Storage::setCityList(CityList cityList)
{

    CityList citys;
    if(SettingApp::mode()==SettingApp::Production) {       
        citys = cityList.getHrefVisible();// показывать только разрешенные города
    } else {
        citys = cityList; // показывать все города
    }
     this->m_city_model->setCityList(citys);

     this->m_cityList = citys.toJson();
     emit cityListChanged(this->m_cityList);
}

void Storage::setUser(QJsonObject user)
{
    if (m_user == user)
        return;

    m_user = user;

    emit userChanged(m_user);
}

void Storage::setUserCity(QJsonObject userCity)
{
    if (m_userCity == userCity)
        return;

    if(!m_userCity["domain"].isNull()&&userCity["domain"]!= m_userCity["domain"]){
        this->setTokenLk("");
        this->setUser(QJsonObject());
    }
    m_userCity = userCity;
    db->saveCity(userCity);
    emit userCityChanged(m_userCity);

}

void Storage::setCityData(QJsonObject cityData)
{
    if (m_cityData == cityData)
        return;

    m_cityData = cityData;    
    emit cityDataChanged(m_cityData);

    this->setIsReadyCityData(true);
}

void Storage::setCityDataJson(CityData cityData)
{
    emit resetPosition();
    this->setCityData(cityData.getData());
    this->m_offersBanerModel->setOffersList(cityData.getOffers().getOffersBaner());
    this->m_offersCatalogModel->setOffersList(cityData.getOffers().getOffersCatalog());
    emit finishLoaded();
}

void Storage::setImagesPath(QString imagesPath)
{
    if (m_imagesPath == imagesPath)
        return;

    m_imagesPath = imagesPath;
    emit imagesPathChanged(m_imagesPath);
}
void Storage::addAdress(QJsonObject address)
{
    City city;
    city.setData(m_userCity);
    db->addAddres(city.getId(), city.getDomain().getPath(), address);
    this->setUserAddress(db->fethAddress(city.getId(), city.getDomain().getPath()));

}

void Storage::setUserAddress(QJsonArray userAddress)
{
    if (m_userAddress == userAddress)
        return;

    m_userAddress = userAddress;
    emit userAddressChanged(m_userAddress);
}

void Storage::saveAddress(QJsonObject data)
{
    int id = data["id"].toInt();
    QJsonObject address  = data["address"].toObject();
    db->saveAddress(id, address);

    City city(m_userCity);
    this->setUserAddress(db->fethAddress(city.getId(), city.getDomain().getPath()));
}

void Storage::removeAddress(int id)
{
    db->removeAddress(id);
    City city(m_userCity);
    this->setUserAddress(db->fethAddress(city.getId(), city.getDomain().getPath()));
}

void Storage::setUserEmail(QString userEmail)
{
    qWarning("storage fucnciton setUserEmail устарела ");
    if (m_userEmail == userEmail)
        return;

    m_userEmail = userEmail;
    emit userEmailChanged(m_userEmail);
    this->db->saveUserEmail(userEmail);
}

void Storage::setTokenLk(QString tokenLk)
{
    if (m_tokenLk == tokenLk){
        return;
    }
    m_tokenLk = tokenLk;
    emit tokenLkChanged(m_tokenLk);
    if(db->loadToken()!=m_tokenLk){
        this->db->saveToken(tokenLk);
    }
}

void Storage::setIsReadyCityData(bool isReadyCityData)
{
    if (m_isReadyCityData == isReadyCityData)
        return;

    m_isReadyCityData = isReadyCityData;
    emit isReadyCityDataChanged(m_isReadyCityData);
}

QJsonValue Storage::processProductToAwailablePoints(QJsonArray productIdList)
{
    //marker
        auto point_delivery = m_cityData.value("point_delivery").toArray();
        auto point_limit =   m_cityData.value("point_limit").toArray();
         QJsonArray all_point_id;
         for(int i =0; i < point_delivery.count(); i++){
             all_point_id.append(point_delivery.at(i).toObject().value("id").toInt());
             }
         for(int i =0; i < productIdList.count();i++){
         auto id = productIdList.at(i).toInt();
        for(int d = 0; d < point_limit.count(); d++){
            if(id == point_limit.at(d).toObject().value("product").toInt()){
                auto points = point_limit.at(i).toObject().value("points").toArray();
                for(int k =0; k < all_point_id.count(); k++){
                    if(!points.contains(all_point_id.at(k))){
                        all_point_id.removeAt(k);
                        i--;
                    }

                }
            }
        }
         }
         if(all_point_id.count() == 0){
             setSorryMessageVisible(true);
         }else{
             setSorryMessageVisible(false);
         }
        return all_point_id;
}



void Storage::updateFromDb()
{
    this->m_userCity = db->loadCity();    
    this->setTokenLk(db->loadToken());
    emit userCityChanged(m_userCity);   
    City city(m_userCity);
    this->setUserAddress(db->fethAddress(city.getId(), city.getDomain().getPath()));
}


void Storage::setTmpPathUploads(const QString &value)
{
    tmpPathUploads = value;
    db->setTmpPathUploads(value);
    this->updateFromDb();
    this->setImagesPath(this->tmpPathUploads+"/"+folderName);
}

QJsonObject Storage::cityData() const
{
    return m_cityData;
}

QString Storage::imagesPath() const
{
    return m_imagesPath;
}

QJsonArray Storage::userAddress() const
{
    return m_userAddress;
}

QString Storage::userEmail() const
{
    return m_userEmail;
}

QString Storage::tokenLk() const
{
    return m_tokenLk;
}

bool Storage::isReadyCityData() const
{
    return m_isReadyCityData;
}

void Storage::logout()
{
    setTokenLk("");
    clearPhoneTextField();
}

