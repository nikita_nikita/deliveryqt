#ifndef CART_H
#define CART_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include "../models/cartproductmodel.h"
#include "../entity/orderrequest.h"
#include "./src/entity/pay/carddata.h"
#include "./src/entity/checkstreet.h"
#include "../entity/loyality.h"
#include "../models/toppingkitmodel.h"

class Cart : public QObject
{
    Q_OBJECT

    Q_PROPERTY(CartProductModel* model MEMBER m_model NOTIFY modelChanged)
    Q_PROPERTY(QJsonArray conteiner READ conteiner WRITE setConteiner NOTIFY conteinerChanged) // корзина (бес бонусов)
    Q_PROPERTY(QJsonValue bonus READ bonus WRITE setBonus NOTIFY bonusChanged) // бонус
    Q_PROPERTY(int personCount READ personCount WRITE setPersonCount NOTIFY personCountChanged) // количество персон
    Q_PROPERTY(QString promocode READ promocode WRITE setPromocode NOTIFY promocodeChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString userPhone READ userPhone WRITE setUserPhone NOTIFY userPhoneChanged)
    Q_PROPERTY(QString orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)
    Q_PROPERTY(ToppingKitModel* toppingkit MEMBER m_toppingkit NOTIFY toppingkitChanged)
    Q_PROPERTY(QString name READ name WRITE setname NOTIFY nameChanged)
    Q_PROPERTY(bool toppings_enable READ  toppings_enable WRITE settoppings_enable NOTIFY  toppings_enableChanged)


    Q_PROPERTY(QString cuponName READ cuponName WRITE setCuponName NOTIFY cuponNameChanged)
    Q_PROPERTY(QString cuponDescroption READ cuponDescroption WRITE setCuponDescroption NOTIFY cuponDescroptionChanged)
    Q_PROPERTY(QString cuponColor READ cuponColor WRITE setCuponColor NOTIFY cuponColorChanged)

    Q_PROPERTY(QJsonValue cuponInfo READ cuponInfo WRITE setCuponInfo NOTIFY cuponInfoChanged)
    Q_PROPERTY(QJsonValue cartInfo READ cartInfo WRITE setCartInfo NOTIFY cartInfoChanged)


    Q_PROPERTY(QString userEmail READ userEmail WRITE setUserEmail NOTIFY userEmailChanged)
    Q_PROPERTY(int takeAwayPoint READ takeAwayPoint WRITE setTakeAwayPoint NOTIFY takeAwayPointChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)
    Q_PROPERTY(QString orderTime READ orderTime WRITE setOrderTime NOTIFY orderTimeChanged)
    Q_PROPERTY(QString time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(int payMethod READ payMethod WRITE setPayMethod NOTIFY payMethodChanged)
    Q_PROPERTY(bool noChange READ noChange WRITE setNoChange NOTIFY noChangeChanged)
    Q_PROPERTY(double cash READ cash WRITE setCash NOTIFY cashChanged)
    Q_PROPERTY(QString street READ street WRITE setStreet NOTIFY streetChanged)
    Q_PROPERTY(QString building READ building WRITE setBuilding NOTIFY buildingChanged)
    Q_PROPERTY(int entrance READ entrance WRITE setEntrance NOTIFY entranceChanged)
    Q_PROPERTY(int floor READ floor WRITE setFloor NOTIFY floorChanged)
    Q_PROPERTY(int room READ room WRITE setRoom NOTIFY roomChanged)

    Q_PROPERTY(bool dataProcessing READ dataProcessing WRITE setDataProcessing NOTIFY dataProcessingChanged)
    Q_PROPERTY(int streetId READ streetId WRITE setStreetId NOTIFY streetIdChanged)
    Q_PROPERTY(bool validCheckOut READ validCheckOut WRITE setValidCheckOut NOTIFY validCheckOutChanged)
    Q_PROPERTY(bool agreement READ agreement WRITE setAgreement NOTIFY agreementChanged)
    //стоимость
    Q_PROPERTY(double total_cost READ totalCost WRITE setTotalCost NOTIFY totalCostChanged) // сумма без скидки
    Q_PROPERTY(double endCost READ endCost WRITE setEndCost NOTIFY endCostChanged) // сумма со скидкой
    //удалить
    Q_PROPERTY(double bonusCost READ bonusCost WRITE setBonusCost NOTIFY bonusCostChanged)
    Q_PROPERTY(double deliveryCost READ deliveryCost WRITE setDeliveryCost NOTIFY deliveryCostChanged)
    // Данные карты
    Q_PROPERTY(QString cardNumber READ cardNumber WRITE setCardNumber NOTIFY cardNumberChanged)
    Q_PROPERTY(QString cardDate READ cardDate WRITE setCardDate NOTIFY cardDateChanged)
    Q_PROPERTY(QString cardCVC READ cardCVC WRITE setCardCVC NOTIFY cardCVCChanged)
    // подсчитывается в CartGoods
    Q_PROPERTY(QJsonValue cuponStatus READ cuponStatus WRITE setCuponStatus NOTIFY cuponStatusChanged) // купон  статус
    Q_PROPERTY(QJsonValue discount READ discount WRITE setDiscount NOTIFY discountChanged) // скидка
    Q_PROPERTY(QJsonObject cupon READ cupon WRITE setCupon NOTIFY cuponChanged)
    Q_PROPERTY(double bonusPay READ bonusPay WRITE setBonusPay NOTIFY bonusPayChanged)
    Q_PROPERTY(int offerValue READ offerValue WRITE setOfferValue NOTIFY offerValueChanged)
    Q_PROPERTY(QString adress READ adress WRITE setAdress NOTIFY adressChanged)
    Q_PROPERTY(QString takeAwayString READ takeAwayString WRITE settakeAwayString NOTIFY takeAwayStringChanged)

public:
    explicit Cart(QObject *parent = nullptr);

    QJsonArray conteiner() const;
    int personCount() const;
    QString promocode() const;
    QString userName() const;
    QString userPhone() const;
    int takeAwayPoint() const;
    QString comment() const;
    QString orderTime() const;
    int payMethod() const;
    bool noChange() const;
    QString street() const;
    int entrance() const;
    int floor() const;
    int room() const;
    double cash() const;
    bool dataProcessing() const;
    int streetId() const;
    QString orderType() const;
    bool validCheckOut() const;
    QString building() const;
    QString cardNumber() const;
    QString cardDate() const;
    QString cardCVC() const;

    CartProductModel *getModel();

    Q_INVOKABLE QString getOsName();

    double totalCost() const;

    QJsonValue cuponStatus() const;

    QJsonArray bonus() const;

    QJsonValue discount() const;

    QJsonObject cupon() const;

    double endCost() const;

    double bonusCost() const;

    QString userEmail() const;

    double bonusPay() const;

    bool agreement() const;

    double deliveryCost() const;

    QString time() const;

    QString promoCodeInfo() const;

    QString promocodeName() const;

    QString cuponName() const;

    QString cuponDescroption() const;

    int offerValue() const;

    QString cuponColor() const;

    QJsonValue cuponInfo() const;

    QJsonValue cartInfo() const;



    bool getPromocodeValid() const;
    void setPromocodeValid(bool value);

    bool getContacles_delivery() const;
    void setContacles_delivery(bool value);
    void setToppingData(QJsonArray jArr);



    QString name() const;

    bool toppings_enable() const;

    QString adress() const
    {


        return m_adress;
    }

    QString takeAwayString() const
    {
        return m_takeAwayString;
    }

    QString getDeliveryTime() const;
    void setDeliveryTime(const QString &value);

    QString getPickUpTime() const;
    void setPickUpTime(const QString &value);

signals:

    void orderCanSend();
    void modelChanged(CartProductModel* model);
    void cartChanged(CartProductList cart);
    void conteinerChanged(QJsonArray conteiner);
    void personCountChanged(int personCount);
    void promocodeChanged(QString promocode);
    void userNameChanged(QString userName);
    void userPhoneChanged(QString userPhone);

    void takeAwayPointChanged(int takeAwayPoint);
    void commentChanged(QString comment);
    void orderTimeChanged(QString orderTime);
    void payMethodChanged(int payMethod);
    void noChangeChanged(bool noChange);
    void streetChanged(QString street);

    void entranceChanged(int entrance);
    void floorChanged(int floor);
    void roomChanged(int room);
    void cashChanged(double cash);
    void dataProcessingChanged(bool dataProcessing);
    void streetIdChanged(int streetId);
    void orderTypeChanged(QString orderType);
    void validCheckOutChanged(bool validCheckOut);
    void buildingChanged(QString building);

    void cardNumberChanged(QString cardNumber);
    void cardDateChanged(QString cardDate);
    void cardCVCChanged(QString cardCVC);
    // Отправка заказа  с оплатой на месте
    void sendOrder(OrderRequest request);
    // при оплате через карту
    void payingByCard(OrderRequest request, CardData card);
    void payingByMarket(OrderRequest request);
    // при оплате через маркет



    //Программа лояльности

    void totalCostChanged(double total_cost);

    void cuponStatusChanged(QJsonValue cuponStatus);

    void bonusChanged(QJsonArray bonus);

    void discountChanged(QJsonValue discount);

    void cuponChanged(QJsonObject cupon);

    void endCostChanged(double endCost);

    void bonusCostChanged(double bonusCost);

    void userEmailChanged(QString userEmail);

    void createdCheckStreetEntity(CheckStreet data);

    void bonusPayChanged(double bonusPay);

    void agreementChanged(bool agreement);

    void deliveryCostChanged(double deliveryCost);

    void timeChanged(QString time);

    void promoCodeInfoChanged(QString promoCodeInfo);

    void promocodeNameChanged(QString promocodeName);

    void cuponNameChanged(QString cuponName);

    void cuponDescroptionChanged(QString cuponDescroption);

    void offerValueChanged(int offerValue);

    void cuponColorChanged(QString cuponColor);

    void cuponInfoChanged(QJsonArray cuponInfo);

    void cartInfoChanged(QJsonArray cartInfo);



    void checkoutEnableChanged(bool data);
    void toppingkitChanged(ToppingKitModel* toppingkit);
    void nameChanged(QString name);
    void toppings_enableChanged(bool toppings_enable);

    void adressChanged(QString adress);

    void takeAwayStringChanged(QString takeAwayString);

public slots:
    void sendOrderSendSignal();
    void add(QJsonObject product);
    void clearPromocode();
    int getToppingCount();
    void updateToppingCount(int index,int count);
    void addList(QJsonArray cartList);
    void addWithOptions(QJsonObject product, QJsonObject operation);
    void setOptions(int index, QJsonObject options);
    void setQuantity(QJsonObject product , int quantity,bool withoutOptions=false);
    void setQuantityByIndex(int index, int quantity);
    void setConteiner(QJsonArray conteiner);
    void updateContainer(CartProductList cart);
    void setPersonCount(int personCount);
    void setPromocode(QString promocode);
    void setUserName(QString userName);
    void setUserPhone(QString userPhone);
    void setTakeAwayPoint(int takeAwayPoint);
    void setComment(QString comment);
    void setOrderTime(QString orderTime);
    void setPayMethod(int payMethod);
    void setNoChange(bool noChange);
    void setStreet(QString street);
    void setEntrance(int entrance);
    void setFloor(int floor);
    void setRoom(int room);
    void setCash(double cash);
    void onLoyalityDataLoaded(LoyalityList data);
    void onLoyalityLoadError();
    void setDataProcessing(bool dataProcessing);
    void setStreetId(int streetId);
    void setOrderType(QString orderType);
    void clear();
    void setValidCheckOut(bool validCheckOut);
    void setTotalCost(double cost);
    void setBuilding(QString building);
    void setCardNumber(QString cardNumber);
    void setCardDate(QString cardDate);
    void setCardCVC(QString cardCVC);
    //   отправка данных на сервер  //0  тип наличные //  1 картой на месте
    void sendOrderToServer(int c = 0);


    // Оплатить картой
    void payByCard();
    // оплатить  через adroid pay или apple pay
    void payByMarket();


    void setCuponStatus(QJsonValue cuponStatus);
    void setBonus(QJsonValue bonus);
    void setDiscount(QJsonValue discount);
    void setCupon(QJsonObject cupon);
    void setEndCost(double end_cost);
    void setBonusCost(double bonusCost);
    void setUserEmail(QString userEmail);
    void createCheckStreetEntity();
    void setStrets(StreetList streets);
    void setBonusPay(double bonusPay);
    void setAgreement(bool agreement);
    void setDeliveryCost(double deliveryCost);
    void setPaidDeliveryEntity(QJsonObject entity);
    void setTime(QString time);
    void setPromoCodeInfo(QString promoCodeInfo);
    void setPromoCodeName(QString promocodeName);
    void setCuponName(QString cuponName);
    void setCuponDescroption(QString cuponDescroption);
    void setOfferValue(int offerValue);
    void onLoyalityLoadEmpty();
    void setCuponColor(QString cuponColor);
    OrderRequest getCurrentOrderRequest(int paymethod);
    OrderRequest getOrderToLoyality();

    void setCuponInfo(QJsonValue cuponInfo);

    void setCartInfo(QJsonValue cartInfo);

    void setname(QString name);

    void settoppings_enable(bool toppings_enable);

    void setAdress(QString adress)
    {
        if (m_adress == adress)
            return;

        m_adress = adress;
        emit adressChanged(m_adress);
    }

    void settakeAwayString(QString takeAwayString)
    {
        if (m_takeAwayString == takeAwayString)
            return;

        m_takeAwayString = takeAwayString;
        emit takeAwayStringChanged(m_takeAwayString);
    }

private slots:
    void sendORderToLoyality();

    void updateToppingMaxQuantity(int total_cost);
private:

    void setLoyalityBonus(QJsonArray jArr);


    CartProductModel *m_model;
    QJsonArray m_conteiner;
    CartProductList m_cart;
    int m_personCount=1;
    QString m_promocode;
    QString m_userName;
    QString m_userPhone;
    QString m_orderType;
    int m_takeAwayPoint = -1;
    QString m_comment;
    QString m_orderTime="on-ready";
    int m_payMethod=0;
    bool m_noChange=0;
    QString m_street;
    int m_entrance=0;
    int m_floor=0;
    int m_room=0;
    double m_cash=0;
    bool m_dataProcessing = true;
    int m_streetId=0;
    bool m_validCheckOut=false;
    QString m_building;
    double total_cost =0;
    // создает заказ
    OrdeAddress getCurrentOrderAddress();
    QString m_cardNumber="";
    QString m_cardDate="";
    QString m_cardCVC="";
    QJsonValue m_cuponStatus;
    QJsonValue m_bonus;
    QJsonValue m_discount;
    QJsonObject m_cupon;
    double m_end_cost=0;
    double m_bonusCost=0;
    QString m_userEmail;
    StreetList streets; //улицы загруженные
    double m_bonusPay=0;
    bool m_agreement=false;
    double m_deliveryCost=0;
    QJsonObject m_paidDeliveryEntity;
    QString m_time;
    QString m_promoCodeInfo;
    QString m_promocodeName;
    QString m_cuponName;
    QString m_cuponDescroption;
    int m_offerValue = 0;
    QString m_cuponColor;
    QJsonValue m_cuponInfo;
    QJsonValue m_cartInfo;
    bool promocodeValid = false ;
    bool contacles_delivery = false;
    QString deliveryTime = "";
    QString pickUpTime = "";
    ToppingKitModel* m_toppingkit;
    QString m_name;
    bool m_toppings_enable;
    QString m_adress;
    QString m_takeAwayString;
};

#endif // CART_H
