#ifndef MENU_H
#define MENU_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include "../entity/citydata.h"
#include "../models/productsmodel.h"
#include "../models/productitemlistmodel.h"

class Menu : public QObject
{
    Q_OBJECT

    Q_PROPERTY(ProductsModel* toppingsJap MEMBER m_toppingsJap  NOTIFY toppingJapChanged)
    Q_PROPERTY(ProductsModel* toppingsPizza MEMBER m_toppingsPizza NOTIFY toppingPizzaChanged)
    Q_PROPERTY(ProductItemListModel* composite MEMBER m_composite NOTIFY compositeChanged)
    Q_PROPERTY(QString item_icon READ item_icon WRITE setItem_icon NOTIFY item_iconChanged)
    Q_PROPERTY(int currentOffers READ currentOffers WRITE setCurrentOffers NOTIFY currentOffersChanged)

public:
    explicit Menu(QObject *parent = nullptr);
    int currentOffers() const;
    QString item_icon() const;

signals:

    void toBackPage();
    void deptChanged(int dept);
    void currentOffersChanged(int currentOffers);
    void toppingJapChanged(ProductsModel* toppingsJap);
    void toppingPizzaChanged(ProductsModel* toppingsPizza);
    void showInfoComposite(QString name);
    void compositeChanged(ProductItemListModel* composite);
    void item_iconChanged(QString item_icon);
public slots:
    void setCityDataJson(CityData cityData);
    void backCategory();
    void setCurrentDept(int dept);
    void setCurrentOffers(int currentOffers);
    void resetPositions();
    void setInfoComposite(Product prod);
    void setItem_icon(QString item_icon);

private:

    QJsonObject m_goods;
    QJsonObject m_products;
    QJsonArray m_categories;

    QJsonArray appendArray(QJsonArray target, QJsonArray source);
    ProductList allProduts;
    int m_currentOffers=0;
    ProductsModel *m_toppingsJap;
    ProductsModel *m_toppingsPizza;
    ProductItemListModel *m_composite;

    QString m_item_icon;
};

#endif // MENU_H
