#ifndef CORE_H
#define CORE_H
#include <QQmlApplicationEngine>
#include <QObject>
#include "servicelocator.h"
#include "controllers/objectcontroller.h"
#include "controllers/profilecontroller.h"
#include "controllers/paymentcontroller.h"
#include "controllers/loadercontroller.h"
#include "controllers/contactscontroller.h"
#include "controllers/checkoutdelivetycontroller.h"
#include "controllers/menucontroller.h"
#include "controllers/cartcontroller.h"
//#include "controllers/firebasemessagecontroller.h"
#include "controllers/feedbackcontroller.h"
#include <QJsonDocument>
class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = nullptr);
    ~Core();
    bool run();

    ServiceLocator *getServiceLocator() const;


public slots:
private:
    QQmlApplicationEngine engine;
    ServiceLocator *serviceLocator;
    void registerController(ObjectController *controller);
    QList<ObjectController*> controllers;
    ProfileController *profileConroller;
    PaymentController *paymentController;
    LoaderController *loaderController;
    ContactsController *contactController;
    CheckoutDelivetyController * deliveryController;
    MenuController* menuController;
    CartController* cartController;
    FeedbackController* feedbackController;
//    FirebaseMessageController *firebaseController;
    void setContextPropertys(QQmlContext *context);
};

#endif // CORE_H
