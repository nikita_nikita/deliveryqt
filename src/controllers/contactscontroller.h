#ifndef CONTACTSCONTROLLER_H
#define CONTACTSCONTROLLER_H

#include "objectcontroller.h"

class ContactsController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(QString country READ country WRITE setCountry NOTIFY countryChanged)
    Q_PROPERTY(QString cityName READ cityName WRITE setCityName NOTIFY cityNameChanged)

public:
    explicit ContactsController(QObject *parent = nullptr);
    virtual void setServiceLocator(ServiceLocator *value);
    QString country() const;

    QString cityName() const;

signals:
    void countryChanged(QString country);

    void cityNameChanged(QString cityName);

public slots:
    void setCountry(QString country);
    void updateParams();
    void setCityName(QString cityName);

private:
    QString m_country;
    QString m_cityName;
};

#endif // CONTACTSCONTROLLER_H
