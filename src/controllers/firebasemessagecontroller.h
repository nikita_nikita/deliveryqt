#ifndef FIREBASEMESSAGECONTROLLER_H
#define FIREBASEMESSAGECONTROLLER_H

#include "objectcontroller.h"
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>

class FirebaseMessageController : public ObjectController
{
    Q_OBJECT
public:
    explicit FirebaseMessageController(QObject *parent = nullptr);
    virtual void setServiceLocator(ServiceLocator *sv);
    Q_PROPERTY(QJsonObject messageData READ messageData WRITE setMessageData NOTIFY messageDataChanged)
    Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY messageChanged)
    QJsonObject messageData() const;

    QString message() const;

public slots:
    void onMessageDownload(QJsonObject message);
    void registrateDevice();
    void setFirebaseToken(QString token);
    void setMessageData(QJsonObject messageData);

    void setMessage(QString message);

signals:
    void messageDataChanged(QJsonObject messageData);

    void messageChanged(QString message);

private slots:
    void onRegisterDevice(QNetworkReply *reply);
private:
    QJsonObject jObj;
    QNetworkAccessManager *firebaseRegistrationDeviceManager;

    void differenceData();
    void saveData(QJsonObject jObj);
    QString path = "/device/auth/registration";
    QString firebaseToken;
#ifdef Q_OS_ANDROID
   QString platform = "android";
#elif defined(Q_OS_LINUX)
       QString platform = "linux";
#elif defined(Q_OS_IOS)
       QString platform = "ios";
#endif
    void prepaireQuery();

    bool sendRegistration = false;
    QJsonObject m_messageData;
    QString m_message = "";
};

#endif // FIREBASEMESSAGECONTROLLER_H
