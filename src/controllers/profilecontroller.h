#ifndef PROFILECONTROLLER_H
#define PROFILECONTROLLER_H

#include "objectcontroller.h"
#include "../models/addresslistmodel.h"

class ProfileController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString userPhone READ userPhone WRITE setUserPhone NOTIFY userPhoneChanged)
    Q_PROPERTY(QString userBirthDay READ userBirthDay WRITE setUserBirthDay NOTIFY userBirthDayChanged)
    Q_PROPERTY(QString userEmail READ userEmail WRITE setUserEmail NOTIFY userEmailChanged)
    Q_PROPERTY(double userWallet READ userWallet WRITE setUserWallet NOTIFY userWalletChanged)
    Q_PROPERTY(bool agreement READ agreement WRITE setAgreement NOTIFY agreementChanged)
    Q_PROPERTY(bool birtDateEnable READ birtDateEnable WRITE setBirtDateEnable NOTIFY birtDateEnableChanged)
    Q_PROPERTY(AddressListModel* addresesModel MEMBER addresesmodel NOTIFY addresesModelChanged)
    Q_PROPERTY(bool user_walet_status READ user_walet_status WRITE setUser_walet_status NOTIFY user_walet_statusChanged)
public:
    explicit ProfileController(QObject *parent = nullptr);

    QString userName() const;

    QString userPhone() const;

    QString userEmail() const;
    bool agreement() const;

    QString userBirthDay() const;

    double userWallet() const;

    bool birtDateEnable() const;

    virtual void setServiceLocator(ServiceLocator *sv);
    bool user_walet_status() const;

signals:

    void userNameChanged(QString userName);

    void userPhoneChanged(QString userPhone);

    void userEmailChanged(QString userEmail);

    void agreementChanged(bool agreement);

    void userBirthDayChanged(QString userBirthDay);

    void userWalletChanged(double userWallet);

    void birtDateEnableChanged(bool birtDateEnable);

    void addresesModelChanged(AddressListModel* addresesModel);

    void user_walet_statusChanged(bool user_walet_status);

public slots:
    void setUserName(QString userName);

    void setUserPhone(QString userPhone);

    void setUserEmail(QString userEmail);

    void setAgreement(bool agreement);


    void setUserBirthDay(QString userBirthDay);

    void setUserWallet(double userWallet);

    void setBirtDateEnable(bool birtDateEnable);

     void setUserData(QJsonObject user);
    void setCityData(CityData data);
      void saveUserData();

      void setUser_walet_status(bool user_walet_status);

private:
    QString m_userName="";
    QString m_userPhone;
    QString m_userEmail;

    bool m_agreement=false;
    QString m_userBirthDay;
    double m_userWallet=0;

    bool m_birtDateEnable;
    AddressListModel *addresesmodel;

    bool m_user_walet_status = true;
};

#endif // PROFILECONTROLLER_H
