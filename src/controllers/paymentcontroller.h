#ifndef PAYMENTCONTROLLER_H
#define PAYMENTCONTROLLER_H

#include "objectcontroller.h"

class PaymentController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(bool googlePayButton READ googlePayButton WRITE setGooglePayButton NOTIFY googlePayButtonChanged)
    Q_PROPERTY(bool applePayButton READ applePayButton WRITE setApplePayButton NOTIFY applePayButtonChanged)
    Q_PROPERTY(bool valid READ valid WRITE setValid NOTIFY validChanged)
    Q_PROPERTY(bool validEmail READ validEmail WRITE setValidEmail NOTIFY validEmailChanged)
    Q_PROPERTY(bool sslSuppurt READ sslSuppurt NOTIFY sslSuppurtChanged)
    Q_PROPERTY(bool delivery_cash READ delivery_cash WRITE setDelivery_cash NOTIFY delivery_cashChanged)
    Q_PROPERTY(bool delivery_card READ delivery_card WRITE setDelivery_card NOTIFY delivery_cardChanged)
    Q_PROPERTY(bool delivery_online READ delivery_online WRITE setDelivery_online NOTIFY delivery_onlineChanged)
    Q_PROPERTY(bool pickup_cash READ pickup_cash WRITE setPickup_cash NOTIFY pickup_cashChanged)
    Q_PROPERTY(bool pickup_card READ pickup_card WRITE setPickup_card NOTIFY pickup_cardChanged)
    Q_PROPERTY(bool pickup_online READ pickup_online WRITE setPickup_online NOTIFY pickup_onlineChanged)
    Q_PROPERTY(QJsonArray payArray READ payArray WRITE setPayArray NOTIFY payArrayChanged)
public:
    explicit PaymentController(QObject *parent = nullptr);

    bool googlePayButton() const;

    bool applePayButton() const;
    virtual void setServiceLocator(ServiceLocator *sv);

    bool valid() const;

    bool validEmail() const;

    bool sslSuppurt() const;

    bool delivery_cash() const
    {
        return m_delivery_cash;
    }

    bool delivery_card() const
    {
        return m_delivery_card;
    }

    bool delivery_online() const
    {
        return m_delivery_online;
    }

    bool pickup_cash() const
    {
        return m_pickup_cash;
    }

    bool pickup_card() const
    {
        return m_pickup_card;
    }

    bool pickup_online() const
    {
        return m_pickup_online;
    }

    QJsonArray payArray() const
    {
        return m_payArray;
    }

signals:

    void googlePayButtonChanged(bool googlePayButton);

    void applePayButtonChanged(bool applePayButton);

    void validChanged(bool valid);

    void validEmailChanged(bool validEmail);

    void sslSuppurtChanged(bool sslSuppurt);

    void delivery_cashChanged(bool delivery_cash);

    void delivery_cardChanged(bool delivery_card);

    void delivery_onlineChanged(bool delivery_online);

    void pickup_cashChanged(bool pickup_cash);

    void pickup_cardChanged(bool pickup_card);

    void pickup_onlineChanged(bool pickup_online);

    void payArrayChanged(QJsonArray payArray);

public slots:
    void setGooglePayButton(bool googlePayButton);
    void setApplePayButton(bool applePayButton);
    void setCityData(CityData data);
    void setValid(bool valid);

    void calculateValid();

    void setValidEmail(bool validEmail);

    void validateEmail();
    void payByMarket();
    void payByCard();

    void setDelivery_cash(bool delivery_cash);

    void setDelivery_card(bool delivery_card);

    void setDelivery_online(bool delivery_online);

    void setPickup_cash(bool pickup_cash);

    void setPickup_card(bool pickup_card);

    void setPickup_online(bool pickup_online);

    void setPayArray(QJsonArray payArray);
private slots:
void onOrderCanSended();
private:
    bool m_googlePayButton=false;
    bool m_applePayButton=false;
    bool m_valid = false;
    bool m_validEmail = true;
    bool isValidEmail();
    bool m_sslSuppurt;
    bool m_delivery_cash;
    bool m_delivery_card;
    bool m_delivery_online;
    bool m_pickup_cash;
    bool m_pickup_card;
    bool m_pickup_online;
    QJsonArray m_payArray;
};

#endif // PAYMENTCONTROLLER_H
