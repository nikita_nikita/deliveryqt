#include "feedbackcontroller.h"

//#include <QDebug>
FeedbackController::FeedbackController(QObject *parent) : ObjectController(parent)
{

    setStars(4);
    imageLoadmanager = new QNetworkAccessManager();
    feedbackLoadmanager = new QNetworkAccessManager();
    iikoOrderManager = new QNetworkAccessManager();
    timer = new QTimer(this);
    //    m_timer = new QTimer(this);
    m_orders = new OrderHystoryModel(this);
    connect(imageLoadmanager, &QNetworkAccessManager::finished,this, &FeedbackController::onImageLoaded);
    connect(m_orders, &OrderHystoryModel::getOrderStatus,this,&FeedbackController::loadOrderData);
    connect(iikoOrderManager, &QNetworkAccessManager::finished,this, &FeedbackController::onOrderDataLoaded);
    connect(feedbackLoadmanager, &QNetworkAccessManager::finished,this, &FeedbackController::onFeedbackLoaded);
    connect(timer, &QTimer::timeout, this, &FeedbackController::onTimerStop);
    connect(m_orders, &OrderHystoryModel::feedbackMessage, this, &FeedbackController::showCallbackMessage);
    //    connect(m_timer, &QTimer::timeout,this,&FeedbackController::onM_timerStoped);
}

void FeedbackController::setServiceLocator(ServiceLocator *value)
{
    ObjectController::setServiceLocator(value);
    connect(serviceLocator->getKapibaras(),&Kapibaras::clientDataLoaded,this,&FeedbackController::setOrderHystory);
    connect(serviceLocator->getRouter(),&Router::pageChanged, this,  &FeedbackController::onRouterPageChanged);
    connect(serviceLocator->getKapibaras(),&Kapibaras::cityDataLoaded,this, &FeedbackController::onCityDataLoaded);
    connect(serviceLocator->getCart(),&Cart::sendOrder,serviceLocator->getKapibaras(),&Kapibaras::updateClientData);
    connect(serviceLocator->getStorage(),&Storage::tokenLkChanged,this,&FeedbackController::clearOrderList);
    connect(serviceLocator->getKapibaras(),&Kapibaras::clientHystoryLoaded,this,&FeedbackController::onClientDataReloaded);


}



int FeedbackController::stars() const
{
    return m_stars;
}

int FeedbackController::order_number() const
{
    return m_order_number;
}

QString FeedbackController::ratingName() const
{
    return m_ratingName;
}

bool FeedbackController::commentVisible() const
{
    return m_commentVisible;
}

QString FeedbackController::comment() const
{
    return m_comment;
}

QString FeedbackController::imageSource() const
{
    return m_imageSource;
}

QStringList FeedbackController::imageList() const
{
    return m_imageList;
}

bool FeedbackController::bigSizePhoto() const
{
    return m_bigSizePhoto;
}

bool FeedbackController::noDataWrite() const
{
    return m_noDataWrite;
}

OrderHystoryModel *FeedbackController::orders() const
{
    return m_orders;
}

bool FeedbackController::feedbackMessage() const
{
    return m_feedbackMessage;
}

void FeedbackController::clearPhotoList()
{
    setImageList(QStringList());
}

void FeedbackController::showCallbackMessage()
{
    setFeedbackMessage(true);
}

void FeedbackController::setOrderHystory(QJsonObject jObj)
{
    auto orders = jObj.value("client_history").toArray();

    m_orders->add(orders);
    //    timer->start(30000);

}

void FeedbackController::sendFeedback()
{
    QJsonObject obj;

    auto hotButton = getHotBunnotComment();
    if(stars() < 4){
        QJsonArray arr;
        for(int i = 0; i < uploadedList.count();i++){
            arr.append(uploadedList.at(i));
        }
        obj["images"] =arr;
        obj["comment"] = getHotBunnotComment()+"    Отзыв клиента: "+comment();
        if(hotButton == ""&&comment() == ""){
            setNoDataWrite(true);
            return;
        }

    }

    obj.insert("value",stars()+1);
    obj.insert("order",order_number());
#ifdef Q_OS_ANDROID
    obj.insert("platform", "android");
#elif defined(Q_OS_LINUX)
    obj.insert("platform", "linux");
#elif defined(Q_OS_IOS)
    obj.insert("platform", "ios");
#endif




    sendComent(obj);


}

void FeedbackController::imagemCaminho()
{

}

void FeedbackController::loadOrderData(int ordernumber)
{

    qDebug()<<"loadOrderData"<<status_order();
    if(!status_order()){
        return;
    }
    QUrl url;
    auto kapibaras = serviceLocator->getKapibaras();
    url.setScheme(kapibaras->getDefaultSheme());
    url.setHost("");
    url.setPath("");
    QNetworkRequest request(url);
    QJsonObject obj;
    obj["order"] = ordernumber;
    QJsonDocument doc(obj);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    iikoOrderManager->post(request, doc.toJson(QJsonDocument::Compact));
    if(loadList.indexOf(ordernumber) == -1){
        loadList.append(ordernumber);
        qDebug()<<" loadList.append(ordernumber);";
    };


}

void FeedbackController::onOrderDataLoaded(QNetworkReply *reply)
{

    if(reply->error()){return;}
    QJsonDocument document = QJsonDocument::fromJson(reply->readAll());

    if(document.toJson() == "[\n]\n"){
        return;
    }


    m_orders->updateOrderInfo(IikoOrder(document.object()));

    //    m_timer->start(3000);
    //айко Entity
    //
}

void FeedbackController::onTimerStop()
{

    qDebug()<<"onTimerStop";

    timer->stop();
    serviceLocator->getKapibaras()->loadClientHistory();
    timer->start(15000);

}

void FeedbackController::onRouterPageChanged(QString page)
{
    //    qDebug()<<"onRouterPageChanged";
    if(page == "orders"){
        serviceLocator->getKapibaras()->loadClientHistory();
        timer->start(15000);
    }else{
        timer->stop();
    }
}

void FeedbackController::onCityDataLoaded(CityData data)
{

    if(data.getConfig().getCityIdStatusVisibleList() != QJsonArray()){
        this->setProgressbarVisible( data.getConfig().getCityIdStatusVisibleList().contains(data.getOrganizationInfo().getId()));
    }else{
        this->m_progressbarVisible = true;
    }
    qDebug()<<"onCityDataLoaded";


    this->setStatus_order(data.getOrganizationInfo().getOrder_status());


    this->setFeedBackButtonVisible( data.getOrganizationInfo().getCallbackEnagle());
}

void FeedbackController::clearOrderList()
{
    if(serviceLocator->getStorage()->tokenLk() ==""){
        this->m_orders->add(QJsonArray());
    }

}


void FeedbackController::onClientDataReloaded(QJsonArray jArr)
{
    this->m_orders->add(jArr);

}



void FeedbackController::setStars(int stars)
{
    if (m_stars == stars)
        return;
    m_stars = stars;
    emit starsChanged(m_stars);
    switch (stars) {
    case 0:
        setRatingName("Ужасно");
        setCommentVisible(true);
        return;
    case 1:
        setRatingName("Плохо");
        setCommentVisible(true);
        return;
    case 2:
        setRatingName("Удовлетворительно");
        setCommentVisible(true);
        return;
    case 3:
        setRatingName("Хорошо");
        setCommentVisible(true);
        return;
    case 4:
        setRatingName("Отлично");
        setCommentVisible(false);
        return;
    default:
        return;
    }

}

void FeedbackController::onFeedbackLoaded(QNetworkReply *reply)
{

    if(!reply->error()){
        if( QTime::currentTime() < QTime::fromString((serviceLocator->getWorktime()->timeClosed(),"hh:mm"))){
            setStatus_order(false);

        }else{
            m_orders->setFeedBackStatusFalse(m_order_number);
        }
    }else{
        qDebug()<<"onFeedbackLoaded error!";
    }
}

void FeedbackController::setOrder_number(int order_number)
{
    if (m_order_number == order_number)
        return;
    uploadedList.clear();
    setImageList(QStringList());
    m_order_number = order_number;
    emit order_numberChanged(m_order_number);
}

void FeedbackController::setRatingName(QString ratingName)
{
    if (m_ratingName == ratingName)
        return;

    m_ratingName = ratingName;
    emit ratingNameChanged(m_ratingName);
}

void FeedbackController::setCommentVisible(bool commentVisible)
{
    if (m_commentVisible == commentVisible)
        return;

    m_commentVisible = commentVisible;
    emit commentVisibleChanged(m_commentVisible);
}

void FeedbackController::addImageUrl(QString url)
{
    QFile file(url);
    if (file.open(QIODevice::ReadOnly)){
        if(file.size() > 5000000){

            setBigSizePhoto(true);
            return;
        }
        file.close();
    }

    m_imageList.append(url);
    imageListChanged(m_imageList);
    uploadPhoto(url);
}

void FeedbackController::removeImageUrl(int index)
{
    m_imageList.removeAt(index);
    imageListChanged(m_imageList);;

}

void FeedbackController::onImageLoaded(QNetworkReply *reply)
{
    if(!reply->error()){
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        QJsonObject root = document.object();
        uploadedList.append(root.value("image").toString());
    }



}

void FeedbackController::setBigSizePhoto(bool bigSizePhoto)
{
    if (m_bigSizePhoto == bigSizePhoto)
        return;

    m_bigSizePhoto = bigSizePhoto;
    emit bigSizePhotoChanged(m_bigSizePhoto);
}

void FeedbackController::setNoDataWrite(bool noDataWrite)
{
    if (m_noDataWrite == noDataWrite)
        return;

    m_noDataWrite = noDataWrite;
    emit noDataWriteChanged(m_noDataWrite);
}

void FeedbackController::setorders(OrderHystoryModel *orders)
{
    if (m_orders == orders)
        return;

    m_orders = orders;
    emit ordersChanged(m_orders);
}

QString FeedbackController::getHotBunnotComment()
{
    QString result = "";
    if(m_rollRaz){
        result.append("* Роллы разваливаются *");
    }
    if(m_neVkus){
        result.append("* Невкусно *");
    }
    if(m_zabSous){
        result.append("* Забыли соус *");
    }
    if(m_opozdali){
        result.append("* Опоздали *");
    }
    if(m_errorInOrder){
        result.append("* Ошибка в заказе *");
    }
    if(m_courierMudak){
        result.append("* Грубый курьер *");
    }
    return result;
}

void FeedbackController::uploadPhoto(QString uri)
{
    QFile *file  =  new QFile(uri);
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);




    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"order\""));
    textPart.setBody("25");

    QHttpPart imagePart;
    //    imagePart.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"image\"; filename=\"1.jpg\""));
    file->open(QIODevice::ReadOnly);
    imagePart.setBody(file->readAll());
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    multiPart->append(textPart);
    multiPart->append(imagePart);

    QUrl url;
    auto kapibaras = serviceLocator->getKapibaras();
    url.setScheme(kapibaras->getDefaultSheme());
    url.setHost("mob."+kapibaras->getDomain());
    url.setPath("/order/loadCommentImage");
    QNetworkRequest request(url);

    imageLoadmanager->post(request, multiPart);

}

void FeedbackController::sendComent(QJsonObject obj)
{
    QUrl url;
    auto kapibaras = serviceLocator->getKapibaras();
    url.setScheme(kapibaras->getDefaultSheme());
    url.setHost("mob."+kapibaras->getDomain());
    url.setPath("/order/comment");
    QNetworkRequest request(url);
    QJsonDocument doc(obj);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    feedbackLoadmanager->post(request, doc.toJson(QJsonDocument::Compact));
    setFeedBackthankMessage(true);

}

void FeedbackController::setComment(QString comment)
{
    if (m_comment == comment)
        return;

    m_comment = comment;
    emit commentChanged(m_comment);
}


void FeedbackController::addHotButton(QString name)
{
    hotButtons.insert(name,false);
}

void FeedbackController::activeHotButton(QString name, bool status)
{
    hotButtons.insertMulti(name,status);
}

void FeedbackController::setImageSource(QString imageSource)
{
    if (m_imageSource == imageSource)
        return;

    m_imageSource = imageSource;
    emit imageSourceChanged(m_imageSource);
}

void FeedbackController::setImageList(QStringList imageList)
{
    if (m_imageList == imageList)
        return;

    m_imageList = imageList;
    emit imageListChanged(m_imageList);
}




