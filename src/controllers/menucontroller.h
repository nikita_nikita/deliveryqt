#ifndef MENUCONTROLLER_H
#define MENUCONTROLLER_H

#include "objectcontroller.h"
#include "../models/pagesmodel.h"
#include "../models/productfiltemodel.h"
#include <QSortFilterProxyModel>
#include  <QVector>
#include <QTextCodec>
class MenuController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(PagesModel* pages READ pages WRITE setPages NOTIFY pagesChanged)
    Q_PROPERTY(QString nameSearch READ nameSearch WRITE setNameSearch NOTIFY nameSearchChanged)
    Q_PROPERTY(ProductFilteModel* searchProductModel READ searchProductModel WRITE setSearchProductModel NOTIFY searchProductModelChanged)
    Q_PROPERTY(QString paper READ paper  NOTIFY paperChanged)


public:
    explicit MenuController(QObject *parent = nullptr);
    virtual void setServiceLocator(ServiceLocator *sv);

    PagesModel *pages() const;

    void setProduct(ProductList product);

    QString nameSearch() const;

    ProductsModel* searchFilterModel() const;

    ProductFilteModel* searchProductModel() const;



    void setProducts(ProductList &value);

    QString paper() const
    {
        QByteArray encodedString = "🌶";
        QTextCodec *codec = QTextCodec::codecForName("UTF-16");
        QString string = codec->toUnicode(encodedString);
        return 	  string;
    }

signals:

    void pagesChanged(PagesModel* pages);

    void nameSearchChanged(QString nameSearch);

    void searchFilterModelChanged(ProductsModel* searchFilterModel);

    void searchProductModelChanged(ProductFilteModel* searchProductModel);

    void paperChanged(std::string paper);

public slots:
    void addProductToCart(int id);

    void setCityData(CityData cityData);
    void setPages(PagesModel* pages);
    void setProductinCart(CartProductList cart);
    void setInfoComposite(Product prod);

    void setNameSearch(QString nameSearch);

    void setSearchFilterModel(ProductsModel* searchFilterModel);
    void showInfoComposition(int id);
    void setSearchProductModel(ProductFilteModel* searchProductModel);

private:
    void search(QString);
    PagesModel* m_pages;
    QString m_nameSearch;
    ProductsModel* m_searchFilterModel;
    const int searchCount = 10;
    QHash<int,Product> products;
    ProductFilteModel* m_searchProductModel;
   
};

#endif // MENUCONTROLLER_H
