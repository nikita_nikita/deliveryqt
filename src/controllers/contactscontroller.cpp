#include "contactscontroller.h"

ContactsController::ContactsController(QObject *parent) : ObjectController(parent)
{

}

void ContactsController::setServiceLocator(ServiceLocator *value)
{
    ObjectController::setServiceLocator(value);
    connect(serviceLocator->getStorage(), &Storage::userCityChanged, this, &ContactsController::updateParams);

    updateParams();
}

QString ContactsController::country() const
{
    return m_country;
}

QString ContactsController::cityName() const
{
    return m_cityName;
}

void ContactsController::setCountry(QString country)
{
    if (m_country == country)
        return;

    m_country = country;
    emit countryChanged(m_country);
}

void ContactsController::updateParams()
{
     City userCity = City::fromJson(serviceLocator->getStorage()->userCity());
    this->setCountry(userCity.getDomain().getCountryName());
     this->setCityName(userCity.getName());

}

void ContactsController::setCityName(QString cityName)
{
    if (m_cityName == cityName)
        return;

    m_cityName = cityName;
    emit cityNameChanged(m_cityName);
}
