#ifndef OBJECTCONTROLLER_H
#define OBJECTCONTROLLER_H

#include <QObject>
#include "../servicelocator.h"

class ObjectController : public QObject
{
    Q_OBJECT
public:
    explicit ObjectController(QObject *parent = nullptr);
     virtual void setServiceLocator(ServiceLocator *value);


    QString getUserPhone() const;
    void setUserPhone(const QString &value);

    QString getUserFirebaseToken() const;
    void setUserFirebaseToken(const QString &value);

signals:

public slots:
protected:
    ServiceLocator *serviceLocator;

private:
    QString userPhone;
    QString userFirebaseToken;
};

#endif // OBJECTCONTROLLER_H
