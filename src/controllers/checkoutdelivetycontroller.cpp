#include "checkoutdelivetycontroller.h"
CheckoutDelivetyController::CheckoutDelivetyController(QObject *parent) : ObjectController(parent)
{
    streetModel  =new StreetModel(this);
    m_streetFilterModel = new StreetFilterModel(this);
    m_streetFilterModel->setSourceModel(streetModel);
    m_streetFilterModel->setFilterRole(StreetModel::NameRole);
    m_streetFilterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    timer = new QTimer(this);
    m_timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &CheckoutDelivetyController::checkWorktime);
    //    connect(m_timer, &QTimer::timeout, this, &CheckoutDelivetyController::updateOrganizationInfo);
}

void CheckoutDelivetyController::setServiceLocator(ServiceLocator *value)
{
    ObjectController::setServiceLocator(value);
    connect(serviceLocator->getKapibaras(), &Kapibaras::cityDataLoaded, this,&CheckoutDelivetyController::setCityData);
    connect(serviceLocator->getCart(), &Cart::streetIdChanged, this, &CheckoutDelivetyController::updateValidCheckStreet);
    connect(serviceLocator->getKapibaras(), &Kapibaras::organizationInfoloaded, this ,&CheckoutDelivetyController::onOrganizationInfoLoaded);

    connect(serviceLocator->getCart(), &Cart::streetIdChanged, this, &CheckoutDelivetyController::checkStreet);
    connect(serviceLocator->getCart(), &Cart::buildingChanged, this, &CheckoutDelivetyController::checkStreet);
    connect(serviceLocator->getCart(), &Cart::endCostChanged, this, &CheckoutDelivetyController::checkStreet);
    connect(serviceLocator->getCart(), &Cart::streetChanged, this, &CheckoutDelivetyController::updateStreetFilter);
    connect(serviceLocator->getCart(), &Cart::buildingChanged, this, &CheckoutDelivetyController::updateStreetFilter);
    connect(serviceLocator->getCart(),&Cart::payMethodChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);

    connect(serviceLocator->getCart(),&Cart::streetChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);
    connect(serviceLocator->getCart(),&Cart::orderTypeChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);
    connect(serviceLocator->getCart(),&Cart::takeAwayPointChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);
    connect(serviceLocator->getCart(),&Cart::payMethodChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);

    connect(serviceLocator->getCart()->getModel(),&CartProductModel::cartChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);
    connect(serviceLocator->getStatusStorage(),&StatusStorage::checkStreetResponseChanged,this,&CheckoutDelivetyController::onDeliveryTypeChanged);

    connect(serviceLocator->getKapibaras(), &Kapibaras::streetLoaded, this, &CheckoutDelivetyController::setStrets);
    connect(serviceLocator->getWorktime(), &WorkTime::deliveryIntervasChanged, this, &CheckoutDelivetyController::onDeliveryIntervasChanged);
    connect(serviceLocator->getWorktime(), &WorkTime::pickupIntervasChanged, this, &CheckoutDelivetyController::obPickUpIntervasChanged);
    connect(serviceLocator->getKapibaras(),&Kapibaras::loadingCityData, this,&CheckoutDelivetyController::clearData);
}

bool CheckoutDelivetyController::validStreet() const
{
    return m_validStreet;
}

bool CheckoutDelivetyController::allowDelivery() const
{
    return m_allowDelivery;
}

bool CheckoutDelivetyController::allowPickUP() const
{
    return m_allowPickUP;
}

bool CheckoutDelivetyController::timeWork() const
{
    return m_timeWork;
}

int CheckoutDelivetyController::tabIndex() const
{
    return m_tab_index;
}

QJsonArray CheckoutDelivetyController::ponintData() const
{
    return m_ponintData;
}



void CheckoutDelivetyController::setValidStreet(bool validStreet)
{
    if (m_validStreet == validStreet)
        return;

    m_validStreet = validStreet;

    emit validStreetChanged(m_validStreet);
}

void CheckoutDelivetyController::setCityData(CityData cityData)
{

    this->m_checkStreet = cityData.getOrganizationInfo().getCheckStreet();
    this->updateValidCheckStreet();
    int currentDayOfWeek = QDate::currentDate().dayOfWeek()-1;
    auto dayEntity = cityData.getWorkTime().toWeekList().at(currentDayOfWeek);
    this->timeDeliveryClose = dayEntity.getTimeCloseDelivery();
    this->timePickupClose = dayEntity.getTimeClosePickup();
    this->checkWorktime();

    this->setWarnings(cityData.getWarnings());
    this->setAllowDelivery(cityData.getOrganizationInfo().getDelivery()==1);
    this->setContactless_delivery(cityData.getOrganizationInfo().getContactless_delivery());
    this->setAllowPickUP(cityData.getOrganizationInfo().getPickup()==1);
    this->setNewToppingLogic(cityData.getOrganizationInfo().getToppingsEnable());

    //    m_timer->start(60000);
}

void CheckoutDelivetyController::updateValidCheckStreet()
{
    if(m_checkStreet){
        this->setValidStreet(serviceLocator->getCart()->streetId()>0);
    } else {
        this->setValidStreet(true);
    }
}

void CheckoutDelivetyController::checkStreet()
{

    if(m_checkStreet){
        if(serviceLocator->getCart()->streetId()!=0&&serviceLocator->getCart()->building()!=""){

            this->serviceLocator->getStatusStorage()->setCheckStreetLoaded(false);
            this->serviceLocator->getCart()->createCheckStreetEntity();


        }
    }else {

        if(serviceLocator->getCart()->streetId()!=0&&serviceLocator->getCart()->building()!=""){
            this->serviceLocator->getCart()->createCheckStreetEntity();
            this->serviceLocator->getStatusStorage()->setCheckStreetLoaded(false);
        }
        else{
            this->serviceLocator->getStatusStorage()->setCheckStreetLoaded(false);
        }
    }
}

void CheckoutDelivetyController::setStrets(StreetList streets)
{
    this->streetModel->setStreetlist(streets);
}

void CheckoutDelivetyController::updateStreetFilter()
{
    QString text = serviceLocator->getCart()->street();
    this->m_streetFilterModel->setFilterWildcard(text);
}

void CheckoutDelivetyController::setAllowDelivery(bool allowDelivery)
{
    if (m_allowDelivery == allowDelivery)
        return;

    m_allowDelivery = allowDelivery;
    emit allowDeliveryChanged(m_allowDelivery);
}

void CheckoutDelivetyController::setAllowPickUP(bool allowPickUP)
{
    if (m_allowPickUP == allowPickUP)
        return;

    m_allowPickUP = allowPickUP;
    emit allowPickUPChanged(m_allowPickUP);
}

void CheckoutDelivetyController::setTimeWork(bool timeWork)
{
    if (m_timeWork == timeWork)
        return;

    m_timeWork = timeWork;
    emit timeWorkChanged(m_timeWork);
}

void CheckoutDelivetyController::setTabIndex(int tab_index)
{
    if(this->serviceLocator->getCart()->orderType()==""){
        switch (tab_index) {
        case 0:
            this->serviceLocator->getCart()->setOrderType("delivery");
            break;
        case 1:
            this->serviceLocator->getCart()->setOrderType("take-away");
            break;

        }
    }
    if (m_tab_index == tab_index)
        return;
    m_tab_index = tab_index;
    switch (tab_index) {
    case 0:
        this->serviceLocator->getCart()->setOrderType("delivery");
        break;
    case 1:
        this->serviceLocator->getCart()->setOrderType("take-away");
        break;

    }

    emit tabIndexchanged(m_tab_index);
}

void CheckoutDelivetyController::setPonintData(QJsonArray ponintData)
{
    if (m_ponintData == ponintData)
        return;

    m_ponintData = ponintData;
    emit ponintDataChanged(m_ponintData);
}

void CheckoutDelivetyController::onDeliveryTypeChanged()
{

    auto deliveryType = serviceLocator->getCart()->orderType();
    auto takeAwayPoint = serviceLocator->getCart()->takeAwayPoint();

    setNotDelivery("");

    if(deliveryType != "delivery" && takeAwayPoint > 0){
        serviceLocator->getRouter()->setCheckoutDelivery_valid(true);
        return;
    }

    auto  status = serviceLocator->getStatusStorage()->checkStreetResponse().value("status").toBool();

    bool checkStreet =serviceLocator->getCart()->streetId()!=0&&serviceLocator->getCart()->building().length()>0;
    auto items = serviceLocator->getStatusStorage()->checkStreetResponse().value("items").toArray();

    if(deliveryType == "delivery" && status &&checkStreet){
        if(serviceLocator->getCart()->getModel()->notDeliverySeparately()){
            serviceLocator->getRouter()->setCheckoutDelivery_valid(false);
        }else{
            serviceLocator->getRouter()->setCheckoutDelivery_valid(true);
        }
        return;
    }

    if(deliveryType == "delivery" && !status &&checkStreet&&items.count()>0){
        QString notDeliveryItems;
        for(int i = 0; i < items.count();i++){
            auto item = items.at(i).toObject();
            notDeliveryItems +=  Product(item).getName() + "<br>";
        }
        notDeliveryItems += "Не доставляется<br> по вашему адресу";
        setNotDelivery(notDeliveryItems);
    }
    auto nulina = serviceLocator->getStatusStorage()->checkStreetResponse().value("amount") == QJsonValue().Null;


    if(deliveryType == "delivery"&&!status &&nulina&&serviceLocator->getCart()->streetId()!=0){
        setNotDelivery("Извините мы не можем<br>доставить по вашему адресу ");

    }
    serviceLocator->getRouter()->setCheckoutDelivery_valid(false);

}

void CheckoutDelivetyController::clearData()
{
    this->setNotDelivery("");
    serviceLocator->getCart()->clear();

}

void CheckoutDelivetyController::setNotDelivery(QString notDelivery)
{
    if (m_notDelivery == notDelivery)
        return;

    m_notDelivery = notDelivery;
    emit notDeliveryChanged(m_notDelivery);
}


void CheckoutDelivetyController::setNewToppingLogic(bool newToppingLogic)
{
    if (m_newToppingLogic == newToppingLogic)
        return;

    m_newToppingLogic = newToppingLogic;
    emit newToppingLogicChanged(m_newToppingLogic);
}

void CheckoutDelivetyController::setContactless_delivery(bool contactless_delivery)
{

    qDebug()<<"contact >> >> >> "<< contactless_delivery;
    if (m_contactless_delivery == contactless_delivery)
        return;

    m_contactless_delivery = contactless_delivery;

    emit contactless_deliveryChanged(m_contactless_delivery);
}

void CheckoutDelivetyController::setContactless_status(bool contactless_status)
{
    if (m_contactless_status == contactless_status)
        return;

    m_contactless_status = contactless_status;
    serviceLocator->getCart()->setContacles_delivery(contactless_status);
    emit contactless_statusChanged(m_contactless_status);
}



void CheckoutDelivetyController::updateOrganizationInfo()
{

    serviceLocator->getKapibaras()->loadWorkTimeAndOrganizationInfo();
    auto method = serviceLocator->getCart()->payMethod();
    if(method==5||method==4){
        serviceLocator->getRouter()->setPageLoadedVisible(false);


    }else{
        this->setLoaderProcess(true);
    }
}

void CheckoutDelivetyController::onDeliveryIntervasChanged(QJsonArray jArr)
{
    auto first = jArr.at(0).toString();
    auto cart = serviceLocator->getCart();
//    if(first == "Как можно скорее"){
//        cart->setOrderTime("on-ready");
//    }

    if(cart->getDeliveryTime() ==""){
        if(cart->orderType()== "delivery"){
            cart->setOrderTime(first);
            cart->setTime(first);
        }
        cart->setDeliveryTime(first);
    }

}

void CheckoutDelivetyController::obPickUpIntervasChanged(QJsonArray jArr)
{
    auto first = jArr.at(0).toString();
    auto cart = serviceLocator->getCart();
//    if(first == "Как можно скорее"){
//        cart->setOrderTime("on-ready");
//    }
    if(cart->getPickUpTime() ==""){

        if(cart->orderType()!= "delivery"){
            cart->setOrderTime(first);
            cart->setTime(first);

        }
        cart->setPickUpTime(first);
        qDebug()<<"first pickUp"<<first<<cart->getPickUpTime() ;
    }

}

void CheckoutDelivetyController::onOrganizationInfoLoaded(OrganizationInfo info, WorkTimeEntity workTime)
{
    //    this->m_checkStreet = info.getCheckStreet();
    //    this->updateValidCheckStreet();


    this->setAllowDelivery(info.getDelivery()==1);
    //    this->setContactless_delivery(info.getContactless_delivery());
    this->setAllowPickUP(info.getPickup()==1);
    //    this->setNewToppingLogic(info.getToppingsEnable());
    int currentDayOfWeek = QDate::currentDate().dayOfWeek()-1;
    auto dayEntity = workTime.toWeekList().at(currentDayOfWeek);
    if(this->timeDeliveryClose != dayEntity.getTimeCloseDelivery()){
        this->timeDeliveryClose = dayEntity.getTimeCloseDelivery();
    }
    this->timePickupClose = dayEntity.getTimeClosePickup();
    //    this->calculateTabIndex();
    this->checkWorktime();
    //    this->clearData();
    //    m_timer->start(60000);
    if (serviceLocator->getRouter()->page() == "payment"){
        this->setLoaderProcess(false);
        auto delivery = serviceLocator->getCart()->orderType() == "delivery";
        if(delivery && !allowDelivery()){
            setDeliveryNotAwalable(true);
            //            serviceLocator->getRouter()->push("checkout");
            return;
        }
        if(!delivery&&!allowPickUP()){
            setDeliveryNotAwalable(true);
            //            serviceLocator->getRouter()->push("checkout");
            return;
        }
        serviceLocator->getCart()->sendOrderSendSignal();
    }
}







void CheckoutDelivetyController::checkWorktime()
{
    timer->stop();
    auto timeNow = QTime::currentTime();
    if (this->allowDelivery()){
        this->setAllowDelivery(timeNow<this->timeDeliveryClose);
    }
    if (this->allowPickUP()) {
        this->setAllowPickUP(timeNow<this->timePickupClose);
    }
    this->setTimeWork(timeNow<timeDeliveryClose || timeNow<timePickupClose);
    if (timeNow>timeDeliveryClose && timeNow>timePickupClose){
        return ;
    }
    QTime mm;

    if (timeDeliveryClose<timePickupClose){
        mm = timeDeliveryClose;
        if (mm <timeNow) {
            mm = timePickupClose;
        }
    } else {
        mm = timePickupClose;
        if (mm <timeNow) {
            mm = timeDeliveryClose;
        }
    }
    timer->start(timeNow.secsTo(mm)*1000);

}

//void CheckoutDelivetyController::calculateTabIndex()
//{
//     return;
//    if(alreadyCalculate){

//    }else{
//        alreadyCalculate = true;

//    }
//    if  (!this->allowDelivery()&&!this->allowPickUP()) {
//        return ;
//    }
//    if (this->allowDelivery()){ // если разрешенна доставка  то  вкладка поумолчанию на ней
//        this->setTabIndex(0);
//        this->serviceLocator->getCart()->setOrderType("delivery");


//    if (this->allowPickUP()){
//        this->setTabIndex(1);
//        this->serviceLocator->getCart()->setOrderType("take-away");

//    }
//}


//}

bool CheckoutDelivetyController::getNewToppingKitLogic() const
{
    return newToppingKitLogic;
}

void CheckoutDelivetyController::setNewToppingKitLogic(bool value)
{
    newToppingKitLogic = value;
}



bool CheckoutDelivetyController::choiceCurrentTime()
{
    auto cart = serviceLocator->getCart();
    auto userDeliveryTime = cart->time();
    auto delivery = cart->orderType();
    auto worktime = serviceLocator->getWorktime();

    if(delivery != 1){
        auto diferentationTime    = worktime->delivery();
    }else{

    }





}


