#include "paymentcontroller.h"
#include "../config/settingapp.h"
#include  <QtGlobal>

PaymentController::PaymentController(QObject *parent) : ObjectController(parent)
{

}

bool PaymentController::googlePayButton() const
{
        if(QT_VERSION_MINOR< 13){

    return false;
          }
 return m_googlePayButton;
}

bool PaymentController::applePayButton() const
{
    return m_applePayButton;
}

void PaymentController::setServiceLocator(ServiceLocator *sv)
{
    ObjectController::setServiceLocator(sv);
    connect(sv->getKapibaras(), &Kapibaras::cityDataLoaded, this, &PaymentController::setCityData);
    connect(sv->getCart(), &Cart::payMethodChanged, this, &PaymentController::calculateValid);
    connect(sv->getCart(), &Cart::orderTypeChanged, this, &PaymentController::calculateValid);
    connect(sv->getCart(), &Cart::noChangeChanged, this, &PaymentController::calculateValid);
    connect(sv->getCart(), &Cart::cashChanged, this, &PaymentController::calculateValid);
    connect(sv->getRouter(), &Router::pageChanged, this, &PaymentController::calculateValid);

     connect(sv->getCart(), &Cart::userEmailChanged, this, &PaymentController::calculateValid);
     connect(sv->getCart(), &Cart::cardNumberChanged, this, &PaymentController::calculateValid);
     connect(sv->getCart(), &Cart::cardDateChanged, this, &PaymentController::calculateValid);
     connect(sv->getCart(), &Cart::cardCVCChanged, this, &PaymentController::calculateValid);
     connect(sv->getCart(),&Cart::orderCanSend,this,&PaymentController::onOrderCanSended);
}

bool PaymentController::valid() const
{
    return m_valid;
}

bool PaymentController::validEmail() const
{
    return m_validEmail;
}

bool PaymentController::sslSuppurt() const
{
    return QSslSocket::supportsSsl();
}

void PaymentController::setGooglePayButton(bool googlePayButton)
{
    if (m_googlePayButton == googlePayButton)
        return;

    m_googlePayButton = googlePayButton;
    emit googlePayButtonChanged(m_googlePayButton);
}

void PaymentController::setApplePayButton(bool applePayButton)
{
    if (m_applePayButton == applePayButton)
        return;

    m_applePayButton = applePayButton;
    emit applePayButtonChanged(m_applePayButton);
}

void PaymentController::setCityData(CityData data)
{
    QString os = this->serviceLocator->getCart()->getOsName();
    if (os=="android"&&SettingApp::suppurtSSl()){

        this->setGooglePayButton(data.getPayments().getGoogle_pay()==1);
    }
    if (os =="ios"&&SettingApp::suppurtSSl()){
        this->setApplePayButton(data.getPayments().getApple_pay()==1);
    }
    this->setPickup_card(data.getPayments().getPickup_card()==1);
    this->setPickup_cash(data.getPayments().getPickup_cash()==1);
    this->setPickup_online(data.getPayments().getPickup_online()==1);
    this->setDelivery_card(data.getPayments().getDelivery_card()==1);
    this->setDelivery_cash(data.getPayments().getDelivery_cash()==1);
    this->setDelivery_online(data.getPayments().getDelivery_online()==1);

}

void PaymentController::setValid(bool valid)
{
    if (m_valid == valid)
        return;

    m_valid = valid;
    serviceLocator->getRouter()->setPaymentController_valid(m_valid);
    emit validChanged(m_valid);
}

void PaymentController::calculateValid()
{

    switch (serviceLocator->getCart()->payMethod()) {
    case 1: {// pay by card


        auto email = isValidEmail();
        bool cardNumber = serviceLocator->getCart()->cardNumber().count()>0;
        bool cardDate = serviceLocator->getCart()->cardDate().count()>0;
        bool cartCVC = serviceLocator->getCart()->cardCVC().count()>0;
         this->setValid(cardNumber&&cardDate&&cartCVC&&email);
        return;
    }
    case 3: {
        if (serviceLocator->getCart()->orderType()=="take-away"){
            this->setValid(true);
            return ;
        } else {
            if (serviceLocator->getCart()->noChange()){
                this->setValid(true);
                return;
            } else {
                auto cart = serviceLocator->getCart();
                auto priceChack = cart->totalCost() - cart->bonusPay();

                if(serviceLocator->getCart()->cash() == 0){

                    this->setValid(false);
                    return;
                }
                auto check = serviceLocator->getCart()->cash() > priceChack;

                setValid(check);
                return;

            }
        }
    }
    case 2 : {

        this->setValid(true);
        return;

    }
    case 4 : {

        this->setValid(true);
        return;

    }
    case 5 : {

        this->setValid(true);
        return;

    }


    default: {
        //this->setValid(true);
        return;
    }
    }

}

void PaymentController::setValidEmail(bool validEmail)
{
    if (m_validEmail == validEmail)
        return;

    m_validEmail = validEmail;
    emit validEmailChanged(m_validEmail);
}

void PaymentController::validateEmail()
{
    QRegExp rx("^[A-Z0-9a-z._-]{1,}@(\\w+)(\\.(\\w+))(\\.(\\w+))?(\\.(\\w+))?$");
    this->setValidEmail(this->isValidEmail());
}

void PaymentController::payByMarket()
{
    this->validateEmail();
    if (validEmail()){
        serviceLocator->getCart()->payByMarket();
    }
}

void PaymentController::payByCard()
{
    this->validateEmail();
    if (validEmail()){
        serviceLocator->getCart()->payByCard();
    }
}

void PaymentController::setDelivery_cash(bool delivery_cash)
{
    if (m_delivery_cash == delivery_cash)
        return;

    m_delivery_cash = delivery_cash;
    emit delivery_cashChanged(m_delivery_cash);
}

void PaymentController::setDelivery_card(bool delivery_card)
{
    if (m_delivery_card == delivery_card)
        return;

    m_delivery_card = delivery_card;
    emit delivery_cardChanged(m_delivery_card);
}

void PaymentController::setDelivery_online(bool delivery_online)
{
    if (m_delivery_online == delivery_online)
        return;

    m_delivery_online = delivery_online;
    emit delivery_onlineChanged(m_delivery_online);
}

void PaymentController::setPickup_cash(bool pickup_cash)
{
    if (m_pickup_cash == pickup_cash)
        return;

    m_pickup_cash = pickup_cash;
    emit pickup_cashChanged(m_pickup_cash);
}

void PaymentController::setPickup_card(bool pickup_card)
{
    if (m_pickup_card == pickup_card)
        return;

    m_pickup_card = pickup_card;
    emit pickup_cardChanged(m_pickup_card);
}

void PaymentController::setPickup_online(bool pickup_online)
{
    if (m_pickup_online == pickup_online)
        return;

    m_pickup_online = pickup_online;
    emit pickup_onlineChanged(m_pickup_online);
}

void PaymentController::setPayArray(QJsonArray payArray)
{
    if (m_payArray == payArray)
        return;

    m_payArray = payArray;
    emit payArrayChanged(m_payArray);
}

void PaymentController::onOrderCanSended()
{
    auto cart = serviceLocator->getCart();
    switch(cart->payMethod()){

    case 1:
        payByCard();
        break;
    case 3:
        if (cart->orderType()=="take-away") {
            cart->sendOrderToServer(6);
        } else {
            cart->sendOrderToServer(3);
        }
        break;
    case 2:
        cart->sendOrderToServer(2);
        break;
    case 4:
       payByMarket();
        break;
    case 5:
        payByMarket();
        break;
    }
}


bool PaymentController::isValidEmail()
{
    QRegExp rx("^[A-Z0-9a-z._-]{1,}@(\\w+)(\\.(\\w+))(\\.(\\w+))?(\\.(\\w+))?$");
    return rx.exactMatch(serviceLocator->getCart()->userEmail());
}
