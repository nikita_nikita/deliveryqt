#include "profilecontroller.h"
#include <QDate>
ProfileController::ProfileController(QObject *parent) : ObjectController(parent)
{
    addresesmodel = new AddressListModel(this);
}

QString ProfileController::userName() const
{
    return m_userName;
}

QString ProfileController::userPhone() const
{
    return m_userPhone;
}

QString ProfileController::userEmail() const
{
    return m_userEmail;
}



bool ProfileController::agreement() const
{
    return m_agreement;
}

QString ProfileController::userBirthDay() const
{
    return m_userBirthDay;
}

void ProfileController::setServiceLocator(ServiceLocator *sv)
{
    ObjectController::setServiceLocator(sv);
    connect(sv->getStorage(), &Storage::userChanged, this, &ProfileController::setUserData);
    connect(sv->getKapibaras(), &Kapibaras::cityDataLoaded, this, &ProfileController::setCityData);
}

bool ProfileController::user_walet_status() const
{
    return m_user_walet_status;
}

bool ProfileController::birtDateEnable() const
{
    return m_birtDateEnable;
}

double ProfileController::userWallet() const
{
    return m_userWallet;
}

void ProfileController::setUserName(QString userName)
{
    if (m_userName == userName)
        return;

    m_userName = userName;
    emit userNameChanged(m_userName);
}

void ProfileController::setUserPhone(QString userPhone)
{
    if (m_userPhone == userPhone)
        return;

    m_userPhone = userPhone;
    emit userPhoneChanged(m_userPhone);
}

void ProfileController::setUserEmail(QString userEmail)
{
    if (m_userEmail == userEmail)
        return;

    m_userEmail = userEmail;
    emit userEmailChanged(m_userEmail);
}



void ProfileController::setAgreement(bool agreement)
{
    if (m_agreement == agreement)
        return;

    m_agreement = agreement;
    emit agreementChanged(m_agreement);
}

void ProfileController::saveUserData()
{

    int agr = this->agreement()?1:0;
    QDate date = QDate::fromString(this->userBirthDay(), "dd.MM.yyyy");
    QJsonObject client_info={
        {"name", this->userName()},
        {"phone", this->userPhone()},
        {"email", this->userEmail()},
        {"birthday", date.toString("yyyy-MM-dd")},
        {"agreement", agr}
    };

 this->serviceLocator->getKapibaras()->saveClient(client_info, this->serviceLocator->getStorage()->tokenLk());
}

void ProfileController::setUser_walet_status(bool user_walet_status)
{
    if (m_user_walet_status == user_walet_status)
        return;

    m_user_walet_status = user_walet_status;
    emit user_walet_statusChanged(m_user_walet_status);
}

void ProfileController::setUserData(QJsonObject user)
{
    this->setUserWallet(user["wallet"].toDouble());
    this->setUser_walet_status(user["wallet_status"].toBool());
    QJsonObject client_info = user["client_info"].toObject();
    this->setUserPhone(client_info["phone"].toString());
   this->setUserName(client_info["name"].toString());

   this->setUserEmail(client_info["email"].toString());
   this->setAgreement(client_info["agreement"].toInt());
   QDate date = QDate::fromString(client_info["birthday"].toString(), "yyyy-MM-dd");

   this->setUserBirthDay(date.toString("dd.MM.yyyy"));
   this->setBirtDateEnable(this->userBirthDay().count()==0);
   this->addresesmodel->setArrayData(AddresesList::fromJson(user["client_address"].toArray()));
}

void ProfileController::setUserBirthDay(QString userBirthDay)
{
    if (m_userBirthDay == userBirthDay)
        return;

    m_userBirthDay = userBirthDay;
    emit userBirthDayChanged(m_userBirthDay);
}

void ProfileController::setUserWallet(double userWallet)
{
    if (qFuzzyCompare(m_userWallet, userWallet))
        return;


    m_userWallet = userWallet ;
    emit userWalletChanged(m_userWallet);
}

void ProfileController::setBirtDateEnable(bool birtDateEnable)
{
    if (m_birtDateEnable == birtDateEnable)
        return;

    m_birtDateEnable = birtDateEnable;
    emit birtDateEnableChanged(m_birtDateEnable);
}

void ProfileController::setCityData(CityData data)
{
    this->addresesmodel->setCheckStreet(data.getOrganizationInfo().getCheckStreet());
}

