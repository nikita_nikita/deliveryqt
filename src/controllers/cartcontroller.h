#ifndef CARTCONTROLLER_H
#define CARTCONTROLLER_H


#include "objectcontroller.h"
#include "../models/servivekitmodel.h"
class CartController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(ServiveKitModel* serviceKit MEMBER m_serviceKitModel NOTIFY serviceKitChanged)
    Q_PROPERTY(bool zeroToppingMessage READ zeroToppingMessage WRITE setZeroToppingMessage NOTIFY zeroToppingMessageChanged)
    Q_PROPERTY(QString screen READ screen WRITE setScreen NOTIFY screenChanged)
public:
    explicit CartController(QObject *parent = nullptr);
    virtual void setServiceLocator(ServiceLocator *sv);
    bool checkoutYellowButton() const;


    void sendORderToLoyality();
    bool zeroToppingMessage() const;

    QString screen() const
    {
        return m_screen;
    }

signals:
    void serviceKitChanged(ServiveKitModel* serviceKit);

    void checkoutEnabkechanged(bool data);




    void zeroToppingMessageChanged(bool zeroToppingMessage);

    void screenChanged(QString screen);

public slots:
//    void updateToppingCount(int index, int count);
    void setCityData(CityData cityData);
    void setProductinCart(CartProductList cart);
//    void onCartChange(Cart cart);
    void onCartProductChanged();





    void setZeroToppingMessage(bool zeroToppingMessage)
    {
        if (m_zeroToppingMessage == zeroToppingMessage)
            return;

        m_zeroToppingMessage = zeroToppingMessage;
        emit zeroToppingMessageChanged(m_zeroToppingMessage);
    }

    void setScreen(QString screen)
    {
        if (m_screen == screen)
            return;

        m_screen = screen;
        emit screenChanged(m_screen);
    }
private slots:
    void onPageChange(QString page);
private:
    ServiveKitModel * m_serviceKitModel;
    bool m_validEmail=true;
    bool m_checkoutYellowButton = false;

    bool m_zeroToppingMessage = false;
    QString m_screen;
};





#endif // CARTCONTROLLER_H
