#include "objectcontroller.h"

ObjectController::ObjectController(QObject *parent) : QObject(parent)
{

}

void ObjectController::setServiceLocator(ServiceLocator *value)
{
    serviceLocator = value;
}

QString ObjectController::getUserPhone() const
{
    return userPhone;
}

void ObjectController::setUserPhone(const QString &value)
{
    userPhone = value;
}

QString ObjectController::getUserFirebaseToken() const
{
    return userFirebaseToken;
}

void ObjectController::setUserFirebaseToken(const QString &value)
{
    userFirebaseToken = value;
}
