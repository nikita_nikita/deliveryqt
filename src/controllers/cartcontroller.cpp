#include "cartcontroller.h"

#include <src/service/cart.h>

#include <QSizeF>
#include <QScreen>
#include <QGuiApplication>


CartController::CartController(QObject *parent) : ObjectController(parent)
{
    m_serviceKitModel = new ServiveKitModel(this);
    //    auto screenSize = QGuiApplication::primaryScreen();
    //    setScreen("Ширина в мм  : "+QString::number(screenSize->physicalSize().toSize().width() )+"<br> высота в мм : "+ QString::number(screenSize->physicalSize().toSize().height() ));

}

void CartController::setServiceLocator(ServiceLocator *sv)
{
    ObjectController::setServiceLocator(sv);
    connect(sv->getKapibaras(), &Kapibaras::cityDataLoaded, this, &CartController::setCityData);

    connect(sv->getCart(), &Cart::cartChanged, this, &CartController::setProductinCart);
    connect(sv->getKapibaras(),&Kapibaras::loyalityLoaded,sv->getCart(),&Cart::onLoyalityDataLoaded);
    connect(sv->getKapibaras(),&Kapibaras::loyalityLoadError,sv->getCart(),&Cart::onLoyalityLoadError);
    connect(sv->getCart()->getModel(),&CartProductModel::cartChanged,this,&CartController::sendORderToLoyality);
    connect(sv->getCart(),&Cart::orderTypeChanged,this,&CartController::sendORderToLoyality);
    connect(sv->getCart(),&Cart::promocodeChanged,this,&CartController::sendORderToLoyality);
    connect(sv->getCart(),&Cart::timeChanged,this, &CartController::sendORderToLoyality);
    connect(sv->getCart(),&Cart::personCountChanged,this, &CartController::sendORderToLoyality);
    connect(sv->getCart(),&Cart::bonusChanged,this,&CartController::onCartProductChanged);
    connect(sv->getCart(),&Cart::conteinerChanged,this,&CartController::onCartProductChanged);
    connect(sv->getRouter(),&Router::pageChanged, this,&CartController::onPageChange );
}

void CartController::sendORderToLoyality()
{
    auto order = this->serviceLocator->getCart()->getOrderToLoyality();

    this->serviceLocator->getKapibaras()->sendOrderToLoyality(order);
}

bool CartController::zeroToppingMessage() const
{
    return m_zeroToppingMessage;
}



void CartController::setCityData(CityData cityData)
{
    if(cityData.getOrganizationInfo().getToppingsEnable()){
        serviceLocator->getCart()->settoppings_enable(cityData.getOrganizationInfo().getToppingsEnable());
        serviceLocator->getCart()->setToppingData(cityData.getToppingKit());
    }else{
        m_serviceKitModel->setServiceKits(cityData.getService_kit());
    }

    if  (cityData.getPayments().getEnable()==0){
        this->serviceLocator->getCart()->setPayMethod(1);
    };
}

void CartController::setProductinCart(CartProductList cart)
{
    m_serviceKitModel->setProductinCart(cart);
}

void CartController::onCartProductChanged()
{
    int cartSize =   this->serviceLocator->getCart()->conteiner().count();
    int bonusCartSize = this->serviceLocator->getCart()->bonus().count();
    this->serviceLocator->getRouter()->setCartCartEnable(cartSize>0 ||bonusCartSize>0);
}

void CartController::onPageChange(QString page)
{

    if(page == "payment" ){
        auto cart = serviceLocator->getCart();

    if(cart->orderType()=="delivery"){
       QString result;
       result.append(cart->street());
       result.append(" "+cart->building()+", <br>");
       if(cart->entrance() != 0){
           result.append( "под. "+ QString::number(cart->entrance())+", ");
       }

       if(cart->floor() != 0){
           result.append( "эт. "+ QString::number(cart->floor())+", ");
       }
       if(cart->room() != 0){
           result.append( "кв. "+ QString::number(cart->room()));
       }
       qDebug()<<result;
       cart->setAdress(result);

    }
    if(cart->orderType()!="delivery"){
        auto storage = serviceLocator->getStorage();
        auto city_data = storage->cityData();
       auto pointList  =city_data["point_delivery"].toArray();
        for(int i = 0; i < pointList.count();i++){
            if(cart->takeAwayPoint() == pointList.at(i).toObject().value("id").toInt()){
               auto takeAwaypoint = pointList.at(i).toObject();
                cart->settakeAwayString(takeAwaypoint.value("name").toString() + "<br>"+pointList.at(i).toObject().value("address").toString());
            }
        }

    }
    }
}


bool CartController::checkoutYellowButton() const
{
    return m_checkoutYellowButton;
}


