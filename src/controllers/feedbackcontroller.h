#ifndef FEEDBACKCONTROLLER_H
#define FEEDBACKCONTROLLER_H


#include <QFileDevice>
#include "objectcontroller.h"
#include "../entity/iikoorder.h"
#include <QFile>
#include <QStringList>
#include <QNetworkAccessManager>
#include <QHttpPart>
#include "../models/orderhystorymodel.h"
#include <QJsonDocument>
#include "../entity/orderloadlist.h"
#include <QTimer>

class FeedbackController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(int stars READ stars WRITE setStars NOTIFY starsChanged)
    Q_PROPERTY(QString ratingName READ ratingName WRITE setRatingName NOTIFY ratingNameChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)
    Q_PROPERTY(bool commentVisible READ commentVisible WRITE setCommentVisible NOTIFY commentVisibleChanged)
    Q_PROPERTY(int order_number READ order_number WRITE setOrder_number NOTIFY order_numberChanged)
    Q_PROPERTY(QString imageSource READ imageSource WRITE setImageSource NOTIFY imageSourceChanged)
    Q_PROPERTY(QStringList imageList READ imageList WRITE setImageList NOTIFY imageListChanged)
    Q_PROPERTY(bool bigSizePhoto READ bigSizePhoto WRITE setBigSizePhoto NOTIFY bigSizePhotoChanged)
    Q_PROPERTY(bool noDataWrite READ noDataWrite WRITE setNoDataWrite NOTIFY noDataWriteChanged)
    Q_PROPERTY(OrderHystoryModel * orders READ orders  WRITE setorders  NOTIFY ordersChanged)
    Q_PROPERTY(bool feedbackMessage READ feedbackMessage WRITE setFeedbackMessage NOTIFY feedbackMessageChanged)
    Q_PROPERTY(bool feedBackthankMessage READ feedBackthankMessage WRITE setFeedBackthankMessage NOTIFY feedBackthankMessageChanged)
    Q_PROPERTY(bool feedBackButtonVisible READ feedBackButtonVisible WRITE setFeedBackButtonVisible NOTIFY feedBackButtonVisibleChanged)
    Q_PROPERTY(bool progressbarVisible READ progressbarVisible WRITE setProgressbarVisible NOTIFY progressbarVisibleChanged)
    Q_PROPERTY(int indexFeedbackOrder READ indexFeedbackOrder WRITE setIndexFeedbackOrder NOTIFY indexFeedbackOrderChanged)
    Q_PROPERTY(bool rollRaz READ rollRaz WRITE setRollRaz NOTIFY rollRazChanged)
    Q_PROPERTY(bool neVkus READ neVkus WRITE setNeVkus NOTIFY neVkusChanged)
    Q_PROPERTY(bool zabSous READ zabSous WRITE setZabSous NOTIFY zabSousChanged)
    Q_PROPERTY(bool opozdali READ opozdali WRITE setOpozdali NOTIFY opozdaliChanged)
    Q_PROPERTY(bool errorInOrder READ errorInOrder WRITE setErrorInOrder NOTIFY errorInOrderChanged)
    Q_PROPERTY(bool courierMudak READ courierMudak WRITE setCourierMudak NOTIFY courierMudakChanged)
    Q_PROPERTY(bool status_order READ status_order WRITE setStatus_order NOTIFY status_orderChanged)
public:
    explicit FeedbackController(QObject *parent = nullptr);
    virtual void setServiceLocator(ServiceLocator *value);

    int stars() const;
    int order_number() const;
//#if defined(Q_OS_IOS)
//QString platform = "ios";
//#if defined(Q_OS_ANDROID)
//    QString platform = "android";
//#endif



    QString ratingName() const;

    bool commentVisible() const;

    QString comment() const;

    QString imageSource() const;

    QStringList imageList() const;

    bool bigSizePhoto() const;

    bool noDataWrite() const;

    OrderHystoryModel * orders() const;

    bool feedbackMessage() const;

    bool feedBackButtonVisible() const
    {
        return m_feedBackButtonVisible;
    }

    bool progressbarVisible() const
    {
        return m_progressbarVisible;
    }

    bool feedBackthankMessage() const
    {
        return m_feedBackthankMessage;
    }

    int indexFeedbackOrder() const
    {
        return m_indexFeedbackOrder;
    }

    bool rollRaz() const
    {
        return m_rollRaz;
    }

    bool neVkus() const
    {
        return m_neVkus;
    }

    bool zabSous() const
    {
        return m_zabSous;
    }

    bool opozdali() const
    {
        return m_opozdali;
    }

    bool errorInOrder() const
    {
        return m_errorInOrder;
    }

    bool courierMudak() const
    {
        return m_courierMudak;
    }

    bool status_order() const
    {
        return m_status_order;
    }

public slots:
    void clearPhotoList();
    void showCallbackMessage();
    void setOrderHystory(QJsonObject jObj);
    void sendFeedback();
    void imagemCaminho();
    void setStars(int stars);
    void onFeedbackLoaded(QNetworkReply *reply);
    void setOrder_number(int order_number);

    void setRatingName(QString ratingName);

    void setCommentVisible(bool commentVisible);

    void setComment(QString comment);

    void addHotButton(QString name);
    void activeHotButton(QString name, bool status);

    void setImageSource(QString imageSource);

    void setImageList(QStringList FimageList);

    void addImageUrl(QString url);
    void removeImageUrl(int index);
    void onImageLoaded(QNetworkReply *reply);
    void setBigSizePhoto(bool bigSizePhoto);

    void setNoDataWrite(bool noDataWrite);

    void setorders(OrderHystoryModel * orders);

    void setFeedbackMessage(bool feedbackMessage)
    {
        if (m_feedbackMessage == feedbackMessage)
            return;

        m_feedbackMessage = feedbackMessage;
        emit feedbackMessageChanged(m_feedbackMessage);
    }

    void setFeedBackButtonVisible(bool feedBackButtonVisible)
    {
        if (m_feedBackButtonVisible == feedBackButtonVisible)
            return;

        m_feedBackButtonVisible = feedBackButtonVisible;
        emit feedBackButtonVisibleChanged(m_feedBackButtonVisible);
    }

    void setProgressbarVisible(bool progressbarVisible)
    {
        if (m_progressbarVisible == progressbarVisible)
            return;

        m_progressbarVisible = progressbarVisible;
        emit progressbarVisibleChanged(m_progressbarVisible);
    }

    void setFeedBackthankMessage(bool feedBackthankMessage)
    {
        if (m_feedBackthankMessage == feedBackthankMessage)
            return;

        m_feedBackthankMessage = feedBackthankMessage;
        emit feedBackthankMessageChanged(m_feedBackthankMessage);
    }

    void setIndexFeedbackOrder(int indexFeedbackOrder)
    {
        if (m_indexFeedbackOrder == indexFeedbackOrder)
            return;

        m_indexFeedbackOrder = indexFeedbackOrder;
        emit indexFeedbackOrderChanged(m_indexFeedbackOrder);
    }

    void setRollRaz(bool rollRaz)
    {
        if (m_rollRaz == rollRaz)
            return;

        m_rollRaz = rollRaz;
        emit rollRazChanged(m_rollRaz);
    }

    void setNeVkus(bool neVkus)
    {
        if (m_neVkus == neVkus)
            return;

        m_neVkus = neVkus;
        emit neVkusChanged(m_neVkus);
    }

    void setZabSous(bool zabSous)
    {
        if (m_zabSous == zabSous)
            return;

        m_zabSous = zabSous;
        emit zabSousChanged(m_zabSous);
    }

    void setOpozdali(bool opozdali)
    {
        if (m_opozdali == opozdali)
            return;

        m_opozdali = opozdali;
        emit opozdaliChanged(m_opozdali);
    }

    void setErrorInOrder(bool errorInOrder)
    {
        if (m_errorInOrder == errorInOrder)
            return;

        m_errorInOrder = errorInOrder;
        emit errorInOrderChanged(m_errorInOrder);
    }

    void setCourierMudak(bool courierMudak)
    {
        if (m_courierMudak == courierMudak)
            return;

        m_courierMudak = courierMudak;
        emit courierMudakChanged(m_courierMudak);
    }

    void setStatus_order(bool status_order)
    {
        qDebug()<<"setStatus_order>> "<< status_order;
        if (m_status_order == status_order)
            return;

        m_status_order = status_order;
        emit status_orderChanged(m_status_order);
    }

signals:
    void clearFeedbackData();
    void starsChanged(int stars);
    void order_numberChanged(int order_number);

    void ratingNameChanged(QString ratingName);

    void commentVisibleChanged(bool commentVisible);

    void commentChanged(QString comment);
    void imageSourceChanged(QString imageSource);
    void openCamera();
    void imageListChanged(QStringList imageList);



    void bigSizePhotoChanged(bool bigSizePhoto);

    void noDataWriteChanged(bool noDataWrite);

    void ordersChanged(OrderHystoryModel * orders);
    void feedbackMessageChanged(bool feedbackMessage);

    void feedBackButtonVisibleChanged(bool feedBackButtonVisible);

    void progressbarVisibleChanged(bool progressbarVisible);

    void feedBackthankMessageChanged(bool feedBackthankMessage);

    void indexFeedbackOrderChanged(int indexFeedbackOrder);

    void rollRazChanged(bool rollRaz);

    void neVkusChanged(bool neVkus);

    void zabSousChanged(bool zabSous);

    void opozdaliChanged(bool opozdali);

    void errorInOrderChanged(bool errorInOrder);

    void courierMudakChanged(bool courierMudak);

    void status_orderChanged(bool status_order);

private slots:
    void loadOrderData(int ordernumber);
    void onOrderDataLoaded(QNetworkReply *reply);
    void onTimerStop();
    void onRouterPageChanged(QString page);
    void onCityDataLoaded(CityData data);
    void clearOrderList();


private:
//    void sendFeedBack(FeedbackData data);
    void onClientDataReloaded(QJsonArray jArr);
    QString getHotBunnotComment();
    void uploadPhoto(QString uri);
    int m_stars;
    int m_order_number;
    QString m_ratingName;
    bool m_commentVisible;
    QString m_comment;
    QHash<QString,bool> hotButtons;
    QList<QString> imagePathList;
    QString m_imageSource = "/document/image:449228";
    QStringList m_imageList;
    QNetworkAccessManager *imageLoadmanager;
    bool m_bigSizePhoto = false;
    QList<QString> uploadedList;
    bool m_noDataWrite = false;
    void sendComent(QJsonObject obj);
    QNetworkAccessManager *feedbackLoadmanager;
    QNetworkAccessManager *iikoOrderManager;
    QList<int> loadList;

    OrderHystoryModel * m_orders;
    QTimer *timer;
//    QTimer *m_timer;

    bool m_feedbackMessage = false;
    bool m_feedBackButtonVisible = false;
    bool m_progressbarVisible = false;
    bool m_feedBackthankMessage = false;
    int m_indexFeedbackOrder;
    bool m_rollRaz = false;
    bool m_neVkus = false;
    bool m_zabSous = false;
    bool m_opozdali = false;
    bool m_errorInOrder = false;
    bool m_courierMudak = false;
    bool m_status_order = false;
};
#endif // FEEDBACKCONTROLLER_H
