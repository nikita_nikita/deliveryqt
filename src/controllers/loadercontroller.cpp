#include "loadercontroller.h"
#include "../config/settingapp.h"
#include <QDesktopServices>
LoaderController::LoaderController(QObject *parent) : ObjectController(parent)
{
    timer = new QTimer(this);
    timer->setInterval(5000);
    connect(timer, &QTimer::timeout, this, &LoaderController::loadFiles);
}

bool LoaderController::visible() const
{
    return m_visible;
}

bool LoaderController::versionCheck() const
{
    return m_versionCheck;
}

QString LoaderController::errorText() const
{
    return m_errorText;
}

bool LoaderController::visibleCityList() const
{
    return m_visibleCityList;
}

void LoaderController::loadVersion()
{
    this->versionLoaded=false;
    this->show();
    serviceLocator->getKapibaras()->loadCurrentVersion();

}

void LoaderController::loadCityList()
{
    this->cityListLoaded= false;
    this->show();
    City userCity = City::fromJson(serviceLocator->getStorage()->userCity());
    QString dom = userCity.getDomain().getCode();
    if(dom == ""){
        dom ="ru";
    }

    serviceLocator->getKapibaras()->loadCitys(dom);
}

void LoaderController::loadCityData()
{
    City userCity = City::fromJson(serviceLocator->getStorage()->userCity());

    if(userCity.getId()<1){
        return;
    }
    this->cityDataLoaded= false;
    this->show();

//    this->serviceLocator->getKapibaras()->setDomain(userCity.getDomain().getPath());
    this->serviceLocator->getKapibaras()->setCity_id(userCity.getId());
    this->serviceLocator->getKapibaras()->loadCityData();
}

void LoaderController::loadUserData()
{
    if(serviceLocator->getStorage()->tokenLk().length()>0){
        serviceLocator->getKapibaras()->loadClientData(serviceLocator->getStorage()->tokenLk());
    }
}

void LoaderController::run()
{
    this->loadVersion();
    this->loadCityList();
    this->loadCityData();
    this->loadUserData();
}

void LoaderController::setServiceLocator(ServiceLocator *value)
{
    ObjectController::setServiceLocator(value);
    //посмотреть чтобы в начале не вызывался
    connect(serviceLocator->getStorage(), &Storage::userCityChanged, this, &LoaderController::userCityChanged);
    connect(serviceLocator->getKapibaras(), &Kapibaras::error, this, &LoaderController::setErrorText);
    connect(serviceLocator->getKapibaras(), &Kapibaras::currentVersionLoaded, this, &LoaderController::checkCurrentVersion);
    connect(serviceLocator->getKapibaras(), &Kapibaras::citysLoaded, this, &LoaderController::setCityList);
    connect(serviceLocator->getKapibaras(), &Kapibaras::cityDataLoaded, this,&LoaderController::setCityData);
}

bool LoaderController::firebaseEnable()
{

#ifdef Q_OS_ANDROID
    return   QT_VERSION > 5.12;
#elif defined(Q_OS_IOS)
    return true;
#endif
}

void LoaderController::setVisible(bool visible)
{

    if (m_visible == visible)
        return;

    m_visible = visible;
    emit visibleChanged(m_visible);
}

void LoaderController::setVersionCheck(bool versionCheck)
{
    if (m_versionCheck == versionCheck)
        return;

    m_versionCheck = versionCheck;
    emit versionCheckChanged(m_versionCheck);
}

void LoaderController::checkCurrentVersion(VersionApp version)
{
    this->versionLoaded = true;
    this->setVersionCheck(version.getVersion()<=SettingApp::versionApp());
    this->update();
}

void LoaderController::setCityList(CityList cityList)
{
    this->cityListLoaded = true;
    m_cityList = cityList;
    serviceLocator->getStorage()->setCityList(this->m_cityList);
    this->checkUserCity();
    this->update();
}

void LoaderController::setCityData(CityData cityData)
{
    this->cityDataLoaded = true;
    m_cityData= cityData;
    this->update();
    this->serviceLocator->getKapibaras()->loadStreetList();
    timer->start();
}

void LoaderController::setErrorText(QString errorText)
{
    if (m_errorText == errorText)
        return;

    m_errorText = errorText;
    emit errorTextChanged(m_errorText);
}

void LoaderController::setVisibleCityList(bool visibleCityList)
{
    if (m_visibleCityList == visibleCityList)
        return;

    m_visibleCityList = visibleCityList;
    emit visibleCityListChanged(m_visibleCityList);
}

void LoaderController::openMarketUrl()
{
#ifdef Q_OS_ANDROID
    QDesktopServices::openUrl(QUrl("https://play.google.com/store/apps/details?id=mob.ru.kapibaras"));
#elif defined(Q_OS_IOS)
    QDesktopServices::openUrl(QUrl("https://apps.apple.com/ru/app/kapibara-доставка-суши/id1474050458"));
#endif

}

void LoaderController::loadFiles()
{
    timer->stop();
    this->serviceLocator->getKapibaras()->loadFiles();
}

void LoaderController::userCityChanged()
{
    this->setVisible(true);
    this->setErrorText("");
    this->loadCityData();
}

void LoaderController::show()
{
    this->setVisible(true);
}

void LoaderController::hide()
{
    this->setVisible(false);
}

void LoaderController::update()
{
    if(!versionLoaded||!m_versionCheck){
        this->show();
        return;

    }
    if(!cityListLoaded||!cityDataLoaded||m_visibleCityList){
        this->show();

        return;
    }

    this->setDataInService();
    this->hide();
}

void LoaderController::checkUserCity()
{
    City userCity = City::fromJson(serviceLocator->getStorage()->userCity());
    if(userCity.getId()<1||this->m_cityList.cityByid(userCity.getId()).getHrefVisible() == 0){
        this->setVisibleCityList(true);
    }else{
        this->setVisibleCityList(false);
    }
}

void LoaderController::setDataInService()
{
    serviceLocator->setCityData(this->m_cityData);
}
