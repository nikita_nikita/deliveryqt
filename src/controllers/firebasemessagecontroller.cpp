#include "firebasemessagecontroller.h"
#include "../config/settingapp.h"
#include <QUrlQuery>
#include <QNetworkInterface>
#include <QUrl>
#include <QFile>
#include <QDir>
#include <QFileDialog>
FirebaseMessageController::FirebaseMessageController(QObject *parent): ObjectController(parent)
{
    firebaseRegistrationDeviceManager = new QNetworkAccessManager(this);
    connect(firebaseRegistrationDeviceManager, &QNetworkAccessManager::finished,this,&FirebaseMessageController::onRegisterDevice);
}

void FirebaseMessageController::setServiceLocator(ServiceLocator *sv)
{
    ObjectController::setServiceLocator(sv);
    connect(serviceLocator->getKapibaras(),&Kapibaras::cityDataLoaded,this,&FirebaseMessageController::differenceData);
    connect(serviceLocator->getKapibaras(),&Kapibaras::clientDataLoaded,this,&FirebaseMessageController::differenceData);
}

QJsonObject FirebaseMessageController::messageData() const
{
    return m_messageData;
}

QString FirebaseMessageController::message() const
{
    return m_message;
}


void FirebaseMessageController::onMessageDownload(QJsonObject message)
{
    Q_UNUSED(message)
}

void FirebaseMessageController::registrateDevice()
{
    QString phone = serviceLocator->getCart()->userPhone();
    if(phone != "" && jObj["device_id"].toString() != ""){
        auto kapibara =    serviceLocator->getKapibaras();
        QUrl url;
        url.setHost("");
        url.setScheme(SettingApp::defaultSheme());
        url.setPath("");
        QNetworkRequest request(url);
        QJsonDocument doc(jObj);
        request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
        firebaseRegistrationDeviceManager->post(request, doc.toJson(QJsonDocument::Compact));
    }
}


void FirebaseMessageController::setFirebaseToken(QString token)
{
    firebaseToken = token;
}

void FirebaseMessageController::setMessageData(QJsonObject messageData)
{
    if (m_messageData == messageData)
        return;

    m_messageData = messageData;
    setMessage(messageData.value("nBody").toString());
    emit messageDataChanged(m_messageData);
}

void FirebaseMessageController::setMessage(QString message)
{
    if (m_message == message)
        return;

    m_message = message;
    emit messageChanged(m_message);
}

void FirebaseMessageController::onRegisterDevice(QNetworkReply *reply)
{
    if(!reply->error()){
        saveData(jObj);
    }
}

void FirebaseMessageController::differenceData()
{
    prepaireQuery();
    auto savepath =  SettingApp::saveFolder();
    QFile file(savepath+"/firebase");
    if(file.exists()){
        if (file.open(QIODevice::ReadOnly)) /*Открываем файл в режиме только для записи. В этом с*/
        {
            auto data =file.readAll();
            auto doc = QJsonDocument::fromJson(data);
            if(jObj!=doc.object()){
            registrateDevice();
            }
        }

    }else{
         registrateDevice();
    }


}

void FirebaseMessageController::saveData(QJsonObject jObj)
{
    auto savepath =  SettingApp::saveFolder();
    QFile file(savepath+"/firebase");
    if (file.open(QIODevice::WriteOnly)) /*Открываем файл в режиме только для записи. В этом с*/
    {
        QJsonDocument doc(jObj);
        QByteArray docByteArray = doc.toJson(QJsonDocument::Compact);
        file.write(docByteArray); /*Записываем данные*/
        file.close(); /*Закрываем файл*/
    }

}

void FirebaseMessageController::prepaireQuery()
{

    QString phone = serviceLocator->getCart()->userPhone();
    auto kapibara =    serviceLocator->getKapibaras();
    auto domein = kapibara->getDomain();
    auto country = serviceLocator->getKapibaras()->getCountry();
    jObj["city_id"] = kapibara->getCity_id();
    jObj["phone"] = phone;
    jObj["chanel"] = "firebase";
    jObj["platform"] = platform;
    jObj["country"] = domein;
    jObj["device_id"] = firebaseToken;
}
