#ifndef LOADERCONTROLLER_H
#define LOADERCONTROLLER_H

#include "objectcontroller.h"
#include <QTimer>
#include <QtCore>
#include <QtGlobal>
class LoaderController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(bool versionCheck READ versionCheck WRITE setVersionCheck NOTIFY versionCheckChanged)
    Q_PROPERTY(bool visibleCityList READ visibleCityList WRITE setVisibleCityList NOTIFY visibleCityListChanged)
    Q_PROPERTY(QString errorText READ errorText WRITE setErrorText NOTIFY errorTextChanged)


public:
    explicit LoaderController(QObject *parent = nullptr);
    bool visible() const;
    bool versionCheck() const;
    QString errorText() const;

    bool visibleCityList() const;
    void loadVersion();
    void loadCityList();
    void loadCityData();
    void loadUserData();
    void run();
    virtual void setServiceLocator(ServiceLocator *value);

signals:

    void visibleChanged(bool visible);
    void versionCheckChanged(bool versionCheck);
    void errorTextChanged(QString errorText);

    void visibleCityListChanged(bool visibleCityList);

public slots:
    bool firebaseEnable();
    void setVisible(bool visible);
    void setVersionCheck(bool versionCheck);
    void checkCurrentVersion(VersionApp version);
     void setCityList(CityList cityList);
     void setCityData(CityData cityData);
     void setErrorText(QString errorText);
     void setVisibleCityList(bool visibleCityList);
     void openMarketUrl();
private slots:
     void loadFiles();
     void userCityChanged();
protected:


private:
    bool m_visible= true;
    bool m_versionCheck= true;
    bool versionLoaded = false;
    bool cityListLoaded = false;
    bool cityDataLoaded = false;
    CityList m_cityList;
    CityData  m_cityData;
    QString m_errorText;
    bool m_visibleCityList=false;
    void show();
    void hide();
    void update();
    void checkUserCity();
    void setDataInService();
    QTimer *timer;

};

#endif // LOADERCONTROLLER_H
