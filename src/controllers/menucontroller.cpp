#include "menucontroller.h"

MenuController::MenuController(QObject *parent) : ObjectController(parent)
{
    m_pages  = new PagesModel(this);
}

void MenuController::setServiceLocator(ServiceLocator *sv)
{
    ObjectController::setServiceLocator(sv);
     m_searchFilterModel  = new ProductsModel(this);
     m_searchProductModel  = new ProductFilteModel(this);
     m_searchProductModel->setSourceModel(m_searchFilterModel);
     m_searchProductModel->setFilterRole(ProductsModel::NameRole);
     m_searchProductModel->setFilterCaseSensitivity(Qt::CaseInsensitive);


     connect(sv->getKapibaras(), &Kapibaras::cityDataLoaded, this, &MenuController::setCityData);
     connect(sv->getCart(), &Cart::cartChanged, this, &MenuController::setProductinCart);

     connect(m_pages, &PagesModel::infoCompositeLoaded, this, &MenuController::setInfoComposite);
}

PagesModel *MenuController::pages() const
{
    return m_pages;
}

void MenuController::setCityData(CityData cityData)
{
    setProduct(cityData.getProductCategory().getProductByCategory(cityData.getCategories().getAliases()));

    this->m_pages->setMenu(cityData.getProductCategory(), cityData.getCategories());

}

void MenuController::setPages(PagesModel *pages)
{
    if (m_pages == pages)
        return;

    m_pages = pages;
    emit pagesChanged(m_pages);
}

void MenuController::setProductinCart(CartProductList cart)
{

    m_searchFilterModel->setCart(cart);
    m_pages->setProductinCart(cart);

}

void MenuController::setInfoComposite(Product prod)
{
    this->serviceLocator->getMenu()->setInfoComposite(prod);
}

void MenuController::setNameSearch(QString nameSearch)
{
    if (m_nameSearch == nameSearch)
        return;

    m_nameSearch = nameSearch;
    search(m_nameSearch);
    emit nameSearchChanged(m_nameSearch);
}

void MenuController::setSearchFilterModel(ProductsModel *searchFilterModel)
{
    if (m_searchFilterModel == searchFilterModel)
        return;

    m_searchFilterModel = searchFilterModel;
    emit searchFilterModelChanged(m_searchFilterModel);
}

void MenuController::showInfoComposition(int id)
{
auto prod = products[id];
this->serviceLocator->getMenu()->setInfoComposite(prod);

}



void MenuController::setSearchProductModel(ProductFilteModel *searchProductModel)
{
    if (m_searchProductModel == searchProductModel)
        return;

    m_searchProductModel = searchProductModel;
    emit searchProductModelChanged(m_searchProductModel);
}

void MenuController::search(QString name)
{
    if(name.length() > 1){
    m_searchProductModel->setSearchName( name);
    }
}

void MenuController::setProducts(ProductList &value)
{
    for(int i =0; i < value.count(); i++){
        auto product = value.at(i);
        if(product.getComposition().count() == 0){
            products.insert(product.getId(),product);
        }
    }
}

void MenuController::addProductToCart(int id)
{

}






void MenuController::setProduct(ProductList product)
{

    m_searchFilterModel->setProductList(product);
    this->setProducts(product);

    m_searchProductModel->setSourceModel(m_searchFilterModel);
}

QString MenuController::nameSearch() const
{
    return m_nameSearch;
}

ProductsModel *MenuController::searchFilterModel() const
{
    return m_searchFilterModel;
}

ProductFilteModel *MenuController::searchProductModel() const
{
    return m_searchProductModel;
}


