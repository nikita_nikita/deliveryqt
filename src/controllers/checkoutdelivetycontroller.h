#ifndef CHECKOUTDELIVETYCONTROLLER_H
#define CHECKOUTDELIVETYCONTROLLER_H

#include "objectcontroller.h"

#include "../models/streetmodel.h"
#include "../models/streetfiltermodel.h"
class CheckoutDelivetyController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(bool validStreet READ validStreet WRITE setValidStreet NOTIFY validStreetChanged)
    Q_PROPERTY(StreetFilterModel* streetModel MEMBER m_streetFilterModel NOTIFY streetModelChanged)
    Q_PROPERTY(bool allowDelivery READ allowDelivery WRITE setAllowDelivery NOTIFY allowDeliveryChanged)
    Q_PROPERTY(bool allowPickUP READ allowPickUP WRITE setAllowPickUP NOTIFY allowPickUPChanged)
    Q_PROPERTY(bool timeWork READ timeWork WRITE setTimeWork NOTIFY timeWorkChanged)
    Q_PROPERTY(int tab_index READ tabIndex WRITE setTabIndex NOTIFY tabIndexchanged)// index tab 0-delivery 1-takaway    
    Q_PROPERTY(QJsonArray ponintData READ ponintData WRITE setPonintData NOTIFY ponintDataChanged)
    Q_PROPERTY(QString notDelivery READ notDelivery WRITE setNotDelivery NOTIFY notDeliveryChanged)
    Q_PROPERTY(bool newToppingLogic READ newToppingLogic WRITE setNewToppingLogic NOTIFY newToppingLogicChanged)
    Q_PROPERTY(bool contactless_delivery READ contactless_delivery WRITE setContactless_delivery NOTIFY contactless_deliveryChanged)
    Q_PROPERTY(bool contactless_status READ contactless_status WRITE setContactless_status NOTIFY contactless_statusChanged)
    Q_PROPERTY(QJsonArray warnings READ warnings WRITE setWarnings NOTIFY warningsChanged)
    Q_PROPERTY(bool loaderProcess READ loaderProcess WRITE setLoaderProcess NOTIFY loaderProcessChanged)
    Q_PROPERTY(bool deliveryNotAwalable READ deliveryNotAwalable WRITE setDeliveryNotAwalable NOTIFY deliveryNotAwalableChanged)
public:
    explicit CheckoutDelivetyController(QObject *parent = nullptr);

    virtual void setServiceLocator(ServiceLocator *value);


    bool validStreet() const;

    bool allowDelivery() const;

    bool allowPickUP() const;

    bool timeWork() const;

    int tabIndex() const;

    QJsonArray ponintData() const;

    QString notDelivery() const
    {
        return m_notDelivery;
    }

    bool getNewToppingKitLogic() const;
    void setNewToppingKitLogic(bool value);

    bool newToppingLogic() const
    {
        return m_newToppingLogic;
    }

    bool contactless_delivery() const
    {
        return m_contactless_delivery;
    }

    bool contactless_status() const
    {
        return m_contactless_status;
    }

    QJsonArray warnings() const
    {
        return m_warnings;
    }

    bool loaderProcess() const
    {
        return m_loaderProcess;
    }

    bool deliveryNotAwalable() const
    {
        return m_deliveryNotAwalable;
    }

signals:
    void validStreetChanged(bool validStreet);

    void streetModelChanged(QSortFilterProxyModel* streetModel);

    void allowDeliveryChanged(bool allowDelivery);

    void allowPickUPChanged(bool allowPickUP);

    void timeWorkChanged(bool timeWork);

    void tabIndexchanged(int tab_index);

    void ponintDataChanged(QJsonArray ponintData);

    void notDeliveryChanged(QString notDelivery);

    void newToppingLogicChanged(bool newToppingLogic);

    void contactless_deliveryChanged(bool contactless_delivery);

    void contactless_statusChanged(bool contactless_status);

    void warningsChanged(QJsonArray warnings);

    void loaderProcessChanged(bool loaderProcess);

    void deliveryNotAwalableChanged(bool deliveryNotAwalable);

public slots:
    void updateOrganizationInfo();

    void clearData();
    bool choiceCurrentTime();
    void setValidStreet(bool validStreet);
    void setCityData(CityData cityData);
    void updateValidCheckStreet();
    void checkStreet();
    void setStrets(StreetList streets);
    void updateStreetFilter();
    void setAllowDelivery(bool allowDelivery);
    void setAllowPickUP(bool allowPickUP);
    void setTimeWork(bool timeWork);    
    void setTabIndex(int tab_index);
    void setPonintData(QJsonArray ponintData);
    void onDeliveryTypeChanged();

    void setNotDelivery(QString notDelivery);

    void setNewToppingLogic(bool newToppingLogic);

    void setContactless_delivery(bool contactless_delivery);

    void setContactless_status(bool contactless_status);

    void setWarnings(QJsonArray warnings)
    {
        if (m_warnings == warnings)
            return;

        m_warnings = warnings;
        emit warningsChanged(m_warnings);
    }

    void setLoaderProcess(bool loaderProcess)
    {
        if (m_loaderProcess == loaderProcess)
            return;

        m_loaderProcess = loaderProcess;
        emit loaderProcessChanged(m_loaderProcess);
    }

    void setDeliveryNotAwalable(bool deliveryNotAwalable)
    {
        if (m_deliveryNotAwalable == deliveryNotAwalable)
            return;

        m_deliveryNotAwalable = deliveryNotAwalable;
        emit deliveryNotAwalableChanged(m_deliveryNotAwalable);
    }

private slots:
    void onOrganizationInfoLoaded(OrganizationInfo info, WorkTimeEntity workTime);
    void onDeliveryIntervasChanged(QJsonArray jArr);
    void obPickUpIntervasChanged(QJsonArray jArr);
private:
    bool m_validStreet;
    bool m_checkStreet=false;
    StreetModel *streetModel;
    StreetFilterModel *m_streetFilterModel;
    bool m_allowDelivery;
    bool m_allowPickUP;
    bool m_timeWork= true;
    QTime timeDeliveryClose;
    QTime timePickupClose;
    QTimer *timer;
    QTimer *m_timer;
    void checkWorktime();
    int m_tab_index = 1;
    //новая схема топпингов(клиент выбирает
    //сколько ему нужно из джоступного ему количества)
    bool newToppingKitLogic;
    QJsonArray m_ponintData;
    QString m_notDelivery;
    bool m_newToppingLogic;
    bool m_contactless_delivery = false;
    bool m_contactless_status = false;
    bool alreadyCalculate =false;
    QJsonArray m_warnings;
    bool m_loaderProcess = false;
    bool m_deliveryNotAwalable = false;
};

#endif // CHECKOUTDELIVETYCONTROLLER_H
