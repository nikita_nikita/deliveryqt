 #ifndef SETTINGAPP_H
#define SETTINGAPP_H
#include <QString>

class SettingApp
{
public:
 enum ModeType {
     Production,
     Development
 };
    SettingApp();
    static QString saveFolder();
   static QString defaultUrl(QString domein);
    static QString defaultSheme();
    static ModeType mode();
    static QString logPath();
    static int versionApp();
    static bool suppurtSSl();



private:



};

#endif // SETTINGAPP_H
