#include "settingapp.h"
#include <QStandardPaths>
#include <QSslConfiguration>
SettingApp::SettingApp()
{

}

QString SettingApp::saveFolder()
{

#ifdef Q_OS_ANDROID
    return QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
#elif defined(Q_OS_LINUX)
    return QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
#elif defined(Q_OS_IOS)
    return QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
#endif

}

QString SettingApp::defaultUrl(QString domein)
{

    if (SettingApp::suppurtSSl()) {
        return "https://..."+ domein;
    } else {
        return "http://..."+ domein;
    }
}


QString SettingApp::defaultSheme()
{
    if (SettingApp::suppurtSSl()) {
        return "https";
    } else {
        return "http";
    }

}

SettingApp::ModeType SettingApp::mode()
{
#ifdef QT_DEBUG
    return ModeType::Development;
#else
    return ModeType::Production;
#endif
}

QString SettingApp::logPath()
{
    return "/home/nikita/Documents/test/";
}

int SettingApp::versionApp()
{
#ifdef Q_OS_ANDROID
    return 15;
#elif defined(Q_OS_IOS)
    return 14;
#endif
    return 23;
}

bool SettingApp::suppurtSSl()
{
    return QSslSocket::supportsSsl();
}
