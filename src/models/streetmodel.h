#ifndef STREETMODEL_H
#define STREETMODEL_H

#include <QAbstractListModel>
#include "../entity/street.h"
class StreetModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        TypeRole,
        TypeShortRole,
        CodeRole,
        StatusRole,
        SityRole,
        JsonRole
    };
    explicit StreetModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;
    void setStreetlist(StreetList list);
signals:

public slots:
private:
 StreetList m_data;
};

#endif // STREETMODEL_H
