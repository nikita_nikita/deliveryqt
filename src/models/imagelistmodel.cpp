#include "imagelistmodel.h"

ImageListModel::ImageListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    addUrl("content://com.android.providers.media.documents/document/image%3A452094");
}


int ImageListModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

       return m_list.count();
}

QHash<int, QByteArray> ImageListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[UrlRole] = "sourseUrl";
    return roles;
}


QVariant ImageListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    QUrl url = this->m_list.at(index.row());
    switch (role) {
    case UrlRole:
        return  url;
    default:
    return QVariant();
    }
}

void ImageListModel::addUrl(QString url)
{
beginResetModel();
m_list.append(url);
endResetModel();
}

void ImageListModel::removeUrl(int index)
{
    beginResetModel();
    m_list.removeAt(index);
    beginResetModel();
}
