#include "pagemodel.h"
PageModel::PageModel(QObject *parent)
    : QAbstractListModel(parent)
{

}



int PageModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_data.count();
}

QVariant PageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == PageRole){
        return QVariant::fromValue(this->m_data.at(index.row()));
    }
    if (role == CategoryNameRole){
        return this->products.at(index.row()).getCategory().getName();
    }
    if (role == CategoryAliasRole){
        return this->products.at(index.row()).getCategory().getAlias();
    }
    if (role == CategoryInfoRole){
        return this->products.at(index.row()).getCategory().getInfo();
    }
    if (role == IdRole){
        return index.row();
    }
    return QVariant();
}

QHash<int, QByteArray> PageModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PageRole] = "page";
    roles[CategoryNameRole] = "categoryName";
    roles[CategoryAliasRole] = "categoryAlias";
    roles[CategoryInfoRole] = "categoryInfo";
    roles[IdRole] = "id_page";

    return roles;
}

QList<CategoryProducts> PageModel::getProducts() const
{
    return products;
}

void PageModel::setProducts(const QList<CategoryProducts> &value)
{
    beginResetModel();
    products = value;
    this->clearData();
    for (int i=0; i<value.count(); i++) {
        auto item = value.at(i);
        auto mod  = new ProductsModel(this);
        connect(mod, &ProductsModel::infoCompositeLoaded, this, &PageModel::infoCompositeLoaded);
        mod->setProductList(item.getProducts());
        this->m_data.append(mod);
    }

    endResetModel();
}



bool PageModel::visible() const
{
    return m_visible;
}

bool PageModel::isBack() const
{
    return m_isBack;
}

int PageModel::current() const
{
    return m_current;
}

int PageModel::getIndexByCategory(QString alias)
{
    for (int i=0; i< products.count(); i++ ) {
        auto cat = products.at(i).getCategory();
        if (cat.getAlias()==alias){
            this->setCurrent(i);
            return i;
        }
    }
    return -1;
}

QString PageModel::parentCategory() const
{
    return m_parentCategory;
}



void PageModel::setVisible(bool visible)
{
    if (m_visible == visible)
        return;

    m_visible = visible;
    emit visibleChanged(m_visible);
}

void PageModel::setIsBack(bool isBack)
{
    if (m_isBack == isBack)
        return;

    m_isBack = isBack;
    emit isBackChanged(m_isBack);
}

void PageModel::setCurrent(int current)
{
    if (m_current == current)
        return;

    m_current = current;
    emit currentChanged(m_current);
}

void PageModel::setParentCategory(QString parentCategory)
{
    if (m_parentCategory == parentCategory)
        return;

    m_parentCategory = parentCategory;
    emit parentCategoryChanged(m_parentCategory);
}

void PageModel::setQuantityByCart(CartProductEntity &ent)
{
    for(int i=0; i<this->m_data.count(); i++ ){
        this->m_data.at(i)->setQuantityByCart(ent);
    }
}

void PageModel::setCart(CartProductList &cart)
{
    for(int i=0; i<this->m_data.count(); i++ ){
        this->m_data.at(i)->setCart(cart);
    }
}


void PageModel::clearData()
{
    for(int i=0; i<this->m_data.count(); i++ ){
        auto mod = this->m_data.at(i);
        disconnect(mod, &ProductsModel::infoCompositeLoaded, this, &PageModel::infoCompositeLoaded);
        delete(mod);
    }
    this->m_data.clear();
}


