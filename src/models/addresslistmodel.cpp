#include "addresslistmodel.h"

AddressListModel::AddressListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int AddressListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_list.count();

}

QVariant AddressListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    AddressItem address = this->m_list.at(index.row());
    QString text = address.getType()+". "+address.getStreet()+", дом: "+address.getBuilding();
    if (address.getRoom()) text+=", кв "+QString::number(address.getRoom());
    if (address.getEntrance()) text+=", подъезд "+QString::number(address.getEntrance());
    if (address.getFloor()) text+=", этаж "+QString::number(address.getFloor());

    switch (role) {
     case Qt::DisplayRole:
        return text;
    }
    return QVariant();
}

void AddressListModel::setArrayData(const AddresesList &arrayData)
{
    beginResetModel();
    m_arrayData = arrayData;
    this->updateList();
    endResetModel();
}

void AddressListModel::setCheckStreet(bool checkStreet)
{
    beginResetModel();
    m_checkStreet = checkStreet;
    this->updateList();
    endResetModel();
}

void AddressListModel::updateList()
{
    this->m_list=m_arrayData.filter(this->m_checkStreet);
}
