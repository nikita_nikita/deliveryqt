#include "productitemlistmodel.h"

ProductItemListModel::ProductItemListModel(QObject *parent) : QAbstractListModel(parent)
{

}

int ProductItemListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_data.count();
}

QVariant ProductItemListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    ProductItem product = this->m_data.at(index.row());
    auto number = product.getNumber();
    switch (role) {
    case IdRole:
        return QVariant(product.getId());
    case NameRole:
        return QVariant(product.getName());
    case WeightRole:
        return QVariant(product.getWeight());
    case CaloricRole:
        return QVariant(product.getCaloric());
    case VolumeRole:
        return QVariant(product.getVolume());
    case CompositionRole:
        return QVariant(product.getComposition());
    case HotRole:
        return  QVariant(product.getHot());
    case ImageRole:
        return QVariant(product.getImages().at(0));
    case NumberRole:
        return QVariant(getInfo(product));
    case TypeName:
        return QVariant(product.getType().getName());
    case JsonRole:
        return QVariant(product.toJson());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ProductItemListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[WeightRole] = "weight";
    roles[CaloricRole] = "caloric" ;
    roles[ImageRole]  = "image";
    roles[CompositionRole] = "composition";
    roles[TypeName]  = "typeName";
    roles[VolumeRole] = "volume";
    roles[NumberRole] = "sizeOfSet";
    roles[HotRole] = "hot";
    roles[JsonRole] = "json";
    return roles;
}

void ProductItemListModel::setProducts(ProductItemList prods)
{
    beginResetModel();
    this->m_data = prods;
    endResetModel();
}

QString ProductItemListModel::getInfo(ProductItem product) const
{
    QString result = "";
//    if(product.getNumber() != ""){
//        result = product.getNumber() + "шт/ " + product.getWeight() + " г/ " + product.getCaloric() + "ккал.";
//    }

    return result;
}
