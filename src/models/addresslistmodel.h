#ifndef ADDRESSLISTMODEL_H
#define ADDRESSLISTMODEL_H

#include <QAbstractListModel>
#include "../entity/address/addreseslist.h"
class AddressListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit AddressListModel(QObject *parent = nullptr);



    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setArrayData(const AddresesList &arrayData);

    void setCheckStreet(bool checkStreet);

private:
    AddresesList m_arrayData;
    AddresesList m_list;
    bool m_checkStreet = false;
    void updateList();
};

#endif // ADDRESSLISTMODEL_H
