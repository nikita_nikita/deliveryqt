#ifndef PAGEMODEL_H
#define PAGEMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/product.h"
#include "../entity/models/categoryproducts.h"
#include "productsmodel.h"

class   PageModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(bool isBack READ isBack WRITE setIsBack NOTIFY isBackChanged)
    Q_PROPERTY(int current READ current WRITE setCurrent NOTIFY currentChanged)
    Q_PROPERTY(QString parentCategory READ parentCategory WRITE setParentCategory NOTIFY parentCategoryChanged)
public:
    enum Roles {
        PageRole = Qt::UserRole + 1,
        CategoryNameRole,
        CategoryAliasRole,
        CategoryInfoRole,
        IdRole,
    };
    explicit PageModel(QObject *parent = nullptr);



    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    QList<CategoryProducts> getProducts() const;
    void setProducts(const QList<CategoryProducts> &value);


    bool visible() const;

    bool isBack() const;

    int current() const;
    int getIndexByCategory(QString alias);

    QString parentCategory() const;


public slots:
    void setVisible(bool visible);

    void setIsBack(bool isBack);

    void setCurrent(int current);


    void setParentCategory(QString parentCategory);

    void setQuantityByCart(CartProductEntity &ent);

    void setCart(CartProductList &cart);




signals:
    void visibleChanged(bool visible);

    void isBackChanged(bool isBack);

    void currentChanged(int current);

    void parentCategoryChanged(QString parentCategory);

    void infoCompositeLoaded(Product prod);



private:
    QList<CategoryProducts> products;

    QList<ProductsModel*> m_data;
    void clearData();

    bool m_visible;
    bool m_isBack = true;

    int m_current;
    QString m_parentCategory;
};

#endif // PAGEMODEL_H
