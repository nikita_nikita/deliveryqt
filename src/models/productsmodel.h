#ifndef PRODUCTSMODEL_H
#define PRODUCTSMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/product.h"
#include "../entity/cartproductentity.h"

class ProductsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
public:
    enum Roles {
        IdRole = Qt::UserRole + 3,
        NameRole,
        WeightRole,
        NewRole,
        HotRole,
        HitRole,
        CaloricRole,
        VolumeRole,
        CompositionRole,
        ImageRole,
        SizeofsetRole,
        PriceRole,
        QuantityRole,
        AddedCompositeString,
        TypeName,
        JsonRole,
        Benefit,
        IsSet,
        PapperRole
    };
    explicit ProductsModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;
    void setProductList(ProductList productList);
    int count() const;

signals:
 void countChanged(int count);
 void infoCompositeLoaded(Product prod);


public slots:
void setCart(CartProductList &cart);
void setQuantityByCart(CartProductEntity &ent);
void setCount(int count);
void getInfoComposite(int id);



private:
    ProductList m_data;
    QList<int> quantityProducts;
    CartProductList m_cartProducts;
    QString toString(QList<Product> products) const;
    QString toString(ProductAddedItemList products) const;

    int m_count;
    QString getInfo(ProductItem product) const;
};

#endif // PRODUCTSMODEL_H
