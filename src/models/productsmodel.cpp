#include "productitemlistmodel.h"
#include "productsmodel.h"

ProductsModel::ProductsModel(QObject *parent) : QAbstractListModel(parent)
{

}

int ProductsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_data.count();
}

QVariant ProductsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    Product product = this->m_data.at(index.row());


    switch (role) {
    case SizeofsetRole:
        return QVariant(product.getName());
    case IdRole:
        return QVariant(product.getId());
    case NameRole:
        return QVariant(product.getName());
    case WeightRole:
        return QVariant(product.getWeight());
    case NewRole:
        return QVariant(product.getNew());
    case HotRole:
        return QVariant(product.getHot());
    case HitRole:
        return QVariant(product.getHit());
    case CaloricRole:
        return QVariant(product.getCaloric());
    case VolumeRole:
        return QVariant(product.getVolume());
    case CompositionRole:
        return QVariant(product.getComposition());
    case ImageRole:
        return QVariant(product.getImages().at(0));
    case PriceRole:
        return QVariant(product.getPrice());
    case QuantityRole:
        return QVariant(this->quantityProducts.at(index.row()));
    case AddedCompositeString:
         return  QVariant(this->toString(product.getAdded().getComposite()));
    case TypeName:
        return QVariant(product.getType().getName());
    case JsonRole:
        return QVariant(product.toJson());
    case Benefit:
        return QVariant(product.getBenefit());
    case IsSet:
        return QVariant(product.IsSet());
    case PapperRole:
        return QVariant("///🌶");
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ProductsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[WeightRole] = "weight";
    roles[HotRole] = "hot";
    roles[HitRole] = "hit";
    roles[CaloricRole] = "caloric" ;
    roles[ImageRole]  = "image";
    roles[PriceRole] = "price";
    roles[CompositionRole] = "composition";
    roles[QuantityRole] = "quantity";
    roles[SizeofsetRole] = "sizeOfSet";
    roles[AddedCompositeString] = "addedCompositeString";
    roles[TypeName]  = "typeName";
    roles[VolumeRole] = "volume";
    roles[JsonRole] = "json";
    roles[Benefit] = "benefit";
    roles[IsSet] = "isSet";
    roles[PapperRole] = "papper";
    return roles;
}

void ProductsModel::setProductList(ProductList productList)
{
    beginResetModel();
    this->m_data = productList;
    this->quantityProducts.clear();
    for (int i=0;i<this->m_data.count();i++) {
        this->quantityProducts.append(0);
    }
    endResetModel();
    this->setCount(m_data.count());
}

int ProductsModel::count() const
{
    return m_count;
}



QString ProductsModel::toString(QList<Product> products) const
{
    QString result;
    for (int i=0;i<products.count();i++) {
        Product product = products.at(i);
        if(i!=0) result.append(", ");
        result.append(product.getName());
    }
    return result;
}

QString ProductsModel::toString(ProductAddedItemList products) const
{
//    QJsonArray ss;
//    for (int i=0;i<products.count();i++) {
//        ProductAddedItem product = products.at(i);
//        QJsonObject jObj;
//        QString result;
//        if(i!=0) result.append(", ");

//        if (product.getQuanatity()>1) result.append(QString::number(product.getQuanatity())+" ");

//        result.append(product.getProduct().getName());
//        jObj["component_name"] = result;
//        if(product.getProduct().getHot()){
//            jObj["hot_status"]=true;
//        }else{
//            jObj["hot_status"]=false;
//     }
//    ss.append(jObj);
//    }
//    return ss;
    QString result;

        for (int i=0;i<products.count();i++) {
            ProductAddedItem product = products.at(i);

            if(i!=0) result.append(", ");

            if (product.getQuanatity()>1) result.append(QString::number(product.getQuanatity())+" ");

            result.append(product.getProduct().getName());
            if(product.getProduct().getHot())
                result.append(" <img src=\"qrc:/view/img/hot-icon.png\" width=\"12\" height=\"12\"/>");
        }
        return result;

}

void ProductsModel::setCart(CartProductList &cart)
{

    QVector<int> roles;
    roles.append(QuantityRole);
    for (int i=0;i<this->m_data.count();i++) {
        Product product = this->m_data.at(i);
        int posCart= cart.findByProduct(product, true);

        if(posCart ==-1) {
            if(this->quantityProducts.at(i)!=0){
                this->quantityProducts.replace(i, 0);
                emit dataChanged(createIndex(i,0), createIndex(i,0), roles);
            }
        } else {
            if(cart.at(posCart).getQuantity()!=this->quantityProducts.at(i)){
                this->quantityProducts.replace(i, cart.at(posCart).getQuantity());
                emit dataChanged(createIndex(i,0), createIndex(i,0), roles);
            }
        }
    }
    this->m_cartProducts = cart;
}

void ProductsModel::setQuantityByCart(CartProductEntity &ent)
{


    QVector<int> roles;
    roles.append(QuantityRole);
    if(!ent.getOptions().isEmpty()){
        return;
    }
    for (int i=0;i<this->m_data.count();i++) {
        Product product = this->m_data.at(i);
        if(ent.getProduct()==product){
            int quantity = this->quantityProducts.at(i);
            if(quantity!=ent.getQuantity()){
                this->quantityProducts.replace(i, ent.getQuantity());              
                emit dataChanged(createIndex(i,0), createIndex(i,0), roles);
            }
        }
    }
}

void ProductsModel::setCount(int count)
{
    if (m_count == count)
        return;

    m_count = count;
    emit countChanged(m_count);
}

QString ProductsModel::getInfo(ProductItem product) const
{
    QString result = "";
    if(product.getNumber() != ""){
        result = product.getNumber() + "шт/ " + product.getWeight() + " г/ ";
    }
    return result;
}


void ProductsModel::getInfoComposite(int id)
{

    emit infoCompositeLoaded(this->m_data.getById(id));
}
