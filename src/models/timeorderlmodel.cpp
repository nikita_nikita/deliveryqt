#include "timeorderlmodel.h"

TimeOrderlModel::TimeOrderlModel(QObject *parent) : QAbstractListModel(parent)
{

}

int TimeOrderlModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return this->m_data.count();
}

QVariant TimeOrderlModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    if (!index.isValid()) {
        return QVariant();
    }
    return m_data.at(index.row());
}

void TimeOrderlModel::setTimeInterval(QTime start, QTime finish)
{
    beginResetModel();
    this->m_data.clear();
       QTime indexTime= getValidTime(start);
       QTime end = getValidTime(finish);
       indexTime=indexTime.addSecs(900);
       while (indexTime<=end) {
           this->m_data.append(indexTime.toString("hh:mm"));
           indexTime=indexTime.addSecs(900);

       }

    endResetModel();
}

QTime TimeOrderlModel::getValidTime(QTime time)
{
    int minute = time.minute();
    if (minute==0){
        return time;
    }
    if (minute>=15&&minute<30){
        time.setHMS(time.hour(), 15,0);
        return time;
    }
    if(minute>=30&&minute<45) {
        time.setHMS(time.hour(),30, 0);
        return time;
    }
    if(minute>=45&&minute<60) {
        time.setHMS(time.hour(),45, 0);
        return time;
    }
    return time;
}


