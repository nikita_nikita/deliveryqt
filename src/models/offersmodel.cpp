#include "offersmodel.h"

OffersModel::OffersModel(QObject *parent) : QAbstractListModel(parent)
{

}

int OffersModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_data.count();
}

QVariant OffersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

   Offers entity = this->m_data.at(index.row());
    switch (role) {
    case IdRole:
        return QVariant(entity.getId());
    case NameRole:
        return QVariant(entity.getName());
    case SortIdRole:
        return entity.getSortId();
    case DescriptionRole:
        return entity.getDescription();
    case ImageBanerRole:
         return entity.getImageBanner();
    case ImageCatalogRole:
        return entity.getImageCatalog();
    case VisibleBanerRole:
        return entity.getVisibleBanner();
    case VisibleCatalogRole:
        return entity.getVisibleCatalog();
    case VisibleButtonRole:
        return entity.getVisibleButton();
    case StartDateRole:
        return entity.getStartDate();
    case ButtonTitleRole:
        return entity.getButtonTitle();
    case EndDateRole:
        return entity.getEndDate();
    case SityRole:
        return entity.getSity();
    case PromocodeRole:
        return entity.getPromocode();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> OffersModel::roleNames() const
{
    QHash<int, QByteArray> roles=QAbstractListModel::roleNames();
        roles[IdRole] = "id";
        roles[NameRole] = "name";
        roles[SortIdRole] = "sortId";
        roles[DescriptionRole] = "description";
        roles[ImageBanerRole] = "imageBanner";
        roles[ImageCatalogRole] = "imageCatalog";
        roles[VisibleBanerRole] = "visibleBaner";
        roles[VisibleCatalogRole] = "visibleCatalog";
        roles[VisibleButtonRole] = "visibleButton";
        roles[StartDateRole] = "startDate";
        roles[ButtonTitleRole] = "buttonTitle";
        roles[EndDateRole] ="endDate";
        roles[SityRole] = "sity";
        roles[PromocodeRole] = "promocode";
    return roles;
}

void OffersModel::setOffersList(OffersList list)
{
    beginResetModel();
  this->m_data=list;   
  endResetModel();

}
