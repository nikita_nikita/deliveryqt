#include "toppingkitmodel.h"

ToppingKitModel::ToppingKitModel(QObject *parent)
    : QAbstractListModel(parent)
{
}



int ToppingKitModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return m_list.count();
}

QVariant ToppingKitModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    auto item = this->m_list.at(index.row());
    switch (role) {
    case ImageRole:
        return item.getImage();
    case NameRole:
        return item.getName();
    case QuantityRole:
        return item.getQuantity();
    case CountRole:
        return item.getCount();
    }
    return QVariant();
}

QHash<int, QByteArray> ToppingKitModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ImageRole]    = "m_image";
    roles[NameRole]     = "m_name";
    roles[QuantityRole] = "m_quantity";
    roles[CountRole]    = "m_count";
    return roles;
}

void ToppingKitModel::setToppinKits(const QJsonArray &value)
{
serviceKits =  Toppings_kit_list(value);

}

void ToppingKitModel::setCount(int index, int count)
{

    auto product = m_list.at(index);
    product.setCount(count);
    m_list.replace(index,product);
    QVector<int> roles;
    roles.append(CountRole);
    dataChanged(createIndex(index,0),createIndex(index,0),roles);
}


void ToppingKitModel::processToppingsByTotalCost(int value)
{

    if(m_list.count()== 0){
        return;
    }
    auto quantities = serviceKits.getQuantitesByPrice(value);
   beginResetModel();
    for(int i =0; i < m_list.count();i++){
        auto id = m_list.at(i).getId();
        auto quantity = quantities[id];
        updateQuantity(i,quantity);
    }

   endResetModel();
}

int ToppingKitModel::getToppingsCount()
{
    int result = 0;
    for(int i = 0; i < m_list.count();i++){
        result += m_list.at(i).getCount();
    }
    return result;
}

void ToppingKitModel::updateQuantity(int index, int quantity)
{
    auto product = m_list.at(index);
    if(product.getQuantity() == quantity){
        return;
    }
    product.setQuantity(quantity);
    if(product.getCount()>quantity){
        product.setCount(quantity);
    }
    m_list.replace(index,product);
    QVector<int> roles;
    roles.append(CountRole);
    dataChanged(createIndex(index,0),createIndex(index,0),roles);
}


void ToppingKitModel::setList(const ToppingList &value)
{
    m_list.clear();
    beginResetModel();
    m_list.append(value);
    endResetModel();


}

void ToppingKitModel::setToppingData(QJsonArray value)
{

    auto toppingArray = value.at(0).toObject().value("data").toArray();
    setList(ToppingList(toppingArray));
    this->setToppinKits(value);
}

QJsonArray ToppingKitModel::getData()
{
    return m_list.toJson();

}
