#ifndef SEARCHLISTMODEL_H
#define SEARCHLISTMODEL_H

#include <QAbstractListModel>

class SearchListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit SearchListModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
};

#endif // SEARCHLISTMODEL_H
