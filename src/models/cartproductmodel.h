#ifndef CARTPRODUCTMODEL_H
#define CARTPRODUCTMODEL_H

#include <QAbstractListModel>
#include "../entity/cartproductentity.h"
#include <QMap>
class CartProductModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int number READ number WRITE setNumber NOTIFY numberChanged)
    Q_PROPERTY(bool notDeliverySeparately READ notDeliverySeparately WRITE setNotDeliverySeparately NOTIFY notDeliverySeparatelyChanged)
    Q_PROPERTY(bool containsPizza READ containsPizza WRITE setContainsPizza NOTIFY containsPizzaChanged)


public:  enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        WeightRole,
        NewRole,
        HotRole,
        HitRole,
        CaloricRole,
        VolumeRole,
        OfferValueRole,
        CompositionRole,
        ImageRole,
        PriceRole,
        QuantityRole,
    };
    Q_INVOKABLE bool isNotDeliverySeparately();

    explicit CartProductModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

    int number() const;

    bool notDeliverySeparately() const;

    bool containsPizza() const;


    CartProductList getCart_list() const;
signals:
    void cartChanged(CartProductList cart);
    void numberChanged(int number);
    void notDeliverySeparatelyChanged(bool notDeliverySeparately);
    void containsPizzaChanged(bool containsPizza);

public slots:

    void reset();
    void add(Product product);
    void add(Product product, CartOptions options);
    void add(CartProductList products);
    void setQuantity(Product product , int quantity, bool withoutOptions=false);
    void setQuantityByIndex(int pos, int quantity);
    void setNumber(int number);
    void setOptions(int pos, CartOptions options);
    void calculateNumber();
    void setNotDeliverySeparately(bool notDeliverySeparately);
    void setContainsPizza(bool containsPizza);
    void setProductsDiscount(QMap<QString, double> data);
private:
    CartProductList cart_list;    
    int m_number=0;
    bool m_notDeliverySeparately;
    bool m_containsPizza;
};

#endif // CARTPRODUCTMODEL_H
