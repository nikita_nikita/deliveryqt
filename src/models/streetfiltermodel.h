#ifndef STREETFILTERMODEL_H
#define STREETFILTERMODEL_H

#include <QSortFilterProxyModel>

class StreetFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit StreetFilterModel(QObject *parent = nullptr);

    int getLimit() const;
    void setLimit(int getLimit);
     int rowCount(const QModelIndex &parent = QModelIndex()) const override;

signals:

public slots:
private:
    int m_limit=0;
};

#endif // STREETFILTERMODEL_H
