#include "worktimemodel.h"

WorkTimeModel::WorkTimeModel(QObject *parent) : QAbstractListModel(parent)
{

}

int WorkTimeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return this->m_daylist.count();
}

QVariant WorkTimeModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid()) {
        return QVariant();
    }
    int index_day=index.row();
    WorkDayEntity day = this->m_daylist.at(index_day);
    switch (role) {
    case DayRole:
        return day.getName();
    case PickupStartRole:
        return day.getPickup().getStart();
    case PickupEndRole:
        return day.getPickup().getEnd();
    case DeliveryStartRole:
        return day.getDelivery().getStart();
    case DeliveryEndRole:
        return day.getDelivery().getEnd();
    case IsCurrentRole:
        return (index_day+1)==currentDayOfWeek;
    default:
        return QVariant();
    }

}

QHash<int, QByteArray> WorkTimeModel::roleNames() const
{
    auto roles =  QAbstractListModel::roleNames();
     roles[DayRole] ="day";
     roles[PickupStartRole] = "pickupStart";
     roles[PickupEndRole]= "pickupEnd";
     roles[DeliveryStartRole] = "deliveryStart";
     roles[DeliveryEndRole]= "deliveryEnd";
     roles[IsCurrentRole] = "isCurrent";
    return roles;
}

QList<WorkDayEntity> WorkTimeModel::daylist() const
{
    return m_daylist;
}

void WorkTimeModel::setDaylist(const QList<WorkDayEntity> &daylist)
{
    beginResetModel();
    m_daylist = daylist;
    endResetModel();
}

int WorkTimeModel::getCurrentDayOfWeek() const
{
    return currentDayOfWeek;
}

void WorkTimeModel::setCurrentDayOfWeek(int value)
{
    currentDayOfWeek = value;
}


