#ifndef PAGESMODEL_H
#define PAGESMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/category.h"
#include "../entity/citydata/productcategory.h"
#include "pagemodel.h"



class PagesModel : public QAbstractListModel
{

    Q_OBJECT


public:
    enum Roles {
        PagesRole = Qt::UserRole + 1,
        PageVisible

    };
    explicit PagesModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setMenu(ProductCategoryList products, CategoryList categorys);


public slots:
 void setCurentCategory(QString alias, bool deep);
 void setProductinCart(CartProductList cart);
 void setQuantityByCart(CartProductEntity &ent);


signals:
    void infoCompositeLoaded(Product prod);


private:

    void createPageDatas(ProductCategoryList products, QList<Category> categorys, QString parent);


    QList<PageModel*> m_pages_list;

    void clearPageList();
    int findPageByParen(QString parent);
    QString m_categoryInfo;
};

#endif // PAGESMODEL_H
