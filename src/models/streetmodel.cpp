    #include "streetmodel.h"
#include <QDebug>
StreetModel::StreetModel(QObject *parent) : QAbstractListModel(parent)
{

}

int StreetModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_data.count();
}

QVariant StreetModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    Street street = this->m_data.at(index.row());
    switch (role) {
    case IdRole:
        return  street.getId();
    case NameRole:
        return street.getName();
    case TypeRole:
        return street.getType();
    case TypeShortRole:
        return street.getTypeShort();
    case CodeRole:
        return street.getCode();
    case StatusRole:
        return street.getStatus();
    case SityRole:
        return street.getSity();
    case JsonRole:
        return street.toJson();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> StreetModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[NameRole]  = "name";
    roles[TypeRole] = "type";
    roles[TypeShortRole] = "typeShort";
    roles[CodeRole] = "code";
    roles[StatusRole] = "status";
    roles[SityRole]= "sity";
    roles[JsonRole] = "json";
    return roles;
}

void StreetModel::setStreetlist(StreetList list)
{
    beginResetModel();
    qDebug()<<"list.count()"<<list.count();
    this->m_data = list;
    endResetModel();
}
