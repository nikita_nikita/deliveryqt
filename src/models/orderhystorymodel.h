#ifndef ORDERHYSTORYMODEL_H
#define ORDERHYSTORYMODEL_H

#include "../entity/orderhystory.h"
#include <QAbstractListModel>
#include "../entity/iikoorder.h"
#include <QDateTime>
class OrderHystoryModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        StatusRole,
        DeliveryRole,
        ProductRole,
        ProductArrayRole,
        TakeAweyPointRole,
        DataTimeRole,
        CanFeedBackRole


    };
    explicit OrderHystoryModel(QObject *parent = nullptr);

    void add(QJsonArray jArr);
    void add(OrderHystory order);
    void updateOrderInfo(IikoOrder iikoOrder );
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

    void setFeedBackStatusFalse(int orderId);
private:
   OrderHystoryList orders;
   bool isOld(QDateTime data);

signals:
   void getOrderStatus(int Orderid);
   void feedbackMessage();
public slots:
   void onOrderStatusloaded(int id, QString status, QJsonArray products, QJsonObject orderType);
   void onClientDataReloaded(QJsonArray list);

};

#endif // ORDERHYSTORYMODEL_H
