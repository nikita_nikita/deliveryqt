#include "cartproductmodel.h"

CartProductModel::CartProductModel(QObject *parent) : QAbstractListModel(parent)
{
    connect(this, &CartProductModel::cartChanged, [this](){
        this->calculateNumber();
        this->setNotDeliverySeparately(this->isNotDeliverySeparately());
        this->setContainsPizza(this->cart_list.isContainsPizza());

    });
}


bool CartProductModel::isNotDeliverySeparately()
{
    for (int i=0;i<cart_list.count();i++) {
        CartProductEntity entity = cart_list.at(i);
        if(entity.getProduct().getNotDeliverySeparately()==false){
            return false;
        }
    }
    return true;
}
int CartProductModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return cart_list.count();
}


QVariant CartProductModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    CartProductEntity entity = this->cart_list.at(index.row());
    switch (role) {
    case IdRole:
        return QVariant(entity.getProduct().getId());
    case NameRole:
        return QVariant(entity.getProduct().getName());

    case WeightRole:
        return QVariant(entity.getProduct().getWeight());
    case NewRole:
        return QVariant(entity.getProduct().getNew());
    case HotRole:
        return QVariant(entity.getProduct().getHot());
    case HitRole:
        return QVariant(entity.getProduct().getHit());
    case CaloricRole:
        return QVariant(entity.getProduct().getCaloric());
    case VolumeRole:
        return QVariant(entity.getProduct().getVolume());
    case CompositionRole:
        return QVariant(entity.getProduct().getComposition());
    case ImageRole:
        return QVariant(entity.getProduct().getImages().at(0));
    case PriceRole:
        return QVariant(entity.getProduct().getPrice());
    case QuantityRole:
        return entity.getQuantity();
    case OfferValueRole:
        return entity.getOfferValue();

    default:
        return QVariant();
    }
}

QHash<int, QByteArray> CartProductModel::roleNames() const
{
    QHash<int, QByteArray>  roles;
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[WeightRole] = "weight";
    roles[HotRole] = "hot";
    roles[HitRole] = "hit";
    roles[CaloricRole] = "caloric" ;
    roles[ImageRole]  = "image";
    roles[PriceRole] = "price";
    roles[CompositionRole] = "composition";
    roles[OfferValueRole] = "offerValue";
    roles[QuantityRole] = "quantity";
    return  roles;
}

int CartProductModel::number() const
{
    return m_number;
}

bool CartProductModel::notDeliverySeparately() const
{
    return m_notDeliverySeparately;
}

bool CartProductModel::containsPizza() const
{
    return m_containsPizza;
}

void CartProductModel::reset()
{
    beginResetModel();
    this->cart_list.clear();
    endResetModel();
    emit cartChanged(this->cart_list);
}

void CartProductModel::add(Product product)
{
    beginInsertRows(QModelIndex(), this->cart_list.count(), this->cart_list.count());
    this->cart_list.add(product);
    endInsertRows();
    emit cartChanged(this->cart_list);
}

void CartProductModel::add(Product product, CartOptions options)
{
    beginInsertRows(QModelIndex(), this->cart_list.count(), this->cart_list.count());
    this->cart_list.add(product, options);
    endInsertRows();
    emit cartChanged(this->cart_list);
}

void CartProductModel::add(CartProductList products)
{
    beginInsertRows(QModelIndex(), this->cart_list.count(),products.count());
    this->cart_list.append(products);
    endInsertRows();
    emit cartChanged(this->cart_list);
}

void CartProductModel::setQuantity(Product product, int quantity, bool withoutOptions)
{

    int pos  = this->cart_list.findByProduct(product,withoutOptions);
    if(pos ==-1) {
        return;
    }
    if(quantity ==0) {
        beginRemoveRows(QModelIndex(),pos, pos);
        CartProductEntity entity = this->cart_list.at(pos);
        entity.setQuality(quantity);
        this->cart_list.replace(pos, entity);
        emit cartChanged(this->cart_list);
        this->cart_list.removeAt(pos);
        endRemoveRows();
        return;
    }

    CartProductEntity entity = this->cart_list.at(pos);
    entity.setQuality(quantity);
    this->cart_list.replace(pos, entity);
    QModelIndex index = createIndex(pos, 0);
    emit cartChanged(this->cart_list);
    emit dataChanged(index, index);
}

void CartProductModel::setQuantityByIndex(int pos, int quantity)
{
    if(pos ==-1) {
        return;
    }
    CartProductEntity entity = this->cart_list.at(pos);
    entity.setQuality(quantity);
    this->cart_list.replace(pos, entity);

    if(quantity ==0) {
        beginRemoveRows(QModelIndex(),pos, pos);
        this->cart_list.removeAt(pos);
        emit cartChanged(this->cart_list);
        endRemoveRows();
        return;
    }



    QModelIndex index = createIndex(pos, 0);
    emit cartChanged(this->cart_list);
    emit dataChanged(index, index);
}

void CartProductModel::setNumber(int number)
{
    if (m_number == number)
        return;

    m_number = number;
    emit numberChanged(m_number);
}

void CartProductModel::setOptions(int pos, CartOptions options)
{
    CartProductEntity entity = this->cart_list.at(pos);
    entity.setOptions(options);
    this->cart_list.replace(pos, entity);
    QModelIndex index = createIndex(pos, 0);
    emit cartChanged(this->cart_list);
    emit dataChanged(index, index);
}

void CartProductModel::calculateNumber()
{
    int number = 0;
    for (int i=0; i< this->cart_list.count();i++) {
        number = number+cart_list.at(i).getQuantity();
    }
    this->setNumber(number);
}

void CartProductModel::setNotDeliverySeparately(bool notDeliverySeparately)
{


    if (m_notDeliverySeparately == notDeliverySeparately)
        return;
    m_notDeliverySeparately = notDeliverySeparately;
    emit notDeliverySeparatelyChanged(m_notDeliverySeparately);
}

void CartProductModel::setContainsPizza(bool containsPizza)
{
    if (m_containsPizza == containsPizza)
        return;

    m_containsPizza = containsPizza;
    emit containsPizzaChanged(m_containsPizza);
}

void CartProductModel::setProductsDiscount(QMap<QString, double> data)
{
    bool modifer = false;
    for(int i =0; i < cart_list.count();i++){
        auto product = cart_list.at(i);
        auto id = product.getId();
        if(data.contains(id)){
            if(product.getOfferValue()!=data[id]){
                product.setOfferValue(data[id]);
                modifer = true;
            }
        }else{
            if(product.getOfferValue()!=0){
                modifer = true;
                product.setOfferValue(0);

            }
        }
        cart_list.replace(i,product);
    }
    if(modifer){
        emit cartChanged(this->cart_list);
    }

}



CartProductList CartProductModel::getCart_list() const
{
    return cart_list;
}



