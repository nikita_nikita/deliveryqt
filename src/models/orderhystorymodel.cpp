#include "orderhystorymodel.h"
#include <QDebug>
OrderHystoryModel::OrderHystoryModel(QObject *parent) :QAbstractListModel(parent)
{

}


void OrderHystoryModel::add(QJsonArray jArr)
{
    if(jArr.count() != orders.count()){
        orders.clear();


        beginResetModel();
        for(int i =0; i < jArr.count();i++){
            OrderHystory order(jArr.at(i).toObject());
            orders.append(order);
            if(order.canFeedBack()){
                emit feedbackMessage();
            }
        }
        endResetModel();
    }

for(int i = 0; i < orders.count();i++){
    auto order = jArr.at(i).toObject();
    QString time_format = "dd.MM.yyyy hh:mm";
    QString  dateTime = order.value("date_update").toString();
    if(!isOld(QDateTime::fromString(dateTime,time_format))){
        qDebug()<<"getOrderStatus";
        emit  getOrderStatus(order.value("id").toInt());
    }
}
}

void OrderHystoryModel::add(OrderHystory order)
{
    //    beginInsertRows(QModelIndex(), this->orders.count(), this->orders.count());
    beginResetModel();
    orders.append(order);
    endResetModel();
    //    endInsertRows();
}

void OrderHystoryModel::updateOrderInfo(IikoOrder iikoOrder)
{

    auto index  = orders.indexOfId(iikoOrder.getId());

    if(index == -1){
        return;
    }
    auto order = orders.at(index);
    order.setStatus(iikoOrder.getStatus());
    qDebug()<<"status >> "<< iikoOrder.getStatus();
    order.setDelivery(iikoOrder.getDelivery());
    //    order.setProducts(iikoOrder.getProducts());
    //    if(order.delivery() == 1){
    //        order.setTakeAweypoint(iikoOrder.getPoint());

    //    }

    orders.replace(index,order);
    QModelIndex indexData = createIndex(index, 0);
    emit dataChanged(indexData,indexData);
}



int OrderHystoryModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return orders.count();
}

QVariant OrderHystoryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    auto order = this->orders.at(index.row());
    switch (role) {
    case IdRole:
        return QVariant(order.id());
    case StatusRole:
        return QVariant(order.status());
    case ProductRole:
        return QVariant(order.getProductString());
    case ProductArrayRole:
        return QVariant(order.getProductArray());
    case DeliveryRole:
        return QVariant(order.delivery());
    case CanFeedBackRole:
        return QVariant(order.canFeedBack());
    case TakeAweyPointRole:
        return QVariant(order.takeAweypoint());
    case DataTimeRole:
        return  QVariant(order.getDateTime());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> OrderHystoryModel::roleNames() const
{
    QHash<int, QByteArray> roles=QAbstractListModel::roleNames();
    roles[IdRole] = "id";
    roles[StatusRole] = "status";
    roles[TakeAweyPointRole] = "point";
    roles[ProductRole] = "products";
    roles[ProductArrayRole] = "productArray";
    roles[CanFeedBackRole] = "canFeedbackOrder";
    roles[DeliveryRole] = "delivery";
    roles[DataTimeRole] = "dateTime";
    return roles;
    
}

void OrderHystoryModel::setFeedBackStatusFalse(int id)
{
    auto index = orders.indexOfId(id);
    if(index != -1){
        auto order = orders.at(index);
        order.setCanFeedBack(false);

        orders.replace(index,order);
        QModelIndex indexData = createIndex(index, 0);
        emit dataChanged(indexData,indexData);
    }else{
    }
}


bool OrderHystoryModel::isOld(QDateTime data)
{
    QDateTime currentDate = QDateTime::currentDateTime();
    auto currentDay = currentDate.date().day();
    auto orderDay = data.date().day();
    auto currentMonth = currentDate.date().month();
    auto orderMonth = data.date().month();
    auto currentYear = currentDate.date().year();
    auto orderYear = data.date().year();
    int comperDay = currentDay - orderDay;
    int comperMonth = currentMonth - orderMonth;
    int comperYear = currentYear - orderYear;
    if(comperDay > 2 || comperMonth > 0 || comperYear > 0){
        return  true;

    }

    return false;
}

void OrderHystoryModel::onOrderStatusloaded(int id, QString status, QJsonArray products, QJsonObject orderType)
{
    auto orderindex = orders.indexOfId(id);
    auto order = orders.at(orderindex);
    order.setStatus(status);
    order.setProducts(products);
    if(orderType.value("orderServiceType").toString()=="DELIVERY_PICKUP"){
        order.setDelivery(0);
    }else{
        order.setDelivery(1);
    }
    //marker ругляркой вытянуть точку самовывоза


}



void OrderHystoryModel::onClientDataReloaded(QJsonArray jArr)
{
    qDebug()<<"reloaded client data"<<jArr.count();
    OrderHystoryList historyList(jArr);
    beginResetModel();
    this->orders =historyList;
    endResetModel();

}

