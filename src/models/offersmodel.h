#ifndef OFFERSMODEL_H
#define OFFERSMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/offers.h"

class OffersModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        SortIdRole,
        NameRole,
        DescriptionRole,
        ImageBanerRole,
        ImageCatalogRole,
        VisibleCatalogRole,
        VisibleBanerRole,
        VisibleButtonRole,
        ButtonTitleRole,
        StartDateRole,
        EndDateRole,
        SityRole,
        PromocodeRole
    };
    explicit OffersModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

signals:

public slots:
    void setOffersList(OffersList list);
private:
    OffersList m_data;
};

#endif // OFFERSMODEL_H
