#ifndef TIMEORDERLMODEL_H
#define TIMEORDERLMODEL_H


#include <QAbstractListModel>

#include <QTime>
class TimeOrderlModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TimeOrderlModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;

signals:

public slots:
    void setTimeInterval(QTime start , QTime finish);

private:
    QStringList m_data;
    QTime getValidTime(QTime time);
};

#endif // TIMEORDERLMODEL_H
