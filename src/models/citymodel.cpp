#include "citymodel.h"

CityModel::CityModel(QObject *parent) : QAbstractListModel(parent)
{

}

int CityModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_data.count();
}

QVariant CityModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    City city = this->m_data.at(index.row());
    switch (role) {
    case IdRole:
        return city.getId();
    case NameRole:
        return city.getName();
    case CodeRole:
        return city.getCode();
    case EnableRole:
        return city.getEnable();
    case CheckStreetRole:
        return city.getCheckStreet();
    case HrefVisibleRole:
        return city.getHrefVisible();
    case SortIdRole:
        return city.getSortId();
    case DomainIdRole:
        return city.getDomain().getId();
    case DomainCodeRole:
        return city.getDomain().getCode();
    case DomainPathRole:
        return city.getDomain().getPath();
    case DomainProtocolRole:
        return city.getDomain().getProtocol();
    case JsonRole:
        return city.toJson();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> CityModel::roleNames() const
{
    QHash<int, QByteArray> roles=QAbstractListModel::roleNames();
    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[CodeRole] = "code";
    roles[EnableRole] = "enable";
    roles[CheckStreetRole] = "checkStreet";
    roles[HrefVisibleRole] = "hrefVisible";
    roles[SortIdRole]  = "sortId";
    roles[DomainIdRole] = "domainId";
    roles[DomainCodeRole]  = "domainCode";
    roles[DomainPathRole] = "domainPath";
    roles[DomainProtocolRole] = "domainProtocol";
    roles[JsonRole]  = "json";
    return roles;
}

void CityModel::setCityList(CityList list)
{
    beginResetModel();
    this->m_data = list;
    endResetModel();

}
