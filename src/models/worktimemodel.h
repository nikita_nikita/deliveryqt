#ifndef WORKTIMEMODEL_H
#define WORKTIMEMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/workdayentity.h"

class WorkTimeModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        DayRole = Qt::UserRole + 1,
        PickupStartRole,
        PickupEndRole,
        DeliveryStartRole,
        DeliveryEndRole,
        IsCurrentRole
    };
    explicit WorkTimeModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;



    QList<WorkDayEntity> daylist() const;
    void setDaylist(const QList<WorkDayEntity> &daylist);

    int getCurrentDayOfWeek() const;
    void setCurrentDayOfWeek(int value);

signals:

public slots:
private:
    QList<WorkDayEntity> m_daylist;
    int currentDayOfWeek=0;

};

#endif // WORKTIMEMODEL_H
