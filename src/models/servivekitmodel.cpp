#include "servivekitmodel.h"

ServiveKitModel::ServiveKitModel(QObject *parent)
    : QAbstractListModel(parent)
{
}



int ServiveKitModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return m_data.count();
}

QVariant ServiveKitModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    auto item = this->m_data.at(index.row());
    switch (role) {
    case ImageRole:
        return item.getImage();
    case TextRole:
        return item.getText();
    }
    return QVariant();
}

QHash<int, QByteArray> ServiveKitModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ImageRole] = "m_image";
    roles[TextRole] = "m_text";
    roles[VendorRole] = "m_vendor";
    roles[QuantityRole] = "m_quantity";
    return roles;
}

void ServiveKitModel::setServiceKits(const ServiceKitList &value)
{
    serviceKits = value;
}

void ServiveKitModel::setProductinCart(CartProductList cart)
{
    beginResetModel();
    auto prices  = cart.getCostsByTypeId();
    auto values  = serviceKits.getValue(prices);
    m_data = values;
    endResetModel();
    this->setCount(m_data.count());
}

int ServiveKitModel::count() const
{
    return m_count;
}

void ServiveKitModel::setCount(int count)
{
    if (m_count == count)
        return;

    m_count = count;
    emit countChanged(m_count);
}


