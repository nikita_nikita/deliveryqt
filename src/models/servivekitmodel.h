#ifndef SERVIVEKITMODEL_H
#define SERVIVEKITMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/servicekit.h"
#include "../entity/cartproductentity.h"
class ServiveKitModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
public:
    enum Roles {
        ImageRole = Qt::UserRole + 1,
        TextRole,
        VendorRole,
        QuantityRole
    };
    explicit ServiveKitModel(QObject *parent = nullptr);



    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setServiceKits(const ServiceKitList &value);
      void setProductinCart(CartProductList cart);

      int count() const;

public slots:
      void setCount(int count);


signals:
      void countChanged(int count);

private:
    ServiceKitList serviceKits ;

    QList<ServiceKitValue> m_data;
    int m_count;
};

#endif // SERVIVEKITMODEL_H
