#include "streetfiltermodel.h"

StreetFilterModel::StreetFilterModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

int StreetFilterModel::getLimit() const
{
    return m_limit;
}

void StreetFilterModel::setLimit(int limit)
{
    m_limit = limit;
    invalidateFilter();
}

int StreetFilterModel::rowCount(const QModelIndex &parent) const
{
    int row = this->sourceModel()->rowCount(parent);
   if(row>m_limit&&m_limit!=0){
       return m_limit;
   }
   return row;
}
