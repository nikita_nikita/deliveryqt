#ifndef TOPPINGKITMODEL_H
#define TOPPINGKITMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/toppings_kit.h"
#include "../entity/citydata/toppinglist.h"
#include <QJsonArray>


class ToppingKitModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        ImageRole = Qt::UserRole + 1,
        NameRole,
        QuantityRole,
        CountRole

    };
    explicit ToppingKitModel(QObject *parent = nullptr);



    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    void setToppinKits(const QJsonArray &value);

    
    void setCount(int index, int quantity);

    ToppingList list() const;
    void setList(const ToppingList &list);

    void setToppingData(QJsonArray);
    QJsonArray getData();
public slots:
    void processToppingsByTotalCost(int value);
    int getToppingsCount();
private:
    //Сколько за сколько
    Toppings_kit_list serviceKits;
    void updateQuantity(int index,int quantity);
    //список топпингов достать из 0 элемента
    ToppingList m_list;

};
#endif // TOPPINGKITMODEL_H
