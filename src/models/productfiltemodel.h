#ifndef PRODUCTFILTEMODEL_H
#define PRODUCTFILTEMODEL_H

#include <QSortFilterProxyModel>
#include "../entity/cartproductentity.h"
#include "../models/productsmodel.h"

class ProductFilteModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ProductFilteModel(QObject *parent = nullptr);
    void setSearchName(QString name);

    int limit() const;
    void setLimit(int limit);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;




signals:
    void setSignalInfoComposite(int id);


public slots:
void getInfoComposite(int id);
private:
    QString m_name = "";
private:
     int m_limit=20;

};

#endif // PRODUCTFILTEMODEL_H
