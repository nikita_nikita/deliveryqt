#include "productfiltemodel.h"
ProductFilteModel::ProductFilteModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

void ProductFilteModel::setSearchName(QString name)
{

    this->m_name = name;
    this->setFilterRegExp(name);
    invalidateFilter();
}


int ProductFilteModel::limit() const
{
    return m_limit;
}

void ProductFilteModel::setLimit(int limit)
{
    m_limit = limit;
}

int ProductFilteModel::rowCount(const QModelIndex &parent) const
{
    int row = this->sourceModel()->rowCount(parent);
//   if(row>m_limit&&m_limit!=0){
//       return m_limit;
//   }
   return row;
}

void ProductFilteModel::getInfoComposite(int id)
{

    emit setSignalInfoComposite(id);

}
