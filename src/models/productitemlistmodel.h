#ifndef PRODUCTITEMLISTMODEL_H
#define PRODUCTITEMLISTMODEL_H

#include <QAbstractListModel>
#include "../entity/citydata/productitem.h"
class ProductItemListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        WeightRole,
        CaloricRole,
        VolumeRole,
        HotRole,
        NumberRole,
        CompositionRole,
        ImageRole,
        TypeName,
        JsonRole

    };
    explicit ProductItemListModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;
signals:

public slots:
    void setProducts(ProductItemList prods);
private:
    ProductItemList m_data;
    QString getInfo(ProductItem product) const;
};

#endif // PRODUCTITEMLISTMODEL_H
