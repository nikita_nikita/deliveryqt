#include "pagesmodel.h"
#include "pagesmodel.h"
PagesModel::PagesModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


int PagesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_pages_list.count();
}

QVariant PagesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    auto page  = this->m_pages_list.at(index.row());
    if (role == PagesRole) {
        return  QVariant::fromValue(page);
    }
    return QVariant();
}

QHash<int, QByteArray> PagesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PagesRole] = "pages_data";
    return roles;
}

void PagesModel::setMenu(ProductCategoryList products, CategoryList categorys)
{
    beginResetModel();
    this->clearPageList();
    this->createPageDatas(products, categorys, "all");
    endResetModel();
}

void PagesModel::setCurentCategory(QString alias, bool deep)
{
    int child = -1;
    if (deep){
        child = this->findPageByParen(alias);
    }
    for(int i=0; i<this->m_pages_list.count(); i++) {
        auto page  = m_pages_list.at(i);
        int index = page->getIndexByCategory(alias);
        if (index>-1){
            page->setCurrent(index);
        }
        if(child>-1) {
            page->setVisible(child==i);
        } else {
            page->setVisible(index>-1);
        }

    }

}

void PagesModel::setProductinCart(CartProductList cart)
{
   /* for (int i =0; i<cart.count(); i++){
        auto item = cart.at(i);
        this->setQuantityByCart(item);
    }*/
    for(int i=0; i<this->m_pages_list.count(); i++) {
        m_pages_list.at(i)->setCart(cart);
    }
}

void PagesModel::setQuantityByCart(CartProductEntity &ent)
{
    for(int i=0; i<this->m_pages_list.count(); i++) {
        auto page  = m_pages_list.at(i);
        page->setQuantityByCart(ent);
    }
}





void PagesModel::createPageDatas(ProductCategoryList products, QList<Category> categorys, QString parent)
{
    QList<CategoryProducts> page_data;
    auto page = new PageModel(this);
    connect(page, &PageModel::infoCompositeLoaded, this, &PagesModel::infoCompositeLoaded);
    page->setParentCategory(parent);
    if (parent == "all"){
        page->setVisible(true);
        page->setIsBack(false);
    }else {
        page->setVisible(false);
        page->setIsBack(true);
    }
    page->setCurrent(0);

    this->m_pages_list.append(page);

    for(int i=0; i<categorys.count(); i++){
        CategoryProducts item;
        auto category = categorys.at(i);
        item.setCategory(category);
        auto aliases  = category.getChildAliases();
        aliases.append(category.getAlias());
        auto prods  =products.getProductByCategory(aliases);
        item.setProducts(prods);
        page_data.append(item);
        auto categ_child = category.getChild();
        if (categ_child.count()>0){
            this->createPageDatas(products, categ_child, category.getAlias());
        }
    }
    page->setProducts(page_data);


}

void PagesModel::clearPageList()
{
    for (int i = 0; i< this->m_pages_list.count(); i++){
        auto page = m_pages_list.at(i);
        disconnect(page, &PageModel::infoCompositeLoaded, this, &PagesModel::infoCompositeLoaded);
        delete(page);
    }
    this->m_pages_list.clear();
}

int PagesModel::findPageByParen(QString parent)
{
    for (int i = 0; i< this->m_pages_list.count(); i++){
        auto page = this->m_pages_list.at(i);
        if (page->parentCategory()==parent){
            return i;
        }
    }
    return -1;
}
