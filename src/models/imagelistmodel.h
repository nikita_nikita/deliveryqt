#ifndef IMAGELISTMODEL_H
#define IMAGELISTMODEL_H

#include <QAbstractListModel>
#include <QUrl>
class ImageListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles{
        UrlRole = Qt::UserRole

    };
    explicit ImageListModel(QObject *parent = nullptr);



    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void addUrl(QString url);
    void removeUrl(int index);

private:
    QList<QString> m_list;
};

#endif // IMAGELISTMODEL_H
