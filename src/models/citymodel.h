#ifndef CITYMODEL_H
#define CITYMODEL_H

#include <QAbstractListModel>
#include "../entity/citylist.h"
class CityModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        CodeRole,
        EnableRole,
        CheckStreetRole,
        HrefVisibleRole,
        SortIdRole,
        DomainProtocolRole,
        DomainPathRole,
        DomainCodeRole,
        DomainIdRole,
        JsonRole,
    };
    explicit CityModel(QObject *parent = nullptr);
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;
    void setCityList(CityList list);

signals:

public slots:
private:
    CityList m_data;
};

#endif // CITYMODEL_H
