VERSION=2.75
 CONFIG -= bitcode
 QT += gui-private
    HEADERS +=src/service/pay/paybyios.h
OBJECTIVE_HEADERS += \
    src/service/pay/ios/SDK/Card.h \
    src/service/pay/ios/SDK/NSDataENBase64.h \
    src/service/pay/ios/SDK/PKPaymentConverter.h
OBJECTIVE_SOURCES += \
    src/service/pay/ios/SDK/Card.m \
    src/service/pay/ios/SDK/NSDataENBase64.m \
    src/service/pay/paybyios.mm \
    src/service/pay/ios/SDK/PKPaymentConverter.m
LIBS += -framework Foundation
LIBS += -framework PassKit


     QMAKE_INFO_PLIST = $$PWD/ios/Info.plist

    QMAKE_ASSET_CATALOGS =$$PWD/ios/Images.xcassets
    QMAKE_TARGET_BUNDLE_PREFIX = mob.ru
    QMAKE_BUNDLE= kapibaras

    DISTFILES += \
       $$PWD/ios/Info.plist \
       $$PWD/ios/GoogleService-Info.plist \
        $$PWD/ios/App.entitlements

 #   # You must deploy your Google Play config file
    deployment.files = $$PWD/ios/GoogleService-Info.plist
deployment.path =
    QMAKE_BUNDLE_DATA += deployment
    APP_ENTITLEMENTS.name = CODE_SIGN_ENTITLEMENTS
    APP_ENTITLEMENTS.value = $$PWD/ios/App.entitlements
    QMAKE_MAC_XCODE_SETTINGS += APP_ENTITLEMENTS
   PRODUCT_NAME=kapibara
