    QT += androidextras
    SOURCES += src/service/pay/paybyandroid.cpp
    HEADERS +=src/service/pay/paybyandroid.h


DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/src/mob/ru/kapibaras/CPCard.java \
    android/src/mob/ru/kapibaras/CPCardType.java \
    android/src/mob/ru/kapibaras/PayGoogle.java \
    android/src/mob/ru/kapibaras/MyActivity.java

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
