import QtQuick 2.12
import QtQuick.Controls 2.4

import "./view/layout"

ApplicationWindow {
    id: root_windows
    width: 360
    height: 568
    visible: true
    property bool quit_program: false
    property bool tables: width>500
    property bool phones: width<361
    DefaultScreen{
        anchors.fill: parent
    }

    LoaderScreen{
        anchors.fill: parent

    }
    onClosing: {
        router.pop()
        close.accepted = false
    }


    function  closeApplication(){
        if(root_windows.quit_program){
            Qt.quit();

        } else {
            global_tooltip.text = "Нажмите еще раз для выхода"
            global_tooltip.visible = true
            timer.start();
            root_windows.quit_program= true
        }


    }
    Timer {
        id: timer
        interval: 3000;
        running: false;
        repeat: false
        onTriggered:  {
            root_windows.quit_program = false
        }

    }

    ToolTip {
        id: global_tooltip
        delay: 0
        timeout: 3000
        x: (parent.width - width) / 2
        y: (parent.height - 100)
        text: "Нажмите еще раз для выхода"
        contentItem: Text {
            id: tip_text
            text: global_tooltip.text
            font: global_tooltip.font
            color: "#fff"
            opacity: 0.8
            horizontalAlignment: Text.AlignHCenter
        }

        background: Rectangle {
            color: "#000"
            radius: 15
            opacity: 0.5

        }
    }
    Text {
        anchors.left:  parent.left
        anchors.bottom: parent.bottom
        width: 200
        height: 260
        text:cartController.screen
        font.pointSize: 20
    }

}


