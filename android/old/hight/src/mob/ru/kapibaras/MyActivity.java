package mob.ru.kapibaras;

import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.PaymentMethodToken;


import android.view.WindowManager;

public class MyActivity extends org.qtproject.qt5.android.bindings.QtActivity
{
        private String publickId;
        private String price;
        private String currency;
        public static MyActivity s_activity = null;
        private PayGoogle payGoogle;
        private PaymentsClient paymentsClient;
        private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;
        private Intent tmp_intent;
	public MyActivity(){

	}
        public void requestPayment() {
                TransactionInfo transaction = PayGoogle.createTransaction(this.price, this.currency);
                 PaymentDataRequest request = PayGoogle.createPaymentDataRequest(transaction, this.publickId);
             Task<PaymentData> futurePaymentData = paymentsClient.loadPaymentData(request);
                AutoResolveHelper.resolveTask(futurePaymentData, this.s_activity, LOAD_PAYMENT_DATA_REQUEST_CODE);


         }
     public String getToken(){
          PaymentData paymentData = PaymentData.getFromIntent(this.tmp_intent);
          PaymentMethodToken token = paymentData.getPaymentMethodToken();
                  if (token != null) {
                      return token.getToken();
                  }
              return "";
      }


         public void setData(String publicId, String price, String  currency) {
                this.publickId = publicId;
                 this.price = price;
                this.currency = currency;

         }
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
                super.onCreate(savedInstanceState);
                s_activity = this;
                paymentsClient = PayGoogle.createPaymentsClient(this);

                //qtfirebase
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}


	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		s_activity = null;
	}
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==991){
            this.tmp_intent = data;
            activityHandler(resultCode);
         }
         super.onActivityResult(requestCode, resultCode, data);
     }
    private static native void activityHandler(int resultCode);




 }
