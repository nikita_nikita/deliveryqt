package mob.ru.kapibaras;

import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;

import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.AutoResolveHelper;



import com.google.android.gms.tasks.Task;
import android.content.Intent;
import java.util.Arrays;
import java.util.List;
import android.app.Activity;

import android.util.Log;
import android.os.Bundle;


public class PayGoogle
{


   public PayGoogle(){ }



    public static final List<Integer> SUPPORTED_METHODS = Arrays.asList(
                // PAYMENT_METHOD_CARD returns to any card the user has stored in their Google Account.
                WalletConstants.PAYMENT_METHOD_CARD,

                // PAYMENT_METHOD_TOKENIZED_CARD refers to cards added to Android Pay, assuming Android
                // Pay is installed.
                // Please keep in mind cards may exist in Android Pay without being added to the Google
                // Account.
                WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD
        );




public static PaymentsClient createPaymentsClient(Activity activity) {
    Wallet.WalletOptions walletOptions = new Wallet.WalletOptions.Builder()
            .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
            .build();
    return Wallet.getPaymentsClient(activity, walletOptions);
}

    public static  TransactionInfo createTransaction(String price, String currency ) {
        return TransactionInfo.newBuilder()
             .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
             .setTotalPrice(price)
             .setCurrencyCode(currency)
             .build();
        }

    public  static PaymentDataRequest createPaymentDataRequest(TransactionInfo transactionInfo, String publickId) {
            PaymentMethodTokenizationParameters  params =
            PaymentMethodTokenizationParameters.newBuilder()
                    .setPaymentMethodTokenizationType(
                    WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                    .addParameter("gateway", "cloudpayments")
                    .addParameter("gatewayMerchantId", publickId)
                    .build();

            return createPaymentDataRequest(transactionInfo, params);
        }
    private static PaymentDataRequest createPaymentDataRequest(TransactionInfo transactionInfo, PaymentMethodTokenizationParameters params) {
            PaymentDataRequest request =
                    PaymentDataRequest.newBuilder()
                            .setPhoneNumberRequired(false)
                            .setEmailRequired(true)
                            .setShippingAddressRequired(false)
                            .setTransactionInfo(transactionInfo)
                            .addAllowedPaymentMethods(SUPPORTED_METHODS)
                            .setCardRequirements(
                                    CardRequirements.newBuilder()
                                            .addAllowedCardNetworks(Arrays.asList(
                                            WalletConstants.CARD_NETWORK_VISA,
                                            WalletConstants.CARD_NETWORK_MASTERCARD))
                                            .setAllowPrepaidCards(true)
                                            .setBillingAddressRequired(true)
                                            .build())
                            .setPaymentMethodTokenizationParameters(params)

                            // If the UI is not required, a returning user will not be asked to select
                            // a card. Instead, the card they previously used will be returned
                            // automatically (if still available).
                            // Prior whitelisting is required to use this feature.
                            .setUiRequired(true)
                            .build();

            return request;
        }



}
