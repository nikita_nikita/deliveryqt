package mob.ru.kapibaras;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.messaging.cpp.DebugLogging;
import com.google.firebase.messaging.cpp.MessageWriter;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.messaging.cpp.MessageWriter;
import com.google.firebase.messaging.cpp.RegistrationIntentService;
/**
 * Listens for Message events from the Firebase Cloud Messaging server and invokes the native
 * OnMessage function.
 */
public class ListenerService extends FirebaseMessagingService {

  // TODO(amablue): Add an IfChange/ThenChange block around this, and the other copy of these
  // variables in com.google.firebase.messaging.RemoteMessageBuilder.
  public static final String MESSAGE_TYPE_DELETED = "deleted_messages";
  public static final String MESSAGE_TYPE_SEND_EVENT = "send_event";
  public static final String MESSAGE_TYPE_SEND_ERROR = "send_error";
  private static final String TAG = "FIREBASE_LISTENER";
  private final MessageWriter messageWriter;

  public ListenerService() {
    this(MessageWriter.defaultInstance());
  }

  public ListenerService(MessageWriter messageWriter) {
    this.messageWriter = messageWriter;
  }

  @Override
  public void onDeletedMessages() {
    DebugLogging.log(TAG, "onDeletedMessages");
//    messageWriter.writeMessageEventToInternalStorage(this, null, MESSAGE_TYPE_DELETED, null);
  }

  @Override
  public void onMessageReceived(RemoteMessage message) {
//      DebugLogging.log(TAG, String.format("onMessageSent messageId=%s", messageId));
             System.out.println("onMessageSent>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//    messageWriter.writeMessage(this, message, false, null);
  }

  @Override
  public void onMessageSent(String messageId) {
    DebugLogging.log(TAG, String.format("onMessageSent messageId=%s", messageId));
           System.out.println("onMessageSent");
//    messageWriter.writeMessageEventToInternalStorage(        this, messageId, MESSAGE_TYPE_SEND_EVENT, null);
  }

  @Override
  public void onSendError(String messageId, Exception exception) {
    DebugLogging.log(
        TAG,
        String.format("onSendError messageId=%s exception=%s", messageId, exception.toString()));
//    messageWriter.writeMessageEventToInternalStorage(        this, messageId, MESSAGE_TYPE_SEND_ERROR, exception.toString());
  }

  @Override
  public void onNewToken(String token) {
    DebugLogging.log(TAG, String.format("onNewToken token=%s", token));
    RegistrationIntentService.writeTokenToInternalStorage(this, token);
  }
}
