package mob.ru.kapibaras;

import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.PaymentMethodToken;
import mob.ru.kapibaras.MessageForwardingService;
//import com.google.firebase.messaging.RemoteMessage;

import android.view.WindowManager;

public class MyActivity extends org.qtproject.qt5.android.bindings.QtActivity
{
        private String publickId;
        private String price;
        private String currency;
        public static MyActivity s_activity = null;
        private PayGoogle payGoogle;
        private PaymentsClient paymentsClient;
        private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;
        private Intent tmp_intent;
	public MyActivity(){

	}
        public void requestPayment() {
                TransactionInfo transaction = PayGoogle.createTransaction(this.price, this.currency);
                 PaymentDataRequest request = PayGoogle.createPaymentDataRequest(transaction, this.publickId);
             Task<PaymentData> futurePaymentData = paymentsClient.loadPaymentData(request);
                AutoResolveHelper.resolveTask(futurePaymentData, this.s_activity, LOAD_PAYMENT_DATA_REQUEST_CODE);


         }
     public String getToken(){
          PaymentData paymentData = PaymentData.getFromIntent(this.tmp_intent);
          PaymentMethodToken token = paymentData.getPaymentMethodToken();
                  if (token != null) {
                      return token.getToken();
                  }
              return "";
      }


         public void setData(String publicId, String price, String  currency) {
                this.publickId = publicId;
                 this.price = price;
                this.currency = currency;

         }
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
                super.onCreate(savedInstanceState);
                s_activity = this;
                paymentsClient = PayGoogle.createPaymentsClient(this);
                System.out.println("-------------------");
                //qtfirebase
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}


	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		s_activity = null;
	}
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==991){
            this.tmp_intent = data;
            activityHandler(resultCode);
         }
         super.onActivityResult(requestCode, resultCode, data);
     }
    private static native void activityHandler(int resultCode);


    /**
        * Messaging example
        */
     // The key in the intent's extras that maps to the incoming message's message ID. Only sent by
     // the server, GmsCore sends EXTRA_MESSAGE_ID_KEY below. Server can't send that as it would get
     // stripped by the client.
     private static final String EXTRA_MESSAGE_ID_KEY_SERVER = "message_id";

     // An alternate key value in the intent's extras that also maps to the incoming message's message
     // ID. Used by upstream, and set by GmsCore.
     private static final String EXTRA_MESSAGE_ID_KEY = "google.message_id";

     // The key in the intent's extras that maps to the incoming message's sender value.
     private static final String EXTRA_FROM = "google.message_id";

     /**
        * Workaround for when a message is sent containing both a Data and Notification payload.
        *
        * When the app is in the foreground all data payloads are sent to the method
        * `::firebase::messaging::Listener::OnMessage`. However, when the app is in the background, if a
        * message with both a data and notification payload is receieved the data payload is stored on
        * the notification Intent. NativeActivity does not provide native callbacks for onNewIntent, so
        * it cannot route the data payload that is stored in the Intent to the C++ function OnMessage. As
        * a workaround, we override onNewIntent so that it forwards the intent to the C++ library's
        * service which in turn forwards the data to the native C++ messaging library.
        */
     @Override
     protected void onNewIntent(Intent intent) {
         // If we do not have a 'from' field this intent was not a message and should not be handled. It
         // probably means this intent was fired by tapping on the app icon.
         System.out.println("get  cloud message");

         Bundle extras = intent.getExtras();
         String from = extras.getString(EXTRA_FROM);
         String messageId = extras.getString(EXTRA_MESSAGE_ID_KEY);

         if (messageId == null) {
             messageId = extras.getString(EXTRA_MESSAGE_ID_KEY_SERVER);
         }

         if (from != null && messageId != null) {
             Intent message = new Intent(this, MessageForwardingService.class);
             message.setAction(MessageForwardingService.ACTION_REMOTE_INTENT);
             message.putExtras(intent);
             startService(message);
         }
         setIntent(intent);

     }
// @Override
// public void onMessageReceived(RemoteMessage remoteMessage) {

//     sendNotification(remoteMessage.getNotification().getBody());

// }
// private void sendNotification(String messageBody) {
//     Intent intent = new Intent(this, MainActivity.class);
//     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//     Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//     NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//             .setSmallIcon(R.drawable.ic_launcher)
//             .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher))
//             .setContentTitle(this.getString(R.string.app_name))
//             .setContentText(messageBody)
//             .setAutoCancel(true)
//             .setSound(defaultSoundUri);

//     NotificationManager notificationManager =
//             (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

//     notificationManager.notify(0, notificationBuilder.build());
// }

 }
