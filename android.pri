    QT += androidextras
    SOURCES += src/service/pay/paybyandroid.cpp
    HEADERS +=src/service/pay/paybyandroid.h

equals(QT_VERSION, 5.12.2) {
DISTFILES += \
    android/old/AndroidManifest.xml \
    android/old/build.gradle \
    android/old/gradle/wrapper/gradle-wrapper.jar \
    android/old/gradle/wrapper/gradle-wrapper.properties \
    android/old/gradle.properties \
    android/old/gradlew \
    android/old/gradlew.bat \
    android/old/res/values/libs.xml \
    android/old/src/mob/ru/kapibaras/CPCard.java \
    android/old/src/mob/ru/kapibaras/CPCardType.java \
    android/old/src/mob/ru/kapibaras/PayGoogle.java \
    android/old/src/mob/ru/kapibaras/MyActivity.java


ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android/old
}
equals(QT_VERSION, 5.14.1) {
DISTFILES += \
    android/hight/AndroidManifest.xml \
    android/hight/build.gradle \
    android/hight/local.properties \
    android/hight/google-services.json\
    android/hight/settings.gradle\
    android/hight/gradle/wrapper/gradle-wrapper.jar \
    android/hight/gradle/wrapper/gradle-wrapper.properties \
    android/hight/gradle.properties \
    android/hight/gradlew \
    android/hight/gradlew.bat \
    android/hight/res/values/libs.xml \
    android/hight/src/mob/ru/kapibaras/CPCard.java \
    android/hight/src/mob/ru/kapibaras/CPCardType.java \
    android/hight/src/mob/ru/kapibaras/PayGoogle.java \
    android/hight/src/mob/ru/kapibaras/MyActivity.java


ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android/hight
}

DISTFILES += \
    $$PWD/android/hight/CameraService.java \
    $$PWD/android/hight/src/mob/ru/kapibaras/ListenerService.java \
    $$PWD/android/hight/src/mob/ru/kapibaras/MessageForwardingService.java



