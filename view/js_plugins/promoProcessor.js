let promoProcessor = {
  cart: null,
  price: null,
  coupon: null,


  calculateBonus(orderType,coupon, cart, price){
    this.cart = cart;
    this.price = price;
    this.coupon = coupon;

        // для совместимости с сайтом
    //this.coupon.homedelivery =this.coupon.homedilivery;
    //this.coupon.range = this.coupon.range;
    //this.coupon.platforms = this.coupon.data.platforms;



    if (!this.coupon) return;
    if (this.coupon.platforms&&!this.coupon.platforms.mob) {//Проверка платформы
      return {status: false, info: 'Только при заказе на сайте!'}
    }

    let ordertTypeStatus = checkDelivery(this.coupon.homedelivery, orderType);
    if (!ordertTypeStatus.status) {//Проверка типа заказа
      return {status: false, info:ordertTypeStatus.info }
    }

    //Получаю промежуток
    let range = this.coupon.range.find(x=>(this.price >= parseFloat(x.from)) && (this.price <= parseFloat(x.to) || parseFloat(x.to) == 0));    
    if (range) {
      if (range.bonus_type.length == 0) return {status: true}

        let result = {
          status: false,
          bonus: [],
          discount: null
        }

      for (let ti = 0; ti < range.bonus_type.length; ti++) {

        let rangeResult = calculateRange(this.cart, range.bonus_type[ti]);
        switch(rangeResult.type) {
          case 'persent':
            result.status = true;
            result.discount = {
              type: rangeResult.type,
              val: rangeResult.discount
            }
            break

          case 'goods':
            result.status = true;
            for (let i = 0; i < rangeResult.bonus.length; i++) {
              rangeResult.bonus[i].quantity = parseInt(rangeResult.bonus[i].number); 
              result.bonus.push(rangeResult.bonus[i]);
            }
            break

          case 'fixed':
            result.status = true;
            result.discount = {
              type: rangeResult.type,
              val: rangeResult.discount
            }
            break

          case 'fixedone':
            result.status = true;
            for (let i = 0; i < rangeResult.bonus.length; i++) {
              //rangeResult.bonus[i].quantity = parseInt(rangeResult.bonus[i].number); 
              result.bonus.push(rangeResult.bonus[i]);
            }
            break
        }        
      }
        return result;
    }
    return {status: false};
    
  }
}
function checkDelivery(couponOrderType, orderType) {
  if (couponOrderType == 2) return {status: true};
  let textResult = [
    'Только на самовывоз!','Только на доставку!'
  ]
  if (couponOrderType == orderType) {
    return {status: true};
  }else{
    return {status: false, info: textResult[couponOrderType]}
  }
}
function checkOptions(cart, arr, total) {
  if (arr.length == 0) {
    return true;
  }

  let numberCheck = 0;
  for (let i = 0; i < cart.length; i++) {
    let cartItem = cart[i];
    let optionsCheck = arr.find(x=>x.product.id == cartItem.id && 
      (x.number == cartItem.quantity || x.number == 0));
    if (optionsCheck) {
      numberCheck += cartItem.quantity;
    }
  }
  if (numberCheck >= total) return true;  
  return false;
}


function calculateRange(cart, range) {
  /*number: 0 => Общее количество
  options: Array(1) => Массив с продуктами, которые должны быть в корзине и их количество
  value: 0 => Бонус в числовом выражении
  values: Array(1) => Бонус продуктом*/
  
  let couponConditions = range.data;
  let bonusDiscount = couponConditions.value;
  let discountType = range.type_string;
  let bonusProducts = couponConditions.values;

  if (discountType == 'fixedone') {
      bonusProducts = [];            
      for (let i = 0; i < couponConditions.options.length; i++) {
        let item  = JSON.parse(JSON.stringify(couponConditions.options[i]));
        item.product.price =  item.product.price - bonusDiscount;
        bonusProducts.push(item);
      }
  }

  let checkTotalCount =  getCartNumber(cart) >= couponConditions.number;

  let optionsStatus = couponConditions.options.length ? checkOptions(cart, couponConditions.options, couponConditions.number): true;
  if (optionsStatus && checkTotalCount || discountType == 'fixedone'){
    return {type: discountType, discount: bonusDiscount, bonus: bonusProducts}    
  }
  return {status: false}
}

function getCartNumber(cart) {
  let res = 0;
  cart.forEach(function(item){
    res += item.quantity;
  })
  return res;
}

