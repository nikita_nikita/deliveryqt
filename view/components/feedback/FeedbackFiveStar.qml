import QtQuick 2.0
Flickable{
    width: parent.width
    contentWidth: width
    contentHeight: two_kapibara_image.height +60
    height: two_kapibara_image.height


        Image {
            anchors.topMargin: 60
            id:two_kapibara_image
            source: "qrc:/view/img/Kapibara_-02.png"
            // source: feedbackController.imageSource
            width: parent.width
            fillMode: Image.PreserveAspectFit
        }
        Rectangle{
            width: 200
            height: 60
            radius: 30
            anchors.top: two_kapibara_image.bottom
            color: "#f9c325"
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                font.pointSize: 30
                anchors.centerIn: parent
                text: "Отправить"
                color: "white"
            }
            MouseArea{
                anchors.fill: parent
                onClicked: feedbackController.sendFeedback()
            }
        }

}
