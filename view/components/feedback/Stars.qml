import QtQuick 2.0
import "../base"
Row{
    spacing: root_compopnent.width/12
    anchors.left: parent.left
    anchors.leftMargin: root_compopnent.width/12
    //            anchors.fill: stars_image
    width:root_component.width
    Repeater{
        model: 5
        KImage {
            width: root_compopnent.width/10
            height: width
            opacity: index > feedbackController.stars ?0.2:1
            source: "qrc:/view/img/star_2b50.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(index === feedbackController.stars){
                        return;
                    }
                    feedbackController.stars = index}
            }
        }


    }
}
