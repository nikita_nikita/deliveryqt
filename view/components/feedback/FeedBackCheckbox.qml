import QtQuick 2.0

Rectangle {
    id:root_component
    property string name: ""
    property bool active: false

    radius: 11
    border.color:root_component.active?"#f7611b" : "#f9c325"
    border.width: 2
    width: textChildren.contentWidth+20
    height: 40
    Text {
        id:textChildren
        color:root_component.active?"#f7611b" : "#f9c325"
        anchors.centerIn: parent
        text: root_component.name
        font.pointSize: 20
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {

            active = !active

        }

    }
}
