import QtQuick 2.0
//import "../feedback"
import "../base"
Grid{
    columns: 3
    OpenGalery{}

    Repeater{
        model: feedbackController.imageList
        Rectangle{
            height: 120
            width: 120
            radius: 5

            Image{
                Component.onCompleted: console.log(source)
                anchors.fill: parent
                anchors.margins: 10
                fillMode: Image.PreserveAspectCrop
                source: modelData
            }
            ButtonClose{
//                maincolor: "black"
                anchors.right: parent.right
                anchors.top: parent.top
                onClicked: feedbackController.removeImageUrl(index)
            }
        }
    }
}
