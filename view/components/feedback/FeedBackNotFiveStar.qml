import QtQuick 2.0
import "../base"

Flickable{
    width: parent.width
    height: parent.height-100
    contentWidth: width
    contentHeight: colonka.height
    clip: true
    property bool clear: feedbackController.feedBackthankMessage
    onClearChanged: {

        first.active = false
        second.active = false
        third.active = false
        four.active = false
        five.active = false
        six.active = false
        comment.text = ""
        feedbackController.stars = 4
        feedbackController.clearPhotoList();
    }
    Column{
        id:colonka
        spacing: 25
        width: parent.width
        KText {
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            text: "Что не понравилось?"
            height: 30
        }
        Flow{
            id:hotButtonList
            width: root_component.width-60
            anchors.left: parent.left
            anchors.leftMargin: 30
            spacing: 20

            FeedBackCheckbox{
                id:first
                active: feedbackController.rollRaz
                onActiveChanged: feedbackController.rollRaz = active

                name: "Роллы разваливаются"}
            FeedBackCheckbox{
                id:second
                active: feedbackController.neVkus

                onActiveChanged: feedbackController.neVkus = active
                name: "Невкусно"}
            FeedBackCheckbox{
                id:third
                active: feedbackController.zabSous
                onActiveChanged: feedbackController.zabSous = active

                name: "Забыли соус"}
            FeedBackCheckbox{
                active: feedbackController.opozdali
                onActiveChanged: feedbackController.opozdali = active

                id:four
                name: "Опоздали"}
            FeedBackCheckbox{
                active: feedbackController.errorInOrder
                onActiveChanged: feedbackController.errorInOrder = active

                id:five
                name: "Ошибка в заказе"}
            FeedBackCheckbox{
                id:six
                onActiveChanged: feedbackController.courierMudak = active

                active: feedbackController.courierMudak

                name: "Грубый курьер"}


        }



        KTextField{

            id:comment

            placeholderText: "Коментарий..."
            onTextChanged: feedbackController.comment = text
            wrapMode: Text.Wrap
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin:  parent.width*0.05
            anchors.rightMargin: parent.width*0.05



        }
        PhotoGrid{width: parent.width;}

        Rectangle{
            width: parent.width*0.9
            height: 60
            radius: 12
            id:sendbutton
            color: "#f9c325"
            anchors.left: parent.left
            anchors.leftMargin:  parent.width*0.05
            KText {
                font.pointSize: 30
                anchors.centerIn: parent
                text: "Отправить"
                color: "white"
            }
            MouseArea{
                anchors.fill: parent
                onClicked:
                {
                    feedbackController.sendFeedback()

                }
            }

        }
        Rectangle{

            height:25
        }

    }




}
