import QtQuick 2.0
import QtQuick.Layouts 1.12
import "../base"
Item {
    id: root_component
    Rectangle {
        anchors.fill: parent
        color: "#000"
        opacity: 0.5
        MouseArea {
            anchors.fill: parent
            onClicked: root_component.visible=false
        }
    }

    Rectangle {
        width: timeColumn.width+40
       anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 60
        radius: 10

        height: timeColumn.height+40
        Image {
            anchors.right: parent.right
          anchors.top: parent.top
            anchors.rightMargin:  8
            anchors.topMargin: 6
            width: 10
            height: 10
           fillMode: Image.PreserveAspectFit

            source: "qrc:/view/img/close.png"
        }
        Column {
            id: timeColumn
            width: table_header.width
           anchors.centerIn: parent
            spacing: 10
            Row {
                id: table_header
                KText {
                    text: ""
                    width: 110
                }
                KText{
                     text: "Самовывоз"
                     width: 105
                     font.bold: true
                     font.pixelSize: 14
                }
                KText{
                     text: "Доставка"
                      width:80
                      font.bold: true
                      font.pixelSize: 14
                }

            }
            Repeater {
                model: worktime.model
                Rectangle {
                   width: timeColumn.width
                   height: 25
                   Rectangle {
                      anchors.fill: parent
                       visible: isCurrent
                       color: "#542A69"
                   }
                   Row {

                       anchors.centerIn: parent
                       property string text_color:  isCurrent?"#fff":"#000"
                       KText {
                           text: day
                           width: 100
                           font.pixelSize: 14
                           color: parent.text_color
                       }
                       KText{
                            text: pickupStart+"-"+pickupEnd
                            width: 100
                            color: parent.text_color
                            font.pixelSize: 14
                       }
                       KText{
                            text: deliveryStart+"-"+deliveryEnd
                            color: parent.text_color
                            font.pixelSize: 14
                       }

                   }
                }
            }

        }
    }

}
