import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Column{
    id: root_component
    property string title: "Удвоить мясо"
    property string text: "Лосось и креветки 40г"
    property string cost:"100 руб"
    property bool fullWidth: true

    property int minW: button_ok.width+button_label.contentWidth+product_cost.contentWidth
    //property bool fullWidth: true
    signal clicked();
    property bool checked: false

    KText {
        text: root_component.title
        color: "#542A69"
        font.pointSize: 16
    }
    RowLayout {
        id: row_l
        spacing: 13
        height: 70
        width: root_component.width
        RoundButton {
            id: button_ok
            Layout.alignment: Qt.AlignVCenter
            height: 33
            width: 33
            icon.source: root_component.checked?"qrc:/view/img/checked_black.png":"qrc:/view/img/plus_black.png"
            onClicked: root_component.checked = !root_component.checked;
        }
        KText {
            id: button_label
            text: root_component.text
            color: "#878787"
            font.pointSize: 14
            horizontalAlignment: Text.AlignLeft
        }
        Item{
            height: 10
            Layout.fillWidth: root_component.fullWidth
           // color: "red"
        }

        KText {
            id: product_cost
            text:  root_component.cost
            font.pointSize: 14
            color: "#542A69"
            horizontalAlignment: Text.AlignRight

        }
    }

}
