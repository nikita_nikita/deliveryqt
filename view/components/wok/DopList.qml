import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
GridLayout{
    id: root_component
    Layout.fillWidth: true
    columns: 4
    property alias model: dop_repeater.model
    property int line_height: 57
    Repeater {
        id: dop_repeater
        KText {
            id: product_name
           // text: modelData.product.name
            text: "eete"
            font.pointSize: 14
            height: root_component.line_height
        }
        KText {
            id: product_cost
           text:  modelData.product.sity_info.price+" руб"
           font.pointSize: 14
           opacity: 0.5
           Layout.fillWidth: true
        }
        KSpinBox {
            value: 0
        }
        KText {
            id: total_cost
            text: "196 руб"
            font.pointSize: 16

        }
    }

}
