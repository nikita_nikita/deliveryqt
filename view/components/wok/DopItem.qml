import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Item {
    id: root_component
    property var model: ({})
    property real cost: quantity_toping.value*root_component.model.product.sity_info.price
    property alias value: quantity_toping.value
    height: 57
    RowLayout {
        id: row_l
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        KText {
            id: product_name
            text: root_component.model.product.name
            font.pointSize: 12
            Layout.preferredWidth: 140
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignVCenter

        }
        KText {
            id: product_cost
           text:  root_component.model.product.sity_info.price+ " руб"
           font.pointSize: 12
           opacity: 0.5
           Layout.fillWidth: true
           Layout.alignment: Qt.AlignVCenter

        }
        KSpinBox {
            id: quantity_toping
            value: 0
            min:0
            max:99

            Layout.alignment: Qt.AlignVCenter
        }
        KText {
            id: total_cost
            text: root_component.cost+" руб"
            font.pointSize: 12
            Layout.alignment: Qt.AlignVCenter

        }

    }
   HorisontalSeparator {
       anchors.left: parent.left
       anchors.right: parent.right
       anchors.bottom: parent.bottom
   }


}
