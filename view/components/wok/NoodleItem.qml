import QtQuick 2.12
import "../base"
Column {
    id: root_component
    property var model:({})
    property alias checked: radio_button.checked
    KRadioButton {
        id: radio_button
        text: root_component.model.product.name
        font.pointSize: 14
       anchors.horizontalCenter: parent.horizontalCenter
    }
    KImage {
        fillMode: Image.PreserveAspectFit      
        width: 150
        anchors.horizontalCenter: parent.horizontalCenter
        imagefile: root_component.model.product.images[0]

    }

}
