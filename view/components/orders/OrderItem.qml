import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
import "../../components"
ColumnLayout {
    id: root_component
    spacing: 10
    property var  order_products: ""
    property var order_id: 0
    property var date: ""
    property var order_status: ""
    property bool canFeedBack: false
    property bool deliveryType: false
    property string pointStr: ""
    Component.onCompleted: console.log("id == " +order_id +""+ deliveryType)
    onOrder_statusChanged: {
        console.log(order_status)
        if(deliveryType){
            progressbar.checkStatus()
        }else{
            pickUpProgressBar.checkStatus()
        }
    }
    onDeliveryTypeChanged: console.log("motherFucker")


    signal appendToCard();
    signal feedback()
    RowLayout {
        Layout.fillWidth: true
        KText {
            Layout.fillWidth: true
            text: "Заказ №"+root_component.order_id
            color: "#542A69";
            font.pointSize: 20
        }
        KText {

            text: root_component.date
            font.pointSize: 19
            color: "#B0B0B0";
        }

    }
    KText {
        Layout.fillWidth: true
        text: order_products
        wrapMode: Text.Wrap
        font.pointSize: 19
        font.weight: Font.ExtraLight
    }


    ProgressBar{
        id:progressbar
        visible: deliveryType && feedbackController.status_order
        //        visible: false
        Layout.alignment: Qt.AlignCenter
        width: parent.width*0.9
        function checkStatus() {
            if(root_component.order_status === "Неподтвержден"|| order_status === "Подтвердить"||root_component.order_status === "Закрыта"||root_component.order_status === "Отменена"){
                visible = false

            }
            if(root_component.order_status === "Ожидает"|| order_status === "Пора готовить"||order_status === "Новая"){
                satusInt = 1
                visible = true

            }
            if(root_component.order_status === "Готовится"){
                satusInt = 2
                visible = true

            }
            if(root_component.order_status === "Приготовлен"|| order_status === "Ждет отправки"|| order_status === "Готово"){
                satusInt = 3
                visible = true

            }
            if(root_component.order_status === "Отправлен"||root_component.order_status === "В пути"){
                satusInt = 4
                visible = true

            }
            if(root_component.order_status === "Доставлена"){
                satusInt = 5
                visible = true

            }

        }
    }
    ProgressBarPickUp{
        id:pickUpProgressBar
        visible: !deliveryType && feedbackController.status_order
//                visible: true
        Layout.alignment: Qt.AlignCenter
        width: parent.width*0.9
        takeAwayString: root_component.pointStr
        function checkStatus() {
            console.log("checkStatus");
            if(root_component.order_status === "Неподтвержден"|| order_status === "Подтвердить" ){
                visible = false
                satusInt = 0
                return
            }
            if(root_component.order_status === "Закрыта"||root_component.order_status === "Отменена"){
                visible = false
                return
            }
            visible = true
            if(root_component.order_status === "Ожидает"|| order_status === "Пора готовить"||order_status === "Новая"){
                satusInt = 1
                visible = true
            }
            if(root_component.order_status === "Готовится"){
                satusInt = 2
                visible = true
            }
            if(root_component.order_status === "Приготовлен"|| order_status === "Ждет отправки"|| order_status === "Готово"){
                satusInt = 3
            }
            if(root_component.order_status === "Отправлен"){
                satusInt = 4
                visible = true
            }
            if(root_component.order_status === "Доставлен"){
                satusInt = 5
                visible = true
            }

        }
    }
    Row{
        Layout.alignment: Qt.AlignCenter
        spacing: 20
        ButtonYollow{
            font.pointSize: 15
            width: 150
            height: 35
            Layout.topMargin: 10
            text: "Повторить  заказ"
            onClicked: root_component.appendToCard();
        }
        ButtonYollow{
            visible:root_component.canFeedBack && feedbackController.feedBackButtonVisible
            width: 85
            height: 35
            font.pointSize: 15
            Layout.topMargin: 10
            text: "Оценить"
            coloric: "purple"
            text_coloric: "white"
            onClicked: {
                feedbackController.order_number =  root_component.order_id
                root_component.feedback();
            }
        }
    }
    Rectangle{
        width: parent.width*0.6
        Layout.alignment: Qt.AlignCenter
        height: 1
        color: "gray"
    }
}
