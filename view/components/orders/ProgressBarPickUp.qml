import QtQuick 2.0
import "../../components/base"
Rectangle {
    height: 40
    property int satusInt: 0
    property int poloskaheight: 10
    property int textSize: 10
    property int  marginTop: 10
    property string takeAwayString: ""
    onSatusIntChanged: image1.updateImage()
    Rectangle{
        id:priniyat
        anchors.top: parent.top
        //        anchors.topMargin: 20
//        color: satusInt == 1 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/3
        height: poloskaheight
        radius: 10

    }
    KText {
        anchors.top: priniyat.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: priniyat.horizontalCenter
        text: "Ждёт<br>приготовления"
        width: priniyat.width
        height: 15
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: textSize
    }

    Rectangle{
        id:gotovitsia
        anchors.top: parent.top
        anchors.left: priniyat.right
        //        anchors.topMargin: 20
//        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/3
        height: poloskaheight
        radius: 10

    }

    KText {
        anchors.top: priniyat.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: gotovitsia.horizontalCenter
        text: "Готовится"
        width: priniyat.width
        height: 15
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: textSize
    }

    Rectangle{
        id:done
        anchors.top: parent.top
        anchors.left: gotovitsia.right
        //        anchors.topMargin: 20
//        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/3
        height: poloskaheight
        radius: 10

    }

    KText {
        anchors.top: done.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: done.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: satusInt !== 3 ?"Готов":"Ожидает вас на <br>"+takeAwayString
        width: priniyat.width
        height: 15
        font.pointSize: textSize
    }



    Rectangle{
    width: parent.width
    height: poloskaheight
    color: "#6b3d7e"
    radius: 5
    }


    Image {
        id: image1
        source: "qrc:/view/img/logo/logo_1.png"
        anchors.top: parent.top
        anchors.topMargin: -11
        function updateImage() {
        if(satusInt ===0){
            visible = false
            return
        }
        visible = true
        if(satusInt ===1){
         anchors.horizontalCenter =priniyat.horizontalCenter
        }
        if(satusInt ===2){
         anchors.horizontalCenter =gotovitsia.horizontalCenter
        }
        if(satusInt ===3){
         anchors.horizontalCenter =done.horizontalCenter
        }
        }

        width: 24
        fillMode: Image.PreserveAspectFit
    }

}
