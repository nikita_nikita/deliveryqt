import QtQuick 2.12
import "../base"

Item {
    Column {
        anchors.centerIn: parent
        spacing: 20
        Image{
            source: "qrc:/view/img/logo/bw.png"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText{
            text: "Вы ещё не заказывали"
            font.pixelSize: 16
            color: "#A8A8A8"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        ButtonYollow{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Вернуться в меню"
            onClicked: router.popToMain();
        }
    }
}
