import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../base"
import "../menu"
import "../offers"

Drawer {
    id: root_component
    height: 300
    Connections{
        target: menu
        onShowInfoComposite:{
            root_component.open()
            productName= name
            flix_products.contentY=0
        }
    }
    property string productName: ""



    background: Rectangle {
        radius: 10
//        KImage {
//            width: column.width
//            fillMode: Image.PreserveAspectFit
//            imagefile: menu.item_icon
//        }
        Image {
            id: arrow_button
            fillMode: Image.PreserveAspectFit
            width: 30
            source: "qrc:/view/img/arrow/down.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 10
            MouseArea {
                anchors.fill: parent
                onClicked: root_component.close()
            }
        }
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 10
            color: "#fff"
        }
        KText {
            anchors.top: arrow_button.bottom
            anchors.left: parent.left
            anchors.leftMargin: 10

            text: "Состав "+productName
            color: "#542A69";
             font.pointSize: 14
        }
    }

    Flickable {
        id: flix_products

        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        contentHeight: composite_column.height
        clip: true
        Column{
            id: composite_column
            spacing: 20
            width: flix_products.width

            Repeater {
                model: menu.composite
                Item{
                    id: product_info
                    width: composite_column.width-20
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: product_image.height>product_text.height?product_image.height:product_text.height

                   KImage{
                        id: product_image
                        imagefile: image
                        anchors.top: parent.top
                        anchors.topMargin: parent.height/2-product_image.height/2
                       fillMode:Image.PreserveAspectFit
                        width: 100

                    }
                   Image {
                       source: "qrc:/view/img/product/hot.png"
                       visible: hot
                       fillMode: Image.PreserveAspectFit
                       width: 20
                   }
                    Column{
                        id: product_text
                        anchors.left: product_image.right

                        anchors.right: parent.right
                        anchors.leftMargin: 10

                        KText{
                            text: name
                            font.bold: true
                            wrapMode: Text.WordWrap
                            width: parent.width
                        }
                        KText{
                            text: composition
                            wrapMode: Text.WordWrap
                            width: parent.width
                        }
                    }
                }

            }

        }


    }



}
