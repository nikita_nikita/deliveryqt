import QtQuick 2.12
import "../base"

Item {
    id: root_component
    property string text:""
    property string notifation: ""
    signal clicked()
    height: 50
    KText{
        id: title
        font.pixelSize: 16
        color: "#fff"
        text: root_component.text
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.verticalCenter: parent.verticalCenter
    }
    Rectangle {
        anchors.left: title.right
        anchors.leftMargin: 10
        color: "#FFC605"
        radius: 10
        height: 20
        width: notifation.width+16
        anchors.verticalCenter: parent.verticalCenter
        visible: root_component.notifation.length>0
        KText {
            id: notifation
            text: root_component.notifation
            anchors.centerIn: parent
            color: "#000"
            font.pixelSize: 9
        }
    }
    MouseArea{
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: root_component.clicked()
    }

    DHorisontalSeparator{
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }

}
