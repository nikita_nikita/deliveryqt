import QtQuick 2.12
import "../base"
Item {
    id: root_component
    height: 68
    opacity: 1
    signal clickedClose()
    signal clickedLogo();
    Image {
        width: 43
        height: 43
        fillMode: Image.PreserveAspectFit
        source: "qrc:/view/img/logo_mini.png"
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: root_component.clickedLogo()
        }
    }
    ButtonClose{
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        onClicked: root_component.clickedClose()
    }

}
