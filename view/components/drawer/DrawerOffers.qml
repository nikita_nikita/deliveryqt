import QtQuick 2.12
import QtQuick.Controls 2.12
import "../base"
import "../menu"
import "../offers"

Drawer {
    id: root_component
    height: {
         var h = swipe_view_offers.height+50
        if(h<layout.height) return h
        else return layout.height
    }
    function openPosition( pos){
        swipe_view_offers.currentIndex = pos;
        root_component.open()
    }

    background: Rectangle {
        radius: 10      
        Image {
            fillMode: Image.PreserveAspectFit
            width: 30
            source: "qrc:/view/img/arrow/down.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 10
            MouseArea {
                anchors.fill: parent
                onClicked: root_component.close()
            }
        }
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 10
            color: "#fff"
        }
    }

    Flickable {
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        contentHeight: swipe_view_offers.height+100
        clip: true

        KSwipeView {
            id: swipe_view_offers
            //height: 200
            interactive: false
            width: parent.width
            spacing: 10
            clip: false           
            onCurrentIndexChanged: {
                menu.currentOffers = currentIndex
            }


            Repeater{
                id: carusel_repeater
                model: storage.offersBaner

                OfferItem {
                    id: offer
                    image: imageCatalog
                    offer_name: name
                    offer_description: description
                    offer_promocode:  promocode
                    onClickedPromocode: {
                        cart.promocode = promocode
                        global_tooltip.text = "Промокод применен"
                        global_tooltip.visible = true
                    }


                }

            }

        }
    }



}
