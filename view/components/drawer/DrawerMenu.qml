import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../base"
Drawer {
    id: root_component

    background:Item{
        Rectangle{
            color: "#542A69"
            opacity: 0.8
            anchors.fill: parent
        }
    }

    DHeader{
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.leftMargin: 20
        onClickedClose: root_component.close()
        onClickedLogo: {
            router.popToMain()
            root_component.close()
        }
    }

    DHorisontalSeparator{
        id:top_separator
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: header.bottom
    }
    Flickable {
         anchors.top:top_separator.bottom
         anchors.bottom: parent.bottom
         anchors.left: parent.left
         anchors.right: parent.right
         contentHeight: colum_menu.height
         clip: true
         Column{
               id: colum_menu
               anchors.left: parent.left
               anchors.right: parent.right
               spacing: 1
                DMenuItem{
                    text: "Акции"
                    width: colum_menu.width
                    onClicked: {
                        router.push("offers")
                        root_component.close()
                    }
                }
                DMenuItem{
                    text: "Корзина"
                    notifation: cart.model.number
                    width: colum_menu.width
                    onClicked: {
                        router.push("cart")
                        root_component.close()
                    }
                }
                DMenuItem{
                    text: "Город"
                    notifation: storage.userCity.name?storage.userCity.name:""
                    width: colum_menu.width
                    onClicked: {
                        router.push("citys")
                        root_component.close()
                    }
                }
                DMenuItem{
                    text: "Заказы"
                    width: colum_menu.width
                    onClicked: {
                        router.push("orders")
                        root_component.close()
                    }
                }
                DMenuItem{
                    text: "Контакты"
                    width: colum_menu.width
                    onClicked: {
                        router.push("contacts")
                        root_component.close()
                    }
                }
                DMenuItem{
                    text: "Выйти"
                    width: colum_menu.width
                    onClicked: {
                        //root_component.close()
                        Qt.quit()
                    }
                }

         }


    }


}
