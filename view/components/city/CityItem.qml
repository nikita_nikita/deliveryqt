import QtQuick 2.12
import "../base"
Item {
    id: root_component
    height: 30
    width: text.width
    signal clicked()
    property string cityName: ""
    KText {
        id: text
        text: root_component.cityName
        anchors.verticalCenter: parent.verticalCenter

    }
    MouseArea{
        anchors.fill: parent
        onClicked: root_component.clicked();
    }

}
