import QtQuick 2.12
import QtQuick.Controls 2.12
import "../base"
Item {
    id: root_component
    property string country: "Россия"
    width: 180
    clip: true
    //height: column.height
    signal citySelect(var city)
    property string domainCode: "RU"


    KText {
        id: title
        text: root_component.country
        font.weight: Font.Bold
        font.pixelSize: 14
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 15
    }
    Flickable{
        contentHeight: column.height+150
        anchors.top: title.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        clip: true
        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOff
        }
        Column{
            id: column
            spacing: 10

            Repeater {
                model: storage.cityModel
                CityItem {
                    cityName: name
                    visible: domainCode==root_component.domainCode
                    onClicked: root_component.citySelect(json)

                }
            }

        }
    }





}




