import QtQuick 2.12
//import QtQuick.Layouts 1.12
import "../base"

Column {
    id: root_component

    property string image: ""
    property alias offer_name: offer_name.text
    property alias offer_description: offer_description.text
    property string offer_promocode: ""

    signal clickedPromocode();
    spacing: 5    
    KImage{
        id: offer_image
        width: root_component.width
        fillMode: Image.PreserveAspectFit
        imagefile: root_component.image;
    }

    KText {
        id: offer_name
        color: "#542A69"
        font.pixelSize: 14
        width: root_component.width        
        topPadding: 5

    }
    KText {
        id: offer_description
        color: "#000"
        font.pixelSize: 12
        wrapMode: Text.WordWrap
        width: root_component.width    
    }
    ButtonViolet {
        id: offer_promocode_button
        //topPadding: 10
        width: root_component.width
        text: "Промокод "+ root_component.offer_promocode
        onClicked: root_component.clickedPromocode();
        visible: root_component.offer_promocode? true: false
    }
}
