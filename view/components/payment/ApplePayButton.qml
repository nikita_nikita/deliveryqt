import QtQuick 2.12

Rectangle {
    id: control
    signal clicked();
    Image {
        id: image_pay
        source: "qrc:/view/img/apple/apple_pay.png"
      anchors.fill:  parent
        mipmap: true
        fillMode: Image.PreserveAspectFit
    }
    MouseArea {
        anchors.fill:image_pay
        onClicked: control.clicked()
    }
}
