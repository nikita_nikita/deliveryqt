import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Column {
    id: root_component
    spacing: 35
    onVisibleChanged: {
        if(visible){
           cart.userEmail= storage.user.client_info? storage.user.client_info.email:""
        }
    }

    KTextField{
        id: cardNumber
        width: root_component.width>290?310:290
        anchors.horizontalCenter: parent.horizontalCenter        
        placeholderText: "Номер карты"
        inputMethodHints: Qt.ImhDigitsOnly
        onFocusChanged: {
            if(focus){
                cursorPosition=0
                inputMask =  "#### #### #### #### ###"
            }
            else {
                  cart.cardNumber = text
            }
        }
        onTextChanged: {

            error_text.visible = false
        }
        Keys.onEnterPressed: cardExpDate.focus = true
        Keys.onReturnPressed: cardExpDate.focus = true
    }
    RowLayout {
        spacing: 30
        width: root_component.width>290?310:290
        anchors.horizontalCenter: parent.horizontalCenter
        KTextField {
            id: cardExpDate
            Layout.preferredWidth:  160
            //inputMask: "##/##"
            placeholderText: "Срок действия"
            inputMethodHints: Qt.ImhDigitsOnly
            onFocusChanged: {
                if(focus){
                    cursorPosition=0
                    inputMask =  "##/##"
                }
                 else {
                     cart.cardDate = text
                }
            }
            onTextChanged:
            {
               if(focus) {
                error_text.visible = false
               }
            }
            Keys.onEnterPressed: cardCvc.focus = true
            Keys.onReturnPressed: cardCvc.focus = true
        }
        KTextField {
            id: cardCvc
            Layout.preferredWidth: 100
            //inputMask: "###"
            placeholderText: "CVC"
            inputMethodHints: Qt.ImhDigitsOnly
            onFocusChanged: {
                if(focus){
                    cursorPosition=0
                     inputMask =  "###"
                } else {
                   // cart.cardCVC = text
                }

            }
            onTextChanged:
            {
                if(focus) {
                    error_text.visible = false
                }
                 cart.cardCVC = text
            }
        }

    }

    KTextField{
        id: cartEmail
        width: root_component.width>290?310:290
        background: Rectangle{
            border.color: cartEmail.text.length !== 0?"red":"red"
            border.width: cartEmail.text.length !== 0&&paymentController.validEmail?0:2
        }
        placeholderText: "Email для чека"
        anchors.horizontalCenter: parent.horizontalCenter
        text: cart.userEmail
        valid: paymentController.validEmail
        onFocusChanged: {
            if(focus){
                cursorPosition=0
            }            
        }
        onTextChanged: {
            cart.userEmail = text
            error_text.visible = false
        }
        KText {
            id: error_email_text
            color: "red"
            text: "Введите email"
            visible: !paymentController.validEmail
           width: parent.width
            anchors.top: parent.bottom

        }

    }
    KText {
        id: error_text
        color: "red"
        text: "error"
        width: cardNumber.width
        wrapMode: Text.WordWrap
        anchors.horizontalCenter: parent.horizontalCenter
        visible:  false
        Connections{
            target: payService
            onError: {
                error_text.text = error
               error_text.visible = true
            }
        }
    }

    KText {
        width: root_component.width>290?310:290
        anchors.horizontalCenter: parent.horizontalCenter
        wrapMode: Text.WordWrap
        font.pointSize: 10
        opacity: 0.2
        text: "Платеж безопасен. Мы не храним данные карт, все операции проводит CloudPayments платежная система, сертифицированная по международному стандарту безопасности PCI DSS"
    }




}
