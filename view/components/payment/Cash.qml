import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Column {
    anchors.top: parent.top
    anchors.topMargin: 10
    spacing: 20


    KText {
            font.pixelSize: 14
            text: "С какой суммы подготовить сдачу?"
            font.weight: Font.Bold
            anchors.left: parent.left
            anchors.leftMargin: 20
            visible: cart.orderType!="take-away"

    }
    RowLayout {
        spacing: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        visible: cart.orderType!="take-away"
        KTextField {
            placeholderText: "Сумма в рублях"
            width: 178
            enabled:!noCash_checkbox.checked
            Layout.alignment: Qt.AlignVCenter
            inputMethodHints: Qt.ImhDigitsOnly
            onEditingFinished: cart.cash = parseFloat(text)
            onTextChanged:  cart.cash = this.text

        }
        KCheckBox {
            id: noCash_checkbox
            text: "Без сдачи"
            Layout.alignment: Qt.AlignVCenter
            checked: cart.noChange
            onCheckedChanged: {
                cart.noChange = checked
            }
        }
    }

}
