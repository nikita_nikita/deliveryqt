import QtQuick 2.12

Rectangle {
    id: control
    height: width/6
    border.width: 1
    radius: 5
    border.color: "#878787"
    signal clicked();
    Image {
        source: "qrc:/view/img/buy_with_googlepay_button_content.svg"
         sourceSize.height: parent.height*0.5
        anchors.centerIn: parent

    }
    MouseArea {
        anchors.fill: parent
        onPressed: {
            control.border.width = 2
        }
        onReleased:  control.border.width  =1
        onClicked: control.clicked()
    }
}
