import QtQuick 2.0
import "../components/base"
Rectangle {
    height: 40
    property int satusInt: 0
    property int poloskaheight: 10
    property int textSize: 10
    property int  marginTop: 10
    onSatusIntChanged:
        image1.updateImage()

    Rectangle{
        id:priniyat
        anchors.top: parent.top
        //        anchors.topMargin: 20
        color: satusInt == 1 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/5
        height: poloskaheight
        radius: 10

    }
    KText {
        anchors.top: priniyat.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: priniyat.horizontalCenter
        text: "Ждёт<br>приготовления"
        width: priniyat.width
        height: 15
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: textSize
    }

    Rectangle{
        id:gotovitsia
        anchors.top: parent.top
        anchors.left: priniyat.right
        //        anchors.topMargin: 20
        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/5
        height: poloskaheight
        radius: 10

    }

    KText {
        anchors.top: priniyat.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: gotovitsia.horizontalCenter
        text: "Готовится"
        width: priniyat.width
        height: 15
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: textSize
    }

    Rectangle{
        id:wait
        anchors.top: parent.top
        anchors.left: gotovitsia.right
        //        anchors.topMargin: 20
        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/5
        height: poloskaheight
        radius: 10

    }

    KText {
        anchors.top: wait.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: wait.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Ждет<br>отправки"
        width: priniyat.width
        height: 15
        font.pointSize: textSize
    }
    Rectangle{
        id:edet
        anchors.top: parent.top
        anchors.left: wait.right
        //        anchors.topMargin: 20
        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/5
        height: poloskaheight
        radius: 10
    }

    KText {
        anchors.top: edet.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: edet.horizontalCenter
        text: "В пути"
        horizontalAlignment:Text.AlignHCenter
        width: priniyat.width
        height: 15
        font.pointSize: textSize
    }

    Rectangle{
        id:done
        anchors.top: parent.top
        anchors.left: edet.right
        //        anchors.topMargin: 20
        color: satusInt == 2 ? "#6b3d7e":"#4d1a606e"
        width: parent.width/5
        height: poloskaheight
        radius: 10

    }

    KText {
        anchors.top: priniyat.bottom
        anchors.topMargin: marginTop
        anchors.horizontalCenter: done.horizontalCenter

        text: "Доставлен"
        horizontalAlignment:Text.AlignHCenter
        width: 50
        height: priniyat.width
        font.pointSize: textSize
    }
    Rectangle{
        width: parent.width
        height: poloskaheight
        color: "#6b3d7e"
        radius: 5
    }


    Image {
        id: image1
        source: "qrc:/view/img/logo/logo_1.png"
        anchors.top: parent.top
        anchors.topMargin: -11
        function updateImage()
        {
            console.log(root_component.order_status)
            if(satusInt ===0){
                visible = false
            }
            if(satusInt ===1){
                anchors.horizontalCenter =priniyat.horizontalCenter
            }
            if(satusInt ===2){
                anchors.horizontalCenter =gotovitsia.horizontalCenter
            }
            if(satusInt ===3){
                anchors.horizontalCenter =wait.horizontalCenter
            }
            if(satusInt ===4){
                anchors.horizontalCenter =edet.horizontalCenter
            }
            if(satusInt ===5){
                anchors.horizontalCenter =done.horizontalCenter
            }
        }

        width: 24
        fillMode: Image.PreserveAspectFit
    }

}
