import QtQuick 2.12
import QtQuick.Controls 2.1

TabButton {
    id: control
     font.family: "qrc:/view/fonts/Montserrat-Regular.ttf"
     font.pixelSize: 13
     font.weight: Font.Bold
    contentItem: Text {
        text: control.text
        font: control.font
        color: control.checked ? "#542A69" : "#000000"
        opacity: control.checked ? 1: 0.3
        horizontalAlignment: Text.AlignHCenter
        leftPadding: 10
        rightPadding: 10
    }
    background:  Rectangle {
        implicitHeight: 30
        //implicitWidth: 140
       // implicitWidth: control.contentItem.width+40
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 2
            color: "#542A69"
            visible: control.checked
        }

    }
}
