import QtQuick 2.0
import QtQuick.Layouts 1.12
Rectangle {
    id: root_component
   // color: "#fef9e6"
    color: "#542A69"
    height: row.height>45?row.height+20:50
    property alias text: textM.text
    signal closed();
    RowLayout {
        id: row
        anchors.left: parent.left
        anchors.leftMargin:10
        anchors.rightMargin: 10
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        KText{
            id: textM
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            color: "#fff"
        }
        /*Image {
           source: "qrc:/view/img/close.png"
           width: 10
           height: 10
           fillMode: Image.PreserveAspectFit
        }*/
        KText{
            text: "⊗"
            color: "#fff"
            font.pixelSize: 20

        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: root_component.closed()

    }




}
