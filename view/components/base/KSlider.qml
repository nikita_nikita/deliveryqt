import QtQuick 2.12
import QtQuick.Controls 2.5

Slider {
    id: control

    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 2
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: "#F2F2F2"

        Rectangle {
            width: control.visualPosition * parent.width
            anchors.verticalCenter: parent.verticalCenter
            height: 4
            color: "#FFC605"
            radius: 2
        }
    }

    handle: Rectangle {
           x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
           y: control.topPadding + control.availableHeight / 2 - height / 2

           implicitWidth: 26
           implicitHeight: 26
           radius: 13
           color: control.pressed ? "#FAC100" : "#FFC605"

       }

}
