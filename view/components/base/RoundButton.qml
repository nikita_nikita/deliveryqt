import QtQuick 2.12
import QtQuick.Controls 2.12

Button{
    id: control
    width: 24
    height: 24
    //property string : value
    icon.source: "qrc:/view/img/plus.png"

    contentItem:Item{

        Image{
            width: 12
            height: 12
            fillMode: Image.PreserveAspectFit
            source: control.icon.source
            anchors.centerIn: parent
        }
    }

    background: Rectangle{
        implicitHeight: control.height
        implicitWidth: control.width
        color: "#FFC605"
        radius: 20
        opacity: control.down?0.5:1
    }
}
