import QtQuick 2.12
import QtQuick.Controls 2.5

Item {
     id: root_component

    Rectangle {
        anchors.fill: parent
        color: "#000"
        opacity: 0.4
    }
    Item {
        anchors.fill: parent
        anchors.bottomMargin: 120
        BusyIndicator {
          anchors.centerIn: parent
          running: true
        }

    }
}
