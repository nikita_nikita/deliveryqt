import QtQuick 2.0
Rectangle{
    property string message: ""
    anchors.fill: parent
    visible: message.length !== 0
    Rectangle{
        width: 250
        height: 250
        anchors.centerIn: parent
        KText{
            text: message
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
        }

    }

}
