import QtQuick 2.12
import QtQuick.Controls 2.5


CheckBox {
    id: control
    //height: 18

    spacing: 5
    topPadding:0
    indicator: Rectangle {
        implicitHeight: 18
        implicitWidth: 18
        border.color: "#F2F2F2"
        border.width: 1
        radius: 3
        Image {
            width: 7
            height: 7
            anchors.centerIn: parent
            fillMode: Image.PreserveAspectFit
            source: "qrc:/view/img/checked.png"
            visible: control.checked
            x: control.leftPadding
            y: parent.height / 2 - height / 2

        }
    }

    contentItem: KText {
        font.pixelSize: 10
        color: "#000"
        text: control.text
        leftPadding: control.indicator.width + control.spacing
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter

    }
}
