import QtQuick 2.12

Image {
    id: root_component
    width: 10
    fillMode: Image.PreserveAspectFit
    source: "qrc:/view/img/pen.png"
    opacity: 0.3
}
