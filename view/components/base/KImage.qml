import QtQuick 2.12

Image {
    signal imageUrlupdated();
    property var imagefile: "";
    asynchronous: true
    source: {
        if(!imagefile) return "";
        if(kapibaras.fileExists(imagefile)){
            return "file:///"+storage.imagesPath+"/"+imagefile;
        } else {
            return kapibaras.urlImage?kapibaras.urlImage+imagefile:""
        }
    }

}
