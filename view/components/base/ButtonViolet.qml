import QtQuick 2.12
import QtQuick.Controls 2.12

Button{
    id: control
     font.family: "qrc:/view/fonts/Montserrat-Regular.ttf"
     font.pointSize: 17
    contentItem: Text {

         id: btn_text
              text: control.text
              font: control.font
              opacity: enabled ? 1.0 : 0.3
               color: "#fff"

              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              elide: Text.ElideRight
          }
    background: Rectangle {
        implicitWidth: btn_text.contentWidth+60
        implicitHeight: 40
        color: "#542A69"
        radius: 20

        opacity: control.down ? 0.8:1
    }
}
