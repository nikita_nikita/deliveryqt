import QtQuick 2.0

Rectangle {


    property string text :""
//    signal clicked

    height: text.height + 20;
    width: layout.width
    border.width: 1
    radius: 4
//    smooth: true

    ButtonClose{
        x:parent.width
        y:parent.height
    }



    Text {
        id: text
        anchors.centerIn:parent
        width: layout -40
        font.pointSize: 10
        text: parent.text
        wrapMode: Text.WordWrap

    }
}
