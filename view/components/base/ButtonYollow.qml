import QtQuick 2.12
import QtQuick.Controls 2.12

Button{
    id: control
    property color coloric: "#FFC605"
    property color text_coloric: "#000"

     font.family: "qrc:/view/fonts/Montserrat-Regular.ttf"
    contentItem: Text {
         id: btn_text
              text: control.text
              font: control.font
              opacity: enabled ? 1.0 : 0.3
               color: text_coloric
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              elide: Text.ElideRight
          }
    background: Rectangle {
        implicitWidth: btn_text.contentWidth+60
        implicitHeight: 40
        color: coloric
        radius: 20

        opacity: control.down ? 0.8:1
    }
}
