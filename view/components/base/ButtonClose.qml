import QtQuick 2.12
import QtQuick.Controls 2.12
ToolButton {

    height: 28
    width: 28
    property color maincolor: "#fff"
    contentItem:Item{
        Image{
            width: 10
            height: 10
            anchors.centerIn: parent
            fillMode: Image.PreserveAspectFit
            source: "qrc:/view/img/close.png"
        }
    }

    background: Rectangle{
        color: maincolor
        radius: 50
    }
}
