import QtQuick 2.12
import QtQuick.Controls 2.12


TextField {
    id: root_component
    property bool horisontalSeparator: true
    font.family: "qrc:/view/fonts/Montserrat-Medium.ttf"
    property bool valid: true
    inputMethodHints: Qt.ImhNoPredictiveText|Qt.ImhSensitiveData
    horizontalAlignment: Text.AlignLeft
   leftPadding:0
//   height: 30
   property string oldtext: ""
    background: Rectangle {
        HorisontalSeparator{
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            visible: root_component.horisontalSeparator
            color: root_component.focus?"#542A69":root_component.valid?"#F2F2F2":"red"
        }
    }
    function updateCursorPositon() {
        var pos = root_component.inputMask.indexOf("#")
        if (root_component.cursorPosition<=pos){
            root_component.cursorPosition = pos
            return
        }
        if (oldtext.length>=root_component.text.length){
            oldtext= root_component.text
            return
        }
        oldtext= root_component.text
        var cursor = calculateCursorPosition(root_component.inputMask, root_component.cursorPosition)
        root_component.cursorPosition = cursor
    }


    function calculateCursorPosition(mask, cursor){
          var symbol = mask[cursor]
          if (symbol==="#") {
                return cursor
          }
          var nc = mask.indexOf("#", cursor)
          if (nc===-1){
              return cursor
          }
          return nc
    }


}
