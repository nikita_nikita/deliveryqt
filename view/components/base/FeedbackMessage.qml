import QtQuick 2.0

Item {
    Rectangle{
        anchors.fill:parent
        MouseArea{
            anchors.fill:parent

        }
        color: "silver"
        opacity: 0.5
    }
    Rectangle{
        width: 250
        height: 200
        anchors.centerIn: parent
        color: "white"
        radius: 20


        Column{

            id:column
            Text{
                width:250
                height: 120


                horizontalAlignment: Text.AlignHCenter
                text: "<br>Здравствуйте,<br> " +profileController.userName + ". <br> Вам понравился<br>последний заказ?"
            }


                Rectangle{
                height: 1
                width: 250
                color: "gray"}
                Rectangle{
                    width: 250
                    height: 40
                    Text {
                        anchors.centerIn: parent
                        text: "Позже"
                        font.bold: true

                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: feedbackController.feedbackMessage = false
                    }

                }
                Rectangle{
                height: 1
                width: 250
                color: "gray"}
                Rectangle{
                    width: 250
                    height: 40
                    radius: 20
                    Text {
                        font.bold: true
                        anchors.centerIn: parent
                        text: "Оценить"
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            onClicked: feedbackController.feedbackMessage = false

                            router.push("orders")                    }
                    }
                }

            }
        }


}
