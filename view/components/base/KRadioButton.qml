import QtQuick 2.12
import QtQuick.Controls 2.5

RadioButton {
    id: control
    property int size: 10
    indicator: Rectangle {
        implicitHeight: 17
        implicitWidth: 17
        x: control.leftPadding
        radius: 9
        color: control.checked?"#FFC605":"#D2D2D2"
        y: parent.height / 2 - height / 2
        opacity: control.enabled?1:0.3
        Rectangle {
            anchors.centerIn: parent
            width: 9
            height: 9
            radius: 5
        }
    }
    contentItem: KText {
        text: control.text
        color: "#000"
        opacity: control.checked?1:0.3
        wrapMode: Text.WordWrap
        leftPadding: control.indicator.width + control.spacing
        font.pixelSize: size
        verticalAlignment: Text.AlignTop
    }


}
