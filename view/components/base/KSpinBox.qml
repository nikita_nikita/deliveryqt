import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
SpinBox {
    id: control
    value: 0
    editable: true
    font.pixelSize: 18    
    property int max: 0
    property int min: 0
    font.family: "qrc:/view/fonts/Montserrat-Regular.ttf"
    contentItem: Text {
        z:2
        font: control.font
        text: control.value
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.centerIn: parent
    }

    up.indicator: Rectangle {
        MouseArea{
        visible: max === value
        anchors.fill: parent
        }
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        implicitWidth: 22
        implicitHeight: 22
        Text {
            visible: max !== value
            text: "+"
            font.pixelSize: control.font.pixelSize*2
            color: "#542A69"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    down.indicator: Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        implicitWidth: 22
        implicitHeight: 22
        MouseArea{
            anchors.fill: parent
            visible: value === min
        }
        Text {
            visible: min !== value
            text: "-"
            font.pixelSize: control.font.pixelSize*2
            color: "#C4C4C4"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    background: Rectangle {
        implicitHeight: 22
        implicitWidth: 20
        //color: "red"
    }
}
