import QtQuick 2.12
import QtQuick.Controls 2.5
import "../base"
Item {
    id: component
    function open(model, index,  callback=null){
        worktime.updateModeles();

        tumbler.model= model
        resFunc=callback
        tumbler.currentIndex= index
        component.visible=true
    }

    Rectangle{
        anchors.fill: parent
        color: "#000"
        opacity: 0.5
        MouseArea{
            anchors.fill: parent
            onClicked: component.visible=false
        }
    }
    property var resFunc:function(){

    }

    Rectangle{
        anchors.top: parent.top
        anchors.topMargin: 70
        anchors.horizontalCenter: parent.horizontalCenter
        radius: 3
        width: 300
        height: 200
        MouseArea{
           anchors.fill: parent
        }

        Tumbler{
            id: tumbler
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            visibleItemCount: 3
            height: 160
            delegate: KText {
                      text: modelData
                      font.pixelSize: 18
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      opacity: 1.0 - Math.abs(Tumbler.displacement) / (tumbler.visibleItemCount / 2)
                  }
            Rectangle {
                     anchors.horizontalCenter: tumbler.horizontalCenter
                     y: tumbler.height * 0.4
                     width: 170
                     height: 2
                     color: "#542A69"
                 }

                 Rectangle {
                      anchors.horizontalCenter: tumbler.horizontalCenter
                     y: tumbler.height * 0.6
                     width: 170
                     height: 2
                     color: "#542A69"
                 }
        }
        Row{
           anchors.bottom: parent.bottom
           anchors.bottomMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 35
            spacing: 40
           ButtonSimple{
               text: "ОТМЕНА"
                onClicked: component.visible=false

           }

           ButtonSimple{
               text: "ОК"
               onClicked: {
                   if(component.resFunc){
                       component.resFunc(tumbler.currentIndex)
                   }
                   component.visible=false
               }

           }

        }



    }



}
