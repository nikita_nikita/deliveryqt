import QtQuick 2.12
import "../base"

Column {
    width: parent.width
//    spacing: 5
    //    visible: root_component.ready&&cart.cupon.coupon?true: false
    property bool status: cart.cuponInfo&&cart.cuponInfo.length>0

    Repeater{
        model:cart.cuponInfo
        id:repeater
        Column{
            width: parent.width
//            spacing: 5
            KText {

                text: modelData.type === 1? "Купон применён": "Условия не выполнены"
                font.pointSize: 15
                color: modelData.type === 1? "green":"red"
                wrapMode: Text.WordWrap
                width: parent.width
                visible:  modelData.type === 2 ? false : true

            }
            KText {

                text: modelData.name
                font.pointSize: 15
                color: modelData.type === 1? "green":"red"
                wrapMode: Text.WordWrap
                width: parent.width

            }
            KText {

                text: modelData.description
                color: modelData.type === 1? "green":"red"
                wrapMode: Text.WordWrap
                width: parent.width
            }
        }

    }
}



