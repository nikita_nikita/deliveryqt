import QtQuick 2.12
import "../base"

Row {
    id: root_component
    spacing: 20
    property alias value: slider.value
    property real stepSize: storage.cityData.config?storage.cityData.config.bonus_step:1
    property real bonus_limit:   storage.cityData.config? storage.cityData.config.bonus_limit:1

    KSlider{
        id: slider
        enabled: storage.user.wallet?true:false
        stepSize: root_component.stepSize
         to: {
             var wallet = storage.user.wallet?storage.user.wallet:0
             if(wallet>0){
                 var bonus_limit = root_component.bonus_limit
                var cost = cart.endCost*bonus_limit;                 
                   if(cost<wallet) {
                       return cost;
                   } else return wallet;
             } else return 0


         }
    }
    KTextField {
        text: parseFloat(slider.value.toFixed(2))
        color: "#542A69"
        font.pixelSize: 12
        inputMethodHints: Qt.ImhDigitsOnly
        validator: IntValidator {bottom: 0; top: slider.to;}
        onTextChanged:  {
            slider.value = parseFloat(text).toFixed(2)
        }
    }

}
