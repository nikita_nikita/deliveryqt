import QtQuick 2.12
import QtQuick.Controls 2.5

TextField{
        id: control
        property int value: 0
        property int to: 99
        property int from: 0
        font.family: "qrc:/view/fonts/Montserrat-Regular.ttf"
        text: value
        color: "#542A69"
         inputMethodHints: Qt.ImhDigitsOnly
        inputMask: "99"
        background: Item {
            implicitWidth: 40
            implicitHeight: 30
            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                color: "#542A69"
                height: 1
                opacity: 0.5

            }
        }

        onFocusChanged: {
            if(focus) {
                cursorPosition =0
               // cursorVisible =true
            }

        }
        onEditingFinished: {
            var value  = parseInt(text);
            if(value>control.to){
                text = to;
                return;
            }
            if(value<from) {
                text = from;
            }
            else  text = value>9?value:"0"+value
        }

}


