import QtQuick 2.0
import "../../components/base"
Column {
    property bool status: promocode_info_column.status
    Row {
        id: promocode_row
        // anchors.verticalCenter: parent.verticalCenter
        height: 40
        spacing: 20
        KTextField {
            id: promocode_value
            placeholderText:  "Введите купон"
            font.pixelSize: 18
            anchors.verticalCenter: parent.verticalCenter
            text: cart.promocode
            enabled: cart.promocode.length==0
            onTextChanged: {
                text= text.replace(/[^a-zA-Z0-9 ]/i, "")
            }
            Connections {
                target: cart
                onPromocodeChanged:{
                    promocode_value.text =cart.promocode
                }

            }

        }
        ButtonViolet {
            width: 140
            text: cart.promocode.length!=0?"Сбросить": "Применить"
            anchors.verticalCenter: parent.verticalCenter
            onClicked:{

                if(cart.promocode.length==0){
                    cart.promocode = promocode_value.text
                }else{
                    cart.promocode = ""
                }

            }
        }


    }
    PromoCodeInfo{
        id: promocode_info_column
        width: parent.width-40
    }

}
