import QtQuick 2.12
import "../base"
import "../address"


Flickable
{
    id: delivery_content

    contentHeight: column_content.height+250
    property bool valid: user_name_field.valid&&user_street_field.valid&&user_building_field.valid&&user_dataProcessing_checkbox.checked

    Column {
        id: column_content
        width: user_contact.width
        spacing: 25
        anchors.horizontalCenter: parent.horizontalCenter


        PromoCode{
            width: parent.width}
        CLabel {
            text: "Персональные данные"
        }
        Grid {

            id: user_contact
            columns: delivery_content.width>=560?2:1
            columnSpacing: 20
            rowSpacing: 20
            CTextField {
                id: user_name_field
                placeholderText: "Ваше имя"
                text: cart.userName
                valid: this.text.length>0
                onAccepted:{
                    set()
                }
                onTextChanged: cart.userName =this.text
            }
            CTextField {
                id: user_phone_field
                placeholderText: "Номер телефона"
                inputMethodHints: Qt.ImhDigitsOnly
                text: cart.userPhone
                enabled: false
                valid: true
            }
        }
        CLabel {
            text: "Адрес доставки"
        }
        Grid {
            id: address_block
            columns: user_contact.columns
            columnSpacing: user_contact.columnSpacing
            rowSpacing: 20
            CTextField {
                id: user_street_field
                placeholderText: "Улица"
                inputMethodHints: Qt.ImhHiddenText
                valid: deliveryController.validStreet
                text: cart.street
                color: {
                    if(!focus&&!valid){
                        return "red"
                    }
                    return "#000000"
                }
                onFocusChanged: {
                    if(focus){
                        delivery_content.contentY =address_block.y
                    }
                }

                onTextChanged: {
                    if(focus){
                        cart.streetId=0
                        cart.street = this.text
                    }

                }


                StreetSelector {
                    visible: user_street_field.focus&&user_street_field.text.length>0
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.bottom
                    model: deliveryController.streetModel
                    onSelectSreet: {
                        cart.street = street.name
                        cart.streetId = street.id
                        user_building_field.focus = true
                    }
                }
                StreetUserSelector {
                    visible: user_street_field.focus&&user_street_field.text.length==0
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.bottom
                    model:storage.user.client_address? storage.user.client_address.filter(function(address){
                        if(storage.cityData.organizaton_info&&storage.cityData.organizaton_info.checkStreet){
                            if(address.street_id) return true;
                            return false;
                        }
                        return true
                    }):[]
                    onSelectAddress: {
                        cart.street = address.street
                        cart.streetId = parseInt(address.street_id)
                        if(address.building) cart.building = address.building
                        if(address.room) cart.room = parseInt(address.room)
                        if(address.entrance) cart.entrance = parseInt(address.entrance)
                        if(address.floor) cart.floor = parseInt(address.floor)
                        user_street_field.focus = false
                    }
                }
                z:2
            }
            Row {
                spacing: 10
                CTextField {
                    id: user_building_field
                    placeholderText: "Дом"
                    width: 130
                    inputMethodHints: Qt.ImhHiddenText
                    maximumLength: 10
                    text: cart.building;
                    valid: this.text.length>0

                    onAccepted: {
                        user_room_field.focus= true

                    }
                    onTextChanged: {
                        cart.building = this.text
                    }

                }
                CTextField {
                    id: user_room_field
                    placeholderText: "Квартира"
                    width: 130
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: cart.room!=0?cart.room:""
                    onAccepted: {
                        user_floor_field.focus = true
                    }
                    onTextChanged: cart.room = this.text
                }
            }
            Row {
                spacing: 10
                CTextField {
                    id: user_floor_field
                    placeholderText: "Этаж"
                    width: 130
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: cart.floor!=0?cart.floor:"";
                    onAccepted: user_entrance_field.focus = true
                    onTextChanged: {
                        cart.floor=this.text
                    }
                }
                CTextField {
                    id: user_entrance_field
                    placeholderText: "Подъезд"
                    width: 130
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: cart.entrance!=0?cart.entrance:""
                    onAccepted: focus = false
                    onTextChanged: cart.entrance= this.text
                }
            }
            z: 3
        }
        KCheckBox{
            visible: deliveryController.contactless_delivery
            text: "Бесконтакнтая доставка. Курьер оставит<br>Ваш заказ и отойдет на безопасную дистанцию."
            checked: deliveryController.contactless_status
            //        onCheckableChanged: deliveryController.contactless_status = checkable
            onClicked: {
                if(deliveryController.contactless_status){
                    deliveryController.contactless_status = false
                }else{
                    deliveryController.contactless_status = true
                }}
        }
        Rectangle{
            width: text.width+10
            height: text.height+10
            border.color: "red"

            Text {
                id:text
                visible: typeof(statusStorage.checkStreetResponse.amount)!=="undefined" && statusStorage.checkStreetResponse.amount|| deliveryController.notDelivery.length>0
                text: deliveryController.notDelivery.length > 0 ?deliveryController.notDelivery:"Минимальная стоимость для доставки <br>" + statusStorage.checkStreetResponse.amount + " руб."
                color: "red"

                //            height: visible?:0
                width: parent.width - 80
            }
        }
        Grid{
            columns: user_contact.columns
            columnSpacing: user_contact.columnSpacing
            rowSpacing: 20
            Column{
                visible: storage.tokenLk.length!==0
                spacing:user_contact.columnSpacing
                CLabel{
                    property real bal: storage.user.wallet?storage.user.wallet-cart.bonusPay:"0"
                    text: "Ваши бонусы: "+parseFloat(bal.toFixed(2))
                }
                SelectorBonus{
                    value: cart.bonusPay
                    onValueChanged: cart.bonusPay=value

                }
            }
            Item{
                width: 300
                height: select_time_column.height

                Column {
                    id: select_time_column
                    spacing:user_contact.columnSpacing
                    CLabel {
                        text: "Время доставки"
                    }
                    Item{
                        width: 300
                        height: 50
                        KText{
                            id: timorder
                            text:cart.orderTime === "on-ready"&&worktime.deliveryIntervas[0] === "Как можно скорее"? "Как можно скорее":cart.time
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        Image {
                            anchors.left: timorder.right
                            anchors.leftMargin: 10
                            source: "qrc:/view/img/icons/clock.png"
                            width: 30
                            anchors.verticalCenter: timorder.verticalCenter
                            fillMode: Image.PreserveAspectFit
                        }


                    }

                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        var index = 0
                        if(cart.orderTime !== "on-ready"){
                            index= worktime.deliveryIntervas.indexOf(cart.time)
                        }
                        tumbler_time.open(worktime.deliveryIntervas,index, function(value){


                            if(value===0&&worktime.deliveryIntervas[0] === "Как можно скорее"){
                                cart.orderTime = "on-ready"
                            } else {
                                cart.orderTime = "on-time"
                                cart.time= worktime.deliveryIntervas[value]
                            }
                        })
                    }
                }

            }

            CLabel {
                text: "Комментарий к заказу"
            }
            Comment {
                width: 290
                text: cart.comment
                onFocusChanged: {
                    if(focus){
                        delivery_content.contentY =parent.y+y
                    }
                }
                onTextChanged: cart.comment = this.text
            }





        }



        KCheckBox {
            id: user_dataProcessing_checkbox
            checked: cart.dataProcessing
            text: "Соглашаюсь на обработку персональных данных"
            onCheckedChanged: cart.dataProcessing = checked
        }
        KCheckBox {
            id: user_agreement
            checked: profileController.agreement
            visible: !checked
            text: "Получать новости от Kapibara"
            onCheckedChanged: {
                if(checked&&!profileController.agreement){

                    var client_info ={
                        name: storage.user.client_info? storage.user.client_info.name:"",
                        phone: storage.user.client_info?storage.user.client_info.phone:"",
                        email: storage.user.client_info?storage.user.client_info.email:"",
                        birthday: storage.user.client_info?storage.user.client_info.birthday:"",
                        agreement: user_agreement.checked
                    }
                    kapibaras.saveClient(client_info, storage.tokenLk);

                }
            }
        }

    }
}
