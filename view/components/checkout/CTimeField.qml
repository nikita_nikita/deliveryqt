import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
RowLayout {
    id: control
    spacing: 5
    property alias hour: control_hour.text
    property alias minute: control_minute.text
    signal  hourEditingFinished();
    signal minuteEditingFinished();
    CTimeTextField{
        id:control_hour
        //text: control.hour
        Layout.alignment: Qt.AlignVCenter
        from: 0
        to: 23
        onEditingFinished: {
            control.hourEditingFinished();
            control_minute.focus=true
        }
    }

    Text {
        text: ":"
        Layout.alignment: Qt.AlignVCenter
    }
    CTimeTextField{
        id: control_minute
        //text: control.minute
        Layout.alignment: Qt.AlignVCenter
        from: 0
        to: 59
        onEditingFinished:
        {
            control.minuteEditingFinished();
            control_minute.focus=false
        }
    }



}

