import QtQuick 2.12
import "../base"

Flickable
{
    id: delivery_content
    contentHeight: column_content.height+250
    property bool valid: user_name_field.valid&&user_phone_field.valid&&user_dataProcessing_checkbox.checked&&cart.takeAwayPoint!=0

    function calcEnablePoint (){
        if(router.page != "checkout")  return;
        var enablePoints = storage.cityData.point_delivery.map(x => x.id);
        var productIdList = [];
        for (let i = 0; i < cart.conteiner.length; i++) {
            productIdList.push(cart.conteiner[i].product.id);
        }
        for ( let k = 0; k < cart.bonus.length; k++) {
            productIdList.push(cart.bonus[k].product.id);
        }
        grid_poins_delivery.enable_points =  storage.processProductToAwailablePoints(productIdList);
    }
    Column {
        id: column_content
        width: user_contact.width
        spacing: 25
        anchors.horizontalCenter: parent.horizontalCenter
        PromoCode{
            width: parent.width}
        CLabel {
            text: "Персональные данные"
        }
        Grid {

            id: user_contact
            columns: delivery_content.width>=560?2:1
            columnSpacing: 20
            rowSpacing: 20
            CTextField {
                id: user_name_field
                placeholderText: "Ваше имя"

                text: cart.userName
                valid: text.length>0

                onAccepted: set()
                onTextChanged: cart.userName =text
                function set(){
                    //cart.userName = text

                }

            }
            CTextField {
                id: user_phone_field
                placeholderText: "Номер телефона"
                inputMethodHints: Qt.ImhDigitsOnly
                text: cart.userPhone
                validator: RegExpValidator { regExp: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/ }
                //valid: text.length>0
                valid: true
                onAccepted: set()
                enabled: false
                function set() {
                    cart.userPhone = text
                    user_street_field.focus = true
                }
                property int start_cursor: 0
                property string text_temp: ""

                Keys.onPressed: {
                    text_temp=""
                }

                onFocusChanged: {
                    if(focus){
                        text_temp= text
                        text=""
                        cursorPosition = start_cursor
                    } else {
                        if(text_temp.length!=0) user_phone_field.text =text_temp
                        cart.userPhone = text
                    }
                }
            }
        }
        Text{
            id:sorrymessage
            text: "Извините, товары в вашей корзине<br>не доступны на самовывоз."
            anchors.left: parent.left
            anchors.right: parent.right
            height: 20
            color: "red"
            visible: storage.sorryMessageVisible
        }
        CLabel {
            visible: !sorrymessage.visible
            text: "Заберу: "
            //color: cart.takeAwayPoint==0?"#f00":"#542A69"
        }
        KText {
            text: "Некоторые продукты из вашей корзины готовятся только в определенных точках"
            color: "#f00"
            width: parent.width
            wrapMode: Text.WordWrap
            visible: storage.cityData.point_delivery!==undefined&&
                     storage.cityData.point_delivery.length!== grid_poins_delivery.enable_points.length&&!sorrymessage.visible

        }
        KText {
            text: "Укажите пункт самовывоза"
            color: "#f00"
            width: parent.width
            wrapMode: Text.WordWrap
            font.pointSize: 10
            visible: cart.takeAwayPoint==0&&!sorrymessage.visible

        }


        Grid {
            visible: !sorrymessage.visible
            id: grid_poins_delivery
            columns: user_contact.columns*2
            columnSpacing: user_contact.columnSpacing
            rowSpacing: 20

            property var enable_points: [];

            Connections {
                target:  router
                onPageChanged: delivery_content.calcEnablePoint()

            }
            Connections{
                target: cart
                onBonusChanged: delivery_content.calcEnablePoint()
            }
            //Marker
            Repeater {
                model: storage.cityData.point_delivery
                KRadioButton {
                    checked: cart.takeAwayPoint===modelData.id
                    text: modelData.name + "<br>" + modelData.address
                    width: 130
                    height: 30
                    onCheckedChanged:{
                        if (cart.takeAwayPoint!==modelData.id) cart.takeAwayPoint = modelData.id
                    }
                    enabled: grid_poins_delivery.enable_points.includes(modelData.id)
                }
            }
        }
        /*---*/
        Grid{
            columns: user_contact.columns
            columnSpacing: user_contact.columnSpacing
            rowSpacing: 20

            Column{
                visible: storage.tokenLk.length!==0
                spacing:user_contact.columnSpacing
                CLabel{
                    property real bal: storage.user.wallet?storage.user.wallet-cart.bonusPay:"0"
                    text: "Ваши бонусы: "+parseFloat(bal.toFixed(2))

                }
                SelectorBonus{
                    value: cart.bonusPay
                    onValueChanged: cart.bonusPay=parseFloat(value.toFixed(2))

                }
            }
            Item{
                width: 300
                height: select_time_column.height

                Column {
                    id: select_time_column
                    spacing:user_contact.columnSpacing
                    CLabel {
                        text: "Когда сможете забрать?"
                    }
                    Item{
                        width: 300
                        height: 50
                        KText{
                            id: timorder
                            text:cart.orderTime === "on-ready"&&worktime.pickupIntervas[0] === "Как можно скорее"? "Как можно скорее":cart.time
                            anchors.verticalCenter: parent.verticalCenter
                            font.underline: true
                        }

                        Image {
                            anchors.left: timorder.right
                            anchors.leftMargin: 10
                            source: "qrc:/view/img/icons/clock.png"
                            width: 40
                            anchors.verticalCenter: timorder.verticalCenter
                            fillMode: Image.PreserveAspectFit
                            smooth: false
                        }


                    }

                }                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        var index = 0
                        if(cart.orderTime !== "on-ready"){
                            index= worktime.pickupIntervas.indexOf(cart.time)
                        }
                        tumbler_time.open(worktime.pickupIntervas,index, function(value){


                            if(value===0&&worktime.pickupIntervas[0] === "Как можно скорее"){
                                cart.orderTime = "on-ready"
                            } else {
                                cart.orderTime = "on-time"
                                cart.time= worktime.pickupIntervas[value]
                            }
                        })
                    }
                }

            }


            CLabel {
                text: "Комментарий к заказу"
            }

            Comment {
                width:  290
                text: cart.comment
                onFocusChanged: {
                    if(focus){
                        delivery_content.contentY =parent.y+y
                    }
                }
                onTextChanged: cart.comment = this.text
            }


        }


        KCheckBox {
            id:user_dataProcessing_checkbox
            checked: cart.dataProcessing
            text: "Соглашаюсь на обработку персональных данных"
            onCheckedChanged: cart.dataProcessing = checked

        }
        KCheckBox {
            id: user_agreement
            checked: profileController.agreement
            visible: !checked
            text: "Получать новости от Kapibara"
            onCheckedChanged: {
                if(checked&&!profileController.agreement){

                    var client_info ={
                        name: storage.user.client_info? storage.user.client_info.name:"",
                        phone: storage.user.client_info?storage.user.client_info.phone:"",
                        email: storage.user.client_info?storage.user.client_info.email:"",
                        birthday: storage.user.client_info?storage.user.client_info.birthday:"",
                        agreement: user_agreement.checked
                    }
                    kapibaras.saveClient(client_info, storage.tokenLk);
                }
            }
        }

    }

}
