import QtQuick 2.12
import QtQuick.Layouts 1.12
import "./base"
import "./city"
Rectangle {
    id: root_component
    color: "#fff"
    signal citySelect(var city)


    Item {
        anchors.fill: parent
        anchors.topMargin: 15
        signal citySelect(var city)

        Column{
            id: find_content
            spacing: 15
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 15
            KText {
                id: text_find_city
                font.pixelSize: 16
                font.weight: Font.Bold
                text: "Выберите город"
            }
            RowLayout {
                Image {
                    source: "qrc:/view/img/search_grey.png"
                    width: 13
                    height: 13
                    fillMode: Image.PreserveAspectFit

                    opacity: 0.3

                }
                KTextField {
                    id:seach_text
                    placeholderText: "Введите название вашего города"
                    horisontalSeparator: false
                    inputMethodHints: Qt.ImhHiddenText
                    onTextChanged: storage.cityModel.setFilterWildcard(text)

                }
            }


        }
        HorisontalSeparator {
            id: hSeparator
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: find_content.bottom
            anchors.topMargin: 14
        }
        Row {
            anchors.left: parent.left
            anchors.top: hSeparator.bottom
            anchors.bottom: parent.bottom
            CityList{
                country: "Россия"
                anchors.top: parent.top
                anchors.bottom: parent.bottom               
                onCitySelect: root_component.citySelect(city)
            }

            VerticalSeparator{
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }
            CityList {
                country: "Беларусь"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                domainCode: 'BY'               
                onCitySelect: root_component.citySelect(city)
            }

        }
    }
}
