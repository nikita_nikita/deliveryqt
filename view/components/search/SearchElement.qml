import QtQuick 2.0
import "../base/"
Rectangle{
    id: root_component

    width: parent.width-20
    height: 40
    color: "black"
    signal up()
    signal down()


    KTextField{
        id: name_search
        placeholderText: "Поиск по названию"
        focus:   router.page === "search"

        anchors.fill: parent
        onTextChanged:

            menuController.nameSearch = text
    }


}
