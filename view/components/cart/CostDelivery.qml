import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"

Rectangle {
    opacity: 0.9
    color: "#542A69"
    visible: cart.orderType=="delivery"&&!statusStorage.checkStreetResponse.status&&statusStorage.checkStreetResponse.paid_delivery

    RowLayout {
        width: 300;
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        KText {
            text: "Доставка:"
            color: "#fff";
            font.pixelSize: 10
        }
        Item{
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        KText {
            text: {
                var result=  cart.deliveryCost+"руб."
                return result.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
            }
            color: "#fff"
            font.weight: Font.Bold
            font.pixelSize: 16
        }
    }
}
