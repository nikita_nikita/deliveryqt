import QtQuick 2.12
import QtQuick.Layouts 1.12

Rectangle{
         id: root_component

         property alias model: repeater_products.model
         clip: true
         height: grid_items.height
         property int good_item_width:root_component.width>600?248:100
         readonly property int currentHeight: grid_items.height


    GridLayout{
        id: grid_items
        anchors.horizontalCenter: parent.horizontalCenter
        columnSpacing: 10
        rowSpacing:  10
          columns: (root_component.width-20)/root_component.good_item_width
          Repeater{
              id: repeater_products

                GoodsItem{
                    model: ({
                                  name: name,
                                image: image,
                                price: price,
                                composition: composition?composition:addedCompositeString,
                                json: json,
                                quantity:quantity,
                                typeName: typeName,
                                volume: volume

                            })
                    Layout.fillHeight: true
                    Layout.preferredWidth: root_component.good_item_width

                }

          }

    }


}


