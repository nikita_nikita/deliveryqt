import QtQuick 2.0
import "../base"
Item {
    id: root_component
    property var model
    property int maxSize: 0
    height: 20
    clip: true
    signal editOptions()
    signal quantityChanged(int quantity);
    function createdOptions() {
        var createdName = "";
        var test = root_component.model;
        if(root_component.model.options!==undefined){
            var obj = root_component.model.options.added;
            var product = root_component.model.product;
            for (var key in obj) {
                if (obj[key] === null) continue;
                if (Array.isArray(obj[key])) {
                    for (var i = 0; i < obj[key].length; i++) {
                        createdName += obj[key][i].item.product.name+'('+obj[key][i].quantity+'), ';
                    }
                    continue;
                }

                if (key==='doubleMeat' || key==='doubleVeg') {
                    if (key==='doubleMeat' && obj[key]) {
                        createdName +=product.added.meat.product.name+', ';
                    }
                    if (key==='doubleVeg' && obj[key]) {
                        createdName +=product.added.vegetable.product.name+', ';
                    }
                    continue;
                }
                if(key==="noodle"&&obj.noodle){
                    var nooldle= product.added.noodle.find(x=>x.product.id === obj.noodle);
                    if(!nooldle.selected){
                        createdName += nooldle.product.name+', ';
                    }

                }
                if(key==="souse"&&obj.souse) {
                    var souse  =  product.added.souse.find(x=>x.product.id === obj.souse);
                    if(!souse.selected){
                        createdName += souse.product.name+', ';
                    }
                }
            }

        }
        return createdName.slice(0, -2);
    }

    KText {
        id: product_options
        wrapMode: Text.WordWrap
        text: root_component.createdOptions()
        font.pixelSize: 10
        width: 170
        opacity: 0.5
        MouseArea {
            anchors.fill: parent
            onClicked:{
                root_component.editOptions();
            }
        }
    }

}
