import QtQuick 2.0
import QtQuick.Layouts 1.12

import "../base"
Column {

    id: root_component
    property int good_item_width:root_component.width>600?248:100
    spacing: 5
    height: grid_items.height+ 50
//    visible: cartController.serviceKit.count>0
    KText {
        text: "К заказу Вы Бесплатно  получите"
        font.pixelSize: 16
        font.weight: Font.Bold
        anchors.left: parent.left
        anchors.leftMargin: 15
    }
    Grid{
        id: grid_items
        columnSpacing: 10
        rowSpacing:  10
        columns: (root_component.width-20)/root_component.good_item_width
        width:root_component.good_item_width
        Repeater{
            model: cartController.serviceKit
            Column{
                id: column
                width: root_component.good_item_width
                KImage{
                    imagefile: m_image
                    width: column.width
                    fillMode: Image.PreserveAspectFit

                }
                KText{
                    text: m_text
                    wrapMode: Text.WordWrap
                    width: column.width
                    font.pixelSize: 10
                    font.weight: Font.Bold
                    horizontalAlignment: Text.AlignHCenter

                    //Layout.maximumWidth:column.width
                }
            }
        }
    }



}
