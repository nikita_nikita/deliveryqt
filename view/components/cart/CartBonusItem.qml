import QtQuick 2.12

import "../base"
Item {
    id: root_component

    property var model
    height: content.height
    signal quantityCanged(int quantity)
    /*
      product
        quantity
*/

    Item {
        id: content
        width: root_component.width
        height: root_component.width>400?product_image.height:product_image.height+product_cost_quantity.height
        anchors.centerIn: parent

        KImage {
            id: product_image
            anchors.top: parent.top
            anchors.left: parent.left
//            anchors.leftMargin: -5
            width: 100
            fillMode: Image.PreserveAspectFit
            imagefile:root_component.model.product.image
            onSourceSizeChanged: {

                if(sourceSize.height!=0&&sourceSize.height>sourceSize.width){
                    product_image.height = 60
                    console.log(sourceSize.height)
                }}
        }

        KText {
            id: product_name
            anchors.left: product_image.right
            anchors.leftMargin: 15
            wrapMode: Text.WordWrap
            // height: root_component.width>400?product_image.height:contentHeight
            // verticalAlignment: root_component.width>400?Text.AlignVCenter:Text.AlignTop
            height: product_image.height
            verticalAlignment: Text.AlignVCenter
            text: root_component.model.product.name
            font.pixelSize: 12
            width: 170

        }
        Item {
            id: product_cost_quantity
            width: 180
            height: 40
            anchors.right: parent.right
            anchors.rightMargin: 15
            // anchors.top: product_image.bottom
            anchors.top: root_component.width>400?undefined:product_image.bottom
            anchors.verticalCenter: root_component.width>400?product_image.verticalCenter:undefined
            clip: true

            KSpinBox {
                id:  product_quantity
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 16
                from: 1
                visible:false

            }
            KText{
                text: ""
                font.pixelSize: 16
                anchors.left: parent.left
                anchors.leftMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                visible: false
            }

            KText {
                text: ""
                anchors.right: parent.right
                font.pixelSize: 16
                font.weight: Font.Bold
                anchors.verticalCenter: parent.verticalCenter
            }
        }

    }

    HorisontalSeparator {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
}
