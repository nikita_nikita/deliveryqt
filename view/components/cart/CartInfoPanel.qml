import QtQuick 2.12
import "../base"

Column {
    width: parent.width
    spacing: 5


    //        visible: root_component.ready&&cart.cupon.coupon?true: false

    Repeater{
        model:cart.cartInfo
        id:repeater
        Column{
            width: parent.width
            spacing: 5
            KText {

                text: modelData.name
                color: modelData.type === 0? "red":"green"
                wrapMode: Text.WordWrap
                width: parent.width

            }
            KText {

                text: modelData.description
                color: modelData.type === 0? "red":"green"
                wrapMode: Text.WordWrap
                width: parent.width
            }
        }

    }
}



