import QtQuick 2.5

import "../base"
Column {
    spacing: 5
    KText {
        text: "Дополнительные соуса к пицце"
        font.pixelSize: 16
        font.weight: Font.Bold
        anchors.left: parent.left
        anchors.leftMargin: 15
        visible:menu.toppingsPizza.count>0&&cart.model.containsPizza

    }
    GoodsPage{
        width: parent.width
        model: menu.toppingsPizza
        visible:menu.toppingsPizza.count>0&&cart.model.containsPizza
    }
    KText {
        text: "Дополнительны топинги"
        font.pixelSize: 16
        font.weight: Font.Bold
        anchors.left: parent.left
        anchors.leftMargin: 15

    }
    GoodsPage{
        width: parent.width
        model: menu.toppingsJap
    }

}
