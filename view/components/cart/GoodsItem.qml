import QtQuick 2.12
import "../base"
import QtQuick.Layouts 1.12
Item{
    id: root_component
    width: 150
    property var model: ({})

    Layout.minimumHeight:{
        if(button_composit.visible)
             return column.height+ product_cost.height+50
        return column.height+ product_cost.height+20
    }
    MouseArea {
        anchors.fill: parent
        onClicked: root_component.focus=true
    }

    ColumnLayout {
        id: column
        anchors.left: parent.left
        anchors.right: parent.right
        spacing: 0
        Row{
            z:2
            height: 20
            Image {
                source: "qrc:/view/img/product/new.png"
                visible: root_component.model.json.new
                fillMode: Image.PreserveAspectFit
                width: 20
            }
            Image {
                source: "qrc:/view/img/product/hit.png"
                visible: root_component.model.json.hit
                fillMode: Image.PreserveAspectFit
                width: 20
            }
            Image {
                source: "qrc:/view/img/product/hot.png"
                visible: root_component.model.json.hot
                fillMode: Image.PreserveAspectFit
                width: 20
            }

        }
        Item{
            id: image_conteiner

            Layout.fillWidth: true
            Layout.preferredHeight: product_image.height
            KImage {
                id: product_image
                width: column.width
                fillMode: Image.PreserveAspectFit
                imagefile: root_component.model.image
            }
        }
        RowLayout{
            id: product_name_row
           Layout.preferredWidth: column.width-10
            KText{
                id: product_name
                text: root_component.model.name
                wrapMode: Text.WordWrap
                Layout.preferredWidth: column.width-40
                font.pixelSize: 10
                font.weight: Font.Bold
                //Layout.maximumWidth:column.width
            }
            Image {
                source: "qrc:/view/img/info.png"
                Layout.preferredHeight: 20
                Layout.preferredWidth: 20
                Layout.alignment: Qt.AlignRight
                fillMode: Image.PreserveAspectFit
                visible: root_component.model.json.pfc|| root_component.model.json.weight|| root_component.model.json.caloric

                MouseArea {
                    anchors.fill: parent
                    onClicked:
                    {
                        var test  = root_component.model.json
                        pfs_modal.focus=true
                    }
                }
                Rectangle {
                    id: pfs_modal
                    color: "#373535"
                    opacity: 0.9
                    width: column.width-10
                    height: pfs_grid.height+20
                    radius: 10
                    anchors.right: parent.right
                    anchors.bottom: parent.top
                    visible: focus
                    GridLayout{
                        id: pfs_grid
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width-10
                        columns: 2
                        KText{
                            id: fat_label
                            text: "Жиры"
                            color: "#fff"
                            font.pixelSize: 10
                            visible:fat_text.visible
                        }
                        KText{
                            id: fat_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.fat+" г":""
                             color: "#fff"
                             font.pixelSize: 10
                             Layout.alignment: Qt.AlignRight
                             visible: text.length>0
                        }
                        KText{
                            id: protein_label
                            text: "Белки"
                              color: "#fff"
                              font.pixelSize: 10
                              visible: protein_text.visible
                        }
                        KText{
                            id: protein_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.protein+" г":""
                              color: "#fff"
                              font.pixelSize: 10
                              Layout.alignment: Qt.AlignRight
                               visible: text.length>0
                        }

                        KText{
                            id: carbohydrates_label
                            text: "Углеводы"
                              color: "#fff"
                              font.pixelSize: 10
                              visible: carbohydrates_text.visible
                        }
                        KText{
                            id: carbohydrates_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.carbohydrates+" г":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                             visible: text.length>0
                        }
                        KText{
                            id: weight_label
                            text: "Вес"
                              color: "#fff"
                              font.pixelSize: 10
                              visible: weight_text.visible
                        }
                        KText{
                            id: weight_text
                            text: root_component.model.json.weight?root_component.model.json.weight+" г":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }
                        KText{
                            id: caloric_label
                            text: "Калории"
                              color: "#fff"
                              font.pixelSize: 10
                              visible: caloric_text.visible
                        }
                        KText{
                            id: caloric_text
                            text: root_component.model.json.caloric?root_component.model.json.caloric+" Ккал":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }


                    }
                }

            }
        }


        //обьем
        KText{
            id: product_volume
            text: root_component.model.volume+" л"
            Layout.maximumWidth:column.width
            wrapMode: Text.WordWrap
            font.pixelSize: 12
            color: "#878787"
            visible: root_component.model.volume>0
           // Layout.alignment: Qt.AlignHCenter

        }

        // состав
//        KText{
//            id: composition
//            text: root_component.model.composition
//            wrapMode: Text.WordWrap
//            font.pixelSize: 12
//            color: "#878787"
//            visible: root_component.model.composition.length>0

//        }

    }
    KText{
        id: product_cost
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.left: parent.left
        font.pixelSize: 12
        color: "#542A69";
        text: root_component.model.price
    }

    KText{
        id: product_currency
        anchors.left: product_cost.right
        anchors.bottom: product_cost.bottom
        text: " руб"
        font.pixelSize: 12
        color: "#542A69";
    }

    RoundButton{
        id: buttonAddToCard
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: product_cost.verticalCenter
        onClicked:{
            cart.add(root_component.model.json)
        }
        visible: root_component.model.quantity ===0

    }
    KSpinBox {
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: product_cost.verticalCenter
        value: root_component.model.quantity
        onValueChanged:{
            if(value!==root_component.model.quantity){
                cart.setQuantity(root_component.model.json, value, true)
            }
        }
        visible: !buttonAddToCard.visible
    }
    Rectangle {
        id: button_composit
        height: 20;
        width: composit_text.width+10;
        anchors.bottom: product_cost.top
        anchors.bottomMargin: 10

        property var product: root_component.model.json
        border.width: 1
        border.color: "#dedede"
        visible: button_composit.product.added.noodle!==undefined || button_composit.product.added.souse!==undefined
        KText {
            id: composit_text
            text:{
              //  var product = root_component.model.json;
                if (button_composit.product.added.noodle!==undefined) {
                    return 'Собрать'
                }
                if (button_composit.product.added.souse!==undefined) {
                    return 'Выбрать соус'
                }
                return "";
            }

            anchors.centerIn: parent
            font.pixelSize: 12
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(button_composit.product.added.noodle!==undefined) {
                    router.pushToWokConstructor({   product: root_component.model.json,
                                                    quantity: 1,
                                                    operation: "add"
                                                })
                    return;
                }
                router.pushToConstructor({  product: root_component.model.json,
                                             quantity: 1,
                                             operation: "add"
                                         })


            }

        }
    }



}
