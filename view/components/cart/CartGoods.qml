import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../../js_plugins/promoProcessor.js" as PP

ColumnLayout {
    id: root_component
    property var model: cart.conteiner
    property var cupon: ({status: false})


    Repeater {
        model: root_component.model
        CartGoodsItem {
            model: modelData
            Layout.fillWidth: true
            Layout.minimumHeight: 60
            Layout.preferredHeight: height
            maxSize:parent.width/3
            onEditOptions: {
                var data = modelData;
                data.index = index;
                data.operation = "edit"
                if(data.product.added.noodle!==undefined) {
                    router.pushToWokConstructor(data)
                } else  {
                    router.pushToConstructor(data)
                }
            }
            onQuantityChanged: {               
                cart.setQuantityByIndex(index, quantity);
            }
        }
    }
    Repeater {
        model: cart.bonus
        CartBonusItem {
            model: modelData
            Layout.fillWidth: true
            Layout.minimumHeight: 60
            Layout.preferredHeight: height
            onQuantityCanged: {
                var bonuses = cart.bonus
                bonuses[index].quantity=quantity
                cart.bonus= bonuses
            }
        }
    }
    Connections {
     target: statusStorage
     onCheckStreetResponseChanged: {
         var  test_amount = statusStorage.checkStreetResponse
         if(!statusStorage.checkStreetResponse.status&&statusStorage.checkStreetResponse.amount) {
            cart.deliveryCost = statusStorage.checkStreetResponse.amount
         } else {
             cart.deliveryCost = 0;

         }

     }
    }
    Connections {
        target: cart
        onPromocodeChanged: {
            if(storage.cityData.organizaton_info.checkPromocode){
                var phone = storage.user.client_info?storage.user.client_info.phone:""
                kapibaras.loadPromocode(cart.promocode, phone)
            }
        }
        onConteinerChanged: {
            root_component.updateTotalCost();
//            root_component.calculateCart()
        }
        onTotalCostChanged: updateEndCost();
        onBonusCostChanged: updateEndCost();
    }
    Connections {
        target: kapibaras
        onCuponLoaded: {
            root_component.cupon = cupon
//            root_component.calculateCart()
//            root_component.bonusCostUpdate()

        }
    }
    Connections {
        target: storage
        onUserChanged:{
            if(cart.promocode.length>0){
                var phone = storage.user.client_info?storage.user.client_info.phone:""
                kapibaras.loadPromocode(cart.promocode, phone)
            }
        }
    }


    function getCartArray(cart){
        let cartArr = [];
        for (let i = 0; i < cart.length; i++) {
            cartArr.push({id:cart[i].product.id, quantity:cart[i].quantity });
        }
        return cartArr;
    }
   //@todo  переписать на с++
    function updateTotalCost(){
        var result =0;
        var conteiner = cart.conteiner
        for( var i=0; i<conteiner.length; i++){
            var cart_item  = conteiner[i];
            var dop_cost =0;
            if(cart_item.options) dop_cost=cart_item.options.price
            var cost  = cart_item.quantity * (cart_item.product.sity_info.price+dop_cost)-cart_item.offerValue
            result+= cost;
        }
        var res = parseFloat(result.toFixed(2))
        cart.total_cost = res

    }
      //@todo  переписать на с++
    function updateEndCost(){      
        var res = cart.total_cost;
        res+=cart.bonusCost;

        if(cart.cuponStatus&&cart.cuponStatus.status){
            if(cart.discount){
                switch(cart.discount.type){
                case "fixed":
                    res = res-cart.discount.val
                    break;
                case "persent":
                    res = res - (res*(cart.discount.val/100));
                    break;
                }
            }
        }
        cart.endCost = res
    }   

}
