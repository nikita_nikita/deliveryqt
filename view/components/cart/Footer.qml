import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Item {
    id: root_component
    Connections {
        target: cart

    }
    //выводит стоимость платной доставки
    /*CostDelivery {

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.top
    height: 30
}*/


    ColumnLayout {
        // visible: cart.endCost>0
        spacing: 16
        anchors.centerIn: parent
        RowLayout {
            Layout.preferredWidth: 300
            KText {
                text: "Общая стоимость:"
                color: "#fff";
                font.pixelSize: 10
            }
            Item{
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            KText {
                text: {
                    var result=  cart.endCost-cart.offerValue-cart.bonusPay
                    result = parseFloat(result.toFixed(2))+" руб."
                    return result.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                }
                color: "#fff"
                font.weight: Font.Bold
                font.pixelSize: 18
            }
        }
        ButtonYollow {
            Layout.fillWidth: true
            text: router.page=="checkout"?"К оплате":"Оформить заказ"
            enabled:   router.footerBottonValid

                    onClicked: {
                        worktime.checkPointWork()
                        if(router.page == "cart"){
//                            if(cart.getToppingCount() === 0 &&cart.toppings_enable){
//                                cartController.zeroToppingMessage = true


//                            }else{
                                router.push("checkout")
                                return;
//                            }



                        }
                        if(router.page == "checkout"){


                            var test_amount = cart.deliveryCost
                            if(cart.orderType=="delivery"&& !statusStorage.checkStreetResponse.status){
                                router.push("product_not_zone");
                            } else {
                                if(deliveryController.tab_index == 0){
                                    cart.orderType = "delivery"
                                }else{
                                    cart.orderType = "take-away"
                                }
                                var total_cost=  cart.endCost
                                if(total_cost=>statusStorage.checkStreetResponse.amount){
                                    router.push("payment")
                                } else {
                                    cart.sendOrderToServer();
                                }

                            }
                            return;
                        }
                        if(router.page == "payment"){

                        deliveryController.updateOrganizationInfo()
                        }


                    }
                }
            }

    function showWarning(){}
        }
