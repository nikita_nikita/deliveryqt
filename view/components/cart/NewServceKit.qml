import QtQuick 2.0
import QtQuick.Layouts 1.12

import "../base"
Column {
    id: root_component
    property int good_item_width:root_component.width>600?248:100
    spacing: 5
    height:visible? grid_items.height+ 50:0
    visible: repeater.model.count > 0
    KText {
        text: "Вы можете выбрать необходимое вам<br>количество Бесплатных топпингов"
        font.pixelSize: 16
        font.weight: Font.Bold
        anchors.left: parent.left
        anchors.leftMargin: 15
    }
    Grid{
        id: grid_items
        columnSpacing: 10
        rowSpacing:  10
        columns: 3
        width:parent.width
        anchors.horizontalCenter: parent.horizontalCenter

        Repeater{
            id:repeater
            model: cart.toppingkit
            Column{

                id: column
                width: grid_items.width/3-10

                KImage{
                    imagefile: m_image
                    width: column.width
                    fillMode: Image.PreserveAspectFit


                }
                KText{
                    text: m_name
                    wrapMode: Text.WordWrap
                    width: column.width
                    font.pixelSize: 10
                    font.weight: Font.Bold
                    horizontalAlignment: Text.AlignHCenter

                    //Layout.maximumWidth:column.width
                }
                KSpinBox {
                    value:m_count
                    max:m_quantity
                    anchors.horizontalCenter: parent.horizontalCenter
                    onValueChanged:{
                        cart.updateToppingCount(index,value)
                        console.log(m_count)
                    }

                }
            }
        }
    }



}
