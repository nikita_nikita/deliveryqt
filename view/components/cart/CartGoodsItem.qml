import QtQuick 2.12
import QtQuick.Layouts 1.12

import "../base"
Item {
    id: root_component
    property var model
    property int maxSize: 0
    height: content.height+20
    clip: true
    signal editOptions()
    signal quantityChanged(int quantity);
    function createdOptions() {
        var createdName = "";
        var test = root_component.model;
        if(root_component.model.options!==undefined){
            var obj = root_component.model.options.added;
            var product = root_component.model.product;
            for (var key in obj) {
                if (obj[key] === null) continue;
                if (Array.isArray(obj[key])) {
                    for (var i = 0; i < obj[key].length; i++) {
                        createdName += obj[key][i].item.product.name+'('+obj[key][i].quantity+'), ';
                    }
                    continue;
                }

                if (key==='doubleMeat' || key==='doubleVeg') {
                    if (key==='doubleMeat' && obj[key]) {
                        createdName +=product.added.meat.product.name+', ';
                    }
                    if (key==='doubleVeg' && obj[key]) {
                        createdName +=product.added.vegetable.product.name+', ';
                    }
                    continue;
                }
                if(key==="noodle"&&obj.noodle){
                    var nooldle= product.added.noodle.find(x=>x.product.id === obj.noodle);
                    if(!nooldle.selected){
                        createdName += nooldle.product.name+', ';
                    }

                }
                if(key==="souse"&&obj.souse) {
                    var souse  =  product.added.souse.find(x=>x.product.id === obj.souse);
                    if(!souse.selected){
                        createdName += souse.product.name+', ';
                    }
                }
            }

        }

        return createdName.slice(0, -2);
    }
    GridLayout {
        id: content
        width: root_component.width-30
        columns:width>(row1.width+row2.width+10)?2:1
        anchors.verticalCenter: parent.verticalCenter
        Row {
            id: row1
            spacing: 15
            KImage {
                id: product_image
                width: root_component.maxSize
                fillMode: Image.PreserveAspectFit
                imagefile: root_component.model.product.images[0]
                onSourceSizeChanged: {

                    if(sourceSize.height!=0&&sourceSize.height>sourceSize.width){
                        product_image.height = 70
                        console.log(sourceSize.height)
                    }}
            }
            Column{
                anchors.verticalCenter: parent.verticalCenter
                KText {
                    id: product_name
                    wrapMode: Text.WordWrap
                    text: root_component.model.product.name
                    font.pixelSize: 12
                    width: 170

                }
                Row {
                    visible: root_component.model.options!==undefined
                    spacing: 5
                    Image {
                        source: "qrc:/view/img/pen.png"
                        width: 10
                        fillMode: Image.PreserveAspectFit
                    }
                    KText {
                        id: product_options
                        wrapMode: Text.WordWrap
                        text: root_component.createdOptions()
                        font.pixelSize: 10
                        width: 170
                        opacity: 0.5
                        MouseArea {
                            anchors.fill: parent
                            onClicked:{
                                root_component.editOptions();
                            }
                        }
                    }
                }
            }
        }
        Row {
            id:row2
            spacing: 0
            Layout.alignment: Qt.AlignRight
            KSpinBox {
                id:  product_quantity
                font.pixelSize: 16
                value: root_component.model.quantity
                onValueChanged:{
                    if(value!==root_component.model.quantity){
                        // cart.setQuantity(root_component.model.product, value)
                        root_component.quantityChanged(value)
                    }
                }
            }
            KText {
                id: cost_text
                text: {
                    var dop_cost =0;
                    if(root_component.model.options!==undefined) dop_cost=root_component.model.options.price
                    var cost =  (root_component.model.product.sity_info.price+dop_cost)*root_component.model.quantity-root_component.model.offerValue
                    cost =parseFloat(cost.toFixed(2))
                    return cost+ " руб"
                }
                //anchors.right: parent.right
                font.pixelSize: 16
                font.weight: Font.Bold
                horizontalAlignment: Text.AlignRight
                anchors.verticalCenter: parent.verticalCenter
                width: 110
            }

        }

    }



    HorisontalSeparator {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: content.bottom
    }
}
