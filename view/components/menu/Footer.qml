import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
Item {

    /*  StackView {
        id: stack_category

        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.top: parent.top
        height: 40
        initialItem: FCategories{
            model: menu.model


        }
       function createNewPage(categorys){
            var component = Qt.createComponent("FCategories.qml");
            var object = component.createObject(stack_category);
            object.model = categorys
            object.isBack= true
            object.height = stack_category.height
            return object;
        }
       Connections {
           target: menu.model
           onCreateNewPage:{
               var page =stack_category.createNewPage(modelData)
                stack_category.push(page);
           }
       }
       Connections {
           target: menu
           onToBackPage: stack_category.pop();
       }
       Connections {
           target: storage
           onCityDataChanged: {
                stack_category.pop(null);
           }

       }


    }*/


    Repeater{
        model: menuController.pages
        FCategories{
            height: 40
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.right: parent.right
            model: pages_data
            visible: pages_data.visible
            isBack: pages_data.isBack
        }
    }

    Rectangle{
        id: separator
        color: "#fff"
        anchors.left: parent.left
        anchors.right: parent.right
        height: 1
        opacity: 0.3

        anchors.top:parent.top
        anchors.topMargin: 40
    }
    BottomFooter{
        width: 320
        anchors.top:separator.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }


}
