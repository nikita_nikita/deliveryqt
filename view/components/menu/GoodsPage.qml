import QtQuick 2.12
import QtQuick.Layouts 1.12

Rectangle{
    id: root_component
    property alias model: repeater_products.model
    clip: false
    height: grid_items.height
    property int good_item_width:root_component.width>600?248:150
    readonly property int currentHeight: grid_items.height



    Rectangle{
        id:category_info
        anchors.top: parent.top
        width: root_component.width-30
        anchors.left: root_component.left
        anchors.leftMargin: 10
        radius: 10
        clip: true
        visible:categoryInfo.length === 0  ? false : true
        height: visible ? tttt.height+10 : 0
        color: "#542a69"
        opacity: 0.77

        Text {
            id:tttt
            color: "white"
            width: parent.width-7
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 5
            font.pointSize: 14
            wrapMode:  Text.WordWrap
            text: categoryInfo

        }
    }

    GridLayout{
        anchors.top: category_info.bottom
        id: grid_items
        anchors.horizontalCenter: parent.horizontalCenter
        columnSpacing: 10
        rowSpacing:  10

        columns: (root_component.width-20)/root_component.good_item_width
        Repeater{
            id: repeater_products

            GoodsItem{
                model: ({
                            name: name,
                            image: image,
                            price: price,
                            composition: composition?composition:addedCompositeString,
                            json: json,
                            quantity:quantity,
                            typeName: typeName,
                            volume: volume,
                            isSet: isSet,
                            benefit: benefit

                        })
                lastElement: {
                    if(index === repeater_products.model.count-1){
                        return true
                    }
                    if(repeater_products.model.count % 2 === 0&&index === repeater_products.model.count-1||
                      repeater_products.model.count % 2 ===0&&index === repeater_products.model.count-2){
                    return true
                    }
                    return false

                }
                Layout.fillHeight: true
                Layout.preferredWidth: root_component.good_item_width
                onClickedCopmposition: {

                    root_component.model.getInfoComposite(id);
                }
                // right separator
                Rectangle {
                    id: right_separator
                    anchors.left: parent.left
                    anchors.leftMargin: -8
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -10
                    width: 1
                    color: "#f2f2f2"
                    visible: ((index+1)%grid_items.columns)!=1
                    // opacity: 0.5
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -5
                    anchors.left: right_separator.left
                    anchors.right: parent.right
                    anchors.rightMargin: -5
                    height: 1
                    color: "#f2f2f2"
                    // opacity: 0.5

                }


            }

        }

    }


}


