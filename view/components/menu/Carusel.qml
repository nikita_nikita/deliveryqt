import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.12
import "../base"

KSwipeView{
    id: root_component
       // onCurrentItemChanged: root_component.height= currentItem.height
spacing: 10

    Repeater{
        id: carusel_repeater
        model: storage.offersBaner
        CaruselItem{
            pwidth: root_component.width-1
            //model: modelData
            offer_imageCatalog: imageCatalog
            Component.onCompleted: {
                if(root_component.height<height){
                    root_component.height=height
                }
            }

            onHeightChanged: {
                if(root_component.height<height){
                    root_component.height=height
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: drawer_offers.openPosition(root_component.currentIndex)
            }
        }

    }


}

