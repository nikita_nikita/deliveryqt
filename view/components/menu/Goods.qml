import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle  {
    id: root_component
    property alias model: repeater.model

    Connections{
        target: repeater.model
        onCurrentChanged:{
            console.log("curent change :" , current)
            swipe_pages.interactive = false
            swipe_pages.setCurrentIndex(current)
            swipe_pages.interactive = true
        }
    }

    height: swipe_pages.height
    clip: true
    SwipeView{
        id: swipe_pages
         property int minHeight:20

//        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height:  currentItem.height
        // currentIndex: repeater.model.current
        contentHeight: swipe_pages.currentItem.currentHeigh
        onCurrentIndexChanged:{
            swipe_pages.updateHeight()
            repeater.model.current = currentIndex
            console.log("swipe currentIndex changed:", currentIndex)
        }


        function updateHeight(){

            var _height = 0
            if(swipe_pages.currentItem && swipe_pages.currentItem.currentHeight){
                _height =swipe_pages.currentItem.currentHeight+50
            }
            if(_height<swipe_pages.minHeight) _height= minHeight
            swipe_pages.height = _height
        }
        Repeater{
            id: repeater
            GoodsPage{
                id: goods_page
                model: page
                onCurrentHeightChanged: swipe_pages.updateHeight()
//                categoryInfo: categoryInfo
            }

        }


    }

}


