import QtQuick 2.12
import "../base"

Item {
    id: root_component
    property string text: ""
    property bool active: false
    signal clicked()
    width: label.contentWidth
    KText{
        id: label
        anchors.centerIn: parent
        text: root_component.text
        opacity: root_component.active?1:0.3
        color: "#fff"
        Behavior on opacity {
               NumberAnimation { duration: 300 }
           }
    }


    MouseArea{
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: root_component.clicked()
    }
}
