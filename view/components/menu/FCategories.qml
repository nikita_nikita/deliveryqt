import QtQuick 2.12

Flickable{
    id: root_component
    contentWidth: category_row.width+20
    property bool isBack: false
    property alias model: category_repeater.model
    property var widthCategories: ([])

    function categoryClicked(cat){
        menuController.pages.setCurentCategory(cat, true)
        router.popToMain()
    }

    Behavior on contentX {
        NumberAnimation{duration: 500}
    }

    function back(){
        menuController.pages.setCurentCategory(category_repeater.model.parentCategory, false)
        router.popToMain()
    }
    MouseArea {
        anchors.fill: parent
    }


    Row{
        id: category_row
        height: parent.height
        spacing: 20
        Item{
            width: 20
            height: root_component.height
            visible: root_component.isBack
            Text{
                anchors.centerIn: parent
                text: "Все"
                color:"white"
            }
            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: root_component.back()
            }
        }
        Repeater{
            id: category_repeater
            FCategoryItem{
                text: categoryName
                height: root_component.height
                onClicked:root_component.categoryClicked(categoryAlias)
                active: root_component.model.current===index
                onActiveChanged: {
                    if(!active)  return;
                    var conX = x+width+40
                    if(root_component.width>0&&conX > root_component.width)
                    {
                        root_component.contentX =conX-root_component.width
                    }
                    else root_component.contentX=0

                }


            }

        }
    }


}
