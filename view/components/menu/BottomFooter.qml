import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Item {
    MouseArea{
    anchors.fill: parent}
    RowLayout{
        anchors.fill: parent

        BottomFItem{
            icon: "qrc:/view/img/clock.png"
            title: "Заказы"
            active: router.page==="orders"
            Layout.fillWidth: true
            onClicked: router.push("orders")
        }
        BottomFItem{
                        Layout.fillWidth: true
            icon: "qrc:/view/img/person.png"
            title: "Профиль"
            active: router.page==="profile"
            onClicked: router.push("profile")

        }

        BottomFItem{
            icon: "qrc:/view/img/search-icon.png"
            title: "Поиск"
            active: router.page==="search"
            onClicked: router.push("search")
                        Layout.fillWidth: true

        }
        BottomFItem{
            icon: "qrc:/view/img/basket.png"
            title: "Корзина"
            Layout.fillWidth: true
            heightImage: 19
            active: router.page==="cart"
            onClicked: router.push("cart")
            notifation: cart.model.number+cart.bonus.length
        }

    }

}
