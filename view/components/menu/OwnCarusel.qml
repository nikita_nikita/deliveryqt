import QtQuick 2.12
import QtQuick.Controls 2.5

Flickable{
    id: root_component
    height: images_row.height
    contentWidth: images_row.width
    //property var model: Array.isArray(storage.cityData.offers)?storage.cityData.offers.filter(function(offer) { return  offer.visibleBanner }):[]
    Row {
        id: images_row
        spacing: 10
        Repeater{
            id: carusel_repeater
            model: storage.offersBaner
            CaruselItem{
                pwidth: root_component.width-10
                //model: modelData
                offer_imageCatalog: imageCatalog
            }

        }
    }
}
