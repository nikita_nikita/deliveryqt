import QtQuick 2.12
import QtGraphicalEffects 1.12
import "../base"

Item{
    id: root_component
    width: offer_image.width
    height: offer_image.height
    property string offer_imageCatalog: "";
    property int pwidth: 0

    KImage {
        id: offer_image
        anchors.left: parent.left
       // anchors.right: parent.right
        width: root_component.pwidth<sourceSize.width? root_component.pwidth:sourceSize.width
        anchors.top: parent.top      
        imagefile: root_component.offer_imageCatalog
        fillMode: Image.PreserveAspectFit
        smooth: true
        visible: false


    }
    Image {
        id: offers_mask
        sourceSize: offer_image.sourceSize
        anchors.fill: offer_image
        source: "qrc:/view/img/mask.png"
        fillMode: Image.PreserveAspectFit
        visible: false
        smooth: true
    }
    OpacityMask {
           anchors.fill: offer_image
           source: offer_image
           maskSource: offers_mask
       }

}
