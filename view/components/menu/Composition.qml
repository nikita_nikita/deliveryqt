import QtQuick 2.0


Rectangle{
    property string comp_name: ""
    property bool hot_status: false
//    property variant peper: "🌶"
    height: 20
    width:hot_status ? name.width: name.width+10
    Text {
        id: name
        text: comp_name
        width: contentWidth
        font.pointSize: 12
    }
    Image {
        anchors.left: name.right
        source: "qrc:/view/img/product/hot.png"
        fillMode: Image.PreserveAspectFit
//      width: modelData.hot_status ?10 : 0
        width:hot_status?10:0

    }

}
