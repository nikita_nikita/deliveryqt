import QtQuick 2.12

Flickable {

    id: root_component
    contentHeight: content.height+200
    property var model: ({})
    property int level: 0
    clip: true

    Item {
        id: content
        height: image_carusel.height+goods.height
        width: root_component.width
        Carusel{
            id: image_carusel
            anchors.top: parent.top
           anchors.horizontalCenter: parent.horizontalCenter
        }
        Goods{
            id: goods
            anchors.top: image_carusel.bottom
            anchors.topMargin: 10
            height: root_component.height
            anchors.left: parent.left
            anchors.right: parent.right
            model: root_component.model            
        }
    }

}
