import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../base"
Item {
    id: root_component

    ButtonYollow {
        Layout.fillWidth: true
        text: "На главную"
        anchors.centerIn: parent

        onClicked: {
            router.popToMain();
        }
    }
}
