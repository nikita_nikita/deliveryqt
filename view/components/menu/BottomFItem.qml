import QtQuick 2.12
import QtGraphicalEffects 1.0
import "../base"
Item {
    id: root_component
    property string icon: ""
    property string title: ""
    property bool active: false
    property int heightImage: 14
    property string notifation: ""
    width: c_text.width>c_image.width?c_text.width:c_image.width
    signal clicked()
    height:38
    Rectangle {
        anchors.left: c_image.right
        anchors.leftMargin: 5
        anchors.top: c_image.top
        color: "transparent"
        border.width: 1
        border.color: "#FFC605"

        radius: 10
        height: 15
        width: notifation.width+10
        visible: root_component.notifation.length>0
        //opacity: c_image.opacity
        KText {
            id: notifation
            text: root_component.notifation
            anchors.centerIn: parent
            color: "#fff"
            font.pixelSize: 9
            //opacity: c_image.opacity
        }
    }
    Image {
        id: c_image
        source: root_component.icon

        height: root_component.heightImage
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: c_text.top
        anchors.bottomMargin: 6
        opacity: root_component.active?1:0.6

    }
    ColorOverlay{
        anchors.fill: c_image
        source:c_image
        color:"#FFC605"
        antialiasing: true
        visible: root_component.active
    }
    KText{
        id:c_text
        text: root_component.title
        color: "#fff"
        font.pixelSize: 10
        opacity: root_component.active?1:0.3
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
    }

    MouseArea{
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: root_component.clicked()
    }

}
