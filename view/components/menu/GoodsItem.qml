import QtQuick 2.12
import "../base"
import QtQuick.Layouts 1.12
Item {
    //Rectangle{
    //    border.color: "black"
    id: root_component
    width: 150

    property var model: ({})
    signal clickedCopmposition()
    property bool lastElement: false

    Layout.minimumHeight:{

                if(button_composit.visible&&lastElement){
                    return column.height+ product_cost.height+160
                }
        if(button_composit.visible){
            return column.height+ product_cost.height+80}

        return column.height+ product_cost.height+50
    }
//    Component.onCompleted: {}
    MouseArea {
        anchors.fill: parent
        onClicked: root_component.focus=true
    }
    ColumnLayout {
        id: column
        anchors.left: parent.left
        anchors.right: parent.right
        spacing: 0
        Row{
            z:2
            height: 20
            Image {
                source: "qrc:/view/img/product/new.png"
                visible: root_component.model.json.new
                fillMode: Image.PreserveAspectFit
                width: 20
            }
            Image {
                source: "qrc:/view/img/product/hit.png"
                visible: root_component.model.json.hit
                fillMode: Image.PreserveAspectFit
                width: 20
            }
            Image {
                source: "qrc:/view/img/product/hot.png"
                visible: root_component.model.json.hot
                fillMode: Image.PreserveAspectFit
                width: 20
            }

        }
        Item{
            id: image_conteiner

            Layout.fillWidth: true
            Layout.preferredHeight: product_image.height
            KImage {
                id: product_image
                width: column.width
                fillMode: Image.PreserveAspectFit
                imagefile: root_component.model.image

            }
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    root_component.clickedCopmposition()
                    parent.focus=true
                }
            }
        }
        RowLayout{
            id: product_name_row
            Layout.preferredWidth: column.width-40
            KText{
                id: product_name
                text: root_component.model.name
                wrapMode: Text.WordWrap
                Layout.preferredWidth: column.width-40
                font.pixelSize: 12
                font.weight: Font.Bold

                //Layout.maximumWidth:column.width
            }
            Image{
                source: "qrc:/view/img/info.png"
                Layout.preferredHeight: 20
                Layout.preferredWidth: 20
                Layout.alignment: Qt.AlignRight
                fillMode: Image.PreserveAspectFit
                visible: root_component.model.json.pfc|| root_component.model.json.weight|| root_component.model.json.caloric

                MouseArea {
                    anchors.fill: parent
                    onClicked:
                    {
                        var test  = root_component.model.json
                        pfs_modal.focus=!pfs_modal.focus
                    }
                }
                Rectangle {
                    id: pfs_modal
                    color: "#373535"
                    opacity: 0.9
                    width: column.width-10
                    height: pfs_grid.height+20
                    radius: 10
                    anchors.right: parent.right
                    anchors.bottom: parent.top
                    visible: focus
                    GridLayout{
                        id: pfs_grid
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width-10
                        columns: 2
                        KText{
                            id: fat_label
                            text: "Жиры"
                            color: "#fff"
                            font.pixelSize: 10
                            visible:fat_text.visible
                        }
                        KText{
                            id: fat_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.fat+" г":""
                            color: "#fff"
                            font.pixelSize: 10
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }
                        KText{
                            id: protein_label
                            text: "Белки"
                            color: "#fff"
                            font.pixelSize: 10
                            visible: protein_text.visible
                        }
                        KText{
                            id: protein_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.protein+" г":""
                            color: "#fff"
                            font.pixelSize: 10
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }

                        KText{
                            id: carbohydrates_label
                            text: "Углеводы"
                            color: "#fff"
                            font.pixelSize: 10
                            visible: carbohydrates_text.visible
                        }
                        KText{
                            id: carbohydrates_text
                            text: root_component.model.json.pfc?root_component.model.json.pfc.carbohydrates+" г":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }
                        KText{
                            id: weight_label
                            text: "Вес"
                            color: "#fff"
                            font.pixelSize: 10
                            visible: weight_text.visible
                        }
                        KText{
                            id: weight_text
                            text: root_component.model.json.weight?root_component.model.json.weight+" г":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }
                        KText{
                            id: caloric_label
                            text: "Калории"
                            color: "#fff"
                            font.pixelSize: 10
                            visible: caloric_text.visible
                        }
                        KText{
                            id: caloric_text
                            text: root_component.model.json.caloric?root_component.model.json.caloric+" Ккал":""
                            font.pixelSize: 10
                            color: "#fff"
                            Layout.alignment: Qt.AlignRight
                            visible: text.length>0
                        }


                    }
                }

            }
        }

        //обьем
        KText{
            id: product_volume
            text: root_component.model.volume+" л"
            Layout.maximumWidth:column.width
            wrapMode: Text.WordWrap
            font.pixelSize: 12
            color: "#878787"
            visible: root_component.model.volume>0
            // Layout.alignment: Qt.AlignHCenter
        }
        //информация о сете
        KText{
            id: product_set_info
            text:   root_component.model.json.number + " шт/ "+ root_component.model.json.weight + "г/ "+root_component.model.json.caloric + " ккал"
            Layout.maximumWidth:column.width
            wrapMode: Text.WordWrap
            font.pixelSize: 12
            color: "#878787"
            visible: root_component.model.json.number !== null
            height: visible ? 0:12

        }

        KText{
                    id: composition
                    text: root_component.model.composition
                    Layout.maximumWidth:column.width
                    wrapMode: Text.WordWrap
                    font.pixelSize: 12
                    color: "#878787"
                    visible: root_component.model.composition.length>0
                    MouseArea {
                        anchors.fill: parent
                        onClicked:{
                            menu.item_icon = root_component.model.image
                            root_component.clickedCopmposition()
                            parent.focus=true
                        }
                    }

                }
// состав
//        Flow {
//            width:  150
//            height: 200

//            Repeater{
//                model: root_component.model.composition

//                Composition{
//                comp_name: modelData.component_name
//                hot_status: modelData.hot_status
//                }


            //}
            //id: composition
            //            data: root_component.model.composition
          //  visible: root_component.model.composition.length>0

//            MouseArea {
//                anchors.fill: parent
//                onClicked:{
//                    root_component.clickedCopmposition()
//                    parent.focus=true
//                }
//            }

        //}

        /*Text{
                anchors.centerIn: parent

                Component.onCompleted: {
                    var compos = ""

                    for(var i =0; i <root_component.model.composition.lenght;i++ ){
                        if(root_component.model.composition[i]["hot_status"]){
                            compos +=root_component.model.composition[i]["name"] + " 🌶"
                        }else{
                            compos +=root_component.model.composition[i]["name"]

                        }
                    }
                    text = compos
                }


                //            Repeater{
                //                model:root_component.model.composition
                //                Text {
                //                    Component.onCompleted: console.log("1231<><>1211")


                //        //            text:hot_status ?name + " 🌶":name
                //                    text: name
                //                    font.pixelSize: 12
                //                }
                //            }
            }*/
    }




    KText{
        id: product_cost
        anchors.bottom:  parent.bottom
        anchors.bottomMargin:lastElement? 40 : 20
        anchors.topMargin: root_component.height-column.height-product_currency.height*3.5
        anchors.left: parent.left
        font.pixelSize: 12
        color: "#542A69";
        text: root_component.model.price
    }
    KText{
        id: product_currency
        anchors.left: product_cost.right
        anchors.bottom: product_cost.bottom

        text: " руб"
        font.pixelSize: 12
        color: "#542A69";
    }
    KText {
        id: product_old_cost
        anchors.bottom: product_cost.top
        anchors.left: product_cost.left
        anchors.bottomMargin: 0
        font.pixelSize: 10
        color: "#542A69";
        text: parseFloat(root_component.model.benefit)
        visible: root_component.model.isSet
    }


    KText{
        id: product_currency_old
        anchors.left: product_old_cost.right
        anchors.bottom: product_old_cost.bottom

        text: " руб"
        font.pixelSize: 10
        color: "#542A69";
        visible: product_old_cost.visible
    }
    Rectangle {

        anchors.left: product_old_cost.left
        anchors.right: product_currency_old.right
        height: 1
        color: "#542A69";
        anchors.verticalCenter: product_old_cost.verticalCenter
        visible: product_old_cost.visible
    }
    //для увиличения области нажатия на кнопку
    MouseArea{
        anchors.top: buttonAddToCard.top
        anchors.bottom: buttonAddToCard.bottom
        anchors.right: buttonAddToCard.left
        width: 50
        visible: buttonAddToCard.visible
        onClicked: {
            cart.add(root_component.model.json)
            console.log(root_component.model.json)

            parent.focus = true
        }

    }

    RoundButton{
        id: buttonAddToCard
        anchors.right: parent.right
        anchors.rightMargin: 10

        anchors.verticalCenter: product_cost.verticalCenter
        onClicked:{
            cart.add(root_component.model.json)
            console.log(root_component.model.json)
            parent.focus = true
        }
        visible: root_component.model.quantity === 0
    }

    Rectangle {
        id: button_composit
        height: 20;
        width: composit_text.width+10;
        anchors.bottom:  parent.bottom
        anchors.bottomMargin: lastElement ? 200:  40
        property var product: root_component.model.json
        border.width: 1
        border.color: "#dedede"
        visible: button_composit.product.added.noodle!==undefined || button_composit.product.added.souse!==undefined
        KText {
            id: composit_text
            text:{
                //  var product = root_component.model.json;
                if (button_composit.product.added.noodle!==undefined) {
                    return 'Собрать'
                }
                if (button_composit.product.added.souse!==undefined) {
                    return 'Выбрать соус'
                }
                return "";
            }

            anchors.centerIn: parent
            font.pixelSize: 12
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(button_composit.product.added.noodle!==undefined) {
                    router.pushToWokConstructor({   product: root_component.model.json,
                                                    quantity: 1,
                                                    operation: "add"
                                                })
                    return;
                }
                router.pushToConstructor({  product: root_component.model.json,
                                             quantity: 1,
                                             operation: "add"
                                         })
                parent.focus = true
            }
        }
    }

    KSpinBox {
        anchors.right: parent.right
        anchors.rightMargin: 10
//        anchors.bottom: parent
        anchors.top: parent.top
        anchors.topMargin:  lastElement ? column.height+50: 50
        anchors.verticalCenter: product_cost.verticalCenter
        value: root_component.model.quantity
        onValueChanged:{
            if(value!==root_component.model.quantity){
                cart.setQuantity(root_component.model.json, value, true)
            }
            parent.focus = true
        }
        visible: !buttonAddToCard.visible
    }

}
