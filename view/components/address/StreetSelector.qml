import QtQuick 2.12
import "../base"
import QtQuick.Controls 2.12

Rectangle {
    id: root_component
    property alias model: street_column.model
    height: street_column.count>5?5*40:street_column.count*40
    signal selectSreet(var street);
    ListView{
        id: street_column
        width: root_component.width
        height: parent.height
        clip: true
        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
        delegate:  Rectangle {

            height: 40
            width: street_column.width

            property string tyty: name
            KText{
                id: adr_text
                text: typeShort+" "+name
                anchors.fill: parent

                anchors.leftMargin: 10
                anchors.rightMargin: 10
                anchors.bottomMargin: 2
                verticalAlignment: Text.AlignVCenter

                wrapMode:Text.WordWrap
                font.pixelSize: 12
                color: "#000"

            }
            HorisontalSeparator{
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right

            }
            MouseArea {
                anchors.fill: parent
                onClicked: root_component.selectSreet(json)
            }

        }


    }



    HorisontalSeparator {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
    VerticalSeparator{
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left

    }
    VerticalSeparator{
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right

    }

}

