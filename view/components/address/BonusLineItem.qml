import QtQuick 2.12
import "../base"

Rectangle {
    id: root_component
    property alias bonus: bonus_value.text
    height: 50
    signal clicked();
    KText{
        id: adr_text
        text: "Бонусы"
        wrapMode:Text.WordWrap
        color: "#542A69"
        font.pointSize: 14
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
    }
    KText{
        id: bonus_value
        text: "127"
        wrapMode:Text.WordWrap
        color: "#542A69"
        font.pointSize: 20
        anchors.right: parent.right
        font.weight: Font.Bold
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
    }

    HorisontalSeparator{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
    MouseArea {
        anchors.fill: parent
        onClicked: root_component.clicked();
    }
}
