import QtQuick 2.12
import "../base"

Rectangle {
    id: root_component
    property string text: ""
    height: adr_text.height+10
    signal clicked();
    signal removed();
    KText{
        id: adr_text
        text: root_component.text
        wrapMode:Text.WordWrap
        //width: root_component.width
        anchors.left: parent.left
        anchors.right: parent.right
        font.pixelSize: 11
        color: "#000"
    }
   /* ButtonClose {
        id: button_remove
        anchors.right: parent.right
        anchors.verticalCenter: adr_text.verticalCenter
        onClicked: root_component.removed();
    }*/

    HorisontalSeparator{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
    MouseArea {
        anchors.fill: adr_text
        onClicked: root_component.clicked();
    }
}
