import QtQuick 2.12
import "../components/menu"
import QtQuick.Layouts 1.12
import "../components/search"

Rectangle{
    id:root_component
    property bool searchFocus: false
    //        y: menuController.nameSearch.length > 1 ? 5 : center
    //        Behavior on y {
    //            NumberAnimation {  duration: 900 }
    //        }

    property int good_item_width:root_component.width>600?248:150



    SearchElement{
        id:search
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.topMargin: 10
        anchors.top: parent.top
        focus: root_component.searchFocus
        onFocusChanged: console.log("see on mee, i chenged focus")
        //        y: menuController.nameSearch.length > 1 ? 5 : center
        //        Behavior on y {
        //            NumberAnimation {  duration: 900 }
        //        }

    }

    //    Flickable{
    //        anchors.top:search.bottom

    //        anchors.left: parent.left
    //        anchors.right: parent.right
    //        anchors.bottom: parent.bottom
    //        contentWidth: parent.width
    //        clip: true


    Flickable{
        anchors.top:search.bottom
        anchors.bottom: root_component.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        contentHeight: grid_items.height
        clip: true
    GridLayout{
//        width: root_component.width*0.9
        id:grid_items


        anchors.horizontalCenter: parent.horizontalCenter
//        columnSpacing: 10
//        rowSpacing:  10
        columns: 2
//        width: root_component.width
        visible: menuController.nameSearch.length > 1
        function updateHeight(){
        }
        Repeater{
            id: repeater_products
            model: menuController.searchProductModel
            GoodsItem{
                model: ({
                            name: name,
                            image: image,
                            price: price,
                            composition: composition?composition:addedCompositeString,
                            json: json,
                            quantity:quantity,
                            typeName: typeName,
                            volume: volume,
                            isSet: isSet,
                            benefit: benefit

                        })
                Layout.fillHeight: true
                Layout.preferredWidth: root_component.good_item_width
                onClickedCopmposition: menuController.showInfoComposition(id);

                // right separator
                Rectangle {
                    id: right_separator
                    anchors.left: parent.left
                    anchors.leftMargin: -8
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -10
                    width: 1
                    color: "#f2f2f2"
                    visible: ((index+1)%grid_items.columns)!=1
                    // opacity: 0.5
                }
                Rectangle {

                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -5
                    anchors.left: right_separator.left
                    anchors.right: parent.right
                    anchors.rightMargin: -5
                    height: 1
                    color: "#f2f2f2"
                    // opacity: 0.5

                }


            }

        }
    }

//    }
//    }


    //    }

 }
}
