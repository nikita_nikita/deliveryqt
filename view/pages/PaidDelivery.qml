import QtQuick 2.12
import QtQuick.Controls 2.5
import "../components/base"

Item {
    id: root_component
    property var amount: 0
    visible:cart.deliveryCost>0&&statusStorage.checkStreetResponse.amount!==amount&&cart.conteiner.length>0||cart.deliveryCost>0&&statusStorage.checkStreetResponse.amount!==amount&&cart.bonus.length>0


    Rectangle {
        //background
        MouseArea{anchors.fill: parent}
        anchors.fill: parent
        color: "#000"
        opacity: 0.4

    }
     Rectangle {
         width: 300
         height: column.height+40
         anchors.centerIn: parent
         radius: 10

         Column {
             id: column
             anchors.left: parent.left
             anchors.right: parent.right
             anchors.verticalCenter: parent.verticalCenter
             spacing: 10
               KText {
                   text: "Минимальная сумма заказа на бесплатную доставку по вашему адресу составляет "+statusStorage.checkStreetResponse.amount+"  руб."
                   anchors.left: parent.left
                   anchors.right: parent.right
                   anchors.leftMargin: 10
                   anchors.rightMargin: 10
                   wrapMode:Text.WordWrap
                   horizontalAlignment: Text.AlignHCenter
                   font.pointSize: 10
               }
               ButtonYollow {
                   text: "Ок"
                   anchors.horizontalCenter: parent.horizontalCenter
                   onClicked: root_component.amount = statusStorage.checkStreetResponse.amount
               }
         }
     }


}
