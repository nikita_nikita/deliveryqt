import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/wok"

Flickable {
    id:root_component
    contentHeight: content_column.height+150
    clip: true
    property real dopCost: getDopCost();
    onVisibleChanged: {
        if(!visible) return;

        var array_topings  = getStructTopingList();
        var options = {
            price: 0,
            added: {
                doubleMeat: 0,
                doubleVeg:0,
                noodle: 0,
                toping: [],
            }
        }

        if(router.wokData.options!==undefined){
            options =router.wokData.options
        }
        setOptions(options, array_topings)
    }
    function setOptions(options, array_topings){
        if(options.added) {
            doubleMeat.checked=options.added.doubleMeat!==0
            doubleVegetable.checked=options.added.doubleVeg!==0
            if(options.noodle) noodleList.checked =options.noodle;
            if(options.toping===undefined) {
                options.toping =[];
            }

            for(var i=0; i<array_topings.length; i++){
                var c_toping = array_topings[i];
                var toping = options.added.toping.find(x=>x.item.id===c_toping.item.id);
                if(toping) array_topings[i] = toping;
                else array_topings[i].quantity=0;
            }
            column_toping.array_data = array_topings;
        }

    }
    function getStructTopingList(){
        var result = [];
        if(router.wokData.product.added&&router.wokData.product.added.toping){
            router.wokData.product.added.toping.forEach(function(item){
              result.push({item: item, quantity:  0})
            });
        }
        return result;
    }

    function getCartOption(){
        var result = {
            price: getDopCost(),
            added: {
                doubleMeat: doubleMeat.checked?router.wokData.product.added.meat.product.id:0,
                doubleVeg:doubleVegetable.checked?router.wokData.product.added.vegetable.product.id:0,
                noodle: noodleList.checked,
                toping: getTopingList(),
            }
        }
        return result;
    }
    function getTopingList(){
        var result = [];
        var test  = column_toping.array_data;
        column_toping.array_data.forEach(function(item){
            if(item.quantity>0){
                result.push(item);
            }
        })
        return result;
    }

    function getDopCost(){
        var result =0
        column_toping.array_cost.forEach(function(item){
            result += item;
        })
        if(doubleMeat.checked) {
            result += router.wokData.product.added.meat.product.sity_info.price;
        }
        if(doubleVegetable.checked){
            result+=router.wokData.product.added.vegetable.product.sity_info.price
        }

        return result;
    }

    ColumnLayout {
        id: content_column
        width: parent.width-30
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 30
        RowLayout{
            spacing: 30
            Layout.topMargin: 35
            Layout.fillWidth: true
            KImage {
                imagefile: root_component.visible?router.wokData.product.images[0]:""
                fillMode: Image.PreserveAspectFit
                Layout.preferredWidth: root_component.width>320?150:100
                Layout.preferredHeight: wok_data_column.height

            }
            Column{
                id: wok_data_column
                Layout.fillWidth: true
                spacing: 10
                KText {
                    text: root_component.visible?router.wokData.product.name:""
                    color: "#542A69"
                    font.pixelSize: root_component.width>320?22:12
                    width: parent.width
                    wrapMode: Text.WordWrap

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {                          
                            var price = router.wokData.product.sity_info.price;
                            var doptCost = root_component.dopCost;                            
                        }
                    }
                }
                KText {
                    font.pointSize: root_component.width>320?12:9
                    color: "#878787";
                    text: "1 шт / 440 г / 225 ккал"
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
                KText {
                    text: root_component.visible?(router.wokData.product.sity_info.price+root_component.dopCost)+"руб.":""
                    font.pointSize: root_component.width>320?24:14
                    color: "#542A69"
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
            }



        }
        KText {
            text: "Заменить лапшу или рис"
            color: "#542A69"
            font.pointSize: 16

        }

        Flow{
            id: noodleList
            property int checked: 0
            Layout.fillWidth: true
            spacing: 30
            // лапша
            Repeater {

                model:  root_component.visible?router.wokData.product.added.noodle:0
                NoodleItem {
                    model: modelData
                    checked: noodleList.checked===modelData.product.id
                    onCheckedChanged:
                    {
                        if(checked)
                            noodleList.checked=modelData.product.id
                    }
                    Component.onCompleted: {
                        if(parseInt(modelData.selected)&&noodleList.checked==0){
                            noodleList.checked=modelData.product.id
                        }
                    }
                }
            }
        }
        KText {
            text: "Добавить по вкусу"
        }
        ColumnLayout{
            id: column_toping
            property var array_data: [];
            property var array_cost: [];


            Repeater {
                model:column_toping.array_data
                DopItem {
                    model: modelData.item
                    Layout.fillWidth: true
                    onCostChanged:{
                        column_toping.array_cost[index] = cost
                        root_component.dopCost = root_component.getDopCost();
                    }
                    value:  column_toping.array_data[index].quantity

                    onValueChanged: column_toping.array_data[index].quantity = value
                    Component.onCompleted: {
                        column_toping.array_cost[index] = cost
                    }
                }
            }

        }
        GridLayout {
            id: double_dop_columns
            Layout.preferredWidth: parent.width

            columns: root_component.width>550?2:1
            columnSpacing: 30
            DoubleDopItem{
                id: doubleMeat
                visible: root_component.visible&& router.wokData.product.added.meat!==undefined
                title: "Удвоить мясо"
                text: root_component.visible? router.wokData.product.added.meat.product.name:""
                cost:  root_component.visible?router.wokData.product.added.meat.product.sity_info.price+" руб":""
                Layout.fillWidth: double_dop_columns.columns==1
                fullWidth: double_dop_columns.columns==1
                Layout.preferredWidth: minW
                onCheckedChanged: {
                    root_component.dopCost = root_component.getDopCost();
                }
            }
            HorisontalSeparator{
                Layout.fillWidth: true
                visible: double_dop_columns.columns==1
            }

            DoubleDopItem{
                id: doubleVegetable
                visible:  root_component.visible&&router.wokData.product.added.vegetable!==undefined
                title: "Удвоить овощи"
                text:  root_component.visible?router.wokData.product.added.vegetable.product.name:""
                cost:  root_component.visible? router.wokData.product.added.vegetable.product.sity_info.price+" руб":""
                Layout.fillWidth: double_dop_columns.columns==1
                fullWidth: double_dop_columns.columns==1
                Layout.preferredWidth: minW
                onCheckedChanged: {
                    root_component.dopCost = root_component.getDopCost();
                }
            }

        }
        ButtonYollow {
            Layout.alignment: Qt.AlignHCenter
            text: router.wokData.operation==="edit"?"Сохранить":"В корзину"
            onClicked:{
                if(router.wokData.operation==="edit"){
                    cart.setOptions(router.wokData.index, getCartOption());
                }
                else {
                    cart.addWithOptions(router.wokData.product, getCartOption())
                }

                router.pop();
            }

        }


    }
}
