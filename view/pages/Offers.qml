import QtQuick 2.12
import "../components/offers"
import QtQuick.Layouts 1.12
Item {
    id: root_component
    Flickable {
        anchors.fill: parent
        anchors.topMargin: 20
        clip:  true
        contentHeight: grid_offers.height+150
        GridLayout {
            id: grid_offers
            columns: {
                if(root_component.width<=320) return 1
                return root_component.width/320
            }
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            columnSpacing: 40
            rowSpacing: 40
            Repeater {
                model: storage.offersCatalog
                OfferItem {
                    Layout.fillWidth: true                  
                    image: imageCatalog
                    offer_name: name
                    offer_description: description
                    offer_promocode:  promocode
                    onClickedPromocode: {
                        cart.promocode = promocode
                        global_tooltip.text = "Промокод применен"
                        global_tooltip.visible = true
                    }


                }
            }

        }
    }

}
