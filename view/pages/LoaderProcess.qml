import QtQuick 2.12
import QtQuick.Controls 2.5

Item {
    id: root_component

    Rectangle {
        anchors.fill: parent
        color: "#000"
        opacity: 0.4
    }
    Item {
        anchors.fill: parent
        anchors.bottomMargin: 120
        BusyIndicator {
          anchors.centerIn: parent
          running: true
        }

    }
//    Connections {
//        target: payService
//        onStartLoaded:  router.pageLoadedVisible= true
//        onSuccess: router.pageLoadedVisible= false
//        onError: router.pageLoadedVisible= false
//        onOpenUrl: router.pageLoadedVisible= false
//        onErrorPay: router.pageLoadedVisible= false
//        onEndLoaded: router.pageLoadedVisible= false
//    }
    Connections {
        target: kapibaras
        onStartLoaded:  router.pageLoadedVisible= true
        onEndLoaded: router.pageLoadedVisible= false
    }

}
