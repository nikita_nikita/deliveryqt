import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/payment"
import "../components/cart"
import "../components/checkout"

Item {
    id: page_checkout
    Flickable{
        anchors.topMargin: 10
        anchors.fill: parent
        anchors.bottomMargin: 105
        contentWidth: parent.width
        contentHeight: column.height+column_user_data.height+150+payColumn.height
        clip: true
        Column{
            id:column
            width: parent.width*0.9
            anchors.left: parent.left
            anchors.leftMargin: parent.width/20
            Rectangle{
                width: parent.width
                height: 65
                radius: 15
                color: "#f7f8f8"
                Rectangle{
                    anchors.bottom: parent.bottom
                    height: 30
                    color: "#f7f8f8"
                    width: parent.width
                }
                Text {
                    anchors.fill: parent
                    anchors.margins: 20
                    font.pointSize: 30
                    text: "<b>Ваш заказ</b>"
                }
                Rectangle{
                    anchors.right: parent.right
                    anchors.top:  parent.top
                    anchors.topMargin: 20
                    anchors.rightMargin: 10
                    width: 55
                    height: 25
                    radius: 13
                    color: "purple"
                    MouseArea{
                        anchors.fill: parent
                        onClicked: router.push("cart")}
                    Text {
                        anchors.centerIn: parent
                        color: "white"
                        text: "ред."
                    }
                }
            }
            Repeater{
                model:cart.conteiner
                Rectangle{
                    id:root_component
                    function createdOptions() {
                        var createdName = "";
                        var test = root_component.modelData;
                        if(modelData.options!==undefined){
                            var obj = modelData.options.added;
                            var product = modelData.product;
                            for (var key in obj) {
                                if (obj[key] === null) continue;
                                if (Array.isArray(obj[key])) {
                                    for (var i = 0; i < obj[key].length; i++) {
                                        createdName += obj[key][i].item.product.name+'('+obj[key][i].quantity+'), ';
                                    }
                                    continue;
                                }

                                if (key==='doubleMeat' || key==='doubleVeg') {
                                    if (key==='doubleMeat' && obj[key]) {
                                        createdName +=product.added.meat.product.name+', ';
                                    }
                                    if (key==='doubleVeg' && obj[key]) {
                                        createdName +=product.added.vegetable.product.name+', ';
                                    }
                                    continue;
                                }
                                if(key==="noodle"&&obj.noodle){
                                    var nooldle= product.added.noodle.find(x=>x.product.id === obj.noodle);
                                    if(!nooldle.selected){
                                        createdName += nooldle.product.name+', ';
                                    }

                                }
                                if(key==="souse"&&obj.souse) {
                                    var souse  =  product.added.souse.find(x=>x.product.id === obj.souse);
                                    if(!souse.selected){
                                        createdName += souse.product.name+', ';
                                    }
                                }
                            }

                        }
                        console.log(createdName)
                        return createdName.slice(0, -2);
                    }

                    width: parent.width
                    height: 30+option_aded.height
                    color: "#f7f8f8"
                    KText {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        height: 20
                        text: modelData.product.name + " " + modelData.quantity+" шт."
                        width: parent.width*0.7
                        wrapMode: Text.WordWrap
                        font.bold:true
                    }
                    KText{
                        anchors.top: parent.top
                        anchors.topMargin: 21
                        id:option_aded
                        anchors.left: parent.left
                        anchors.leftMargin: 35
                        font.pointSize: 14
                        text:root_component.createdOptions()
                        width: 200
                        visible: this.text !== ''
                        height: visible? 20: 0
                    }
                    KText {
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        width: 100
                        horizontalAlignment: Text.AlignRight
                        text:  modelData.quantity *modelData.product.sity_info.price + " руб."
                    }
                }
            }
            Repeater{
                model:cart.bonus
                Rectangle{
                    width: parent.width
                    height: 45
                    color: "#f7f8f8"
                    KText {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        height: 30
                        text: modelData.product.name
                        width: contentWidth
                        font.bold:true
                    }

                }
            }
            Rectangle{
                width: parent.width*0.8
                anchors.left: parent.left
                anchors.leftMargin: parent.width/10
                height: 1
                color: "gray"
            }
            Rectangle{
                width: parent.width
                height: 30+bottom_sum.height

                radius: 15
                color: "#f7f8f8"
                Rectangle{
                    anchors.top:  parent.top
                    height: 15
                    color: "#f7f8f8"
                    width: parent.width
                }
                KText {
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    width: 100
                    //                    height: 30
                    text:cart.bonusPay === 0 ? "<br>Сумма к оплате":  "<br>Сумма к оплате<br><br>Будет списано "
                }
                KText {
                    id:bottom_sum
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    width: 100
                    //                    height: cart.bonusPay === 0 ? 15 : 60
                    horizontalAlignment: Text.AlignRight
                    text: {
                        var result=  cart.endCost-cart.offerValue-cart.bonusPay
                        if(cart.bonusPay !== 0){
                            result = "<br><b>"+parseFloat(result.toFixed(2))+"</b> руб.<br><br><b>"+cart.bonusPay+"</b> бал."

                        }else{
                            result = "<br>"+parseFloat(result.toFixed(2))+" руб."

                        }
                        return result.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                    }
                }

            }


        }

        Rectangle{
            id: user_data
            width: parent.width*0.9
            anchors.left: parent.left
            anchors.leftMargin: parent.width/20
            anchors.top: column.bottom
            anchors.topMargin: 30
            radius: 15
            height: column_user_data.height+40
            color: "#f7f8f8"
            Column{
                anchors.top: parent.top
                anchors.topMargin: 20
                width: parent.width
                spacing: 10
                id:column_user_data
                KText {
                    //                    font.bold:true
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    height: text.height

                    text: cart.userName
                }
                KText {
                    //                    font.bold:true
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    height: text.height

                    text: cart.userPhone
                }

                Rectangle{
                    id:horizontal_separator
                    width: parent.width
                    height: 1
                    color: "gray"
                }
                KText {
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    font.pointSize: 20
                    height: 20
                    id: order_type_text
                    text: cart.orderType === "delivery"? "Адрес доставки":"Точка самовывоза"
                }
                KText {
                    height: 40
                    anchors.left: parent.left
                    anchors.leftMargin: 20

                    text: cart.orderType === "delivery"? cart.adress : cart.takeAwayString
                }
                KText {
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    text: cart.time === "Как можно скорее"?"Как можно скорее":"К "+cart.time

                }
                KText {

                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    width: 300
                    wrapMode: Text.WordWrap
                    text: cart.comment === ""?"Коментарий не указан":"Коментарий <br>"+cart.comment

                }
                KText {
                    visible: cart.promocode !==  ""
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    font.pointSize: 20
                    height:visible? 20:0
                    text:"Промокод - <b>"+cart.promocode+"</b>"
                }
                PromoCodeInfo{
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    width: parent.width-40
                }
            }
        }
        Rectangle{
            width: parent.width*0.9
            anchors.left: parent.left
            anchors.leftMargin: parent.width/20
            anchors.top: user_data.bottom
            anchors.topMargin: 30
            height:payColumn.height+40
            radius: 15
            color: "#f7f8f8"

            Column{
                id:payColumn
                spacing: 20
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: 20

                KRadioButton {
                    size:20
                    checked: cart.payMethod === 3
                    text: cart.orderType === "delivery"? "Наличными": "На кассе"
                    width: 300
                    height: visible ?30:0
                    visible: paymentController.pickup_cash&&cart.orderType !== "delivery"||paymentController.delivery_cash &&cart.orderType === "delivery"&&!deliveryController.contactless_status
                    onCheckedChanged:{
                        if (cart.payMethod!==3) cart.payMethod = 3
                    }
                }
                Rectangle{
                    color: "silver"
                    width: page_checkout.width*0.9
                    height: visible?90:0
                    visible: cart.payMethod === 3 && cart.orderType === "delivery"
                    anchors.left: parent.left
                    anchors.leftMargin: -20
                    Cash{
                     }
                }
                KRadioButton {

                    id:cartOnline
                    checked: cart.payMethod ===1
                    text: "Картой онлайн"
                    width: 300
                    size:20
                    visible: paymentController.pickup_online&&cart.orderType !=="delivery" ||paymentController.delivery_online &&cart.orderType ==="delivery"
                    height: visible? 30: 0
                    onCheckedChanged:{
                        if (cart.payMethod!==1) cart.payMethod = 1
                    }
                }
                Rectangle{
                    anchors.left: parent.left
                    anchors.leftMargin: -20
                    color: "silver"
                    width: page_checkout.width*0.9
                    height: visible?250:0
                    visible: cart.payMethod === 1&&cartOnline.visible
                    PlatformPay{
                        anchors.centerIn: parent
                    }
                }
                KRadioButton {
                    size:20
                    checked: cart.payMethod === 2
                    text: "Картой курьеру"
                    width: 300
                    height: 30
                    visible: paymentController.delivery_card&&!deliveryController.contactless_status
                    onCheckedChanged:{
                        if (cart.payMethod!==2) cart.payMethod = 2
                    }
                }
                KRadioButton {
                    size:20
                    checked: cart.payMethod === 4
                    text: "GooglePay"
                    width: 300
                    height: 30
                    visible:   paymentController.googlePayButton&&cart.orderType==="delivery"&&paymentController.delivery_online||
                               paymentController.googlePayButton&&cart.orderType!=="delivery"&&paymentController.pickup_online
                    onCheckedChanged:{
                        if (cart.payMethod!==4) cart.payMethod = 4
                    }
                }
                KRadioButton {
                    size:20
                    checked: cart.payMethod === 5
                    visible: paymentController.applePayButton&&cart.orderType==="delivery"&&paymentController.delivery_online||
                             paymentController.applePayButton&&cart.orderType!=="delivery"&&paymentController.pickup_online
                    text: "ApplePay"
                    width: 300
                    height: 30
                    onCheckedChanged:{
                        if (cart.payMethod!==5) cart.payMethod = 5
                    }
                }


            }
        }
    }
    Rectangle{
        id:main
        anchors.fill: parent

        property bool delivery: deliveryController.allowDelivery
        property bool pickUp: deliveryController.allowPickUP

        visible: deliveryController.deliveryNotAwalable

            MouseArea{
            anchors.fill: parent
        }
        Rectangle{

            anchors.fill: parent
            color: "gray"
            opacity: 0.5
        }
        Rectangle{
            width: 250
            height: 121
            anchors.centerIn: parent
            color: "white"
            radius: 20


            Column{

                Text{
                    width:250
                    height: 80
                    horizontalAlignment: Text.AlignHCenter
                    text: !main.delivery  ? "<br>Доставка в данный<br> момент недоступна":"<br>Свамовывоз в данный<br> момент недоступен"
                }


                Rectangle{
                    height: 1
                    width: 250
                    color: "gray"}
                Rectangle{
                    width: 250
                    height: 40
                    radius: 20
                    Text {
                        anchors.centerIn: parent
                        text: "Принять"
                        font.bold: true

                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked:{
                            main.visible = false
                            router.push("checkout")

                        }

                    }
                }
            }
        }
    }
//    LoaderProcess{
//        anchors.fill: parent
////        visible: deliveryController.loaderProcess

//        visible:  false
//    }
}
