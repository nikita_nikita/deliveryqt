import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/checkout"
Rectangle {
    id: page_checkout
    property bool delivery: deliveryController.allowDelivery
    property bool pickUp: deliveryController.allowPickUP
    onDeliveryChanged: {
        if(delivery && pickUp){
        return}
        if(!delivery){
            deliveryController.tab_index= 1
        }else{
            deliveryController.tab_index= 0
        }
    }
    Component.onCompleted: {
//        deliveryController.tab_index = 1
        if(!delivery && !pickUp){
            if(!delivery){
                deliveryController.tab_index = 1

            }
            if(!pickUp ){
                deliveryController.tab_index = 0
            }
        }
    }
    onPickUpChanged: {
        if(delivery && pickUp){
        return}
        if(pickUp){
            deliveryController.tab_index= 1
        }else{
            deliveryController.tab_index= 0

        }
    }
    Connections{
        target: storage
    }
    KTabBar {
        id: type_delivery_tabbar
        anchors.horizontalCenter: parent.horizontalCenter
        currentIndex: deliveryController.tab_index
        onCurrentIndexChanged: {
            console.log("type_delivery_tabbar changed >> "+  currentIndex)
            deliveryController.tab_index= currentIndex
        }
        visible: deliveryController.allowDelivery||deliveryController.allowPickUP
        KTabButton {

            //marker
            visible:deliveryController.allowDelivery

            id: button_delivery
            text: "Доставка"
            width: visible ? undefined : 0

        }
        KTabButton {
            id: button_pickup
            text: "Заберу сам"
            visible: deliveryController.allowPickUP
            width: visible ? undefined : 0
        }
    }
    Rectangle {
        border.width: page_checkout.width>600?1:0
        border.color: "#F2F2F2"
        width:page_checkout.width>600?600:320

        anchors.top: type_delivery_tabbar.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        clip: true
        SwipeView {
            id: type_deliver_swipe
            visible:type_delivery_tabbar.visible
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 40
            anchors.bottom: parent.bottom
            currentIndex: type_delivery_tabbar.currentIndex
            onCurrentIndexChanged: {
                cart.validCheckOut = currentItem.valid
            }


            interactive: false
            Delivery {
                property string orderType: "delivery"
                onValidChanged: {
                    if(SwipeView.isCurrentItem) {
                        cart.validCheckOut=valid
                    }
                }

            }
            TakeAway {
                property string orderType: "take-away"
                onValidChanged: {
                    if(SwipeView.isCurrentItem) {
                        cart.validCheckOut=valid
                    }
                }
            }

        }

    }
    Rectangle {
        visible: !type_deliver_swipe.visible
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top

        anchors.bottom: parent.bottom
        ColumnLayout {
            anchors.top: parent.top
            anchors.topMargin: 60
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            KText{
                wrapMode: Text.WordWrap
                color: "#542a69"
                font.pixelSize: 40
                text: "Внимание!"
                Layout.fillWidth: true
            }
            KText{
                wrapMode: Text.WordWrap
                Layout.fillWidth: true
                color: "#542a69"
                font.pixelSize: 14
                visible: !not_worktime.visible
                text: "Дорогой друг! К сожалению, мы временно не принимаем заказы через приложение! Более подробную информацию Вы можете получить по телефону."
            }
            KText{
                id: not_worktime
                wrapMode: Text.WordWrap
                Layout.fillWidth: true
                color: "#542a69"
                font.pixelSize: 14
                visible: !deliveryController.timeWork
                text: "Дорогой друг! На сегодня мы закрыты, ждем вас завтра."
            }
        }

    }
    Item{
        visible: type_delivery_tabbar.currentIndex==0&&cart.model.notDeliverySeparately
        anchors.fill: parent
        Rectangle {
            anchors.fill: parent
            color: "#373535"
            opacity: 0.5
        }
        Rectangle{
            anchors.horizontalCenter: parent.horizontalCenter
            y: parent.height*0.30
            width: 300
            radius: 10
            height: column_not_delivery.height+30

            Column{
                id: column_not_delivery
                width: parent.width-20
                spacing: 10
                anchors.centerIn: parent
                KText{
                    id: text_notDelivery
                    text: "Некоторые категории (топинги, напитки, подарки/сувениры) доставляются только с заказом нашей продукции."
                    width: parent.width
                    wrapMode: Text.WordWrap
                    font.pointSize: 12
                }
                ButtonYollow{
                    text: "Хорошо"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked:{
                        if(deliveryController.allowPickUP){
                            deliveryController.tab_index = 1

                        }else{
                            router.pop()
                        }}
                }
            }
        }
    }
    KTumblerTime{
        id: tumbler_time
        anchors.fill: parent
        visible: false
    }


}
