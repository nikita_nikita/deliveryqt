import QtQuick 2.12
import "../components/base"
import "../components/cart"
import "../components/checkout"

Item {

    id:root_component
    property bool noGetMessasge: cartController.zeroToppingMessage
    Item{
        id: item_cart_isEmpy
        anchors.fill: parent
        anchors.bottomMargin: 150
        visible: cart.model.number ===0&& cart.bonus.length===0
        Column {
            anchors.centerIn: parent
            spacing: 20
            Image{
                source: "qrc:/view/img/logo/bw.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            KText{
                text: "Ваша корзина пуста"
                font.pixelSize: 16
                color: "#A8A8A8"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ButtonYollow{
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Вернуться в меню"
                onClicked: router.popToMain();
            }
            PromoCode{
                anchors.horizontalCenter: parent.horizontalCenter

            }

        }

    }



    Flickable {
        id: cart_flickable
        anchors.fill: parent
        anchors.topMargin: 20

        contentHeight: content.height+200
        clip: true
        // header
        visible: !item_cart_isEmpy.visible

        Column {
            id: content
            width:cart_flickable.width
            spacing: 5
            CartInfoPanel{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 20
            }
            KText {
                text: "Товары"
                font.pixelSize: 16
                font.weight: Font.Bold
                anchors.left: parent.left
                anchors.leftMargin: 15
            }
            CartGoods {
                id: goods_list
                width: cart_flickable.width
            }
            Rectangle {
                height: 55
                width: parent.width
//                color: "silver"
                Row {
                    id:person_number_row
                    anchors.verticalCenter: parent.verticalCenter

                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    spacing: 20
                    KText {
                        text: "Количество персон"
                        font.pixelSize: 12
                        font.weight: Font.Bold
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    KSpinBox {
                        value: cart.personCount
                        font.pixelSize: 16
                        min:1
//                        canToZero: false
                        anchors.verticalCenter: parent.verticalCenter
                        onValueChanged: {
                            cart.personCount = value
                        }
                    }
                    Item {
                        width: person_number_row.width>350?110:50
                        height: 5
                    }
                }
                HorisontalSeparator {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                }
            }
            Item {
                height: promocode_row.height+10
                width: cart_flickable.width

                PromoCode{
                    anchors.left: parent.left
                    anchors.leftMargin: 50
                    id:promocode_row
                    width: parent.width

                }
                HorisontalSeparator {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: promocode_row.bottom
                    anchors.topMargin: promocode_row.status?10:0
                }
            }
            Item {
                width: parent.width
                height: 5
            }
            ServiceKit{
                visible:cartController.serviceKit.count > 0&&!deliveryController.newToppingLogic
                width: cart_flickable.width
            }
            NewServceKit{
                visible:deliveryController.newToppingLogic
                width: cart_flickable.width
            }

            CartToppings{
                width: cart_flickable.width
            }
        }


    }
    Rectangle{
        visible: cartController.zeroToppingMessage
        anchors.fill: parent
        opacity: 0.7
        color: "silver"
    }
    Rectangle{
        visible: cartController.zeroToppingMessage
        anchors.centerIn: parent
        width: 250
        height: 115
        radius: 12
        Text {
            text: "Вы не выбрали<br>ни одного топинга."
            horizontalAlignment: Text.AlignHCenter
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: 15
            height: 60

        }
        Rectangle{
             color: "#FFC605"
            anchors.horizontalCenter: parent.horizontalCenter
            width: 60
            height: 30
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            radius: 15
            MouseArea{
                anchors.fill: parent
                onClicked:
                {
                 cartController.zeroToppingMessage = false
                }

            }
            Text {
                anchors.centerIn: parent
                text: "Ок"
            }
        }
    }

}
