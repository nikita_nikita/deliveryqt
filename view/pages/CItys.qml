import QtQuick 2.12
import "../components"
Item {
    id: root_component
    signal close();
    CitySelect{
        anchors.fill: parent
       // citys: storage.cityList
        onCitySelect: {
                if(storage.userCity.id === city.id){
                root_component.close();
                return;
            }

            storage.setUserCity(city)
            kapibaras.setDomain(storage.userCity.domain.path)
            kapibaras.setCity_id(storage.userCity.id)
            kapibaras.loadCityData();
                root_component.close();
        }
    }
}
