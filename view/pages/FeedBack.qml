import QtQuick 2.12
import "../components/base"
import "../components/feedback"
//import QtQuick.Controls 1.4

Rectangle {

    id:root_compopnent
    //    contentHeight:ocenca.height+content.height

    KText{height: 40;text: feedbackController.ratingName;font.pointSize: 40; anchors.horizontalCenter: parent.horizontalCenter}
    Stars{id:stars;anchors.top: parent.top;anchors.topMargin: 60}
    FeedbackFiveStar{anchors.top: stars.bottom;visible: !feedbackController.commentVisible}
    FeedBackNotFiveStar{ id: not_five_star; anchors.top: stars.bottom;anchors.topMargin: 20;visible: feedbackController.commentVisible; anchors.bottom: parent.bottom; anchors.bottomMargin: 20}


    Rectangle{
        anchors.fill: parent
        color: "gray"
        visible: feedbackController.bigSizePhoto
        opacity: 0.7
        MouseArea{
            anchors.fill: parent
            onClicked: feedbackController.bigSizePhoto = false
        }
        Rectangle{
            anchors.centerIn: parent
            width: 250
            height: 150
            radius: 10
            Text {
                anchors.centerIn: parent
                text: "Максимальный размер<br> фото - 5мб"
                horizontalAlignment: Text.AlignHCenter
            }
            ButtonYollow{
                text: "OK"
                width: 180
                height: 50
                text_coloric:"black"
                onClicked: feedbackController.bigSizePhoto = false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
            }
        }
    }
    Rectangle{
        anchors.fill: parent
        visible: feedbackController.noDataWrite

    Rectangle{
        anchors.fill: parent
        color: "gray"
        opacity: 0.7
        MouseArea{
            anchors.fill: parent
            onClicked: feedbackController.noDataWrite = false
        }
    }
        Rectangle{
            anchors.centerIn: parent
            width: 250
            height: 150
            radius: 10
            Text {
                anchors.top: parent.top
                anchors.topMargin: 30
                id:emptyCommentMessage
                anchors.centerIn: parent
                text: "Вы оставили <br> пустой коментарий"
                horizontalAlignment: Text.AlignHCenter
            }
            ButtonYollow{
                text: "OK"
                width: 180
                height: 50
                text_coloric:"black"
                onClicked: feedbackController.noDataWrite = false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: emptyCommentMessage.bottom
                anchors.topMargin: 30

            }
        }
    }

MouseArea{
    anchors.fill: parent
    visible: feedbackController.feedBackthankMessage
}
        Rectangle{
            width: 250
            height: 121
            anchors.centerIn: parent
            color: "white"
            radius: 20
            visible: feedbackController.feedBackthankMessage


            Column{

                id:column
                Text{
                    width:250
                    height: 80
                    horizontalAlignment: Text.AlignHCenter
                    text: "<br>Спасибо за <br> оставленный отзыв"
                }


                Rectangle{
                    height: 1
                    width: 250
                    color: "gray"}
                Rectangle{
                    width: 250
                    height: 40
                    radius: 20
                    Text {
                        anchors.centerIn: parent
                        text: "Закрыть"
                        font.bold: true

                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            router.push("menu")
                            feedbackController.feedBackthankMessage = false
                        }
                    }

                }
            }
        }

}
