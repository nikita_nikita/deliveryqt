import QtQuick 2.12
import "../components/base"
Rectangle {
    Flickable {
        anchors.fill: parent
        anchors.topMargin: 40
        contentHeight: content.height+150
        clip: true


    Column {
        id: content
        anchors.horizontalCenter: parent.horizontalCenter

        spacing: 20
        width: 290
        Image {
            source: "qrc:/view/img/plate.png"
            width: 130
            height: 130
            fillMode: Image.PreserveAspectFit
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText {
            text: "Извините, данные товары не доставляются по вашему адресу"
            font.pixelSize: 18
            font.weight: Font.Bold
            color: "#f00"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
        }
        Repeater {
            model: statusStorage.checkStreetResponse.items
            KText {

                text: modelData.name
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                width: content.width
                font.pixelSize: 15
            }

        }



        ButtonYollow {
            text: "В корзину"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: router.popToCart();
        }



    }
    }

}
