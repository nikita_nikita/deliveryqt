import QtQuick 2.12
import "../components/base"
Rectangle {
    Column {
        id: content
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 40
        spacing: 20
        width: 290
        Image {
            source: "qrc:/view/img/plate.png"
            width: 130
            height: 130
            fillMode: Image.PreserveAspectFit
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText {
            text: "Заказ не принят"
            font.pixelSize: 18
            font.weight: Font.Bold
            color: "#f00"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText {
            id: message
            text: ""
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            width: content.width
            font.pixelSize: 15
        }
        Connections {
            target: payService
            onErrorPay: {
                message.text =error
            }
        }


    }

}
