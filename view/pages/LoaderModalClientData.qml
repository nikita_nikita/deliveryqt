import "../components/base"
import QtQuick 2.12
LoaderModal {
    id: root_component
     visible: kapibaras.statusClientDataLoad&&(router.page === "profile"||router.page === "checkout")


     Rectangle {
         id: modal_error
         width: 300
         height: column.height+40
         anchors.centerIn: parent
         radius: 10
         visible: false
         Connections {
             target: kapibaras
             onErrorLoadClientData:modal_error.visible=true
         }

         Column {
             id: column
             anchors.left: parent.left
             anchors.right: parent.right
             anchors.verticalCenter: parent.verticalCenter
             spacing: 10
               KText {
                   text: "Произошла ошибка загрузки данных клиента. Перезагрузите приложение и попробуйте авторизоваться  снова"
                   anchors.left: parent.left
                   anchors.right: parent.right
                   anchors.leftMargin: 10
                   anchors.rightMargin: 10
                   wrapMode:Text.WordWrap
                   horizontalAlignment: Text.AlignHCenter
                   font.pointSize: 10
               }
               ButtonYollow {
                   text: "Ок"
                   anchors.horizontalCenter: parent.horizontalCenter
                   onClicked: {
                       storage.tokenLk ="";
                       Qt.quit()
                   }

               }
         }
     }
}
