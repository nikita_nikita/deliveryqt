import QtQuick 2.12
import "../components/menu"
import QtQuick.Controls 2.12
Flickable {

    id: root_component
    contentHeight: content.height+110
    clip: true

    Item {
        id: content
        height: image_carusel.height+stack_goods_prerender.height
        width: root_component.width
        Carusel{
            id: image_carusel
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            anchors.rightMargin: 20
            currentIndex: menu.currentOffers
            onCurrentIndexChanged: {
                menu.currentOffers = currentIndex
            }
        }
        Item{
            id: flex_stack_goods
            anchors.top: image_carusel.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.right: parent.right
            height: stack_goods_prerender.height
            Row  {
                id: stack_goods_prerender
                clip: true
                Repeater {
                    model: menuController.pages
                    Goods{
                        model: pages_data

                        // minHeight: root_component.height-image_carusel.height
                        width: flex_stack_goods.width
                        visible: pages_data.visible
                        Behavior on visible {
                            NumberAnimation {
                                duration: 200
                            }
                        }
                    }
                }

            }
        }


    }



}




