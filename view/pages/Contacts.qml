import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/contacts"

Item {
    id: root_component

    Flickable {
        id: contact_content
        anchors.fill: parent
        anchors.leftMargin: 20
        anchors.topMargin: 40
        contentHeight: contact_company_name.height+contact_city.height+contact_addreses.height+contact_other.height +work_time_column.height+200
        clip: true
        Image {
            id: contact_company_name
            source: "qrc:/view/img/logo/kapibara.png"
            width: 104
            fillMode: Image.PreserveAspectFit
            mipmap: true
        }
        KText {
            id: contact_city
            anchors.top: contact_company_name.bottom
            anchors.topMargin: 10
            font.pixelSize: 16
            text: contactController.country+", "+contactController.cityName
        }
        Column {
            id: contact_addreses
            anchors.top: contact_city.bottom
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 10
            KText {
                color: "#9F9F9F"
                text: "Адреса наших точек:"
                font.pixelSize: 10;
            }
            GridLayout {
                columns: 1
                rowSpacing: 23
                columnSpacing: 60
                anchors.left: parent.left
                anchors.right: parent.right
                Repeater {
                    model: storage.cityData.point_delivery
                    KText {
                        //width: parent.width
                        Layout.fillWidth: true
                        font.pixelSize: 14
                        text: modelData.name+", "+modelData.address
                        wrapMode: Text.WordWrap
                    }
                }

            }

        }
        Column {
            id: work_time_column
            anchors.top: contact_addreses.bottom
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 10
            KText {
                color: "#9F9F9F"
                text: "Время работы("+worktime.currentDay+")"
                font.pixelSize: 10;
            }
            Grid {
                columns: 2
                spacing: 10
                anchors.left: parent.left
                anchors.right: parent.right
                KText {
                    //width: parent.width
                    Layout.fillWidth: true
                    font.pixelSize: 14
                    text: "Самовывоз:"
                    wrapMode: Text.WordWrap
                    font.underline: true
                }
                KText {
                    //width: parent.width
                    Layout.fillWidth: true
                    font.pixelSize: 14
                    text: worktime.pickup
                    wrapMode: Text.WordWrap
                     font.underline: true
                }
                KText {
                    //width: parent.width
                    Layout.fillWidth: true
                    font.pixelSize: 14
                    text: "Доставка:"
                    wrapMode: Text.WordWrap
                     font.underline: true
                }
                KText {
                    //width: parent.width
                    Layout.fillWidth: true
                    font.pixelSize: 14
                    text: worktime.delivery
                    wrapMode: Text.WordWrap
                     font.underline: true
                }
            }
        }
        // при щелчке на время показать модалку c  временем
        MouseArea {
            anchors.fill: work_time_column
            onClicked: worktime_modal.visible=true
        }

        Grid {
            id: contact_other
            anchors.top: work_time_column.bottom
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.right: parent.right
            columns: 1
            rowSpacing: 30
            columnSpacing: 60
            spacing: 10
            Column {
                id: telephone_item
                spacing: 10
                KText {
                    color: "#9F9F9F"
                    text: "Телефон:"
                    font.pixelSize: 10;
                }
                KText {
                    color: "#000"
                    text: storage.cityData.organizaton_info?storage.cityData.organizaton_info.phone:""
                    font.pixelSize: 22
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked:  router.openUrl("tel:"+storage.cityData.organizaton_info.phone);
                    }

                }
            }
            Column {
                id: telephone1_item
                spacing: 10
                visible: storage.cityData.organizaton_info?storage.cityData.organizaton_info.phone1:false
                KText {
                    color: "#9F9F9F"
                    text: "Телефон:"
                    font.pixelSize: 10;
                }
                KText {
                    color: "#000"
                    text: storage.cityData.organizaton_info?storage.cityData.organizaton_info.phone1:""
                    font.pixelSize: 22
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked:  router.openUrl("tel:"+storage.cityData.organizaton_info.phone1);
                    }


                }
            }


            Column {
                id: telephone2_item
                spacing: 10
                visible: storage.cityData.organizaton_info?storage.cityData.organizaton_info.phone2:false
                KText {
                    color: "#9F9F9F"
                    text: "Телефон:"
                    font.pixelSize: 10;
                }
                KText {
                    color: "#000"
                    text:  storage.cityData.organizaton_info?storage.cityData.organizaton_info.phone2:""
                    font.pixelSize: 22
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked:  router.openUrl("tel:"+storage.cityData.organizaton_info.phone2);
                    }

                }
            }
            Column {
                id: email_item
                  spacing: 10
                KText {
                    color: "#9F9F9F"
                    text: "E-mail:"
                    font.pixelSize: 10;
                }
                KText {
                    color: "#000"
                    text: storage.cityData.organizaton_info?storage.cityData.organizaton_info.email:""
                    font.pixelSize: 18

                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked:  router.openUrl("mailto:"+storage.cityData.organizaton_info.email);
                    }


                }
            }
            Row {
                spacing: 11
                Image {
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/view/img/social/vk.png"
                    visible: storage.cityData.organizaton_info?storage.cityData.organizaton_info.vk:false
                    width: 50
                    height: 50
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked: router.openUrl(storage.cityData.organizaton_info.vk);
                    }
                }
                Image {
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/view/img/social/instagramm.png"
                    width: 50
                    height: 50
                    visible: storage.cityData.organizaton_info?storage.cityData.organizaton_info.instagram:false
                    MouseArea {
                        anchors.fill: parent
                         cursorShape: Qt.PointingHandCursor
                        onClicked: router.openUrl(storage.cityData.organizaton_info.instagram);
                    }
                }
            }

        }
    }
    WorkTime {
        id: worktime_modal
      anchors.fill: parent
        visible: false
    }

}
