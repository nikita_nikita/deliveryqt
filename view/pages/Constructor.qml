import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/wok"
import "../components/constructror"

Flickable {
    id:root_component
    contentHeight: content_column.height+150
    clip: true
    property real dopCost: getDopCost();
    onVisibleChanged: {
        if(!visible) return;
        var test  = router.constructorData;
        var options = {
            price: 0,
            added: {
                souse:0
            }
        }
        if(router.constructorData.options!==undefined){
            options =router.constructorData.options
        }
        setOptions(options)
    }
    function setOptions(options, array_topings=[]){
        if(options.added) {
             souseList.checked =options.added.souse;
        }
    }   

    function getCartOption(){
        var result = {
            price: getDopCost(),
            added: {
                souse: souseList.checked
            }
        }
        return result;
    }


    function getDopCost(){
        var result =0
        return result;
    }

    ColumnLayout {
        id: content_column
        width: parent.width-30
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 30
        RowLayout{
            spacing: 30
            Layout.topMargin: 35
            Layout.fillWidth: true
            KImage {
                imagefile: router.constructorData.product!==undefined?router.constructorData.product.images[0]:""
                fillMode: Image.PreserveAspectFit
                Layout.preferredWidth: root_component.width>320?150:100
                Layout.preferredHeight: wok_data_column.height

            }
            Column{
                id: wok_data_column
                Layout.fillWidth: true
                spacing: 10
                KText {
                    text: root_component.visible?router.constructorData.product.name:""
                    color: "#542A69"
                    font.pixelSize: root_component.width>320?22:12
                    width: parent.width
                    wrapMode: Text.WordWrap

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            var price = router.constructorData.product.sity_info.price;
                            var doptCost = root_component.dopCost;
                        }
                    }
                }
                KText {
                    font.pointSize: root_component.width>320?12:9
                    color: "#878787";
                    text: "1 шт / 440 г / 225 ккал"
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
                KText {
                    text: root_component.visible?(router.constructorData.product.sity_info.price+root_component.dopCost)+"руб.":""
                    font.pointSize: root_component.width>320?24:14
                    color: "#542A69"
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
            }



        }
        KText {
            text: "Выбор Соуса"
            color: "#542A69"
            font.pointSize: 16

        }

        Flow{
            id: souseList
            property int checked: 0
            Layout.fillWidth: true
            spacing: 30
            // соусы
            Repeater {

                model:  root_component.visible?router.constructorData.product.added.souse:0
                SouseItem {
                    model: modelData
                    checked: souseList.checked===modelData.product.id
                    onCheckedChanged:
                    {
                        if(checked) {
                            souseList.checked=modelData.product.id
                        }
                    }
                    Component.onCompleted: {
                        if(parseInt(modelData.selected)&&souseList.checked==0){

                            souseList.checked=modelData.product.id
                        }
                    }
                }
            }
        }



        ButtonYollow {
            Layout.alignment: Qt.AlignHCenter
            text: router.constructorData.operation==="edit"?"Сохранить":"В корзину"
            onClicked:{
                var test = getCartOption()
                if(router.constructorData.operation==="edit"){
                    cart.setOptions(router.constructorData.index, getCartOption());
                }
                else {
                    cart.addWithOptions(router.constructorData.product, getCartOption())
                }

                router.pop();
            }

        }


    }
}
