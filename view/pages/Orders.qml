import QtQuick 2.12
import "../components/orders"

Item {
    id: root_component

    function findProductById(id){
        var catProducts = storage.cityData.products;
        for(var key in catProducts){
            var prod = catProducts[key].find(function(item){
                return item.id === id;
            });
            if (prod) {
                return prod;
            }
        }
    }
    OrderStub{
        id: item_cart_isEmpy
        anchors.fill: parent
        anchors.bottomMargin: 150
        visible: !storage.user.client_history||storage.user.client_history.length===0

    }


    Flickable{
        anchors.fill: parent
        anchors.topMargin: 20
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        visible: !item_cart_isEmpy.visible
        contentHeight: orders_content.height+150
        clip: true

        Column {
            id: orders_content
            width: parent.width
            spacing:50
            Repeater{
                model:feedbackController.orders
                OrderItem{
                    width: orders_content.width
                    order_products: products
                    date: dateTime
                    order_id: id
                    order_status:  status
                    deliveryType: delivery
                    canFeedBack: canFeedbackOrder
                    pointStr: point
                    onAppendToCard: {
                        var result=[];
                        var products_cart =  productArray;
                        products_cart.forEach(function(item, index){
                            var pr = root_component.findProductById(item.product.id)
                            if(pr){
                                result.push({
                                                product: pr,
                                                quantity: item.quantity
                                            });
                            }

                        });
                        if(result.length===0){
                            global_tooltip.text = "Эти  товары не продаются";
                            global_tooltip.visible = true
                             return;
                        }

                        global_tooltip.text = "В корзину добаленны "+result.length+" товар(а)"
                        global_tooltip.visible = true
                        cart.addList(result);
                        router.push("cart")

                    }
                    onFeedback:{

                        feedbackController.order_number = order_id
                        router.push("feedback")
                }

            }




        }


    }



}
}
