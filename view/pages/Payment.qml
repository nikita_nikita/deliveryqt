import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../components/base"
import "../components/payment"
Rectangle {
    id: page_checkout


    KTabBar {
        id: type_payment_tabbar
        anchors.horizontalCenter: parent.horizontalCenter
        currentIndex: cart.payMethod

        KTabButton {
            id: card_tab_button
            text:  "Картой онлайн"
            width: visible ? undefined : 0
            visible: paymentController.sslSuppurt&&paymentController.delivery_online===1||paymentController.pickup_online&&paymentController.sslSuppurt

        }
        KTabButton {
            id: cash_tab_button
            visible: paymentController.pickup_cash&&!deliveryController.contactless_status||paymentController.delivery_cash&&!deliveryController.contactless_status
            text: cart.orderType=="take-away"?"На кассе":"Наличными"
             width: visible ? undefined : 0
        }
        KTabButton {
            id: curier_tab_button
            text: "Картой курьеру"
            width: visible ? undefined : 0
             visible:paymentController.delivery_card&&cart.orderType==="delivery"
        }



    }
    Rectangle {
        border.width: page_checkout.width>389?1:0
        border.color: "#F2F2F2"
        width: page_checkout.width>389?389:290
        anchors.top: type_payment_tabbar.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        clip: true

                        Text {
                            height: 80
                            width: parent.width -40
                            anchors.centerIn: parent
                            property string noteText: ""
                            visible: !paymentController.pickup_card&&cart.payMethod === 6||!paymentController.pickup_cash&&cart.payMethod === 6
                            id: note
                            text: noteText
                            Component.onCompleted: {
                                note.noteText = ""
                                if(!paymentController.pickup_card){
                                    note.noteText += "Оплата картой не достуана <br>"
                                }if(!paymentController.pickup_cash){
                                    note.noteText += "Оплата наличными не достуана"

                                }
                           }
                        }


        SwipeView {
            id: type_deliver_swipe

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 20
            anchors.bottom: parent.bottom
            currentIndex: type_payment_tabbar.currentIndex
            onCurrentIndexChanged: {
                cart.payMethod = currentIndex
                var data = storage.cityData
            }

            interactive: false


            PlatformPay{
                visible: card_tab_button.visible
            }
            Cash {

            }
            Item{}

        }

    }

    Text {
        anchors.centerIn: parent
        text: "Тип оплаты не выбран"
        horizontalAlignment: Text.AlignHCenter
        color: "gray"
        visible: cart.payMethod === -1
    }

}

