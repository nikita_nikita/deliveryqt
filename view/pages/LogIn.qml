import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../components/base"

Rectangle {
    id: root_component
    visible: false
    property string nextPage: "";

    KText{
        id: page_title
        text: "Вход"
        font.pixelSize: 13
        color: "#542A69"
        anchors.horizontalCenter: parent.horizontalCenter
    }
 Timer{
    id: timer_sms
    property int counter: 60
    repeat: true
    interval: 1000
    onTriggered: {
        counter--
        if(counter<=0){
            stop();
            counter = 60;
            content.redyToSendSms = true
            return;
        }


    }
 }

    Rectangle {
        id: content
        property bool isSendSms: false
        property bool redyToSendSms: true
        border.width: root_component.width>389?1:0
        border.color: "#F2F2F2"
        width: root_component.width>389?389:290

        height: content_column.height+50
        anchors.horizontalCenter: parent.horizontalCenter
        clip: true
        Column {
            id: content_column
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            spacing: 20

            KTextField{
                id: user_phone
                placeholderText: "Телефон"
                inputMethodHints: Qt.ImhSensitiveData | Qt.ImhDigitsOnly
                width: parent.width
                valid: acceptableInput
                focus: router.page === "profile" && user_phone.length !== 10 && root_component.visible
                text: storage.user.client_info?storage.user.client_info.phone:""
                onTextChanged: {
                                    updateCursorPositon()
                    console.log(user_phone.length)
                }
                property int start_cursor: 0
                onFocusChanged: {
                    if(focus){
                        cursorPosition = start_cursor
                    } else {
                        if(!valid) {
                            user_phone.text =""
                        }
                    }
                }
                //validator: RegExpValidator { regExp: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/ }
                onInputMaskChanged: {
                    user_phone.start_cursor  =  user_phone.inputMask.indexOf("#");
                    user_phone.text = "";
                    user_code.text = "";
                }



                Keys.onEnterPressed: set()
                Keys.onReturnPressed: set()
                function set(){
                    user_code.focus= true
                }

                ButtonPen{
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                }
                Connections {
                    target: storage
                    onCityDataChanged: {
                        user_phone.inputMask = cityData.organizaton_info.phoneMask                        
                    }
                }
            }
            Text {
                id: test_test

            }

            KTextField{
                id: user_code
                visible: content.isSendSms;
                placeholderText: "Введите  код из СМС"
                width: parent.width
                inputMethodHints: Qt.ImhDigitsOnly
                valid: text.length>0
                text: ""
                onVisibleChanged: if(visible){
                                  focus = true}
                onTextChanged: {
                   code_error_label.visible = false

                }


                Keys.onEnterPressed: set()
                Keys.onReturnPressed: set()
                function set(){
                    user_code.focus= false
                }
                KText {
                    id: code_error_label
                    text: "Неверный код!"
                    color: "red"
                    anchors.top: parent.bottom
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    visible: false

                }

            }
            KText {
                text: "Отправить еще раз через "+timer_sms.counter
                color: "#ACACAC";
                 visible: content.isSendSms&&!content.redyToSendSms;
                  anchors.horizontalCenter: parent.horizontalCenter
            }
            KText {
                text: "Отправить еще раз"
                color: "#542A69";
                font.underline: true
                 visible: content.isSendSms&&content.redyToSendSms;
                  anchors.horizontalCenter: parent.horizontalCenter
                  MouseArea{
                      anchors.fill: parent
                        onClicked: {
                            kapibaras.sendSmsCode(user_phone.text)
                            content.redyToSendSms = false
                            timer_sms.start();
                        }
                  }
            }

            ButtonYollow {
                 visible: content.isSendSms;
                text:"Продолжить"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: kapibaras.auth(user_phone.text, user_code.text)
                enabled:!code_error_label.visible

            }
            ButtonYollow {
                text: "Отправить код"
                anchors.horizontalCenter: parent.horizontalCenter
                visible: !content.isSendSms;
                enabled: user_phone.acceptableInput
                onClicked: {

                    kapibaras.sendSmsCode(user_phone.text)
                    content.isSendSms= true
                    content.redyToSendSms = false
                    timer_sms.start();
                }
            }

        }

    }
    Connections {
       target: kapibaras
       onAuthLoaded: {
           if(status){
                code_error_label.visible = false
               root_component.visible=false
               content.isSendSms = false
           } else {
               code_error_label.visible = true
           }
       }
    }
    Connections {
        target: router
        onPageChanged: {
           if(storage.tokenLk.length==0){
               var pagesAcl = ["orders", "profile","checkout","payment", "cart"];
                if(pagesAcl.indexOf(router.page)!==-1) {
                    root_component.visible = true
                } else {
                    root_component.visible = false
                }
           }
           else {
               root_component.visible = false
           }
        }
    }



}
