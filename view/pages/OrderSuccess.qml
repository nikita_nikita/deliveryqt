import QtQuick 2.12
import "../components/base"
Rectangle {
    Column {
        id: content
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 40
        spacing: 20
        width: 290
        Image {
            source: "qrc:/view/img/plate.png"
            width: 130
            height: 130
            fillMode: Image.PreserveAspectFit
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText {
            text: "Ваш заказ успешно принят!"
            font.pixelSize: 18
            font.weight: Font.Bold
            color: "#542A69"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        KText {
            text: "Kapibara благодарит за заказ!\n\r Для подтверждения мы Вам позвоним или отправим СМС. Давайте есть дружно!"
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            width: content.width
            font.pixelSize: 15
        }
        KText {
            text: "Пожалуйста оставте отзыв."
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            width: content.width
            font.pixelSize: 15
        }



    }

}
