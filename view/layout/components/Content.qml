import QtQuick 2.12
import "../../pages"
Item {
    id: root_component



  CItys{
        anchors.fill: parent
        visible: router.page === "citys"
        onClose: router.popToMain();


    }
   Menu{
        anchors.fill:parent
        visible: router.page === "menu"
    }
   Orders{
        anchors.fill:parent
        visible: router.page === "orders"
    }
    Cart{
        anchors.fill:parent
        visible: router.page === "cart"

    }
    Profile{
        anchors.fill:parent
        visible: router.page === "profile"
    }  
    Search{
        //что бы не тащить сквозь несколько элементов
        //focus InputText = router.page === "search"
        anchors.fill:parent
        anchors.bottomMargin: 80
        visible: router.page === "search"        

    }
    FeedBack{
        //что бы не тащить сквозь несколько элементов
        //focus InputText = router.page === "search"
        anchors.fill:parent
        anchors.bottomMargin: 80
        visible: router.page === "feedback"

    }
    Offers {
        anchors.fill: parent
        visible: router.page =="offers"
    }
    Contacts {
        anchors.fill: parent
        visible: router.page == "contacts"
    }
    Checkout {
        anchors.fill: parent
        visible: router.page == "checkout"
        //visible: true
    }
    NewPayments{
        anchors.fill: parent
        visible: router.page == "payment"
    }
    OrderSuccess {
        anchors.fill: parent
        visible: router.page == "order_success"
    }
    OrderError {

        anchors.fill: parent
        visible: router.page == "order_error"
    }
    ProductsNotZone {
        anchors.fill: parent
         visible: router.page == "product_not_zone"
    }
    WokConstructor {
        anchors.fill: parent
        visible: router.page == "wok_constructor"
    }
   Constructor {
       anchors.fill: parent
       visible: router.page == "constructor"
   }


   // модалка платной доставки
   PaidDelivery {
       anchors.fill: parent
   }
// модалка загрузки
    LoaderProcess{
        id: pageLoaded
        anchors.fill: parent
        visible:  router.pageLoadedVisible
    }

    // если данные клиента  еще не загруженны показываем индикатор загкрузки

    LoaderModalClientData {
        anchors.fill: parent

    }
    //авторизация
       LogIn {
           anchors.fill: parent

          }

}
