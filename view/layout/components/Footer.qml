import QtQuick 2.0
import "../../components/cart" as CartFooter
import "../../components/menu" as MenuFooter
Item{
    height: 101

    Rectangle{
        anchors.fill: parent
        opacity: 0.9
        color: "#542A69"
    }

    CartFooter.Footer{
        anchors.fill: parent
        visible: router.footer==="cart"
    }
    MenuFooter.Footer {
         anchors.fill: parent
         visible: router.footer==="menu"
    }
    MenuFooter.FooterToMain{
        anchors.fill: parent
        visible: router.footer==="tomaint"
    }
}
