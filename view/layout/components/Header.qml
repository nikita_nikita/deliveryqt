import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import "../../components/base"

Item {
    id: root_component
    height:  68
    signal clickedSandwich()
    signal clickedGeo()
    signal clickedBack()
    RowLayout {

        anchors.fill: parent
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        ToolButton {
            id: sandwich
            height: 16
            width: 20
            contentItem: Image {

                fillMode: Image.PreserveAspectFit
                source: "qrc:/view/img/sandwich.png"
                opacity: sandwich.down ? 0.7 : 1
            }
            background: Rectangle {
                color: "#fff"

            }
            onClicked: root_component.clickedSandwich()
        }
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Row {
             height: parent.height
              anchors.centerIn: parent
              spacing: 10

              Image {
                  id: button_back_image
                  source: "qrc:/view/img/arrow/left.png"
                  width: 14
                  height: parent.height
                  fillMode: Image.PreserveAspectFit
                  verticalAlignment: Image.AlignVCenter
                  visible: router.stack>1
                  MouseArea {
                      anchors.horizontalCenter: button_back_image.horizontalCenter
                      height: parent.height
                      width: 30
                      cursorShape: Qt.PointingHandCursor
                      onClicked: router.pop()

                  }

              }

              Image {
                  id: image_logo
                  source: "qrc:/view/img/logo/kapibara.png"
                  width: 93
                  height: parent.height
                  fillMode: Image.PreserveAspectFit
                  mipmap: true
                  verticalAlignment: Image.AlignVCenter
                  visible: router.pageName.length==0
                  MouseArea {
                     anchors.fill: parent
                      cursorShape: Qt.PointingHandCursor
                      onClicked: router.pop()
                  }

              }
              KText {
                    text:router.pageName
                    visible: !image_logo.visible
                    font.pixelSize: 18
                    font.weight: Font.Bold
                    color: "#542A69"
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    MouseArea {
                       anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked: router.pop()
                    }
              }

            }
        }


        ToolButton {
            id: geo_button
            height: 16
            width: 20
            contentItem:KText {
                text: storage.userCity.name?storage.userCity.name:""
            }
                /*Image {

                fillMode: Image.PreserveAspectFit
                source: "qrc:/view/img/geopoint.png"
                 opacity: geo_button.down ? 0.7 : 1
            }*/
            background: Rectangle {
                //color: "#f5f5f5"
                //implicitWidth: geo_button.width+20
                radius: 40
                border.color: "#542A69"
                border.width: 1
            }
             onClicked: root_component.clickedGeo()

        }
    }

}
