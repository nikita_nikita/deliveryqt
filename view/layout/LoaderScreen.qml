import QtQuick 2.12
import "../components"

Rectangle {
    id: layout
    color: "#fff"  
    visible: loaderController.visible

    Item {
        anchors.centerIn: parent
        height: image1.height+image2.height+30
        width: image1.width+image2.width
        Image {
            id: image1
            source: "qrc:/view/img/logo/logo_1.png"
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: 120
            fillMode: Image.PreserveAspectFit
        }
        Image {
            id: image2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: image1.bottom
            anchors.topMargin: 30
            source: "qrc:/view/img/logo/kapibara.png"
            width: 174
            fillMode: Image.PreserveAspectFit
        }
    }

    Text {
        id: text_update
        text: "Пожалуйста, обновите приложение!"
        color: "#f00"
        width: layout.width-20
        wrapMode: Text.WordWrap
        horizontalAlignment:  Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        font.pixelSize: 18
        visible: !loaderController.versionCheck
    }

    Text {
        id: error_load
        text: "Не могу подключиться к серверу!"
        color: "#f00"
        width: layout.width-20
        wrapMode: Text.WordWrap
        horizontalAlignment:  Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        font.pixelSize: 18
        visible: loaderController.errorText!=""
    }

    Rectangle{
    color: "#ffc605"
    radius: 10
    visible: !loaderController.versionCheck
    anchors.horizontalCenter: parent.horizontalCenter
    width:200
    height: 40

    anchors.bottom: parent.bottom
    anchors.bottomMargin: 60
    Text {
        anchors.centerIn: parent
        text: "ОБНОВИТЬ"
        color: "#653f78"
    }
    MouseArea{
    anchors.fill: parent
    onClicked:loaderController.openMarketUrl()
    }
    }
    CitySelect {
        id: city_select
        anchors.fill: parent
        onCitySelect: {
            storage.setUserCity(city)
            loaderController.visibleCityList=false
        }
        visible: loaderController.visibleCityList

    }




}
