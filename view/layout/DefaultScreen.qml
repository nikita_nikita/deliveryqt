import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtWebView 1.1
import QtQuick.Controls 2.12
import "components"
import "../components/base"
import "../components/drawer"
Item {
    id: layout

    Header{
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        onClickedSandwich: drawer.open()
        onClickedGeo: router.page = "citys"
    }
    HorisontalSeparator {
        id: top_separator
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: header.bottom
        visible: router.page!=="menu"
    }

    Content {
        id: content
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: top_separator.bottom
        anchors.bottom: parent.bottom

    }
    Footer{
        id: footer
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
    DrawerMenu{
        id: drawer
        width: 320
        height: layout.height-footer.height
    }
    DrawerOffers{
        id: drawer_offers
        width: parent.width
        //height: 300
        edge: Qt.BottomEdge

    }
    DrawerComposite{
        id: drawer_composite
        width: parent.width
        edge: Qt.BottomEdge
    }


    WebView {
        id: webView
        anchors.fill: parent
        visible: router.page === "webView"
        // url: initialUrl
        onLoadingChanged: {
            if (loadRequest.errorString)
                console.error(" qml WebView ", loadRequest.errorString);
        }
        onUrlChanged:  payService.checkPayUrl(url)
        Connections {
            target: payService
            onOpenUrl: {
                webView.url= sourceUrl;
                router.page ="webView";
            }
        }
    }
    FlashMessage {
        visible: worktime.visibleMessage
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        text: "Мы закрыты, но вы можете оставить предзаказ! Работаем с "+worktime.timeOpened+" до "+worktime.timeClosed+"."
        onClosed: worktime.visibleMessage=false
    }
//    FeedbackMessage{
//        visible: feedbackController.feedbackMessage
//        //        visible: false
//        anchors.fill: parent

//    }
    Column{
        y:65
        Repeater{
            model:deliveryController.warnings
            Rectangle {

                visible: modelData.status === 1
                property string text :modelData.reason
            //    signal clicked

                height: text.height + 20;
                width: root_windows.width-30
                x:10
                border.width: 1
                radius: 20
                color: "#542A69"
//                smooth: true
                Text {
                    id: text
                    anchors.centerIn:parent
                    width: layout.width-50
                    text: parent.text
                    color:"white"

                    wrapMode: Text.WordWrap
                }
                ButtonClose{

                    x:parent.width-20
                    y:-20
                    onClicked: parent.visible = false


                }

            }
        }

    }

}
