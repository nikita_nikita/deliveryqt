import QtQuick 2.12
import QtQuick.Controls 2.4
import QtFirebase 1.0


import "./view/layout"

ApplicationWindow {
    id: root_windows
    width: 360
    height: 568
    visible: true
    property bool quit_program: false
    property bool tables: width>500
    property bool phones: width<361
    DefaultScreen{
        anchors.fill: parent
    }

    LoaderScreen{
        anchors.fill: parent

    }
    onClosing: {
        router.pop()
        close.accepted = false
    }


    function  closeApplication(){
        if(root_windows.quit_program){
            Qt.quit();

        } else {
            global_tooltip.text = "Нажмите еще раз для выхода"
            global_tooltip.visible = true
            timer.start();
            root_windows.quit_program= true
        }


    }
    Timer {
        id: timer
        interval: 3000;
        running: false;
        repeat: false
        onTriggered:  {
            root_windows.quit_program = false
        }

    }

    ToolTip {
        id: global_tooltip
        delay: 0
        timeout: 3000
        x: (parent.width - width) / 2
        y: (parent.height - 100)
        text: "Нажмите еще раз для выхода"
        contentItem: Text {
            id: tip_text
            text: global_tooltip.text
            font: global_tooltip.font
            color: "#fff"
            opacity: 0.8
            horizontalAlignment: Text.AlignHCenter
        }

        background: Rectangle {
            color: "#000"
            radius: 15
            opacity: 0.5

        }
    }
    Messaging {
        id: messaging

        //        onReadyChanged: {
        //            console.log("Messaging.ready", ready)
        //        }
        onTokenChanged: {
            console.log("token-> ", token)
            firebaseController.setFirebaseToken(token)
        }


                onDataChanged: {
                    console.log("Messaging.data", JSON.stringify(data))
                }
        onMessageReceived: {
            firebaseController.messageData = data
            console.log("onMessageReceived","Messaging.data", JSON.stringify(data))
        }

        onSubscribed: {
            console.log("Messaging::onSubscribed", topic)
        }
        //        onUnsubscribed: {
        //            console.log("Messaging::onUnsubscribed", topic)
        //        }

        //        onError: {
        //            console.error('Messaging::error',code, message);
        //        }
    }

    /*Rectangle{
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 20
        //        anchors.horizontalCenter: parent.horizontalCenter
        y: 70
        width: 300
        radius: 15
        visible: firebaseController.message !== ""
        height: text_message.height + 100
        color: "#542A69"
        Column{
            id: column_not_delivery
            width: parent.width-20
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 80
            Text{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 10
                anchors.rightMargin: 20
                id: text_message
                text: firebaseController.message
                width: parent.width
                wrapMode: Text.WordWrap
                font.pointSize: 22
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 15
                color: "white"
                height: contentHeight
            }
            Rectangle{
                anchors.horizontalCenter: parent.horizontalCenter
                color: "#FFC605"
                radius: 15
                width: 100
                height: 30
                MouseArea{
                    anchors.fill: parent
                    onClicked: firebaseController.message = ""
                }
                Text{
                    anchors.centerIn: parent
                    text: "Хорошо"
                    color: "white"
                    font.pointSize: 22
                }
            }


        }
    }*/
    Text {
        anchors.left:  parent.left
        anchors.bottom: parent.bottom
        width: 200
        height: 260
        text:cartController.screen
        font.pointSize: 20
    }

}


